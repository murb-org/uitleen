# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.2].define(version: 2025_02_11_104543) do
  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.integer "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "articles", force: :cascade do |t|
    t.string "external_article_code"
    t.string "name"
    t.decimal "vat_rate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "external_article_subcode"
    t.boolean "allow_discount", default: true
  end

  create_table "collect_mandates", force: :cascade do |t|
    t.integer "customer_id"
    t.date "signed_on"
    t.string "sign_location"
    t.text "data"
    t.string "uuid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cost_centers", force: :cascade do |t|
    t.string "source"
    t.string "code"
    t.string "currency", default: "EUR"
    t.string "dim1"
    t.string "dim2"
    t.string "key"
    t.string "number"
    t.string "status"
    t.decimal "value", precision: 16, scale: 2
    t.string "yearperiod"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["source", "key"], name: "index_cost_centers_on_source_and_key", unique: true
  end

  create_table "customer_transactions", force: :cascade do |t|
    t.string "source"
    t.string "code"
    t.string "currency"
    t.string "customer_code"
    t.date "date"
    t.string "key"
    t.string "number"
    t.string "invoice_number"
    t.decimal "open_value"
    t.string "status"
    t.decimal "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["source", "key"], name: "index_customer_transactions_on_source_and_key", unique: true
  end

  create_table "customers", force: :cascade do |t|
    t.string "name"
    t.string "postal_code"
    t.string "external_customer_id"
    t.string "twinfield_raw"
    t.string "uuid", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "comments"
    t.datetime "deactivated_at"
    t.string "first_name"
    t.string "last_name"
    t.string "last_name_prefix"
    t.string "initials"
    t.string "title"
    t.date "born_on"
    t.text "old_data"
    t.string "import_id"
    t.datetime "blocked_at"
    t.string "blocked_reason"
    t.string "sex"
    t.string "customer_type"
    t.datetime "deceased_at"
    t.string "id_document_type"
    t.string "id_document_number"
    t.date "id_document_valid_until"
    t.string "company_name"
    t.string "email"
    t.boolean "auto_finalize_invoices", default: true
    t.string "company_vat"
    t.index ["uuid"], name: "index_customers_on_uuid"
  end

  create_table "gift_cards", force: :cascade do |t|
    t.string "code"
    t.decimal "amount"
    t.integer "customer_id"
    t.string "action"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "invoice_items", force: :cascade do |t|
    t.integer "invoice_id"
    t.string "invoiceable_type"
    t.string "invoiceable_id"
    t.string "intent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "object_cache"
    t.string "time_span_uuid"
    t.decimal "quantity", default: "1.0"
    t.decimal "unit_price_ex_vat"
    t.string "note"
    t.boolean "input_price_ex_vat"
    t.string "rental_type"
    t.date "invoicing_period_starts_at"
    t.date "invoicing_period_ends_at"
    t.decimal "discount_amount", default: "0.0"
    t.string "discount_type"
  end

  create_table "invoices", force: :cascade do |t|
    t.integer "customer_id"
    t.text "footertext"
    t.string "created_by_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "intents"
    t.string "twinfield_invoicenumber"
    t.string "rental_type", default: "basic_rent"
    t.datetime "finalized_at"
    t.integer "previous_invoice_id"
    t.boolean "use_customer_savings_for_purchase", default: true
    t.string "import_id"
    t.decimal "discount_percentage", default: "0.0"
    t.integer "recurrence_interval_in_months", default: 1
    t.text "old_data"
    t.text "external_invoice_data"
    t.date "next_invoice_at"
    t.datetime "on_hold_at"
    t.integer "on_hold_by_user_id"
    t.datetime "invoice_sent_at"
    t.integer "credits_invoice_id"
    t.string "reference"
    t.string "invoiceable_item_collection_external_id"
    t.string "invoiceable_item_collection_type"
    t.datetime "requires_review_at"
    t.string "po_number"
    t.index ["twinfield_invoicenumber"], name: "index_invoices_on_twinfield_invoicenumber", unique: true
  end

  create_table "o_auth_clients", force: :cascade do |t|
    t.string "raw_data"
    t.string "name"
    t.string "sub"
    t.string "access_token"
    t.string "refresh_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "expires_at"
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "name"
    t.string "sub"
    t.string "access_token"
    t.string "refresh_token"
    t.text "roles"
    t.text "groups"
    t.text "resources"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "id_token"
    t.integer "expires_at"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object", limit: 1073741823
    t.datetime "created_at"
    t.text "object_changes", limit: 1073741823
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
end
