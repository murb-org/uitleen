# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

invoice_item_config = Rails.application.config_for(:uitleen)[:invoice_items]

a = Article.find_or_initialize_by(external_article_code: invoice_item_config[:customer_savings][:use][:article], external_article_subcode: invoice_item_config[:customer_savings][:use][:subarticle])
a.name = "Sparen"
a.allow_discount = false
a.vat_rate = 0
a.save

Article.find_or_create_by(external_article_code: "WINKEL", external_article_subcode: "WINKELPRODUCT") do |a|
  a.name = "Winkelproduct"
  a.vat_rate = 21
end

Article.find_or_create_by(external_article_code: "PUBLICATIE", external_article_subcode: "CATALOGIBOEK") do |a|
  a.name = "Verkoop catalogi/boeken (BTW laag)"
  a.vat_rate = 9
end

Article.find_or_create_by(external_article_code: "PUBLICATIE", external_article_subcode: "PUBLICATIE") do |a|
  a.name = "Publicatie - BTW normaal"
  a.vat_rate = 21
end

a = Article.find_or_initialize_by(external_article_code: "HUURCORRECTIE", external_article_subcode: "HUURCORRECTIE")
a.name = "Huurcorrectie - particulier"
a.allow_discount = false
a.vat_rate = 21
a.save

a = Article.find_or_initialize_by(external_article_code: "HUURCORRECTIE", external_article_subcode: "HUURCORRZAK")
a.name = "Huurcorrectie - zakelijk"
a.allow_discount = false
a.vat_rate = 21
a.save

Article.find_or_create_by(external_article_code: "HUURMAATWERK", external_article_subcode: "HUURMAATWERKZAK") do |a|
  a.name = "Huur maatwerk - zakelijk"
  a.vat_rate = 21
end

Article.find_or_create_by(external_article_code: "HUURMAATWERK", external_article_subcode: "HUURMAATWERK") do |a|
  a.name = "Huur maatwerk - particulier"
  a.vat_rate = 21
end

Article.find_or_create_by(external_article_code: "KORTING") do |a|
  a.name = "Korting"
  a.vat_rate = 21
end

a = Article.find_or_initialize_by(external_article_code: invoice_item_config[:sell_art_external][:article], external_article_subcode: invoice_item_config[:sell_art_external][:subarticle])
a.name = "Verkoop BKR"
a.allow_discount = false
a.vat_rate = 21
a.save
a = Article.find_or_initialize_by(external_article_code: invoice_item_config[:sell_art_owned][:article], external_article_subcode: invoice_item_config[:sell_art_owned][:subarticle])
a.name = "Verkoop Heden"
a.allow_discount = false
a.vat_rate = 21
a.save
a = Article.find_or_initialize_by(external_article_code: invoice_item_config[:sell_art_artist][:article], external_article_subcode: invoice_item_config[:sell_art_artist][:subarticle])
a.name = "Verkoop Consignatie"
a.allow_discount = false
a.vat_rate = 9
a.save

Article.find_or_create_by(external_article_code: "MATERIAAL", external_article_subcode: "MATERIALEN") do |a|
  a.name = "Verkoop (ophang)materialen"
  a.vat_rate = 21
end

Article.find_or_create_by(external_article_code: "OMZETOVERIGE", external_article_subcode: "OMZETOVERIGE") do |a|
  a.name = "Overige kosten"
  a.vat_rate = 21
end

Article.find_or_create_by(external_article_code: "HUURRUIMTE", external_article_subcode: "HUURRUIMTE") do |a|
  a.name = "Huur ruimte"
  a.vat_rate = 21
end

Article.find_or_create_by(external_article_code: "TRANSPORT", external_article_subcode: "TRANSPORT") do |a|
  a.name = "Transport"
  a.vat_rate = 21
end
Article.find_or_create_by(external_article_code: "ADVIES", external_article_subcode: "ADVIES") do |a|
  a.name = "Advies"
  a.vat_rate = 21
end
Article.find_or_create_by(external_article_code: "ARTHANDLING", external_article_subcode: "ARTHANDLING") do |a|
  a.name = "Art handling"
  a.vat_rate = 21
end
Article.find_or_create_by(external_article_code: "SPAAR", external_article_subcode: "SPRSPAARTGD") do |a|
  a.name = "Sprinten Spaartegoed"
  a.vat_rate = 0
end

a = Article.find_or_create_by(external_article_code: "VEILINGKOSTEN", external_article_subcode: "VEILINGKOSTEN")
a.name = "Veilingkosten"
a.vat_rate = 21
a.save

a = Article.find_or_create_by(external_article_code: "CADEAUTGD", external_article_subcode: "GEBRUIK")
a.name = "Cadeautegoed/gebruik"
a.allow_discount = false
a.vat_rate = 0
a.save
