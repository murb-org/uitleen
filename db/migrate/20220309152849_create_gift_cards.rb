class CreateGiftCards < ActiveRecord::Migration[7.0]
  def change
    create_table :gift_cards do |t|
      t.string :code
      t.decimal :amount
      t.integer :customer_id
      t.string :action

      t.timestamps
    end
  end
end
