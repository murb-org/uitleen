class CreateCustomers < ActiveRecord::Migration[6.1]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :postal_code
      t.string :twinfield_code
      t.string :twinfield_raw
      t.string :uuid, index: true, null: false, unique: true
      t.timestamps
    end
  end
end
