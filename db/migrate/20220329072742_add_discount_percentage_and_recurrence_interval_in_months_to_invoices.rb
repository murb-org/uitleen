class AddDiscountPercentageAndRecurrenceIntervalInMonthsToInvoices < ActiveRecord::Migration[7.0]
  def change
    add_column :invoices, :discount_percentage, :decimal, default: 0
    add_column :invoices, :recurrence_interval_in_months, :integer, default: 1
  end
end
