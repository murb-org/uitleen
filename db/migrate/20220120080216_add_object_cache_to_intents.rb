class AddObjectCacheToIntents < ActiveRecord::Migration[7.0]
  def change
    add_column :intents, :object_cache, :text
  end
end
