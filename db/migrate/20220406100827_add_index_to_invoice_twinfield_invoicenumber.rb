class AddIndexToInvoiceTwinfieldInvoicenumber < ActiveRecord::Migration[7.0]
  def change
    add_index :invoices, :twinfield_invoicenumber, unique: true
  end
end
