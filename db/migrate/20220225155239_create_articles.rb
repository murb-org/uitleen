class CreateArticles < ActiveRecord::Migration[7.0]
  def change
    create_table :articles do |t|
      t.string :external_article_code
      t.string :name
      t.decimal :vat_rate

      t.timestamps
    end
  end
end
