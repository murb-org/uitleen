class AddAllowDiscountToArticles < ActiveRecord::Migration[7.0]
  def change
    add_column :articles, :allow_discount, :boolean, default: true
  end
end
