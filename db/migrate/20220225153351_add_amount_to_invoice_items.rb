class AddAmountToInvoiceItems < ActiveRecord::Migration[7.0]
  def change
    add_column :invoice_items, :quantity, :decimal, default: 1
    add_column :invoice_items, :unit_price_ex_vat, :decimal
    add_column :invoice_items, :note, :string
  end
end
