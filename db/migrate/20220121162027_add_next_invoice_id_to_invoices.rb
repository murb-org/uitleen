class AddNextInvoiceIdToInvoices < ActiveRecord::Migration[7.0]
  def change
    add_column :invoices, :previous_invoice_id, :integer
  end
end
