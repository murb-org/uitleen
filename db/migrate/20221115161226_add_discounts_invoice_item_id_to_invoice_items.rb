class AddDiscountsInvoiceItemIdToInvoiceItems < ActiveRecord::Migration[7.0]
  def change
    add_column :invoice_items, :discount_amount, :decimal, default: 0
    add_column :invoice_items, :discount_type, :string
  end
end
