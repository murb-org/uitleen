class RenameObjectIdToInvoiceableId < ActiveRecord::Migration[7.0]
  def change
    rename_column :invoice_items, :object_id, :invoiceable_id
    rename_column :invoice_items, :object_type, :invoiceable_type
  end
end
