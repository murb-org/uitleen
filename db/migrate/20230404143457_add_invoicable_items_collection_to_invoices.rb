class AddInvoicableItemsCollectionToInvoices < ActiveRecord::Migration[7.0]
  def change
    add_column :invoices, :invoicable_item_collection_external_id, :string
    add_column :invoices, :invoicable_item_collection_type, :string
  end
end
