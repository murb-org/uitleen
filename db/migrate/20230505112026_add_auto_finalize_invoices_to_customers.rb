class AddAutoFinalizeInvoicesToCustomers < ActiveRecord::Migration[7.0]
  def change
    add_column :customers, :auto_finalize_invoices, :boolean, default: true
  end
end
