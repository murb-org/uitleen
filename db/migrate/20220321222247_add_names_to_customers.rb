class AddNamesToCustomers < ActiveRecord::Migration[7.0]
  def change
    add_column :customers, :first_name, :string
    add_column :customers, :last_name, :string
    add_column :customers, :last_name_prefix, :string
    add_column :customers, :initials, :string
    add_column :customers, :title, :string
    add_column :customers, :born_on, :date
    add_column :customers, :old_data, :text
  end
end
