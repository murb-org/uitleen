class AddOldDataToImports < ActiveRecord::Migration[7.0]
  def change
    add_column :invoices, :old_data, :text
  end
end
