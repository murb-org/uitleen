class ChangeUsersExpiresAtToBigint < ActiveRecord::Migration[7.0]
  def change
    remove_column :users, :expires_at, :datatime
    add_column :users, :expires_at, :bigint
  end
end
