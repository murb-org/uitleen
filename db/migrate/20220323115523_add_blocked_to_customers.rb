class AddBlockedToCustomers < ActiveRecord::Migration[7.0]
  def change
    add_column :customers, :blocked_at, :datetime
    add_column :customers, :blocked_reason, :string
    add_column :customers, :sex, :string
    add_column :customers, :customer_type, :string
    add_column :customers, :deceased_at, :datetime
  end
end
