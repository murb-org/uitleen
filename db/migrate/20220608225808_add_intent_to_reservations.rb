class AddIntentToReservations < ActiveRecord::Migration[7.0]
  def change
    add_column :reservations, :intent, :string
  end
end
