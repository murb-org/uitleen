class RenameTwinfieldCodeToExternalCustomerId < ActiveRecord::Migration[7.0]
  def change
    rename_column :customers, :twinfield_code, :external_customer_id
  end
end
