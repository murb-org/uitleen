class AddOldDataToReservations < ActiveRecord::Migration[7.0]
  def change
    add_column :reservations, :old_data, :text
  end
end
