class AddCompanyVatToCustomers < ActiveRecord::Migration[7.2]
  def change
    add_column :customers, :company_vat, :string
  end
end
