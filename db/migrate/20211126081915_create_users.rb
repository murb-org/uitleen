class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :email
      t.string :name
      t.string :sub
      t.string :access_token
      t.string :refresh_token
      t.datetime :expires_at
      t.text :roles
      t.text :groups
      t.text :resources

      t.timestamps
    end
  end
end
