class AddFinalizedAtToDraftInvoices < ActiveRecord::Migration[7.0]
  def change
    add_column :draft_invoices, :finalized_at, :datetime
  end
end
