class AddRentalTypeToInvoiceItems < ActiveRecord::Migration[7.0]
  def change
    add_column :invoice_items, :rental_type, :string
  end
end
