class AddImportIdToCustomers < ActiveRecord::Migration[7.0]
  def change
    add_column :customers, :import_id, :string
    add_column :reservations, :import_id, :string
    add_column :invoices, :import_id, :string

    # additional cleanup
    remove_column :collect_mandates, :twinfield_id, :integer
  end
end
