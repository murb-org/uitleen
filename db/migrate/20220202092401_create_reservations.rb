class CreateReservations < ActiveRecord::Migration[7.0]
  def change
    create_table :reservations do |t|
      t.integer :work_id
      t.integer :customer_id
      t.string :name
      t.string :address
      t.string :postal_code
      t.string :city
      t.string :email
      t.string :telephone
      t.text :comments

      t.timestamps
    end
  end
end
