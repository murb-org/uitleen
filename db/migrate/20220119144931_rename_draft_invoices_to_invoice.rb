class RenameDraftInvoicesToInvoice < ActiveRecord::Migration[7.0]
  def change
    rename_table :draft_invoices, :invoices
  end
end
