class CreateCustomerTransactions < ActiveRecord::Migration[7.0]
  def change
    create_table :customer_transactions do |t|
      t.string :source
      t.string :code
      t.string :currency
      t.string :customer_code
      t.date :date
      t.string :key
      t.string :number
      t.string :invoice_number
      t.decimal :open_value, precision: 16, scale: 2
      t.string :status
      t.decimal :value, precision: 16, scale: 2

      t.timestamps
    end

    add_index :customer_transactions, [:source, :key], unique: true
  end
end
