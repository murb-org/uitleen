class AddUseCustomerSavingsForPurchaseToInvoices < ActiveRecord::Migration[7.0]
  def change
    add_column :invoices, :use_customer_savings_for_purchase, :boolean, default: true
  end
end
