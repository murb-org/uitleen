class AddOnHoldAtToInvoices < ActiveRecord::Migration[7.0]
  def change
    add_column :invoices, :on_hold_at, :datetime
    add_column :invoices, :on_hold_by_user_id, :integer
  end
end
