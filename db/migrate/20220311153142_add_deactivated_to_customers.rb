class AddDeactivatedToCustomers < ActiveRecord::Migration[7.0]
  def change
    add_column :customers, :deactivated_at, :datetime, default: nil
  end
end
