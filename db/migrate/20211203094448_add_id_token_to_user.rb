class AddIdTokenToUser < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :id_token, :text
  end
end
