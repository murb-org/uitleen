class AddTwinfieldInvoiceIdToDraftInvoice < ActiveRecord::Migration[6.1]
  def change
    add_column :draft_invoices, :twinfield_invoicenumber, :string
  end
end
