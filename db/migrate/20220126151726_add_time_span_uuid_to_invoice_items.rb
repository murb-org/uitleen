class AddTimeSpanUuidToInvoiceItems < ActiveRecord::Migration[7.0]
  def change
    add_column :invoice_items, :time_span_uuid, :string
  end
end
