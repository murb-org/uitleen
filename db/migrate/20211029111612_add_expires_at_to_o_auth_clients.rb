class AddExpiresAtToOAuthClients < ActiveRecord::Migration[6.1]
  def change
    add_column :o_auth_clients, :expires_at, :bigint
  end
end
