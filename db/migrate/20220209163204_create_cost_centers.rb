class CreateCostCenters < ActiveRecord::Migration[7.0]
  def change
    create_table :cost_centers do |t|
      t.string :source
      t.string :code
      t.string :currency, default: "EUR"
      t.string :dim1
      t.string :dim2
      t.string :key
      t.string :number
      t.string :status
      t.decimal :value, precision: 16, scale: 2
      t.string :yearperiod

      t.timestamps
    end

    add_index :cost_centers, [:source, :key], unique: true
  end
end
