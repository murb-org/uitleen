class RenameIntentsInvoiceItems < ActiveRecord::Migration[7.0]
  def change
    rename_table :intents, :invoice_items
  end
end
