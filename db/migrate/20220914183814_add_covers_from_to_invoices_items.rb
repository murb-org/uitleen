class AddCoversFromToInvoicesItems < ActiveRecord::Migration[7.0]
  def change
    add_column :invoice_items, :invoicing_period_starts_at, :date
    add_column :invoice_items, :invoicing_period_ends_at, :date
  end
end
