class CreateDraftInvoices < ActiveRecord::Migration[6.1]
  def change
    create_table "draft_invoices", force: :cascade do |t|
      t.bigint "customer_id"
      t.text "footertext"
      t.string "created_by_user_id"
      t.datetime "created_at", precision: 6, null: false
      t.datetime "updated_at", precision: 6, null: false
      t.text "intents"
    end
  end
end
