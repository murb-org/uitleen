class AddCommentsToCustomers < ActiveRecord::Migration[7.0]
  def change
    add_column :customers, :comments, :text
  end
end
