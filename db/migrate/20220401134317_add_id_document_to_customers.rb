class AddIdDocumentToCustomers < ActiveRecord::Migration[7.0]
  def change
    add_column :customers, :id_document_type, :string
    add_column :customers, :id_document_number, :string
    add_column :customers, :id_document_valid_until, :date
  end
end
