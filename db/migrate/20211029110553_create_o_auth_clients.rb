class CreateOAuthClients < ActiveRecord::Migration[6.1]
  def change
    create_table :o_auth_clients do |t|
      t.string :raw_data
      t.string :name
      t.string :sub
      t.string :access_token
      t.string :refresh_token

      t.timestamps
    end
  end
end
