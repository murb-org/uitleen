class DropTableReservations < ActiveRecord::Migration[7.0]
  def change
    drop_table "reservations", force: :cascade do |t|
      t.integer "work_id"
      t.integer "customer_id"
      t.string "name"
      t.string "address"
      t.string "postal_code"
      t.string "city"
      t.string "email"
      t.string "telephone"
      t.text "comments"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.string "import_id"
      t.text "old_data"
      t.string "intent"
    end
  end
end
