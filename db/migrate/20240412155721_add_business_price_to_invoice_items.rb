class AddBusinessPriceToInvoiceItems < ActiveRecord::Migration[7.0]
  def change
    InvoiceItem.where(invoiceable_type: "CollectionManagement::Work").joins(:invoice).each do |ii|
      selling_price = ii.object_cache["selling_price"].to_f
      ii.object_cache["business_rent_price_ex_vat"] ||= if selling_price.nil? || selling_price == 0
        nil
      elsif selling_price < 500
        7.0
      elsif selling_price < 1500
        12.4
      else
        [selling_price / 100.0].min
      end
      ii.save! if ii.changed?
    end
  end
end
