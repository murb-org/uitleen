class AddRequiresReviewAtToInvoices < ActiveRecord::Migration[7.0]
  def change
    add_column :invoices, :requires_review_at, :datetime
  end
end
