class AddInputPriceExVatToInvoiceItems < ActiveRecord::Migration[7.0]
  def change
    add_column :invoice_items, :input_price_ex_vat, :boolean
  end
end
