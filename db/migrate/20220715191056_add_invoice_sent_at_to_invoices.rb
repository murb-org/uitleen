class AddInvoiceSentAtToInvoices < ActiveRecord::Migration[7.0]
  def change
    add_column :invoices, :invoice_sent_at, :datetime
  end
end
