class AddCreditsInvoiceIdToInvoices < ActiveRecord::Migration[7.0]
  def change
    add_column :invoices, :credits_invoice_id, :integer
  end
end
