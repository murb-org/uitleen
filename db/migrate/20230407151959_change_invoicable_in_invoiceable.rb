class ChangeInvoicableInInvoiceable < ActiveRecord::Migration[7.0]
  def change
    rename_column :invoices, :invoicable_item_collection_external_id, :invoiceable_item_collection_external_id
    rename_column :invoices, :invoicable_item_collection_type, :invoiceable_item_collection_type
  end
end
