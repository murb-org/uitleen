class AddExternalArticleSubcodeToArticles < ActiveRecord::Migration[7.0]
  def change
    add_column :articles, :external_article_subcode, :string
  end
end
