class AddRemoteInvoiceDataToInvoice < ActiveRecord::Migration[7.0]
  def change
    add_column :invoices, :external_invoice_data, :text
  end
end
