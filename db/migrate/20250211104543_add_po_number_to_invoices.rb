class AddPoNumberToInvoices < ActiveRecord::Migration[7.2]
  def change
    add_column :invoices, :po_number, :string
  end
end
