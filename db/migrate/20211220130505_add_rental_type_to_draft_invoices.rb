class AddRentalTypeToDraftInvoices < ActiveRecord::Migration[7.0]
  def change
    add_column :draft_invoices, :rental_type, :string, default: :basic_rent
  end
end
