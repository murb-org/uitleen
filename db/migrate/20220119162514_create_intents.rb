class CreateIntents < ActiveRecord::Migration[7.0]
  def change
    create_table :intents do |t|
      t.integer :invoice_id
      t.string :object_type
      t.string :object_id
      t.string :intent

      t.timestamps
    end
  end
end
