class CreateCollectMandates < ActiveRecord::Migration[7.0]
  def change
    create_table :collect_mandates do |t|
      t.integer :twinfield_id
      t.integer :customer_id
      t.date :signed_on
      t.string :sign_location
      t.text :data
      t.string :uuid
      t.timestamps
    end
  end
end
