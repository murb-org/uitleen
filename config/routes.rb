require "sidekiq/web"
require "letter_opener_web"

Rails.application.routes.draw do
  get "reports", to: "reports#index"
  get "reports/savings"
  get "reports/savings_per_year"
  get "reports/lent"
  get "reports/unfinished_ended"
  resources :articles
  mount Sidekiq::Web => "/sidekiq", :constraints => AdminConstraint.new
  mount(LetterOpenerWeb::Engine, at: "/letter_opener", constraints: AdminConstraint.new) if Rails.env.staging? || Rails.env.development?
  namespace :api do
    namespace :v1 do
      resources :customers, only: [:index, :show]
      resources :works, only: [:show]
      resources :collections, only: [:index, :show] do
        resources :works, only: [:index, :show] do
        end
      end
    end
  end

  resources :reservations, except: [:new]

  resources :draft_invoices do
    collection do
      post :reset # , to: 'draft_invoices#reset'
    end
    resources :invoice_items do # , only: [:new, ,:destroy, :create] do
      get :edit_discount
    end
    get :mutation_form
    post :create_invoice
    patch :finalize_invoice
    patch :reviewed
    patch :refresh
  end

  resources :invoices do
    get "old_data", to: "invoices#old_data"
    get :specification, to: "invoices#specification"
    member do
      post :register_payment
      patch :on_hold
    end
  end

  resources :customers do
    get "raw", to: "customers#raw"
    resources :time_spans
    resource :collect_mandate
    resources :invoices
  end

  resources :collections do
    resources :works do
      resources :reservations
      resource :return, only: [:new, :create]
    end
  end

  resources :works, only: :show

  post "/auth/:provider/callback", to: "sessions#create"
  get "/auth/:provider/callback", to: "sessions#create"
  get "/auth/new", to: "sessions#new"
  get "/application_status", to: "application#status"
  delete "/auth", to: "sessions#destroy", as: :auth

  root "application#home"
end
