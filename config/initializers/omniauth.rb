Rails.application.config.middleware.use OmniAuth::Builder do
  provider :twinfield, Rails.application.credentials.oauth_twinfield_id, Rails.application.credentials.oauth_twinfield_secret
  provider :central_login, Rails.application.credentials.central_login_id, Rails.application.credentials.central_login_secret, {client_options: {site: Rails.application.credentials.central_login_site}, scope: "openid email profile", response_type: "id_token"}
end

OmniAuth.config.logger = Rails.logger
