# frozen_string_literal: true

class Uitleen::SidekiqErrorLogger
  def call(worker, msg, queue)
    yield
  rescue => ex
    begin
      SystemMailer.sidekiq_error_message(ex, worker).deliver!
    rescue
    end
    raise ex
  end
end

Sidekiq.configure_server do |config|
  config.client_middleware do |chain|
    # chain.add SidekiqUniqueJobs::Middleware::Client
  end
  config.server_middleware do |chain|
    chain.add Uitleen::SidekiqErrorLogger
  end
end
