Rails.application.config.active_record.yaml_column_permitted_classes = [
  ActiveSupport::HashWithIndifferentAccess,
  Symbol,
  DateTime,
  Time,
  Date,
  "User",
  Hashie::Array,
  "ActiveModel::Attribute::FromDatabase"
]
Rails.application.config.active_record.use_yaml_unsafe_load = true

# ActiveRecord::Base.yaml_column_permitted_classes = Rails.application.config.active_record.yaml_column_permitted_classes
::ActiveRecord.yaml_column_permitted_classes = Rails.application.config.active_record.yaml_column_permitted_classes
::ActiveRecord.use_yaml_unsafe_load = Rails.application.config.active_record.use_yaml_unsafe_load
