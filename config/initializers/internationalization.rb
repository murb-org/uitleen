Rails.application.config.time_zone = "Amsterdam"
I18n.available_locales = [:en, :nl]

Rails.application.config.i18n.default_locale = "nl"
Time.zone = "Amsterdam"
