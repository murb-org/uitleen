unless Rails.env.development?
  Rails.application.config.middleware.use ExceptionNotification::Rack,
    email: {
      email_prefix: "[Uitleen #{Rails.env}] ",
      sender_address: %("notifier" <notifier@heden.nl>),
      exception_recipients: %w[uitleen@murb.nl]
    }
end
