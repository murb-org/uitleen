require "test_helper"

class CustomerMailerTest < ActionMailer::TestCase
  test "welcome" do
    mail = CustomerMailer.welcome(customers(:one))
    assert_equal "Welkom bij Heden", mail.subject
    assert_equal ["test@test.nl"], mail.to
    assert_equal ["info@heden.nl"], mail.from
    assert_match "Geachte heer, mevrouw,", mail.body.encoded
  end
end
