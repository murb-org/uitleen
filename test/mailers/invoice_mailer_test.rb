require "test_helper"

class InvoiceMailerTest < ActionMailer::TestCase
  test "created doesn't return a mail when pdf is not availabel" do
    invoice = Invoice.new(customer: customers(:one))

    mail = InvoiceMailer.created(invoice)
    assert_equal ActionMailer::Base::NullMail, mail.message.class
  end

  test "created returns a mail with a pdf attachment" do
    invoice = Invoice.new(customer: customers(:one), twinfield_invoicenumber: 123)
    invoice.pdf.attach(io: File.open(Rails.root.join("test", "fixtures", "files", "test.pdf")), filename: "test.pdf", content_type: "application/pdf")
    invoice.save
    mail = InvoiceMailer.created(invoice)
    assert_equal "Factuur 123", mail.subject
    assert_equal ["test@test.nl"], mail.to
    assert_equal ["info@heden.nl"], mail.from
    assert_match "filename=factuur.pdf", mail.body.encoded
  end
end
