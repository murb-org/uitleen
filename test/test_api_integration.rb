#!/usr/bin/env ruby

require "yaml"
require "json"

secrets_file_path = File.join(File.dirname(__FILE__), "..", "config", "secrets.yml")
secrets = YAML.safe_load_file(secrets_file_path)["development"]
uitleen_site = "http://localhost:5001/"

# secrets["central_login_id"] =
# secrets["central_login_secret"] =
# secrets["central_login_site"] =
test_results = {}

command = "curl -X POST -d 'grant_type=client_credentials' -d 'client_id=#{secrets["central_login_id"]}' -d 'client_secret=#{secrets["central_login_secret"]}' #{secrets["central_login_site"]}oauth/token"
puts "CURL COMMAND: #{command}"

token = JSON.parse(`#{command}`)
puts token

test_results["Retrieves token?"] = !!token

id_token = token["id_token"]

command = "curl -H 'Authorization: Bearer #{id_token}' #{uitleen_site}api/v1/collections.json"
puts "CURL COMMAND: #{command}"
collections = `#{command}`

collections = JSON.parse(collections)
p collections # returns a hash with the collections (if it doesn't you may not have access to a collection)

test_results["Retrieves collections"] = collections.count > 0

if collections == []
  puts "No collections returned, probably you need access"
  exit
end
command = "curl -H 'Authorization: Bearer #{id_token}' #{uitleen_site}api/v1/collections/#{collections[0]["id"]}/works.json"

puts "CURL COMMAND: #{command}"

works = JSON.parse(`#{command}`)

p works
first_work = works["data"].compact.first

test_results["Retrieves works"] = works["data"].compact.count > 0

p first_work

command = "curl -H 'Authorization: Bearer #{id_token}' #{uitleen_site}api/v1/collections/#{collections[0]["id"]}/works/#{first_work["id"]}.json"

puts "CURL COMMAND: #{command}"

work = JSON.parse(`#{command}`)
p work
test_results["Retrieves single work"] = !!work

puts " "
puts " "
puts " "
p test_results
