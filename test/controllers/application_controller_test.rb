require "test_helper"

class ApplicationControllerTest < ActionDispatch::IntegrationTest
  test "status" do
    skip "ci" if ENV["CI_SERVER_NAME"] == "GitLab"

    work = CollectionManagement::Work.new(id: "1", name: "test")
    collection = CollectionManagement::Collection.new(id: "1", name: "test")
    collection.stub(:works, [work]) do
      CollectionManagement::Work.stub(:find, work) do
        CollectionManagement::Collection.stub(:all, [collection]) do
          get application_status_path

          assert_response :ok
        end
      end
    end
  end
end
