require "test_helper"

class CustomersControllerTest < ActionDispatch::IntegrationTest
  test "anonymous should not get index" do
    get customers_url
    assert_response :redirect
  end

  test "anonymous should not get new" do
    get new_customer_url
    assert_response :redirect
  end

  test "anonymous should not get show" do
    get customer_url(customers(:one))
    assert_response :redirect
  end

  test "anonymous should not get edit" do
    get edit_customer_url(customers(:one))
    assert_response :redirect
  end

  test "anonymous should not delete" do
    delete customer_url(customers(:one))

    assert_nil(customers(:one).reload.deactivated_at)
    assert_response :redirect
  end

  test "admin should be able to delete" do
    sign_in(users(:admin))
    stub_twinfield do
      delete customer_url(customers(:one))
    end

    assert_not_nil(customers(:one).reload.deactivated_at)
    assert_response :redirect
  end

  test "admin should be able to edit" do
    sign_in(users(:admin))
    stub_twinfield do
      get edit_customer_url(customers(:one))
    end
    assert_no_match("<input class=\"string optional\" type=\"text\" value=\"Nieuw adres\"", response.body)
    assert_response :ok

    stub_twinfield do
      get edit_customer_url(customers(:one), {add_address: "true"})
    end
    assert_response :ok
    assert_match("<input class=\"string optional\" type=\"text\" value=\"Nieuw adres\"", response.body)
  end

  test "admin should get index" do
    sign_in(users(:admin))
    stub_twinfield do
      get customers_url
    end

    assert_match("First Customer Pieter", response.body)
    assert_match("Second Customer", response.body)
  end

  test "clerk should get index" do
    sign_in(users(:clerk))
    stub_twinfield do
      get customers_url
    end

    assert_match("First Customer Pieter", response.body)
    assert_match("Second Customer", response.body)
  end

  test "only admin should get csv index" do
    sign_in(users(:clerk))
    stub_twinfield do
      get customers_url(format: "csv")
    end
    assert_response :redirect
    sign_in(users(:admin))
    stub_twinfield do
      get customers_url(format: "csv")
    end

    assert_match("First Customer,,Pieter", response.body)
    assert_match("Second,,Customer", response.body)
  end

  test "user should get show" do
    sign_in(users(:admin))

    [:one, :two, :four, :five].each do |cust_id|
      customer = customers(cust_id)

      stub_twinfield do
        get customer_url(customer)
      end

      assert_match(customer.last_name, response.body)
    end
  end

  test "user should get new" do
    sign_in(users(:admin))

    stub_twinfield customer: Twinfield::Customer.new(office: 1, addresses: [Twinfield::Customer::Address.new(id: 1, name: "", default: true, country: "NL", type: "invoice")]) do
      get new_customer_url
    end

    assert_match("<input class=\"string optional\" type=\"text\" name=\"customer[first_name]\" id=\"customer_first_name\" />", response.body)
  end

  test "user should post create" do
    sign_in(users(:admin))

    fake_customer = Twinfield::Customer.new(**customers(:one).twinfield_raw)

    fake_customer.stub(:save, fake_customer) do
      stub_twinfield do
        Twinfield::Customer.stub(:new, fake_customer) do
          post customers_url(customer: {twinfield_customer: {name: "naam"}})
          Twinfield.configuration = nil
          follow_redirect!
        end
      end
    end
    assert_match("<p class=\"notice hide-for-print\">Klant is toegevoegd</p>", response.body)
  end

  test "user should patch update" do
    user = users(:admin)
    customer = customers(:one)

    sign_in(user)

    request = stub_request(:post, "https://accounting.twinfield.com/webservices/processxml.asmx")
      .with(body: /NL30TRIO0254651658/)
      .to_return(status: 200, body: File.read("test/fixtures/files/customer_update_response.xml"))

    stub_twinfield do
      patch customer_url(customer.uuid, "customer" =>
        {"customer_type" => "business",
         "company_name" => "Samkalde v.o.f.",
         "initials" => "",
         "first_name" => "Saar",
         "last_name_prefix" => "",
         "last_name" => "Samkalde",
         "email" => "test@test.nl",
         "auto_finalize_invoices" => "0",
         "twinfield_customer" =>
          {"website" => "",
           "cocnumber" => "",
           "vatnumber" => "",
           "twinfield_customer_addresses" =>
            {"1" =>
              {"_destroy" => "0",
               "type" => "invoice",
               "default" => "1",
               "name" => "Samkalden",
               "field1" => "Mevrouw S. Samkalde",
               "field2" => "Nassaulaan 4611",
               "field3" => "",
               "field4" => "",
               "field5" => "",
               "field6" => "",
               "contact" => "",
               "postcode" => "2305 FR",
               "city" => "Haarlem",
               "country" => "NL",
               "telephone" => "023-5741558",
               "email" => "home@murb.nl",
               "telefax" => ""}},
           "twinfield_customer_banks" =>
            {"1" =>
              {"accountnumber" => "254651658",
               "address" => {"name" => "", "field1" => "", "field2" => "", "field3" => "", "field4" => "", "field5" => "", "field6" => "", "contact" => "", "postcode" => "", "city" => "", "telephone" => "", "email" => "", "telefax" => "", "type" => "", "country" => ""},
               "bankname" => "",
               "biccode" => "",
               "city" => "",
               "country" => "NL",
               "natbiccode" => "",
               "postcode" => "",
               "state" => "",
               "default" => "1",
               "ascription" => "Samkalden",
               "iban" => "NL30TRIO0254651658"}}}})
      Twinfield.configuration = nil
      follow_redirect!
    end
    assert_match("<p class=\"notice hide-for-print\">Klant is bijgewerkt</p>", response.body)
    customer.reload
    assert_equal("Samkalde v.o.f.", customer.name)
    assert_requested(request)
  end
end
