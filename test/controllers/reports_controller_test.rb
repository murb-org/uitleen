require "test_helper"

class ReportsControllerTest < ActionDispatch::IntegrationTest
  test "should get savings" do
    get reports_savings_url
    assert_response :redirect
  end

  test "should not get index publicly" do
    get reports_url
    assert_redirected_to auth_new_path(redirect_to: reports_url)
  end

  test "should not get index publicly for clerk" do
    sign_in(users(:one))

    get reports_url
    assert_redirected_to root_path
    get reports_lent_url
    assert_redirected_to root_path
  end

  test "should get index publicly for admin" do
    sign_in(users(:admin))

    get reports_url
    assert_response :success
  end

  test "should show lent" do
    sign_in(users(:admin))
    CollectionManagement::Collection.stub :all, [CollectionManagement::Collection.new(id: 1)] do
      CollectionManagement::TimeSpan.stub :where, [CollectionManagement::TimeSpan.new(contact: {url: customers(:one).uri}, subject: CollectionManagement::Work.new(collection_id: 1, id: 12, title_rendered: "One", artist_name_rendered: "Andriesen, Jannette"))] do
        get reports_lent_url
      end
    end
    assert_response :success
    assert_match "Andriesen, Jannette", response.body
  end

  test "should show unfinished_ended" do
    sign_in(users(:admin))

    CollectionManagement::Collection.stub :all, [CollectionManagement::Collection.new(id: 1)] do
      CollectionManagement::TimeSpan.stub :where, [CollectionManagement::TimeSpan.new(ends_at: Date.new(2021, 2, 23), contact: {url: customers(:one).uri}, subject: CollectionManagement::Work.new(collection_id: 1, id: 12, title_rendered: "One", artist_name_rendered: "Andriesen, Jannette"))] do
        get reports_unfinished_ended_url
      end
    end
    assert_response :success
    assert_match "Actieve uitleenacties met een einddatum in het verleden", response.body
    assert_match "Andriesen, Jannette", response.body
    assert_match "23 feb `21", response.body
  end
end
