require "test_helper"

class InvoiceItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @invoice_item = invoice_items(:purchase_shop_product)
    @draft_invoice = invoices(:two)
  end

  test "should get new" do
    get new_draft_invoice_invoice_item_url(@draft_invoice)
    assert_response :redirect
    sign_in(users(:one))
    get new_draft_invoice_invoice_item_url(@draft_invoice)
    assert_response :redirect

    sign_in(users(:clerk))
    get new_draft_invoice_invoice_item_url(@draft_invoice)
    assert_response :success
  end

  test "should create invoice_item" do
    sign_in(users(:clerk))

    assert_difference("InvoiceItem.count") do
      post draft_invoice_invoice_items_url(@draft_invoice), params: {invoice_item: {quantity: 2, intent: "purchase", article: articles(:shop_product).id.to_s, unit_price: 20}}
    end

    assert_equal((20 * (1 / BigDecimal(121)) * 100).round(10), @draft_invoice.invoice_items.last.unit_price_ex_vat.round(10))

    assert_redirected_to draft_invoice_url(@draft_invoice)
  end

  test "should create invoice_item without vat" do
    sign_in(users(:clerk))

    assert_difference("InvoiceItem.count") do
      post draft_invoice_invoice_items_url(@draft_invoice), params: {invoice_item: {quantity: 2, intent: "purchase", article: articles(:shop_product).id.to_s, unit_price: 20, input_price_ex_vat: true}}
    end

    assert_equal(20, @draft_invoice.invoice_items.last.unit_price_ex_vat)
    assert_redirected_to draft_invoice_url(@draft_invoice)
  end

  test "should create invoice_item with gift card" do
    sign_in(users(:clerk))

    assert_difference("InvoiceItem.count") do
      post draft_invoice_invoice_items_url(@draft_invoice), params: {invoice_item: {gift_card: {code: "ABC123", amount: 50.0, action: :trade_in}}}
    end

    assert_redirected_to draft_invoice_url(@draft_invoice)

    assert_equal GiftCard, @draft_invoice.invoice_items.last.invoiceable.class
  end

  test "should render edit when failing action" do
    sign_in(users(:clerk))

    assert_no_difference("InvoiceItem.count") do
      post draft_invoice_invoice_items_url(@draft_invoice), params: {invoice_item: {a: 2}}
    end

    assert_response :unprocessable_entity
  end

  test "should destroy invoice_item" do
    sign_in(users(:clerk))

    assert_difference("InvoiceItem.count", -1) do
      delete draft_invoice_invoice_item_url(@draft_invoice, @invoice_item)
    end

    assert_redirected_to draft_invoice_url(@draft_invoice)
  end
end
