require "test_helper"

class InvoicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @invoice = invoices(:recently_expired_quarterly_interval)
  end

  test "GET #show should get show" do
    get invoice_url(@invoice)
    assert_response :redirect

    stub_twinfield do
      sign_in(users(:one))
      get invoice_url(@invoice)
      assert_response :redirect

      Twinfield::SalesInvoice.stub :find, nil do
        sign_in(users(:clerk))
        get invoice_url(@invoice, scope: "recent")
        assert_response :success
      end
    end
  end

  test "GET #show should display basic draft invoice" do
    Twinfield::SalesInvoice.stub :find, nil do
      stub_twinfield do
        sign_in(users(:clerk))
        get invoice_url(@invoice)
      end
    end
    assert_match "Deze factuur is niet gegenereerd in de uitleen applicatie of het aangesloten boekhoudpakket.", response.body
    assert_match "Gelieve het bovenstaande bedrag van €36,27 te voldoen", response.body
  end

  test "GET #show should display twinfield invoice" do
    hash = {lines: [{id: "1", article: "HUUR", subarticle: "1", quantity: 1, units: 1, allowdiscountorpremium: "true", description: "Huur van iets beschreven", unitspriceexcl: 7.23, valueinc: 8.75, unitspriceinc: 8.75, freetext1: nil, freetext2: nil, freetext3: nil, dim1: "8000", vatcode: "VH", performancetype: nil, performancedate: nil}, {id: "2", article: "-", subarticle: nil, quantity: nil, units: nil, allowdiscountorpremium: nil, description: "Beschrijving", unitspriceexcl: nil, unitspriceinc: nil, freetext1: nil, freetext2: nil, freetext3: nil, dim1: nil, vatcode: nil, performancetype: nil, performancedate: nil}], vat_lines: [{vatcode: "VH", vatvalue: 1.73, performancetype: "", performancedate: "", vatname: "BTW 21%"}], invoicetype: "FACTUUR", invoicedate: "2022-06-24", duedate: "2022-07-24", performancedate: nil, bank: "BNK", invoiceaddressnumber: 1, deliveraddressnumber: 1, customer_code: 2321, period: "2022/6", currency: "EUR", status: "final", paymentmethod: "bank", headertext: "", footertext: "Footer", office: "NL123", invoicenumber: "2022-0003"}
    sales_invoice = Twinfield::SalesInvoice.new(**hash)

    @invoice.po_number = "PO NUMMER TEST123"
    @invoice.customer.company_vat = "NL 123 VAT NL"
    @invoice.customer.save
    @invoice.save
    Twinfield::SalesInvoice.stub :find, sales_invoice do
      stub_twinfield do
        sign_in(users(:clerk))
        get invoice_url(@invoice)
      end
    end
    assert_no_match "Deze factuur is niet gegenereerd in de uitleen applicatie of het aangesloten boekhoudpakket.", response.body
    assert_match "Huur van iets beschreven", response.body
    assert_match "Gelieve het bovenstaande bedrag van €8,75 te voldoen", response.body
    assert_match "BTW: NL 123 VAT NL", response.body
    assert_match "PO NUMMER TEST123", response.body
  end

  test "GET #show should show summary when many works are shown" do
    hash = {
      lines: [
        {id: "1", article: "HUUR", subarticle: "1", quantity: 5, units: 1, allowdiscountorpremium: "true", description: "Huur van iets beschreven", unitspriceexcl: 7.23, valueinc: 8.75, unitspriceinc: 8.75, freetext1: nil, freetext2: nil, freetext3: nil, dim1: "8000", vatcode: "VH", performancetype: nil, performancedate: nil},
        {id: "2", article: "-", subarticle: nil, quantity: nil, units: nil, allowdiscountorpremium: nil, description: "Een andere simpele beschrijving", unitspriceexcl: nil, unitspriceinc: nil, freetext1: nil, freetext2: nil, freetext3: nil, dim1: nil, vatcode: nil, performancetype: nil, performancedate: nil}
      ],
      vat_lines: [
        {vatcode: "VH", vatvalue: 1.73, performancetype: "", performancedate: "", vatname: "BTW 21%"}
      ], invoicetype: "FACTUUR", invoicedate: "2022-06-24", duedate: "2022-07-24", performancedate: nil, bank: "BNK", invoiceaddressnumber: 1, deliveraddressnumber: 1, customer_code: 2321, period: "2022/6", currency: "EUR", status: "final", paymentmethod: "bank", headertext: "", footertext: "Footer", office: "NL123", invoicenumber: "2022-0003"
    }

    sales_invoice = Twinfield::SalesInvoice.new(**hash)

    @invoice.invoice_items << invoice_items(:just_a_description).dup
    @invoice.invoice_items << invoice_items(:purchase).dup
    25.times do
      @invoice.invoice_items << invoice_items(:recently_expired_quarterly_interval_rent).dup
    end

    Twinfield::SalesInvoice.stub :find, sales_invoice do
      stub_twinfield do
        sign_in(users(:clerk))
        get invoice_url(@invoice)
      end
    end
    assert_match "Zie specificatie", response.body
    assert_no_match "Just a simple line with a description", response.body # not yet in twinfield response; so not rendering
    assert_no_match "Een andere simpele beschrijving", response.body # twinfield line that will no longer be shown, even though not summarized, since we're hiding all descriptions on front page
    assert_no_match "Huur van iets beschreven", response.body # twinfield line
    assert_match "Toon specificatie</a>", response.body
  end

  test "GET #specification should show specification when many works are shown" do
    hash = {lines: [{id: "1", article: "HUUR", subarticle: "1", quantity: 1, units: 1, allowdiscountorpremium: "true", description: "Huur van iets beschreven", unitspriceexcl: 7.23, valueinc: 8.75, unitspriceinc: 8.75, freetext1: nil, freetext2: nil, freetext3: nil, dim1: "8000", vatcode: "VH", performancetype: nil, performancedate: nil}, {id: "2", article: "-", subarticle: nil, quantity: nil, units: nil, allowdiscountorpremium: nil, description: "Beschrijving", unitspriceexcl: nil, unitspriceinc: nil, freetext1: nil, freetext2: nil, freetext3: nil, dim1: nil, vatcode: nil, performancetype: nil, performancedate: nil}], vat_lines: [{vatcode: "VH", vatvalue: 1.73, performancetype: "", performancedate: "", vatname: "BTW 21%"}], invoicetype: "FACTUUR", invoicedate: "2022-06-24", duedate: "2022-07-24", performancedate: nil, bank: "BNK", invoiceaddressnumber: 1, deliveraddressnumber: 1, customer_code: 2321, period: "2022/6", currency: "EUR", status: "final", paymentmethod: "bank", headertext: "", footertext: "Footer", office: "NL123", invoicenumber: "2022-0003"}
    sales_invoice = Twinfield::SalesInvoice.new(**hash)

    @invoice.invoice_items << invoice_items(:just_a_description).dup
    @invoice.invoice_items << invoice_items(:purchase).dup
    20.times do
      @invoice.invoice_items << invoice_items(:recently_expired_quarterly_interval_rent).dup
    end

    Twinfield::SalesInvoice.stub :find, sales_invoice do
      stub_twinfield do
        sign_in(users(:clerk))
        get invoice_specification_url(@invoice)
      end
    end
    assert_no_match "Zie specificatie", response.body
    assert_match "Huur van iets beschreven", response.body # twinfield line
    assert_match "Factuur</a>", response.body
  end

  test "GET #show with paid status" do
    hash = {
      lines: [
        {id: "1", article: "HUUR", subarticle: "1", quantity: 1, units: 1, allowdiscountorpremium: "true", description: "Huur van iets beschreven", unitspriceexcl: 7.23, valueinc: 8.75, unitspriceinc: 8.75, freetext1: nil, freetext2: nil, freetext3: nil, dim1: "8000", vatcode: "VH", performancetype: nil, performancedate: nil},
        {id: "2", article: "-", subarticle: nil, quantity: nil, units: nil, allowdiscountorpremium: nil, description: "Beschrijving", unitspriceexcl: nil, unitspriceinc: nil, freetext1: nil, freetext2: nil, freetext3: nil, dim1: nil, vatcode: nil, performancetype: nil, performancedate: nil}
      ],
      vat_lines: [
        {vatcode: "VH", vatvalue: 1.73, performancetype: "", performancedate: "", vatname: "BTW 21%"}
      ],
      invoicetype: "FACTUUR",
      invoicedate: "2022-06-24",
      duedate: "2022-07-24",
      performancedate: nil,
      bank: "BNK",
      invoiceaddressnumber: 1,
      deliveraddressnumber: 1,
      customer_code: 2321,
      period: "2022/6", currency: "EUR",
      status: "final",
      paymentmethod: "bank",
      headertext: "",
      footertext: "Footer",
      office: "NL123",
      invoicenumber: "2022-0003"
    }
    sales_invoice = Twinfield::SalesInvoice.new(**hash)
    transaction = CustomerTransaction.new(status: "matched")
    @invoice.update_columns(updated_at: 4.minutes.ago)

    sales_invoice.stub :uitleen_invoice, @invoice do
      @invoice.stub :transaction, transaction do
        @invoice.stub :fully_final?, true do
          Twinfield::SalesInvoice.stub :find, sales_invoice do
            stub_twinfield do
              sign_in(users(:clerk))
              get invoice_url(@invoice)
            end
          end
        end
      end
    end
    assert_match "De factuur is betaald.", response.body
  end

  test "GET #show format pdf" do
    hash = {lines: [{id: "1", article: "HUUR", subarticle: "1", quantity: 1, units: 1, allowdiscountorpremium: "true", description: "Huur van iets beschreven", unitspriceexcl: 7.23, valueinc: 8.75, unitspriceinc: 8.75, freetext1: nil, freetext2: nil, freetext3: nil, dim1: "8000", vatcode: "VH", performancetype: nil, performancedate: nil}, {id: "2", article: "-", subarticle: nil, quantity: nil, units: nil, allowdiscountorpremium: nil, description: "Beschrijving", unitspriceexcl: nil, unitspriceinc: nil, freetext1: nil, freetext2: nil, freetext3: nil, dim1: nil, vatcode: nil, performancetype: nil, performancedate: nil}], vat_lines: [{vatcode: "VH", vatvalue: 1.73, performancetype: "", performancedate: "", vatname: "BTW 21%"}], invoicetype: "FACTUUR", invoicedate: "2022-06-24", duedate: "2022-07-24", performancedate: nil, bank: "BNK", invoiceaddressnumber: 1, deliveraddressnumber: 1, customer_code: 2321, period: "2022/6", currency: "EUR", status: "final", paymentmethod: "bank", headertext: "", footertext: "Footer", office: "NL123", invoicenumber: "2022-0003"}
    sales_invoice = Twinfield::SalesInvoice.new(**hash)

    Twinfield::SalesInvoice.stub :find, sales_invoice do
      stub_twinfield do
        sign_in(users(:clerk))
        get invoice_url(@invoice, format: :pdf)
      end
    end

    assert_response :redirect
  end

  test "GET #index should work on global level, and be accessible to clerks" do
    stub_twinfield do
      stub_twinfield_invoice do
        sign_in(users(:clerk))

        get invoices_url(scope: "recent")
        assert_response :success
        assert_match("Recent aangemaakte, niet definitieve facturen", response.body)

        get invoices_url(scope: "tobe")
        assert_no_match("Binnenkort te vernieuwen facturen", response.body)

        sign_in(users(:admin))

        get invoices_url(scope: "recent")
        assert_response :success
        assert_match("Recent aangemaakte, niet definitieve facturen", response.body)

        get invoices_url(scope: "tobe")
        assert_match("Binnenkort te vernieuwen facturen", response.body)

        get invoices_url(scope: "final")
        assert_match("2022-0001", response.body)
      end
    end
  end

  test "POST #create should credit invoice if created with credit id" do
    sign_in(users(:admin))

    old_invoice = invoices(:recently_expired)

    stub_twinfield do
      stub_create_twinfield_invoice
      post invoices_url(params: {invoice: {credits_invoice_id: old_invoice.id}})
      follow_redirect!
      assert_match("Creditfactuur concept aangemaakt", response.body)
    end
  end

  test "POST #create should show error if failed" do
    sign_in(users(:admin))

    old_invoice = invoices(:recently_expired)

    stub_twinfield do
      CollectionManagement::TimeSpan.stub :find, nil do
        stub_create_twinfield_invoice
        post invoices_url(params: {invoice: {credits_invoice_id: old_invoice.id}})
        post invoices_url(params: {invoice: {credits_invoice_id: old_invoice.id}})
        follow_redirect!
        assert_match("Creditfactuur kon niet gemaakt worden (Creditfactuur is al in gebruik)", response.body)
        assert_match("Open de creditfactuur", response.body)
      end
    end
  end

  test "POST #create should duplicate invoice if created with manually next id" do
    sign_in(users(:admin))

    old_invoice = invoices(:recently_expired)

    stub_twinfield do
      stub_create_twinfield_invoice
      post invoices_url(params: {invoice: {manually_create_next_for_invoice_id: old_invoice.id}})
      follow_redirect!
      assert_match("Factuur in concept aangemaakt.", response.body)
    end
  end

  test "PATCH #on_hold should duplicate invoice if created with manually next id" do
    sign_in(users(:admin))

    old_invoice = invoices(:recently_expired)
    old_invoice.update(next_invoice_at: 1.day.from_now)

    stub_twinfield do
      CollectionManagement::TimeSpan.stub :find, nil do
        stub_create_twinfield_invoice

        on_hold_message = "De factuur is on hold geplaatst en de vervolgfactuur zal niet meer automatisch worden verstuurd."
        to_be_sent_message = "Onderstaande factuur (nog in voorvertoning) zal automatisch worden verstuurd op"
        stop_on_hold_button = "Stel verzending vervolgfactuur niet langer uit</button></form>"
        create_button = "Maak factuur in concept aan en pas aan</button>"
        start_on_hold_button = "Stel verzending vervolgfactuur uit</button></form>"

        patch on_hold_invoice_url(old_invoice)
        follow_redirect!
        assert_match(on_hold_message, response.body)
        assert_no_match(to_be_sent_message, response.body)
        assert_no_match(start_on_hold_button, response.body)
        assert_match(stop_on_hold_button, response.body)
        assert_match(create_button, response.body)

        get invoice_url(old_invoice, preview: true)
        patch on_hold_invoice_url(old_invoice)
        follow_redirect!
        assert_no_match(on_hold_message, response.body)
        assert_match(to_be_sent_message, response.body)
        assert_no_match(stop_on_hold_button, response.body)
        assert_match(start_on_hold_button, response.body)
        assert_no_match(create_button, response.body)
      end
    end
  end

  test "PATCH #update allows for updating footer text by admin" do
    sign_in(users(:admin))

    invoice = invoices(:recently_expired)

    stub_twinfield do
      CollectionManagement::TimeSpan.stub :find, nil do
        Twinfield::SalesInvoice.stub :find, nil do
          patch invoice_url(invoice, params: {invoice: {footertext: "een nieuwe voettekst"}})
        end
      end
    end

    assert_match("een nieuwe voettekst", invoice.reload.footertext)
  end

  test "PATCH #update does not allow for updating footer text by clerk" do
    sign_in(users(:clerk))

    invoice = invoices(:recently_expired)

    stub_twinfield do
      CollectionManagement::TimeSpan.stub :find, nil do
        Twinfield::SalesInvoice.stub :find, nil do
          patch invoice_url(invoice, params: {invoice: {footertext: "een nieuwe voettekst"}})
        end
      end
    end

    assert_no_match("een nieuwe voettekst", invoice.reload.footertext)
  end
end
