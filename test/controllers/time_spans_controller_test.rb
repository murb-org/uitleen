require "test_helper"

class TimeSpansControllerTest < ActionDispatch::IntegrationTest
  test "anonymous gets time spans for customer" do
    get customer_time_spans_path(customers(:one))
    assert_response :redirect
  end
  test "admin gets time spans for customer" do
    sign_in(:admin)

    request = stub_request(:get, /localhost:3000\/api\/v1\/time_spans\?contact_url=https:\/\/localhost:5001\/customers\/17b37441-2862-416d-81f6-46318071578b/).to_return(body: {data: []}.to_json)
    get customer_time_spans_path(customers(:one))

    assert_response :ok
    assert_requested(request)
  end
end
