require "test_helper"

class Api::V1::WorksControllerTest < ActionDispatch::IntegrationTest
  setup do
    stub_request(:get, "http://localhost:4000/oauth/discovery/keys")
      .to_return(status: 200, body: "{}")

    stub_request(:get, "http://localhost:3000/api/v1/collections?include_children=true")
      .with(headers: {"Authorization" => "Bearer fake.token.really"})
      .to_return(body: {data: [{id: 1, name: "Collectie", parent_collection_id: nil}]}.to_json)
  end

  test "#index redirect when no auth" do
    get api_v1_collection_works_url(1, format: :json)
    assert_response 401
  end

  test "#index not ok when invalid human user" do
    omniauth = OmniAuth.config.add_mock(:central_login, {uid: "12345", sub: "12345", credentials: {expires_at: 1.hour.from_now.to_i}})

    stub_api_auth_response(omniauth) do
      get api_v1_collection_works_url(1, format: :json), headers: {"Authorization" => "Bearer fake.token.really"}
    end
    assert_response 401
  end

  test "#index ok when valid human user" do
    stub_request(:get, "http://localhost:3000/api/v1/collections/1/works?limit=24")
      .to_return(body: {data: [{id: 1, collection_id: 1, title_rendered: "Title", object_categories: [{name: "Grafiek"}], artists: [first_name: "John"]}, {id: 2, collection_id: 1, title_rendered: "Title two"}]}.to_json)

    omniauth = OmniAuth.config.add_mock(:central_login, {roles: ["uitleen:clerk"], uid: "12345", sub: "12345", credentials: {expires_at: 1.hour.from_now.to_i}})

    stub_api_auth_response(omniauth) do
      get api_v1_collection_works_url(1, format: :json), headers: {"Authorization" => "Bearer fake.token.really"}
    end
    assert_response 200
  end

  test "#index ok when valid human user, with insufficient rights" do
    stub_request(:get, "http://localhost:3000/api/v1/collections/1/works?limit=24")
      .to_return(body: {data: [{id: 1, collection_id: 1, title_rendered: "Title", object_categories: [{name: "Grafiek"}], artists: [first_name: "John"]}, {id: 2, collection_id: 1, title_rendered: "Title two"}]}.to_json)

    omniauth = OmniAuth.config.add_mock(:central_login, {roles: ["uitleen:clerka"], uid: "12345", sub: "12345", credentials: {expires_at: 1.hour.from_now.to_i}})

    stub_api_auth_response(omniauth) do
      get api_v1_collection_works_url(1, format: :json), headers: {"Authorization" => "Bearer fake.token.really"}
    end
    assert_response 401
  end

  test "#index ok when valid human user, not found collection" do
    omniauth = OmniAuth.config.add_mock(:central_login, {roles: ["uitleen:clerk"], uid: "12345", sub: "12345", credentials: {expires_at: 1.hour.from_now.to_i}})

    stub_api_auth_response(omniauth) do
      get api_v1_collection_works_url(2, format: :json), headers: {"Authorization" => "Bearer fake.token.really"}
      assert_response 404
    end
  end

  test "#index ok when app user" do
    stub_request(:get, "http://localhost:3000/api/v1/collections/1/works?limit=24")
      .to_return(body: {data: [
        {id: 1, collection_id: 1, title_rendered: "Title", object_categories: [{name: "Grafiek"}], artists: [first_name: "John"]},
        {id: 2, collection_id: 1, title_rendered: "Title two", publish: false}
      ]}.to_json)

    omniauth = OmniAuth.config.add_mock(:central_login, {uid: "app-12345", sub: "app-12345", credentials: {expires_at: 1.hour.from_now.to_i}})

    stub_api_auth_response(omniauth) do
      get api_v1_collection_works_url(1, format: :json), headers: {"Authorization" => "Bearer fake.token.really"}
    end

    parsed_body = JSON.parse(response.body)
    assert_equal("Title two", parsed_body["data"][1]["title_rendered"])
    assert_equal("John", parsed_body["data"][0]["artists"][0]["first_name"])
    assert_equal("Grafiek", parsed_body["data"][0]["object_categories"][0]["name"])
    assert_equal(false, parsed_body["data"][1]["publish"])
  end

  test "#allow passing of options" do
    stubbed_request = stub_request(:get, "http://localhost:3000/api/v1/collections/1/works?limit=1000&id_gt=1000")
      .to_return(body: {meta: {count: 100, total_count: 3000}, data: [{id: 1, collection_id: 1, title_rendered: "Title"}, {id: 2, collection_id: 1, title_rendered: "Title two"}]}.to_json)

    omniauth = OmniAuth.config.add_mock(:central_login, {uid: "app-12345", sub: "app-12345", credentials: {expires_at: 1.hour.from_now.to_i}})

    stub_api_auth_response(omniauth) do
      get api_v1_collection_works_url(1, limit: 1000, id_gt: 1000, format: :json), headers: {"Authorization" => "Bearer fake.token.really"}
    end

    parsed_body = JSON.parse(response.body)
    assert_equal(100, parsed_body["meta"]["count"])
    assert_equal(3000, parsed_body["meta"]["total_count"])

    assert_equal("Title two", parsed_body["data"][1]["title_rendered"])
    assert_requested(stubbed_request)
  end

  test "#show" do
    stub_request(:get, "http://localhost:3000/api/v1/works/1")
      .to_return(body: {data: {id: 1, collection_id: 1, title_rendered: "Title"}}.to_json)
    stub_request(:get, "http://localhost:3000/api/v1/collections?include_children=true")
      .with(
        headers: {
          "Authorization" => "Bearer fake.appid.token"
        }
      )
      .to_return(status: 200, body: {data: [{id: 1, name: "Collection"}]}.to_json, headers: {})

    omniauth = OmniAuth.config.add_mock(:central_login, {uid: "app-12345", sub: "app-12345", credentials: {expires_at: 1.hour.from_now.to_i}})

    stub_api_auth_response(omniauth) do
      get api_v1_collection_work_url(1, 1, format: :json), headers: {"Authorization" => "Bearer fake.token.really"}
    end

    parsed_body = JSON.parse(response.body)

    assert_equal("Title", parsed_body["data"]["title_rendered"])
  end
end
