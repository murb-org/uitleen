require "test_helper"

class Api::V1::CollectionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    stub_request(:get, "http://localhost:4000/oauth/discovery/keys")
      .to_return(status: 200, body: "{}")

    stub_request(:get, "http://localhost:3000/api/v1/collections?include_children=false")
      .with(headers: {"Authorization" => "Bearer fake.token.really"})
      .to_return(body: {data: [{id: 1, name: "Collectie", parent_collection_id: nil}]}.to_json)
  end

  test "#index redirect when no auth" do
    get api_v1_collections_url(format: :json)
    assert_response 401
  end

  test "#index forbidden when invalid user" do
    omniauth = OmniAuth.config.add_mock(:central_login, {uid: "12345", sub: "12345", credentials: {expires_at: 1.hour.from_now.to_i}})

    stub_api_auth_response(omniauth) do
      get api_v1_collections_url(format: :json), headers: {"Authorization" => "Bearer fake.token.really"}
    end
    assert_response 401
  end

  test "#index ok when app user" do
    omniauth = OmniAuth.config.add_mock(:central_login, {uid: "app-12345", sub: "app-12345", credentials: {expires_at: 1.hour.from_now.to_i}})

    stub_api_auth_response(omniauth) do
      get api_v1_collections_url(format: :json), headers: {"Authorization" => "Bearer fake.token.really"}
    end

    parsed_body = JSON.parse(response.body)
    assert_equal("Collectie", parsed_body[0]["name"])
  end
end
