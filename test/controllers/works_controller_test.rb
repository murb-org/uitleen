require "test_helper"

class WorksControllerTest < ActionDispatch::IntegrationTest
  test "should not get index by default" do
    get collection_works_url(collection_id: 1)
    assert_response :redirect
  end

  test "should get not index when signed without any roles" do
    user = users(:one)
    sign_in(user)
    user.reload
    stub_collection_management do
      get collection_works_url(collection_id: 1)
      assert_response :redirect
    end
  end

  test "should get get index when signed in with an admin role" do
    user = users(:admin)
    sign_in(user)
    user.reload
    stub_collection_management works: [CollectionManagement::Work.new(id: 1, collection_id: 1, title_rendered: "Titel van het werk")] do
      get collection_works_url(collection_id: 1)

      assert_match("Titel van het werk", response.body)
    end
  end

  test "should show a work when signed in with an admin role" do
    user = users(:admin)
    sign_in(user)
    user.reload
    stub_collection_management work: CollectionManagement::Work.new(id: 1, collection_id: 1, title_rendered: "Titel van het werk") do
      # get collection_work_url(collection_id: 1, id: 1)
      get "/collections/1/works/1"

      assert_match("Titel van het werk", response.body)
    end
  end

  test "visiting works as anonymous" do
    stub_collection_management do
      get "/customers"
      follow_redirect!
      assert_select "h1", "Connect"
      assert_match("action=\"/auth/central_login\"><button type=\"submit\">CentralLogin</button>", response.body)
    end
  end

  test "visiting works as admin" do
    sign_in(users(:admin))
    stub_collection_management do
      get "/collections/1/works"

      assert_select "h1", "Werken"
      assert_select "h4", "Work title"
    end
  end
end
