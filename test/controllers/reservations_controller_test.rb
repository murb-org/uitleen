require "test_helper"

class ReservationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @reservation = CollectionManagement::TimeSpan.new
    stub_request(:get, "http://localhost:3000/api/v1/works/124").to_return(status: 200, body: {data: {id: 124, collection_id: 1}}.to_json)
    stub_request(:get, "http://localhost:3000/api/v1/works/123").to_return(status: 200, body: {data: {id: 123, collection_id: 1}}.to_json)
  end

  # skip "should get index" do
  #   get reservations_url
  #   assert_redirected_to auth_new_path(redirect_to: reservations_url())
  #
  #   sign_in(:admin)
  #   get reservations_url
  #   assert_response :success
  # end

  test "should get new" do
    get new_collection_work_reservation_url(1, 124)
    assert_redirected_to auth_new_path(redirect_to: new_collection_work_reservation_url(1, 124))

    sign_in(:admin)
    get new_collection_work_reservation_url(1, 124)
    assert_response :success
  end

  test "should create reservation" do
    post_new_event_call = stub_request(:post, "http://localhost:3000/api/v1/collections/1/works/124/work_events").and_return(body: {}.to_json)

    post collection_work_reservations_url(1, 124), params: {reservation: {address: "adres 123", city: "amsterdam", comments: "do not forget about", customer_id: nil, email: "someone@example.com", name: "You know who", postal_code: "1234AS", telephone: "0612345678", work_id: 124}}

    assert_redirected_to auth_new_path(redirect_to: collection_work_reservations_url(1, 124))
    assert_not_requested(post_new_event_call)

    sign_in(:admin)

    post collection_work_reservations_url(1, 124), params: {reservation: {address: "adres 123", city: "amsterdam", comments: "do not forget about", customer_id: nil, email: "someone@example.com", name: "You know who", postal_code: "1234AS", telephone: "0612345678", work_id: 124}}

    assert_requested(post_new_event_call)

    assert_redirected_to collection_work_path(1, 124)
  end

  test "should show reservation" do
    stub_request(:get, "http://localhost:3000/api/v1/time_spans/125")
      .and_return(body: {data: {uuid: "125", subject_id: 123, subject_type: "Work"}}.to_json)

    get reservation_url(125)
    assert_redirected_to auth_new_path(redirect_to: reservation_url(125))

    sign_in(:admin)
    get reservation_url(125)

    assert_redirected_to(collection_work_reservation_url(1, 123, 125))
    follow_redirect!
    assert_response :success
  end
end
