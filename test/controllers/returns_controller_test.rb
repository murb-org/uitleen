require "test_helper"

class TimeSpansControllerTest < ActionDispatch::IntegrationTest
  test "anonymous gets redirected" do
    work = CollectionManagement::Work.new(id: 1, collection_id: 1, title_rendered: "Titel van het werk", current_active_time_span: {uuid: "a"})
    stub_collection_management(work: work) do
      get new_collection_work_return_path(1, 1)
    end
    assert_response :redirect
  end

  test "admin is redirected to work page if no active times pan present" do
    sign_in(:admin)

    work = CollectionManagement::Work.new(id: 1, collection_id: 1, title_rendered: "Titel van het werk")
    stub_collection_management(work: work) do
      get new_collection_work_return_path(1, 1)
    end

    assert_response :redirect
    assert_redirected_to work_path(1)
  end

  test "admin gets to see the new page when time span is set" do
    sign_in(:admin)

    work = CollectionManagement::Work.new(id: 1, collection_id: 1, title_rendered: "Titel van het werk", current_active_time_span: {uuid: "a"})
    stub_collection_management(work: work) do
      get new_collection_work_return_path(1, 1)
    end
    assert_response :success
  end
end
