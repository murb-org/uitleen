require "test_helper"

class ArticlesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @article = articles(:shop_product)
  end

  test "should get index" do
    get articles_url
    assert_response :redirect

    sign_in(users(:admin))

    get articles_url
    assert_response :success

    sign_in(users(:one))
    get articles_url
    assert_response :redirect
  end

  test "should get new" do
    get new_article_url
    assert_response :redirect

    sign_in(users(:admin))
    get new_article_url
    assert_response :success

    sign_in(users(:one))
    get new_article_url
    assert_response :redirect
  end

  test "should create article" do
    sign_in(users(:admin))

    assert_difference("Article.count") do
      post articles_url, params: {article: {external_article_code: @article.external_article_code, name: @article.name, vat_rate: @article.vat_rate}}
    end

    assert_redirected_to article_url(Article.last)
  end

  test "should show article" do
    sign_in(users(:admin))

    get article_url(@article)
    assert_response :success
  end

  test "should get edit" do
    sign_in(users(:admin))

    get edit_article_url(@article)
    assert_response :success
  end

  test "should update article" do
    sign_in(users(:admin))

    patch article_url(@article), params: {article: {external_article_code: @article.external_article_code, name: @article.name, vat_rate: @article.vat_rate}}
    assert_redirected_to article_url(@article)
  end

  test "should destroy article" do
    sign_in(users(:admin))

    assert_difference("Article.count", -1) do
      delete article_url(@article)
    end

    assert_redirected_to articles_url
  end
end
