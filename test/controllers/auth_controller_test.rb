require "test_helper"

class AuthControllerTest < ActionDispatch::IntegrationTest
  test "sign in redirects to oauth" do
    get auth_new_url
    assert_match(/CentralLogin/, response.body)
    post "/auth/central_login"
    assert_match("auth/central_login/callback", response.body)
  end

  test "create oauth user" do
    OmniAuth.config.add_mock(:central_login, {uid: "12345", credentials: {token: "token", expires_at: 1.hour.from_now.to_i}})
    get "/auth/central_login/callback?response_type=code"
    stub_twinfield do
      follow_redirect!
    end
    assert("Koppeling met central_login gemaakt", response.body)
  end

  test "redirect to" do
    get collection_work_path(13, 91981)
    follow_redirect!
    assert_match(/CentralLogin/, response.body)
    post "/auth/central_login"
    assert_match("auth/central_login/callback", response.body)
    OmniAuth.config.add_mock(:central_login, {uid: "12345", credentials: {token: "token", expires_at: 1.hour.from_now.to_i}})
    get "/auth/central_login/callback?response_type=code"
    CollectionManagement::Collection.stub :find, CollectionManagement::Collection.new(id: 13, name: "Abc") do
      follow_redirect!
      assert("Koppeling met central_login gemaakt", response.body)
      assert_equal(collection_work_url(13, 91981), response.request.url)
    end
  end
end
