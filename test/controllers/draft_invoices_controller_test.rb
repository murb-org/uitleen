require "test_helper"

class DraftInvoicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @draft_invoice = invoices(:one)
    Twinfield.configuration = Twinfield::Configuration.new
    Twinfield.configuration.company = 123
  end

  test "should get index" do
    get draft_invoices_url
    assert_response :redirect
  end

  test "should nog create Invoice when not signed in" do
    assert_difference("Invoice.count", 0) do
      post draft_invoices_url, params: {invoice: {created_by_user_id: @draft_invoice.created_by_user_id, footertext: @draft_invoice.footertext, customer_id: customers(:one).id}}
    end

    assert_redirected_to auth_new_url(redirect_to: draft_invoices_url)
  end

  test "should  create Invoice when signed in" do
    sign_in(users(:admin))

    assert_difference("Invoice.count", 1) do
      post draft_invoices_url, params: {invoice: {created_by_user_id: @draft_invoice.created_by_user_id, footertext: @draft_invoice.footertext, customer_id: customers(:one).id}}
    end

    assert_redirected_to draft_invoice_path(Invoice.last)
  end

  test "should show draft_invoice" do
    get draft_invoice_url(@draft_invoice)
    assert_response :redirect
  end

  test "should get edit" do
    get edit_draft_invoice_url(@draft_invoice)
    assert_response :redirect
  end

  test "should update draft_invoice" do
    patch draft_invoice_url(@draft_invoice), params: {invoice: {created_by_user_id: @draft_invoice.created_by_user_id, footertext: @draft_invoice.footertext, customer_id: customers(:one).id}}
    assert_redirected_to auth_new_url(redirect_to: draft_invoice_url(@draft_invoice))
  end

  test "should not destroy draft_invoice when anonymous" do
    assert_difference("Invoice.count", 0) do
      delete draft_invoice_url(@draft_invoice)
    end
  end

  test "should  destroy draft_invoice when admin" do
    sign_in(users(:admin))
    assert_difference("Invoice.count", -1) do
      delete draft_invoice_url(@draft_invoice)
    end
  end

  test "should not destroy draft_invoice invoice is finalized, even when admin" do
    sign_in(users(:admin))
    @draft_invoice.update_columns(finalized_at: Time.now)
    assert_no_difference("Invoice.count") do
      delete draft_invoice_url(@draft_invoice)
    end
  end

  test "visiting the index redirects to connect" do
    sign_in(users(:admin))
    stub_twinfield do
      follow_redirect!

      get draft_invoice_path(@draft_invoice)
    end
    assert_select "h1", "Opdracht"
  end

  test "GET /draft_invoices/:id/edit ananomously" do
    get edit_draft_invoice_url(@draft_invoice)
    assert_redirected_to auth_new_url(redirect_to: edit_draft_invoice_url(@draft_invoice))
  end

  test "GET /draft_invoices/:id/edit " do
    sign_in(users(:admin))

    get edit_draft_invoice_url(@draft_invoice)
    assert_select "h1", "Bewerk concept factuur"
  end

  test "GET /draft_invoices/:id/new without auth" do
    get new_draft_invoice_url
    assert_redirected_to auth_new_url(redirect_to: new_draft_invoice_url)
  end

  test "GET /draft_invoices/:id/new with auth" do
    sign_in(users(:clerk))

    get new_draft_invoice_url
    assert_redirected_to draft_invoice_url(Invoice.last)
  end

  test "GET /draft_invoices/:id/new?invoiceable_item_collection=set with auth" do
    sign_in(users(:clerk))
    customer = customers(:one)
    works = [CollectionManagement::Work.new(id: 1, selling_price: 100.0, current_active_time_span: {contact: {url: customer.uri}, contact_url: customer.uri, status: :active, classification: :rental_outgoing})]
    CollectionManagement::WorkSet.stub(:find, CollectionManagement::WorkSet.new(uuid: "some-uuid-or-not", works: works)) do
      get new_draft_invoice_url(invoiceable_item_collection_type: "CollectionManagement::WorkSet", invoiceable_item_collection_external_id: "some-uuid-or-not")
    end
    new_invoice = Invoice.last
    assert_redirected_to draft_invoice_url(new_invoice)
    assert_equal("CollectionManagement::WorkSet", new_invoice.invoiceable_item_collection_type)
    assert_equal("some-uuid-or-not", new_invoice.invoiceable_item_collection_external_id)
    assert_equal(works.first.default_rent_price.to_f, new_invoice.total_price)
  end

  test "PATCH /draft_invoices/:id/ " do
    stub_twinfield do
      sign_in(users(:admin))

      patch draft_invoice_url(@draft_invoice), params: {invoice: {footertext: "Nieuwe footertext"}}
      follow_redirect!
      assert_select "h1", "Opdracht"
      assert_match("Nieuwe footertext", response.body)
    end
  end

  test "PATCH /draft_invoices/:id/ with new intent" do
    stub_twinfield do
      sign_in(users(:admin))

      stub_request(:post, "http://localhost:3000/api/v1/collections/1/works/1/work_events").to_return(body: {}.to_json)

      Invoice.stub :find_by_id_param, @draft_invoice do
        stub_twinfield_invoice do
          @draft_invoice.stub :external_invoice, Twinfield::SalesInvoice.find do
            stub_collection_management do
              patch draft_invoice_url(@draft_invoice), params: {invoice: {new_intent_attributes: {intent: "rent", work_id: 1}}}
              follow_redirect!
            end
          end
        end
      end
      assert_select "h1", "Opdracht"
      assert_match("De voorlopige factuur is bijgewerkt.", response.body)
      assert_match("Gelieve het bovenstaande bedrag van €14,00 te voldoen voor", response.body)
    end
  end

  test "POST /draft_invoices/:id/finalize_invoice" do
    stub_twinfield do
      sign_in(users(:admin))

      stub_request(:post, "http://localhost:3000/api/v1/collections/13/works/12/work_events").to_return(body: {}.to_json)

      almost_done_invoice = invoices(:two)
      stubbed_twinfield_invoice = Twinfield::SalesInvoice.new(customer_code: 123, invoicetype: "FACTUUR")
      stubbed_twinfield_invoice.invoicenumber = 123123

      Twinfield::SalesInvoice.stub :find, stubbed_twinfield_invoice do
        stubbed_twinfield_invoice.stub :save, stubbed_twinfield_invoice do
          almost_done_invoice.stub :external_invoice, stubbed_twinfield_invoice do
            Invoice.stub :find, almost_done_invoice do
              Invoice.stub :find_by, almost_done_invoice do
                patch draft_invoice_finalize_invoice_path(almost_done_invoice) # , params: {invoice: {footertext: "Nieuwe footertext"}}
                follow_redirect!
                assert_select "h1", "Factuur 123123"
              end
            end
          end
        end
      end
    end
  end

  test "POST /draft_invoices/:id/finalize_invoice with work set" do
    stub_twinfield do
      sign_in(users(:admin))

      almost_done_invoice = invoices(:work_set_based_invoice_with_invoice_items)

      stub_request(:post, "http://localhost:3000/api/v1/collections/13/works/12/work_events")
        .with(
          body: "{\"work_event\":{\"contact_uri\":\"https://localhost:5001/customers/17b37441-2862-416d-81f6-46318071578b\",\"event_type\":\"rental_outgoing\",\"status\":\"active\",\"time_span_uuid\":\"#{almost_done_invoice.invoice_items.first.time_span_uuid}\",\"comments\":null,\"contact\":null}}"
        )
        .to_return(status: 200, body: "{}", headers: {})
      stub_request(:get, "http://localhost:3000/api/v1/time_spans/#{almost_done_invoice.invoice_items.first.time_span_uuid}").to_return(body: {data: {
        status: :reservation
      }}.to_json)

      stubbed_twinfield_invoice = Twinfield::SalesInvoice.new(customer_code: 123, invoicetype: "FACTUUR")
      stubbed_twinfield_invoice.invoicenumber = 123123

      Twinfield::SalesInvoice.stub :find, stubbed_twinfield_invoice do
        stubbed_twinfield_invoice.stub :save, stubbed_twinfield_invoice do
          almost_done_invoice.stub :external_invoice, stubbed_twinfield_invoice do
            Invoice.stub :find, almost_done_invoice do
              Invoice.stub :find_by, almost_done_invoice do
                patch draft_invoice_finalize_invoice_path(almost_done_invoice) # , params: {invoice: {footertext: "Nieuwe footertext"}}
                follow_redirect!
                assert_select "h1", "Factuur 123123"
              end
            end
          end
        end
      end
    end
  end

  test "POST /draft_invoices/reset anonymously" do
    post reset_draft_invoices_url

    assert_redirected_to auth_new_url(redirect_to: reset_draft_invoices_url)
  end

  test "POST /draft_invoices/reset" do
    stub_twinfield do
      sign_in(users(:admin))

      post reset_draft_invoices_url

      assert_response :redirect
      follow_redirect!

      assert_match("Een leeg voorstel is klaargezet", response.body)
    end
  end

  test "POST /draft_invoices/reset with return_to" do
    sign_in(users(:admin))

    stub_request(:get, "http://localhost:3000/api/v1/collections?include_children=true").and_return(body: {data: [{id: 12, name: "1"}]}.to_json)
    stub_request(:get, "http://localhost:3000/api/v1/collections/12/works?limit=24&q").and_return(body: {data: []}.to_json)

    post reset_draft_invoices_url, params: {return_to: collection_works_url(12)}
    follow_redirect!

    assert_match("Klantsessie is gereset", response.body)
    assert_match("<h1>Werken</h1>", response.body)
  end

  test "GET #mutation_form" do
    sign_in(users(:admin))
    get draft_invoice_mutation_form_path(@draft_invoice)

    assert_response :success
  end

  test "PATCH #reviewed should mark invoice as reviewed" do
    sign_in(users(:admin))

    old_invoice = invoices(:two)
    old_invoice.update(next_invoice_at: nil, customer: nil)

    stub_twinfield do
      stub_create_twinfield_invoice

      patch draft_invoice_reviewed_url(old_invoice)
      follow_redirect!
      assert_match("De factuur is gemarkeerd als gecontroleerd", response.body)
    end

    old_invoice = invoices(:two)
    old_invoice.update(next_invoice_at: nil, customer: nil, requires_review: true)
    assert(old_invoice.requires_review?)

    stub_twinfield do
      stub_create_twinfield_invoice

      patch draft_invoice_reviewed_url(old_invoice)
      follow_redirect!
      assert_match("De factuur is gemarkeerd als gecontroleerd", response.body)
    end

    old_invoice = old_invoice.reload
    assert(!old_invoice.requires_review?)
  end
end
