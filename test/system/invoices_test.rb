require "application_system_test_case"

class InvoicesSystemTest < ApplicationSystemTestCase
  setup do
    @draft_invoice = invoices(:one)

    OmniAuth.config.test_mode = true
    OmniAuth.config.add_mock(:central_login, {
      uid: "user_one_sub",
      credentials: {token: "token", expires_at: 1.hour.from_now.to_i},
      extra: {raw_info: {roles: ["uitleen:clerk"]}}
    })
  end

  test "visiting the index redirects to connect" do
    sign_in
    click_on "Actieve klantsessie"
    assert_text "Concept-factuur"
  end

  test "creating a Draft invoice" do
    sign_in
    visit draft_invoices_url
    click_on "New Draft Invoice"

    fill_in "Created by user", with: @draft_invoice.created_by_user_id
    fill_in "Footertext", with: @draft_invoice.footertext
    fill_in "Twinfield customer code", with: @draft_invoice.twinfield_customer_code
    click_on "Create Draft invoice"

    assert_text "Draft invoice was successfully created"
    click_on "Back"
  end

  test "updating a Draft invoice" do
    visit draft_invoices_url
    click_on "Edit", match: :first

    fill_in "Created by user", with: @draft_invoice.created_by_user_id
    fill_in "Footertext", with: @draft_invoice.footertext
    fill_in "Twinfield customer code", with: @draft_invoice.twinfield_customer_code
    click_on "Update Draft invoice"

    assert_text "Draft invoice was successfully updated"
    click_on "Back"
  end

  test "destroying a Draft invoice" do
    visit draft_invoices_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Draft invoice was successfully destroyed"
  end

  def sign_in
    visit root_url
    assert_selector "h1", text: "Connect"
    click_on "CentralLogin"
    assert_selector "h1", text: "Uitleen"
  end
end
