require "test_helper"
require_relative "application_integration_test"

class DraftInvoicesIntegrationTest < ApplicationIntegrationTest
  test "invoice with customer is shown" do
    sign_in(users(:admin))

    invoice = invoices(:two)

    stub_create_twinfield_invoice(invoice: invoice)

    stub_twinfield do
      get "/draft_invoices/#{invoice.id}"
      assert_match("Winkel product Iets €20,00", response.body)
      assert_match("€16,53", response.body)
      assert_match("469001:  - Work for purchase", response.body)
      assert_match("€826,45", response.body)
      assert_match("<tr><th>Totaal</th><td><td class=\"number\"><strong>€1.020,00</strong></td></tr>", response.body)
    end
  end

  test "invoice with customer is shown, including using gift card credit" do
    sign_in(users(:admin))

    invoice = invoices(:two)
    invoice.invoice_items.works.first.update(intent: "rent", invoicing_period_starts_at: "2021-02-01", invoicing_period_ends_at: "2021-02-28")
    create_customer_savings invoice.customer, 200

    stub_create_twinfield_invoice(invoice: invoice)

    stub_twinfield do
      get "/draft_invoices/#{invoice.id}"
      assert_match("Winkel product Iets €20,00", response.body)
      assert_match("€16,53", response.body)
      assert_match("469001:  - Work for purchase", response.body) # updaed
      assert_match("€14,00", response.body)
      assert_match("-€14,00", response.body)
      assert_match("<tr><th>Totaal</th><td><td class=\"number\"><strong>€20,00</strong></td></tr>", response.body)
    end
  end

  test "updated invoiceing interval" do
    sign_in(users(:admin))

    invoice = invoices(:two)
    invoice.invoice_items.works.first.update(intent: "rent", invoicing_period_starts_at: "2021-02-01", invoicing_period_ends_at: "2021-02-28")
    create_customer_savings invoice.customer, 200

    stub_twinfield do
      patch "/draft_invoices/#{invoice.id}", params: {invoice: {recurrence_interval_in_months: 6}}
    end

    assert_equal(6, invoice.invoice_items.works.first.quantity)

    stub_create_twinfield_invoice(invoice: invoice)

    stub_twinfield do
      get "/draft_invoices/#{invoice.id}"

      assert_match("Winkel product Iets €20,00", response.body)
      assert_match("€16,53", response.body)
      assert_match("469001:  - Work for purchase", response.body) # updaed
      assert_match("€84,00", response.body)
      assert_match("-€84,00", response.body)
      assert_match("<tr><th>Totaal</th><td><td class=\"number\"><strong>€20,00</strong></td></tr>", response.body)
    end
  end

  test "updated invoiceing interval, with limited customer savings" do
    sign_in(users(:admin))

    invoice = invoices(:two)
    invoice.invoice_items.works.first.update(intent: "rent", invoicing_period_starts_at: "2021-02-01", invoicing_period_ends_at: "2021-02-28")
    create_customer_savings invoice.customer, 50

    stub_twinfield do
      patch "/draft_invoices/#{invoice.id}", params: {invoice: {recurrence_interval_in_months: 6}}
    end

    assert_equal(6, invoice.invoice_items.works.first.quantity)

    stub_create_twinfield_invoice(invoice: invoice)

    stub_twinfield do
      get "/draft_invoices/#{invoice.id}"

      assert_match("Winkel product Iets €20,00", response.body)
      assert_match("€16,53", response.body)
      assert_match("469001:  - Work for purchase", response.body) # updaed
      assert_match("€84,00", response.body)
      assert_match("-€50,00", response.body)
      assert_match("<tr><th>Totaal</th><td><td class=\"number\"><strong>€54,00</strong></td></tr>", response.body)
    end
  end
end
