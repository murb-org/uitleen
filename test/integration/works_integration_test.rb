require "test_helper"
require_relative "application_integration_test"

class WorkIntegrationTest < ActionDispatch::IntegrationTest
  test "visiting works as anonymous" do
    stub_collection_management do
      get "/collections/1/works"
      follow_redirect!
      assert_select "h1", "Connect"
      assert_match("action=\"/auth/central_login\"><button type=\"submit\">CentralLogin</button>", response.body)
    end
  end

  test "visiting works as admin" do
    sign_in(users(:admin))
    stub_collection_management work: CollectionManagement::Work.new(id: 1, collection_id: 1, title_rendered: "Work title", selling_price: 1000, available: true) do
      get "/collections/1/works"

      assert_select "h1", "Werken"
      assert_select "h4", "Work title"

      get "/collections/1/works/1"

      assert_select "h1", "Work title"
    end
  end

  test "time out after two hours of inactivity" do
    sign_in(users(:admin))
    stub_collection_management work: CollectionManagement::Work.new(id: 1, collection_id: 1, title_rendered: "Work title", selling_price: 1000, available: true) do
      get "/collections/1/works"

      assert_select "h1", "Werken"
    end

    travel_to(1.hour.from_now) do
      stub_collection_management work: CollectionManagement::Work.new(id: 1, collection_id: 1, title_rendered: "Work title", selling_price: 1000, available: true) do
        User.update_all(access_token: "new_access_token", refresh_token: "new_refresh_token", expires_at: 2.hours.from_now, id_token: "new_id_token")

        get "/collections/1/works"

        assert_select "h1", "Werken"
      end
    end

    travel_to(3.hours.from_now + 1.minute) do
      get "/collections/1/works"
      follow_redirect!
      assert_select "h1", "Connect"
    end
  end
end
