require "test_helper"
require_relative "application_integration_test"

class InvoicesIntegrationTest < ApplicationIntegrationTest
  test "invoice with customer is shown" do
    sign_in(users(:admin))

    invoice = invoices(:finalized)

    stub_create_twinfield_invoice(invoice: invoice)
    invoice.save_external_invoice!(true)
    # invoice.extern

    invoice.update(external_invoice_data: invoice.external_invoice.to_h)

    stub_twinfield(transactions: [Twinfield::Browse::Transaction::Customer.new(code: "VRK", customer_code: invoice.customer_code, value: 200, open_value: 200, invoice_number: invoice.twinfield_invoicenumber)]) do
      CustomerTransaction.update
      Twinfield::Browse::Transaction::Customer.new(code: 123, value: 200)
      get "/invoices/#{invoice.id}"
    end

    assert_match('<form class="button_to" method="post" action="/invoices/' + invoice.id.to_s + '/register_payment"><button type="submit">Betaald met KAS</button><input type="hidden" name="payment_type" value="cash" autocomplete="off" /></form>', response.body)
  end
end
