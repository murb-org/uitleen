class ApplicationIntegrationTest < ActionDispatch::IntegrationTest
  # self.abstract_class = true

  setup do
    OmniAuth.config.test_mode = true
    OmniAuth.config.add_mock(:central_login, {
      uid: "user_one_sub",
      credentials: {token: "token", expires_at: 1.hour.from_now.to_i},
      extra: {raw_info: {roles: ["uitleen:clerk"]}}
    })
    OmniAuth.config.add_mock(:twinfield, {
      uid: "twinfield_sub",
      credentials: {token: "token", refresh_token: "abc", expires_at: 1.hour.from_now.to_i}
    })
  end
end
