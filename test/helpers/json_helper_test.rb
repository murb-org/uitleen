require "test_helper"

class JsonHelperTest < ActionView::TestCase
  test "should work" do
    assert_equal "<dl><dt>A</dt><dd>2</dd></dl>", render_hash({a: 2})
    assert_equal "<dl><dt>A</dt><dd><ul><li>1</li><li>2</li><li>3</li></ul></dd></dl>", render_hash({a: [1, 2, 3]})
    assert_equal "<dl><dt>A</dt><dd>2021-01-01</dd></dl>", render_hash({a: Date.new(2021, 1, 1)})
  end
end
