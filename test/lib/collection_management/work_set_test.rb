require "test_helper"

class CollectionManagement::WorkSetTest < ActiveSupport::TestCase
  test "initialize" do
    assert_equal(CollectionManagement::WorkSet, CollectionManagement::WorkSet.new.class)
  end

  test "#works" do
    expected_request = stub_request(:get, "http://localhost:3000/api/v1/work_sets/abc-abc-123.json")
      .to_return(body: {data: {uuid: "abc", works: [{id: 1, stock_number: "ABC00123"}, {id: 2, stock_number: "ABC00124"}]}}.to_json)
    work_set = CollectionManagement::WorkSet.find("abc-abc-123")
    works = work_set.works
    assert_requested expected_request
    assert_equal(2, works.count)
    assert_equal("ABC00123", works.first.stock_number)
  end
end
