require "test_helper"

class CollectionManagement::WorkTest < ActiveSupport::TestCase
  test "initialize" do
    assert_equal(CollectionManagement::Work, CollectionManagement::Work.new.class)
  end

  def stub_create_work_event event
    stub_request(:post, "http://localhost:3000/api/v1/collections/#{event.work.collection_id}/works/#{event.work.id}/work_events").with(
      body: event.to_json
    ).to_return(body: {}.to_json)
  end

  test ".where returns array of CollectionManagement::Works" do
    stub_request(:get, "http://localhost:3000/api/v1/collections/1/works")
      .to_return(body: {data: [{id: 1, stock_number: "ABC00123"}, {id: 2, stock_number: "ABC00124"}], meta: {count: 2}}.to_json)

    result = CollectionManagement::Work.where(collection_id: 1)
    assert_equal(2, result.count)
    assert_equal(CollectionManagement::Work, result.first.class)
  end

  test ".where raises on 404" do
    stub_request(:get, "http://localhost:3000/api/v1/collections/1/works")
      .to_return(status: 404)

    assert_raises(ActiveRecord::RecordNotFound) { CollectionManagement::Work.where(collection_id: 1) }
  end

  test ".where raises on 400" do
    stub_request(:get, "http://localhost:3000/api/v1/collections/1/works")
      .to_return(status: 400, body: {error: "Bad request"}.to_json)

    assert_raises(ActionController::BadRequest) { CollectionManagement::Work.where(collection_id: 1) }
  end

  test ".where tries to recover from a 401" do
    stub_request(:get, "http://localhost:3000/api/v1/collections/1/works")
      .to_return({status: 401}, {body: {data: [{id: 1, stock_number: "ABC00123"}, {id: 2, stock_number: "ABC00124"}], meta: {count: 2}}.to_json})

    result = CollectionManagement::Work.where(collection_id: 1)
    assert_equal(2, result.count)
    assert_equal(CollectionManagement::Work, result.first.class)
  end
  test ".where tries to recover from a 401, but will raise if is unable to do so" do
    stub_request(:get, "http://localhost:3000/api/v1/collections/1/works")
      .to_return({status: 401}, {status: 401})

    assert_raises(ActiveRecord::RecordNotFound) { CollectionManagement::Work.where(collection_id: 1) }
  end

  test "#availability_status_extended" do
    w = CollectionManagement::Work.new(availability_status: "available")
    assert_equal(:available_not_for_rent_or_purchase, w.availability_status_extended)

    w = CollectionManagement::Work.new(availability_status: "available", for_rent: true)
    assert_equal(:available_for_rent, w.availability_status_extended)

    w = CollectionManagement::Work.new(availability_status: "available", for_purchase: true)
    assert_equal(:available_for_purchase, w.availability_status_extended)

    w = CollectionManagement::Work.new(availability_status: "available", for_purchase: true, for_rent: true)
    assert_equal("available", w.availability_status_extended)

    w = CollectionManagement::Work.new(availability_status: "lent", for_purchase: true)
    assert_equal("lent", w.availability_status_extended)
  end

  test "#mark_event returns a timespan" do
    initial_hash = {id: 12, collection_id: 1, themes: [{name: "Natuur", id: 1}], artist_name_rendered: "Voornaam met Achternaam"}

    work = CollectionManagement::Work.new(initial_hash)

    stub_create_work_event CollectionManagement::WorkEvent.new(work: work, event_type: :purchase, contact_uri: "https://customer.uri", status: :active)

    time_span = work.mark_event!(contact_uri: "https://customer.uri", event_type: :purchase, status: :active, current_user: users(:admin))

    assert_equal(CollectionManagement::TimeSpan, time_span.class)
  end

  test "#to_h_without_empty_values filters public keys" do
    w = CollectionManagement::Work.new(title_unknown: false)
    assert(!w.to_h_without_empty_values.has_key?(:title))
    assert(!w.to_h_without_empty_values.has_key?(:themes))
    assert(w.to_h_without_empty_values.has_key?(:title_unknown))
  end

  test "#public_to_h filters private keys" do
    w = CollectionManagement::Work.new(selling_price: 1000, purchase_price: 100)
    assert(!w.public_to_h.has_key?(:purchase_price))
    assert(w.public_to_h.has_key?(:selling_price))
    assert_nil(w.public_to_h[:selling_price])

    w.publish_selling_price = true
    assert_equal(1000, w.public_to_h[:selling_price])
  end

  test "#to_h" do
    w = CollectionManagement::Work.new
    assert_nil(w.to_h[:id])
    assert_equal([], w.to_h[:themes])
  end

  test "#to_h initialize loop" do
    initial_hash = {id: 12, themes: [{name: "Natuur", id: 1}], artist_name_rendered: "Voornaam met Achternaam", artists: [{first_name: "Voornaam"}], work_sets: [{work_set_type: "A", identification_number: 1}], owner: {creating_artist: true}}
    w = CollectionManagement::Work.new(initial_hash)
    w_hash_based_clone = CollectionManagement::Work.new(w.to_h)
    assert("Natuur", w_hash_based_clone.themes.first.name)
    assert("Voornaam", w_hash_based_clone.artists.first.first_name)
  end

  test "active record like stub methods" do
    w = CollectionManagement::Work.new(selling_price: 1000)
    assert_equal(1000, w._read_attribute(:selling_price))
    assert(!w.marked_for_destruction?)
    assert(!w.destroyed?)
    assert(!w.new_record?)
  end
end
