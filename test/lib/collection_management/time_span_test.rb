require "test_helper"

class CollectionManagement::TimeSpanTest < ActiveSupport::TestCase
  test "initialize" do
    assert_equal(CollectionManagement::TimeSpan, CollectionManagement::TimeSpan.new.class)
  end

  test "invoicing_period_starts_at" do
    time_span = CollectionManagement::TimeSpan.new
    assert_nil(time_span.invoicing_period_starts_at)
    time_span.starts_at = DateTime.parse("2020-01-01")
    assert_equal(DateTime.parse("2020-01-01"), time_span.invoicing_period_starts_at)
    time_span.starts_at = DateTime.parse("2020-01-02")
    assert_equal(DateTime.parse("2020-02-01"), time_span.invoicing_period_starts_at)
  end

  test "validations" do
    time_span = CollectionManagement::TimeSpan.new
    assert(!time_span.valid?)

    time_span = CollectionManagement::TimeSpan.new(subject: CollectionManagement::Work.new, status: "active", classification: "transport")
    assert(time_span.valid?)
  end
end
