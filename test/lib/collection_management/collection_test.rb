require "test_helper"

class CollectionManagement::CollectionTest < ActiveSupport::TestCase
  test "initialize" do
    assert_equal(CollectionManagement::Collection, CollectionManagement::Collection.new.class)
  end

  test ".all returns array of CollectionManagement::Collection" do
    stub_request(:get, "http://localhost:3000/api/v1/collections?include_children=false")
      .to_return(body: {data: [{id: 1, name: "ABC00123"}, {id: 2, name: "ABC00124"}], meta: {count: 2}}.to_json)

    result = CollectionManagement::Collection.all
    assert_equal(2, result.count)
    assert_equal(CollectionManagement::Collection, result.first.class)
  end

  test ".all raises on 404" do
    stub_request(:get, "http://localhost:3000/api/v1/collections?include_children=false")
      .to_return(status: 404)

    assert_raises(ActiveRecord::RecordNotFound) { CollectionManagement::Collection.all }
  end

  test ".all raises on 400" do
    stub_request(:get, "http://localhost:3000/api/v1/collections?include_children=false")
      .to_return(status: 400, body: {error: "Bad request"}.to_json)

    assert_raises(ActionController::BadRequest) { CollectionManagement::Collection.all }
  end

  test ".all tries to recover from a 401" do
    stub_request(:get, "http://localhost:3000/api/v1/collections?include_children=false")
      .to_return({status: 401}, {body: {data: [{id: 1, name: "ABC00123"}, {id: 2, name: "ABC00124"}], meta: {count: 2}}.to_json})

    result = []
    user = users(:admin)
    user.stub(:refresh!, user) do
      result = CollectionManagement::Collection.all(current_user: user)
    end
    assert_equal(2, result.count)
    assert_equal(CollectionManagement::Collection, result.first.class)
  end
  test ".all tries to recover from a 401, but will raise if is unable to do so" do
    stub_request(:get, "http://localhost:3000/api/v1/collections?include_children=false")
      .to_return({status: 401}, {status: 401})

    assert_raises(ActiveRecord::RecordNotFound) { CollectionManagement::Collection.all }
  end
end
