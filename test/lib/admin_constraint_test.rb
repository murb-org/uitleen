require "test_helper"

class AdminConstraintTest < ActiveSupport::TestCase
  test "#matches?" do
    request = ActionDispatch::Request.new({})
    request.stub(:session, {user_sub: "123"}) do
      assert(!AdminConstraint.new.matches?(request))
    end

    request = ActionDispatch::Request.new({})
    request.stub(:session, {user_sub: "admin_sub"}) do
      assert(AdminConstraint.new.matches?(request))
    end
  end
end
