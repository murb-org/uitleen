# == Schema Information
#
# Table name: cost_centers
#
#  id         :integer          not null, primary key
#  code       :string
#  currency   :string           default("EUR")
#  dim1       :string
#  dim2       :string
#  key        :string
#  number     :string
#  source     :string
#  status     :string
#  value      :decimal(16, 2)
#  yearperiod :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_cost_centers_on_source_and_key  (source,key) UNIQUE
#
require "test_helper"

class CostCenterTest < ActiveSupport::TestCase
  test "scope .for_customer" do
    assert_equal 3, CostCenter.for_customer(customers(:two)).count
    assert_equal 0, CostCenter.for_customer(customers(:one)).count
  end

  test "scope .final" do
    assert_equal 4, CostCenter.final.count
  end

  test "scope .customer_gift_card_credit" do
    assert_equal 1, CostCenter.customer_gift_card_credit.count
  end

  test "make sure scope .join_customer_transactions doesn't return duplicate cost centers due to join" do
    first_cost_center = cost_centers(:one)
    ctrans = first_cost_center.customer_transaction
    CustomerTransaction.create ctrans.attributes.merge("id" => nil, "key" => ctrans.key + "clone")

    assert_equal 4, CostCenter.count
    assert_equal 4, CostCenter.join_customer_transactions.count
  end

  test "customer" do
    assert_equal customers(:two).id, cost_centers(:one).customer.id
    assert_equal customers(:four).id, cost_centers(:four).customer.id
  end
end
