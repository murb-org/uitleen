# == Schema Information
#
# Table name: customer_transactions
#
#  id             :integer          not null, primary key
#  code           :string
#  currency       :string
#  customer_code  :string
#  date           :date
#  invoice_number :string
#  key            :string
#  number         :string
#  open_value     :decimal(, )
#  source         :string
#  status         :string
#  value          :decimal(, )
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_customer_transactions_on_source_and_key  (source,key) UNIQUE
#
require "test_helper"

class CustomerTransactionTest < ActiveSupport::TestCase
  test ".update updates" do
    stub_request(:post, "https://accounting.twinfield.com/webservices/processxml.asmx").with(body: /yearperiod/).and_return(status: 200, body: File.read("test/fixtures/files/customer_transaction_update.xml"))

    Finance.stub(:configure_twinfield, true) do
      assert_difference "CustomerTransaction.count", 16 do
        CustomerTransaction.update
      end
    end
  end
end
