# == Schema Information
#
# Table name: users
#
#  id            :integer          not null, primary key
#  access_token  :string
#  email         :string
#  expires_at    :integer
#  groups        :text
#  id_token      :text
#  name          :string
#  refresh_token :string
#  resources     :text
#  roles         :text
#  sub           :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
require "test_helper"

class UserTest < ActiveSupport::TestCase
  test "groups, roles and resources are arrays" do
    u = User.new
    assert_equal(Array, u.roles.class)
    assert_equal(Array, u.resources.class)
    assert_equal(Array, u.groups.class)
  end

  test "#admin? returns false when no admin" do
    u = User.new
    assert_not(u.admin?)
  end

  test "#admin? returns true when 'uitleen:admin' role" do
    u = User.new(roles: ["uitleen:admin"])
    assert(u.admin?)

    # fixture test
    u = users(:admin)
    assert(u.admin?)
  end

  test "#refresh!" do
    u = users(:admin)
    u.refresh!

    oauth_client = Minitest::Mock.new
    oauth_token = Minitest::Mock.new
    oauth_token.expect :==, true, [String]
    oauth_token.expect :==, true, [String]
    oauth_token.expect :params, {"id_token" => "fake.userid.token", "expires_in" => 7200}
    oauth_token.expect :token, "fake.userid.token"
    oauth_token.expect :refresh_token, "new_refresh_token"
    oauth_token.expect :expires_at, (Time.now + 1.hour).to_i
    oauth_token.expect :id_token, "fake.userid.token"

    oauth_strategy = Minitest::Mock.new
    oauth_strategy.expect :validate_id_token, {"sub" => "admin_sub"}, [oauth_token]

    oauth_client.expect :get_token, oauth_token, [{grant_type: "refresh_token", refresh_token: "new_refresh_token", response_type: "id_token"}]

    u.stub :client, oauth_client do
      u.stub :oauth_strategy, oauth_strategy do
        u.refresh!(force: true)
      end
    end
  end
end
