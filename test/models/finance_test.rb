require "test_helper"

class FinanceTest < ActiveSupport::TestCase
  test ".configure_twinfield" do
    client = OAuthClient.where(name: "twinfield").last

    OAuthClient.stub(:where, [client]) do
      client.stub(:refresh!, nil) do
        Twinfield.configuration.stub(:access_token_expired?, true) do
          Finance.configure_twinfield
        end
      end
    end
  end
end
