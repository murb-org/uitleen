require "test_helper"

class InvoiceItem::MinimalTest < ActiveSupport::TestCase
  test "#description" do
    invoice_item = InvoiceItem::Minimal.new("rent", "CollectionManagement::Work", 1, Date.new(2021, 1, 1), Date.new(2021, 2, 1), "some note", "")
    assert_equal("Huur werken", invoice_item.description)

    invoice_item = InvoiceItem::Minimal.new("purchase", "CollectionManagement::Work", 1, Date.new(2021, 1, 1), Date.new(2021, 2, 1), "some note", "")
    assert_equal("Aankoop werken", invoice_item.description)

    invoice_item = InvoiceItem::Minimal.new("purchase", "Something", 1, Date.new(2021, 1, 1), Date.new(2021, 2, 1), "some note", "")
    assert_equal("some note", invoice_item.description)

    invoice_item = InvoiceItem::Minimal.new("purchase", "Something", 1, Date.new(2021, 1, 1), Date.new(2021, 2, 1), nil, "")
    assert_equal("Verkoop overig", invoice_item.description)

    invoice_item = InvoiceItem::Minimal.new("rent", "Something", 1, Date.new(2021, 1, 1), Date.new(2021, 2, 1), nil, "")
    assert_equal("Huur overig", invoice_item.description)
  end
end
