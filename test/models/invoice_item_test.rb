# == Schema Information
#
# Table name: invoice_items
#
#  id                         :integer          not null, primary key
#  discount_amount            :decimal(, )      default(0.0)
#  discount_type              :string
#  input_price_ex_vat         :boolean
#  intent                     :string
#  invoiceable_type           :string
#  invoicing_period_ends_at   :date
#  invoicing_period_starts_at :date
#  note                       :string
#  object_cache               :text
#  quantity                   :decimal(, )      default(1.0)
#  rental_type                :string
#  time_span_uuid             :string
#  unit_price_ex_vat          :decimal(, )
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  invoice_id                 :integer
#  invoiceable_id             :string
#
require "test_helper"

class InvoiceItemTest < ActiveSupport::TestCase
  def assert_match_line expected_values_hash, line_of_interest
    expected_values_hash.each do |k, v|
      if v.nil?
        assert_nil(line_of_interest.send(k))
      else
        assert_equal(v, line_of_interest.send(k))
      end
    end
  end

  test ".initialize sets default period" do
    a = InvoiceItem.new(article: articles(:shop_product))
    assert_nil(a.invoicing_period_starts_at)
    assert_nil(a.invoicing_period_ends_at)

    travel_to Date.new(2023, 1, 1) do
      a = InvoiceItem.new(invoiceable: CollectionManagement::Work.new, intent: :rent)
      assert_equal(Date.new(2023, 2, 1), a.invoicing_period_starts_at)
      assert_equal(Date.new(2023, 2, 28), a.invoicing_period_ends_at)
    end

    travel_to Date.new(2023, 2, 1) do
      a = InvoiceItem.new(invoiceable: CollectionManagement::Work.new, intent: :rent, quantity: 3)
      assert_equal(Date.new(2023, 3, 1), a.invoicing_period_starts_at)
      assert_equal(Date.new(2023, 5, 31), a.invoicing_period_ends_at)
    end
  end

  test ".invoicing_period_spans_current" do
    Time.stub(:current, Date.new(2021, 1, 16)) do
      assert_equal(1, InvoiceItem.invoicing_period_spans_current.count)
    end
    Time.stub(:current, Date.new(2021, 2, 16)) do
      assert_equal(2, InvoiceItem.invoicing_period_spans_current.count)
    end
    Time.stub(:current, Date.new(2021, 4, 16)) do
      assert_equal(0, InvoiceItem.invoicing_period_spans_current.count)
    end
  end

  test ".summarize" do
    assert(InvoiceItem.summarize.count < InvoiceItem.count)
    assert_equal(InvoiceItem::Minimal, InvoiceItem.summarize.first.class)
    assert("Verhuur werken", InvoiceItem.summarize.find { |a| a.intent == "rent" && a.invoiceable_type == "CollectionManagement::Work" }.description)
  end

  test "#article=" do
    a = InvoiceItem.new(article: articles(:shop_product))
    assert_equal(articles(:shop_product), a.article)

    a = InvoiceItem.new(article: articles(:shop_product).id.to_s)
    assert_equal(articles(:shop_product), a.article)
  end

  test "#finalize! returns true on credit invoice" do
    item = invoice_items(:credit_purchase_shop_product_before_finalized)
    assert(item.finalize!)
    item = invoice_items(:credit_recently_expired_rent)
    assert(item.finalize!)
  end

  test "#finalize! starts event when invoice item is a work and quantity is positive" do
    item = invoice_items(:uncredit_recently_expired_rent)
    request = stub_request(:get, "http://localhost:3000/api/v1/time_spans/42535e49-b3db-4947-9b02-69be79f499b5").and_return(body: {data: {uuid: "abc"}}.to_json)
    assert(item.finalize!)
    assert_requested(request)
  end

  test "#gift_card_credit_mutation" do
    assert_equal(9.99, invoice_items(:gift_card_trade_in).gift_card_credit_mutation)
    assert_equal(0, invoice_items(:purchase_shop_product).gift_card_credit_mutation)
  end

  test "#initialize_next_intent" do
    next_intent = invoice_items(:rent).initialize_next_intent
    assert_equal("2021-03-01".to_date, next_intent.invoicing_period_starts_at)
    assert_equal("2021-03-31".to_date, next_intent.invoicing_period_ends_at)
    next_next_intent = next_intent.initialize_next_intent
    assert_equal("2021-04-01".to_date, next_next_intent.invoicing_period_starts_at)
    assert_equal("2021-04-30".to_date, next_next_intent.invoicing_period_ends_at)
  end

  test "#initialize_next_intent with multiple month period" do
    item = invoice_items(:rent)
    item.invoicing_period_ends_at = "2021-04-30".to_date
    item.quantity = 3
    next_intent = item.initialize_next_intent
    assert_equal("2021-05-01".to_date, next_intent.invoicing_period_starts_at)
    assert_equal("2021-07-31".to_date, next_intent.invoicing_period_ends_at)
    next_next_intent = next_intent.initialize_next_intent
    assert_equal("2021-08-01".to_date, next_next_intent.invoicing_period_starts_at)
    assert_equal("2021-10-31".to_date, next_next_intent.invoicing_period_ends_at)
    assert(item.valid?)
  end

  test "#initialize_next_intent with percentage discount" do
    item = invoice_items(:rent)
    item.discount_amount = 10
    item.discount_type = :percentage
    next_intent = item.initialize_next_intent
    assert_equal(item.discount_amount, next_intent.discount_amount)
    assert_equal(item.discount_type, next_intent.discount_type)
    assert(item.valid?)
  end

  test "#initialize_next_intent with absolute discount" do
    item = invoice_items(:rent)
    item.discount_amount = 10
    item.discount_type = :absolute
    next_intent = item.initialize_next_intent
    assert_equal(item.discount_amount, next_intent.discount_amount)
    assert_equal(item.discount_type, next_intent.discount_type)
    assert(item.valid?)
  end

  test "#invoiced_invoicing_period_overlap?" do
    item = invoice_items(:rent)
    other = item.initialize_next_intent
    assert(!other.invoiced_invoicing_period_overlap?)

    # make sure that if invoice item is saved, do not take self into account
    other.invoice = item.invoice
    other.save
    assert(!other.invoiced_invoicing_period_overlap?)

    # 'clone'
    other.invoicing_period_ends_at = item.invoicing_period_ends_at
    other.invoicing_period_starts_at = item.invoicing_period_starts_at

    assert(other.invoiced_invoicing_period_overlap?)

    # 'rebill a credited invoice'
    credit_invoice = Invoice.create(credits_invoice: item.invoice, finalized_at: Time.now)
    assert(credit_invoice.persisted?)

    assert(!other.invoiced_invoicing_period_overlap?)
  end

  test "#invoiced_invoicing_period_overlap? ignores non final invoice items" do
    item = invoice_items(:rent)
    assert(item.invoice.finalized?)
    other = item.initialize_next_intent
    assert(!other.invoiced_invoicing_period_overlap?)

    # 'clone'
    other.invoicing_period_ends_at = item.invoicing_period_ends_at
    other.invoicing_period_starts_at = item.invoicing_period_starts_at

    assert(other.invoiced_invoicing_period_overlap?)

    item.invoice.update_columns(finalized_at: nil)
    assert(!other.invoiced_invoicing_period_overlap?)
  end

  test "#savings_mutation" do
    assert_equal(0, invoice_items(:gift_card_trade_in).savings_mutation)
    assert_equal(0, invoice_items(:purchase_shop_product).savings_mutation)

    invoice_item = invoice_items(:rent)
    invoice_item.stub :invoice, Invoice.new(rental_type: :saving) do
      assert_equal(40, invoice_item.savings_mutation)
    end
  end

  test "#name" do
    a = InvoiceItem.new(article: articles(:shop_product))
    assert_equal("Winkel product", a.name)
  end

  test "#quantity=" do
    a = InvoiceItem.create(article: articles(:shop_product), quantity: "3.5", invoice: invoices(:one), intent: :rent)
    assert_equal(3.5, a.quantity)
  end

  test "#object_cache" do
    a = InvoiceItem.new(invoiceable: CollectionManagement::Work.new(id: 1))
    a.save

    assert_equal(1, a.object_cache["id"])
  end

  test "#invoiceable" do
    a = InvoiceItem.new(invoiceable: CollectionManagement::Work.new(id: 1))
    a.save

    assert_equal(1, a.invoiceable.id)
  end

  test "#invoiceable with complex invoiceable cached" do
    i = invoice_items(:rent)
    assert_equal("12", i.invoiceable.id)
    assert_equal("In collectie", i.invoiceable.themes.first.name)
  end

  test "#time_span_not_expired? returns true when no work inolved" do
    assert(InvoiceItem.new.time_span_not_expired?)
    assert(InvoiceItem.new(invoiceable: CollectionManagement::Work.new(id: 1)).time_span_not_expired?)
    CollectionManagement::TimeSpan.stub :find, CollectionManagement::TimeSpan.new do
      assert(InvoiceItem.new(invoiceable: CollectionManagement::Work.new(id: 1), time_span_uuid: 123).time_span_not_expired?)
    end
    stub_request(:get, "http://localhost:3000/api/v1/time_spans/123").to_return(status: 404)
    assert(InvoiceItem.new(invoiceable: CollectionManagement::Work.new(id: 1), time_span_uuid: 123).time_span_not_expired?)

    CollectionManagement::TimeSpan.stub :find, CollectionManagement::TimeSpan.new(ends_at: Time.now) do
      assert(!InvoiceItem.new(invoiceable: CollectionManagement::Work.new(id: 1), time_span_uuid: 123).time_span_not_expired?)
    end
  end

  test "#to_h" do
    InvoiceItem.all.each do |i|
      value = i.to_h
      assert_equal(Hash, value.class)
      assert_equal(i.invoice_id, value["invoice_id"])
      if i.invoiceable_type.nil?
        assert_nil(value["invoiceable_id"])
      else
        assert_equal(i.invoiceable_type, value["invoiceable_type"])
      end
    end
  end

  test "#unit_price" do
    i = InvoiceItem.new
    i.unit_price = 12.10
    assert_equal(10, i.unit_price_ex_vat)

    i = InvoiceItem.new
    i.unit_price = "12.10"
    assert_equal(10, i.unit_price_ex_vat)

    i = InvoiceItem.new(article: articles(:artist_artwork))
    i.unit_price = 10.90
    assert_equal(10, i.unit_price_ex_vat)

    i = InvoiceItem.new(unit_price_ex_vat: 10, input_price_ex_vat: true)
    assert_equal(10, i.unit_price)
  end

  test "#vat_rate" do
    {rent: 21, purchase: 21, purchase_cons: 9}.each do |type, rate|
      i = invoice_items(type)
      assert_equal(rate, i.vat_rate)
    end
  end

  test "#to_twinfield_invoice_purchase_lines raises error when not supported" do
    assert_raise { InvoiceItem.new.to_twinfield_invoice_lines }
  end

  test "#to_twinfield_invoice_lines on regular sale" do
    invoice_item = invoice_items(:purchase)
    assert_equal 2, invoice_item.to_twinfield_invoice_lines.count

    invoice_item_config = Rails.application.config_for(:uitleen)[:invoice_items]

    assert_match_line({article: invoice_item_config[:sell_art_owned][:article], subarticle: invoice_item_config[:sell_art_owned][:subarticle]}, invoice_item.to_twinfield_invoice_lines.first)
  end

  test "#to_twinfield_invoice_lines on regular rent" do
    invoice_item = invoice_items(:rent)
    invoice_item.invoice.finalized_at = nil

    assert_equal 2, invoice_item.to_twinfield_invoice_lines.count

    invoice_item_config = Rails.application.config_for(:uitleen)[:invoice_items]

    assert_match_line({article: invoice_item_config[:rent][:private][:article], subarticle: invoice_item_config[:rent][:private][:subarticle]}, invoice_item.to_twinfield_invoice_lines.first)
  end

  test "#to_twinfield_invoice_lines on discounted rent" do
    invoice_item = invoice_items(:rent)
    invoice_item.invoice.finalized_at = nil

    invoice_item.discount_amount = 10
    invoice_item.discount_type = "percentage"

    assert_equal 4, invoice_item.to_twinfield_invoice_lines.count

    invoice_item_config = Rails.application.config_for(:uitleen)[:invoice_items]

    assert_match_line({article: invoice_item_config[:rent][:private][:article], subarticle: invoice_item_config[:rent][:private][:subarticle]}, invoice_item.to_twinfield_invoice_lines.first)
    assert_match_line({article: invoice_item_config[:rent][:private][:article], subarticle: invoice_item_config[:rent][:private][:subarticle]}, invoice_item.to_twinfield_invoice_lines[2])
    assert_match(1.16.to_s, invoice_item.to_twinfield_invoice_lines[2].unitspriceexcl.round(2).to_f.to_s)
  end

  test "#to_twinfield_invoice_lines on regular rent with gift card credit" do
    invoice_item = invoice_items(:rent)
    invoice_item.invoice.update_column(:customer_id, customers(:four).id)
    invoice_item.invoice.finalized_at = nil
    assert_equal 3, invoice_item.to_twinfield_invoice_lines.count

    invoice_item_config = Rails.application.config_for(:uitleen)[:invoice_items]

    assert_match_line({article: invoice_item_config[:rent][:private][:article], subarticle: invoice_item_config[:rent][:private][:subarticle]}, invoice_item.to_twinfield_invoice_lines.first)
    assert_match_line({article: invoice_item_config[:gift_card][:use][:article], subarticle: invoice_item_config[:gift_card][:use][:subarticle]}, invoice_item.to_twinfield_invoice_lines.last)
  end

  test "#to_twinfield_invoice_lines on savings rent" do
    invoice_item = invoice_items(:rent)
    invoice_item.invoice.update_columns(rental_type: :saving)

    assert invoice_item.invoice.saving_rental_type?

    assert_equal 2, invoice_item.to_twinfield_invoice_lines.count

    invoice_item_config = Rails.application.config_for(:uitleen)[:invoice_items]

    assert_match_line({article: invoice_item_config[:customer_savings][:add][:article], subarticle: invoice_item_config[:customer_savings][:add][:subarticle]}, invoice_item.to_twinfield_invoice_lines.first)
  end

  test "#to_twinfield_invoice_lines on article rent" do
    invoice_item = invoice_items(:rent_article_finalized)

    assert_equal 2, invoice_item.to_twinfield_invoice_lines.count
    assert_match_line({article: "HUURMAATWERK", subarticle: nil}, invoice_item.to_twinfield_invoice_lines.first)
  end

  test "#to_twinfield_invoice_lines raises on invoiceables" do
    invoice_item = InvoiceItem.new(invoiceable: invoice_items(:rent), intent: :purchase)

    assert_raise(InvoiceItem::NotSupportedError) { invoice_item.to_twinfield_invoice_lines }

    invoice_item = InvoiceItem.new(invoiceable: invoice_items(:rent), intent: :rent)

    assert_raise(InvoiceItem::NotSupportedError) { invoice_item.to_twinfield_invoice_lines }
  end

  test "#to_twinfield_invoice_purchase_lines uses external twinfield lines when owner is a municipality" do
    invoice_item = invoice_items(:purchase_ext)
    assert_equal 2, invoice_item.to_twinfield_invoice_lines.count

    invoice_item_config = Rails.application.config_for(:uitleen)[:invoice_items]

    assert_match_line({article: invoice_item_config[:sell_art_external][:article], subarticle: invoice_item_config[:sell_art_external][:subarticle]}, invoice_item.to_twinfield_invoice_lines.first)
  end

  test "#to_twinfield_invoice_purchase_lines with decimal" do
    invoice_item = invoice_items(:purchase_shop_product_finalized)
    invoice_item.quantity = "1.5"

    assert_equal 1.5, invoice_item.to_twinfield_invoice_lines.first.quantity
  end

  test "#to_twinfield_invoice_purchase_lines returns 2 twinfield lines when consignation" do
    invoice_item = invoice_items(:purchase_cons)
    assert_equal 2, invoice_item.to_twinfield_invoice_lines.count
  end

  test "#to_twinfield_invoice_purchase_lines returns 2 twinfield lines when trade in of gift card" do
    invoice_item = invoice_items(:gift_card_trade_in)
    assert_equal 2, invoice_item.to_twinfield_invoice_lines.count
  end

  test "#to_twinfield_invoice_purchase_lines returns 1 twinfield lines when just a description" do
    invoice_item = invoice_items(:just_a_description)
    invoice_item.valid?
    assert(invoice_item.valid?)
    assert_equal 1, invoice_item.to_twinfield_invoice_lines.count
  end

  test "#invoicing_period_desc" do
    {
      ["2012-01-01", "2012-01-31"] => "januari 2012",
      ["2012-01-01", "2012-12-31"] => "2012",
      ["2012-01-01", "2012-11-30"] => "januari 2012 t/m november 2012",
      ["2012-01-15", "2012-11-30"] => "januari 2012 t/m november 2012"
    }.each do |period, description|
      assert_equal(description, InvoiceItem.new(invoicing_period_starts_at: period[0], invoicing_period_ends_at: period[1]).invoicing_period_desc)
    end
  end

  test "#time_span returns a TimeSpan" do
    i = invoice_items(:rent)

    stub_request(:get, "http://localhost:3000/api/v1/time_spans/#{i.time_span_uuid}")
      .to_return(
        body: {data: {
          "status" => "active",
          "uuid" => i.time_span_uuid,
          "contact_url" => "http://localhost:5001/customers/fc0b8374-a7a6-424d-958e-abdb4844be3f",
          "subject_id" => 75808,
          "subject_type" => "Work",
          "subject" =>
           {"title_rendered" => "Zonder titel",
            "id" => 75808,
            "artist_name_rendered" => "Test, Abc",
            "collection_id" => 13,
            "photo_front" => {"thumb" => "http://localhost:3000/uploads/work/photo_front/75808/thumb_ca5488a1-a559-4164-9d77-e5bf778ea0e7.", "original" => "http://localhost:3000/uploads/work/photo_front/75808/ca5488a1-a559-4164-9d77-e5bf778ea0e7.", "screen" => "http://localhost:3000/uploads/work/photo_front/75808/screen_ca5488a1-a559-4164-9d77-e5bf778ea0e7."},
            "available" => false,
            "url" => "http://localhost:3000/collections/13/works/75808"},
          "starts_at" => "2022-01-27T09:56:48.811+01:00",
          "ends_at" => nil,
          "classification" => "rental_outgoing"
        }}.to_json
      )
    time_span = i.time_span
    assert_equal(CollectionManagement::TimeSpan, time_span.class)
    assert_equal(i.time_span_uuid, time_span.uuid)
  end
end
