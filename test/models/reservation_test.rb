# == Schema Information
#
# Table name: reservations
#
#  id          :integer          not null, primary key
#  address     :string
#  city        :string
#  comments    :text
#  email       :string
#  intent      :string
#  name        :string
#  old_data    :text
#  postal_code :string
#  telephone   :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  customer_id :integer
#  import_id   :string
#  work_id     :integer
#
require "test_helper"

class ReservationTest < ActiveSupport::TestCase
  test "#work" do
    stub_request(:get, "http://localhost:3000/api/v1/works/123").to_return(status: 200, body: {data: {id: 123}}.to_json, headers: {})

    reservation = Reservation.new(work_id: 123)
    assert_equal(reservation.work_id, reservation.work.id)
  end
end
