# == Schema Information
#
# Table name: customers
#
#  id                      :integer          not null, primary key
#  auto_finalize_invoices  :boolean          default(TRUE)
#  blocked_at              :datetime
#  blocked_reason          :string
#  born_on                 :date
#  comments                :text
#  company_name            :string
#  company_vat             :string
#  customer_type           :string
#  deactivated_at          :datetime
#  deceased_at             :datetime
#  email                   :string
#  first_name              :string
#  id_document_number      :string
#  id_document_type        :string
#  id_document_valid_until :date
#  initials                :string
#  last_name               :string
#  last_name_prefix        :string
#  name                    :string
#  old_data                :text
#  postal_code             :string
#  sex                     :string
#  title                   :string
#  twinfield_raw           :string
#  uuid                    :string           not null
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  external_customer_id    :string
#  import_id               :string
#
# Indexes
#
#  index_customers_on_uuid  (uuid)
#
require "test_helper"

class CustomerTest < ActiveSupport::TestCase
  #####
  # INSTANCE METHODS
  #####
  test "#invoice_address should return a formatted address" do
    customer = customers(:two)
    assert_equal("Mevrouw S. Samkalde\nNassaulaan 45\nNL 2305 FR Haarlem", customer.invoice_address)
  end

  test "#invoice_address_id should return 1" do
    customer = customers(:two)
    assert_equal(1, customer.invoice_address_id)
  end

  test "#uri returns a valid uri" do
    customer = customers(:two)
    assert_equal("https://localhost:5001/customers/#{customer.uuid}", customer.uri)
  end

  test "#total_savings" do
    customer = customers(:one)
    assert_equal 0, customer.total_savings
    customer = customers(:two)
    assert_equal 0.10991e3, customer.total_savings
  end

  test "#total_gift_card_credit" do
    customer = customers(:one)
    assert_equal 0, customer.total_gift_card_credit
    customer = customers(:four)
    assert_equal 0.55e2, customer.total_gift_card_credit
  end

  test "#existing_customer?" do
    customer = customers(:one)
    assert(customer.existing_customer?)

    customer = Customer.new
    assert(!customer.existing_customer?)

    customer = customers(:four)
    assert(!customer.existing_customer?)

    customer = customers(:five)
    assert(customer.existing_customer?)
  end

  #####
  # CLASS METHODS
  #####

  test ".last_updated_at" do
    Customer.update_all(updated_at: 2.hour.ago)
    assert(Customer.last_updated_at < 1.second.ago)

    c = Customer.first
    c.update(name: "First customer?")
    assert(Customer.last_updated_at > 1.second.ago)
  end
end
