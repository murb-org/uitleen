# == Schema Information
#
# Table name: collect_mandates
#
#  id            :integer          not null, primary key
#  data          :text
#  sign_location :string
#  signed_on     :date
#  uuid          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  customer_id   :integer
#
require "test_helper"

class CollectMandateTest < ActiveSupport::TestCase
  test "it validates iban" do
    cm = CollectMandate.create(iban: "NL12INVA1234", customer: customers(:one))
    assert_not(cm.valid?)
    assert(cm.errors.map(&:attribute).include?(:iban))

    cm = CollectMandate.create(iban: "NL30TRIO0254651658", customer: customers(:one))
    assert_not(cm.errors.map(&:attribute).include?(:iban))
  end

  test "uuid" do
    cm = CollectMandate.create(iban: "NL30TRIO0254651658", customer: customers(:one))
    assert(cm.uuid)
  end

  test "sepa_proof_uuid" do
    cm = CollectMandate.create(iban: "NL30TRIO0254651658", customer: customers(:one))
    assert_equal(32, cm.sepa_proof_uuid.length) # needs to be below 35
  end
end
