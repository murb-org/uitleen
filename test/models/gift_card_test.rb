# == Schema Information
#
# Table name: gift_cards
#
#  id          :integer          not null, primary key
#  action      :string
#  amount      :decimal(, )
#  code        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  customer_id :integer
#
require "test_helper"

class GiftCardTest < ActiveSupport::TestCase
  test "trade_in?" do
    assert_equal false, gift_cards(:one).trade_in?
    assert_equal true, gift_cards(:two).trade_in?
  end
  test "purchase?" do
    assert_equal true, gift_cards(:one).purchase?
    assert_equal false, gift_cards(:two).purchase?
  end
  test "validations" do
    gc = GiftCard.create
    assert_equal false, gc.valid?
    gc.errors.map(&:attribute).include? :action
    gc.errors.map(&:attribute).include? :amount
  end
end
