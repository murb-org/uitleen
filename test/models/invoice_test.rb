# == Schema Information
#
# Table name: invoices
#
#  id                                      :integer          not null, primary key
#  discount_percentage                     :decimal(, )      default(0.0)
#  external_invoice_data                   :text
#  finalized_at                            :datetime
#  footertext                              :text
#  intents                                 :text
#  invoice_sent_at                         :datetime
#  invoiceable_item_collection_type        :string
#  next_invoice_at                         :date
#  old_data                                :text
#  on_hold_at                              :datetime
#  po_number                               :string
#  recurrence_interval_in_months           :integer          default(1)
#  reference                               :string
#  rental_type                             :string           default("basic_rent")
#  requires_review_at                      :datetime
#  twinfield_invoicenumber                 :string
#  use_customer_savings_for_purchase       :boolean          default(TRUE)
#  created_at                              :datetime         not null
#  updated_at                              :datetime         not null
#  created_by_user_id                      :string
#  credits_invoice_id                      :integer
#  customer_id                             :integer
#  import_id                               :string
#  invoiceable_item_collection_external_id :string
#  on_hold_by_user_id                      :integer
#  previous_invoice_id                     :integer
#
# Indexes
#
#  index_invoices_on_twinfield_invoicenumber  (twinfield_invoicenumber) UNIQUE
#

require "test_helper"

class InvoiceTest < ActiveSupport::TestCase
  test ".new initial state" do
    assert_equal([], Invoice.new.purchase_invoice_items)
    assert_equal([], Invoice.new.rent_invoice_items)
  end

  test "#create_payment_transaction raises NotFinalError when not final" do
    invoice = invoices(:one)

    assert_raises(Invoice::NotFinalError) {
      invoice.create_payment_transaction(:pin)
    }
  end

  test "#create_payment_transaction creates a pin transaction when final" do
    invoice = invoices(:finalized)

    assert(invoice.finalized?)
    assert(!invoice.fully_final?)
    invoice.stub :external_invoice, Twinfield::SalesInvoice.new(status: "final", invoicetype: "a", customer: "b", financials: {code: "VRK", number: "123"}) do
      invoice.store_external_invoice_data_if_finalized
    end
    invoice.stub :booked_transaction, Twinfield::Browse::Transaction::Customer.new(code: 123, open_value: 100) do
      assert(invoice.fully_final?)
    end

    payment_transaction = Twinfield::Transaction.new(code: "PIN", currency: "EUR")
    payment_transaction.stub :save, true do
      Twinfield::Transaction.stub :new, payment_transaction do
        invoice.stub :twinfield_invoice, Twinfield::SalesInvoice.new(status: "final", invoicetype: "a", customer: "b") do
          invoice.stub :booked_transaction, Twinfield::Browse::Transaction::Customer.new(code: 123, open_value: 100) do
            payment_transaction = invoice.create_payment_transaction(:pin)
            assert_equal(Twinfield::Transaction, payment_transaction.class)
            assert_equal(3, payment_transaction.lines.count)
            assert_equal(1230, payment_transaction.lines.find { |a| a.type == :total }.balance_code)
            assert_equal(1300, payment_transaction.lines.find { |a| a.invoicenumber == invoice.twinfield_invoicenumber }.balance_code)
            assert_equal(:credit, payment_transaction.lines.find { |a| a.invoicenumber == invoice.twinfield_invoicenumber }.debitcredit)
            assert_equal(1234, payment_transaction.lines.find { |a| a.invoicenumber.nil? && a.debitcredit == :debit && a.type == :detail }.balance_code)
          end
        end
      end
    end
  end

  test "#credit_invoice?" do
    old_invoice = invoices(:finalized)
    credit_invoice = Invoice.create(credits_invoice: old_invoice)
    assert(credit_invoice.credit_invoice?)
    assert(!old_invoice.credit_invoice?)
    assert(credit_invoice.valid?)

    assert(credit_invoice.customer)
    stub_twinfield_invoice do
      assert_equal((-1 * old_invoice.total_price), credit_invoice.total_price)
    end
  end

  test "#customer_total_gift_card_credit" do
    invoice = Invoice.new

    assert_equal(0, invoice.customer_total_gift_card_credit)

    customer = Customer.new
    customer.stub :total_gift_card_credit, 50 do
      invoice.stub :customer, customer do
        assert_equal(50, invoice.customer_total_gift_card_credit)
        invoice.invoice_items << invoice_items(:gift_card_trade_in)
        assert_equal(59.99, invoice.customer_total_gift_card_credit)
        invoice.invoice_items << invoice_items(:rent)
        assert_equal(45.99, invoice.customer_total_gift_card_credit)
        invoice.invoice_items << invoice_items(:rent)
        assert_equal(31.99, invoice.customer_total_gift_card_credit)
        invoice.invoice_items << invoice_items(:rent)
        invoice.invoice_items << invoice_items(:rent)
        assert_equal(3.99, invoice.customer_total_gift_card_credit)
        invoice.invoice_items << invoice_items(:rent)
        assert_equal(0, invoice.customer_total_gift_card_credit)
      end
    end
  end

  test "#customer_total_gift_card_credit with saving" do
    invoice = Invoice.new

    assert_equal(0, invoice.customer_total_gift_card_credit)

    customer = Customer.new
    customer.stub :total_gift_card_credit, 10 do
      invoice.stub :customer, customer do
        invoice.rental_type = :saving
        assert_equal(10, invoice.customer_total_gift_card_credit)
        invoice.invoice_items << invoice_items(:gift_card_trade_in)
        assert_equal(19.99, invoice.customer_total_gift_card_credit)
        invoice.invoice_items << invoice_items(:rent)
        assert_equal(-19.99, invoice.invoice_items.last.gift_card_credit_mutation)
        assert_equal(0, invoice.customer_total_gift_card_credit)
      end
    end
  end

  test "#customer_total_savings complex mutations" do
    invoice = Invoice.new

    assert_equal(0, invoice.customer_total_savings)

    customer = Customer.new
    customer.stub :total_savings, 50 do
      invoice.stub :customer, customer do
        assert_equal(50, invoice.customer_total_savings)
        invoice.invoice_items << invoice_items(:gift_card_trade_in)
        assert_equal(50, invoice.customer_total_savings)
        invoice.invoice_items << invoice_items(:rent)
        assert_equal(50, invoice.customer_total_savings)
        invoice.rental_type = "saving"
        assert_equal((BigDecimal(50) + BigDecimal(40)), invoice.customer_total_savings)
        invoice.invoice_items << invoice_items(:purchase)
        assert_equal(0, invoice.customer_total_savings)
      end
    end
  end

  test "#customer_total_savings just purchase" do
    invoice = Invoice.new

    assert_equal(0, invoice.customer_total_savings)

    customer = Customer.new
    customer.stub :total_savings, 50 do
      invoice.stub :customer, customer do
        assert_equal(50, invoice.customer_total_savings)

        invoice.invoice_items << invoice_items(:purchase)
        assert_equal(0, invoice.customer_total_savings)

        assert_equal(invoice.invoice_items.first.work.selling_price.to_f - 50, invoice.total_price.to_f)
      end
    end
  end

  test "#finalize! drops empty invoices" do
    invoice = invoices(:one)

    assert(!invoice.finalized?)
    invoice.finalize!

    assert_raises(ActiveRecord::RecordNotFound) { invoice.reload }
  end

  test "#finalize! finalizes non final invoices" do
    invoice = invoices(:two)

    stub_request(:post, "http://localhost:3000/api/v1/collections/13/works/12/work_events")
      .with(body: /purchase/)
      .to_return(body: {uuid: "d99c9847-d1c8-4835-ba55-0335460f970c"}.to_json)

    assert(!invoice.finalized?)

    twinfield_invoice = invoice.external_invoice
    twinfield_invoice.stub :save, twinfield_invoice do
      invoice.stub :twinfield_invoice, twinfield_invoice do
        invoice.finalize!
      end
    end

    assert(invoice.finalized?)
  end

  test "#finalize! returns error when twinfield invoice cannot be found" do
    invoice = Invoice.new

    assert(!invoice.finalized?)

    invoice.stub :twinfield_invoice, nil do
      assert(!invoice.finalize!)
    end

    assert_match("factuur kon niet gevonden worden", invoice.errors.full_messages.join(","))

    assert(!invoice.finalized?)
  end

  test "#initialize_next_invoice when invoice has no items should return nil" do
    di = Invoice.new(created_by_user: users(:admin))
    next_invoice = di.initialize_next_invoice

    assert_nil(next_invoice)
  end

  test "#initialize_next_invoice when invoice has a rent item" do
    di = invoices(:old)
    next_invoice = nil
    CollectionManagement::TimeSpan.stub :find, CollectionManagement::TimeSpan.new do
      next_invoice = di.initialize_next_invoice
    end

    assert(next_invoice)
    next_invoice.save
    assert_equal(1, next_invoice.invoice_items.count)
    assert_equal("Referentitest", next_invoice.reference)
    assert_equal("rent", next_invoice.invoice_items[0].intent)
    assert_not_equal(di.invoice_items.rent.first.id, next_invoice.invoice_items[0].id)
  end

  test "#initialize_next_invoice when quarterly interval and has a rent item" do
    travel_to Date.new(2021, 4, 2) do
      di = invoices(:recently_expired_quarterly_interval)
      next_invoice = nil
      CollectionManagement::TimeSpan.stub :find, CollectionManagement::TimeSpan.new do
        next_invoice = di.initialize_next_invoice
      end

      assert(next_invoice)
      next_invoice.save
      assert_equal(1, next_invoice.invoice_items.count)
      assert_equal("rent", next_invoice.invoice_items[0].intent)
      assert_equal("april 2021 t/m juni 2021", next_invoice.invoice_items[0].invoicing_period_desc)
      assert_not_equal(di.invoice_items.rent.first.id, next_invoice.invoice_items[0].id)

      next_invoice.invoice_items.first.stub(:start_event, true) do
        next_invoice.stub(:save_external_invoice!, true) do
          next_invoice.finalize!
        end
      end

      assert_equal(Date.new(2021, 7, 1), next_invoice.next_invoice_at)
    end
  end

  test "#new_intent_attributes - can't rent and buy" do
    work = CollectionManagement::Work.new(id: 12)

    user = users(:one)
    user.expires_at = 1.year.from_now

    di = Invoice.new(customer: customers(:one), created_by_user: users(:admin))

    stub_collection_management work: work do
      work.stub :mark_event!, nil do
        di.new_intent_attributes = {work_id: work.id, intent: "purchase"}
      end
      di.save

      assert_equal([work.id.to_s], di.purchase_invoice_items.map(&:invoiceable_id))

      # stub mark event
      work.stub :mark_event!, nil do
        di.new_intent_attributes = {work_id: work.id, intent: "rent"}
      end
    end

    di.save
    di.reload

    assert_equal([], di.rent_invoice_items)
    assert_equal([work.id.to_s], di.purchase_invoice_items.map(&:invoiceable_id))
  end

  test "#next_invoice_date is nil for invoices without invoice items" do
    invoice = Invoice.new
    assert_nil(invoice.next_invoice_date)
  end

  test "#next_invoice_date is nil if there is nothing to rent" do
    invoice = Invoice.create
    invoice.invoice_items << InvoiceItem.new(invoiceable: articles(:shop_product), intent: :purchase, quantity: 3, invoicing_period_starts_at: 1.month.from_now.beginning_of_month.to_date, invoicing_period_ends_at: 3.month.from_now.end_of_month.to_date)
    assert_nil(invoice.next_invoice_date)
  end

  test "#next_invoice_date is beginning of next next month for first invoice of a series" do
    invoice = Invoice.create
    invoice.invoice_items << InvoiceItem.new(invoiceable: articles(:shop_product), intent: :rent, quantity: 3, invoicing_period_starts_at: 1.month.from_now.beginning_of_month.to_date, invoicing_period_ends_at: 3.month.from_now.end_of_month.to_date)
    assert_equal(4.month.from_now.beginning_of_month.to_date, invoice.next_invoice_date)
  end

  test "#next_invoice_date is next_invoice_at if set" do
    invoice = Invoice.create
    invoice.invoice_items << InvoiceItem.new(invoiceable: articles(:shop_product), intent: :rent, quantity: 3, invoicing_period_starts_at: 1.month.from_now.beginning_of_month.to_date, invoicing_period_ends_at: 3.month.from_now.end_of_month.to_date)
    invoice.next_invoice_at = 3.months.from_now.beginning_of_month.to_date
    invoice.save
    invoice.reload
    assert_equal(3.months.from_now.beginning_of_month.to_date, invoice.next_invoice_date)
  end

  test "#next_invoice_date is on_hold is set" do
    invoice = Invoice.create
    invoice.invoice_items << InvoiceItem.new(invoiceable: articles(:shop_product), intent: :rent, quantity: 3, invoicing_period_starts_at: 1.month.from_now.beginning_of_month.to_date, invoicing_period_ends_at: 3.month.from_now.end_of_month.to_date)
    invoice.next_invoice_at = 3.months.from_now.beginning_of_month.to_date
    invoice.on_hold_at = Time.now
    invoice.save
    invoice.reload
    assert_nil(invoice.next_invoice_date)
  end

  test "#remove_intent_attributes - removes line" do
    work = CollectionManagement::Work.new(id: 12)

    user = users(:one)
    user.expires_at = 1.year.from_now

    di = Invoice.new(customer: customers(:one), created_by_user: users(:admin))

    stub_collection_management work: work do
      work.stub :mark_event!, nil do
        di.new_intent_attributes = {work_id: work.id, intent: "purchase"}
      end
      di.save

      assert_equal([work.id.to_s], di.purchase_invoice_items.map(&:invoiceable_id))

      # stub mark event
      work.stub :mark_event!, nil do
        di.remove_intent_attributes = {work_id: work.id, intent: "purchase"}
      end
    end

    di.save
    di.reload

    assert_equal([], di.rent_invoice_items)
    assert_equal([], di.purchase_invoice_items.map(&:invoiceable_id))
  end

  test "#total_price_as_intended rounding of numbers" do
    # the issue at hand is that some invoices lead to rounding errors.
    invoice = Invoice.new

    invoice_item = invoice_items(:purchase)

    # ((750 / 1.21).round(2) * 1.21).round(2) !== 750
    invoice_item.unit_price = 750

    invoice_item.time_span_uuid = nil

    customer = Customer.new(external_customer_id: 123, twinfield_raw: {addresses: [{default: true}]})
    invoice.customer = customer
    invoice.invoice_items << invoice_item

    stub_create_twinfield_invoice invoice: invoice
    invoice.stub :customer, customer do
      stub_twinfield do
        stub_create_event
        invoice.finalize!
      end
      assert_equal(750, invoice.total_price_as_intended)
    end
  end

  test "#total_price rounding of numbers wilst there is no problem" do
    # the issue at hand is that some invoices lead to rounding errors.
    invoice = Invoice.new

    invoice_item = invoice_items(:purchase)

    # ((750 / 1.21).round(2) * 1.21).round(2) !== 750
    invoice_item.unit_price = 740

    invoice_item.time_span_uuid = nil

    customer = Customer.new(external_customer_id: 123, twinfield_raw: {addresses: [{default: true}]})
    invoice.customer = customer
    invoice.invoice_items << invoice_item

    stub_create_twinfield_invoice invoice: invoice
    invoice.stub :customer, customer do
      stub_twinfield do
        stub_create_event
        invoice.finalize!
      end
      assert_equal(740, invoice.total_price)
    end
  end

  test "#total_price rounding of numbers wilst there is no problem, as it is all without vat" do
    # the issue at hand is that some invoices lead to rounding errors.
    invoice = Invoice.new

    invoice_item = invoice_items(:purchase)

    # ((750 / 1.21).round(2) * 1.21).round(2) !== 750
    invoice.rental_type = :saving # no vat
    invoice_item.unit_price = BigDecimal("619.83")
    invoice_item.time_span_uuid = nil

    customer = Customer.new(external_customer_id: 123, twinfield_raw: {addresses: [{default: true}]})
    invoice.customer = customer
    invoice.invoice_items << invoice_item

    stub_create_twinfield_invoice invoice: invoice
    invoice.stub :customer, customer do
      stub_twinfield do
        stub_create_event
        invoice.finalize!
      end
      assert_equal(619.83, invoice.total_price)
    end
  end

  test "#recurring?" do
    invoice = invoices(:old)
    stub_twinfield_invoice do
      assert(invoice.recurring?)
    end
  end

  test "#recurring? should return false for negative invoices" do
    invoice = invoices(:old)
    invoice.invoice_items.update_all(quantity: -1)
    stub_twinfield_invoice do
      assert(!invoice.recurring?)
    end
  end

  test "#sync_remotely?" do
    invoice = invoices(:old)
    assert(!invoice.sync_remotely?)

    invoice = invoices(:one)
    assert(invoice.sync_remotely?)
  end

  test "#save_external_invoice!" do
    invoice = invoices(:old)
    assert_nil(invoice.save_external_invoice!)

    invoice = invoices(:one)

    assert_raises(WebMock::NetConnectNotAllowedError) { invoice.save_external_invoice! }
  end

  test "#invoice_item_start_date_reset doesn't update finalized invoices" do
    invoice = invoices(:old)
    assert_nil(invoice.invoice_item_start_date_reset)
    invoice.invoice_item_start_date_reset = Date.new(2022, 1, 1)
    assert_nil(invoice.invoice_item_start_date_reset)
  end

  test "#invoice_item_start_date_reset does update draft invoices" do
    invoice = invoices(:work_set_based_invoice_with_invoice_items)
    assert_equal(invoice.invoice_items.first.invoicing_period_starts_at, invoice.invoice_item_start_date_reset)
    invoice.invoice_item_start_date_reset = Date.new(2022, 1, 1)
    assert_equal(Date.new(2022, 1, 1), invoice.invoice_item_start_date_reset)
  end

  test "#invoiceable_item_collection returns nil when not set" do
    invoice = Invoice.new
    assert_nil(invoice.invoiceable_item_collection)
  end

  test "#invoiceable_item_collection returns work set when set" do
    stub_request(:get, "http://localhost:3000/api/v1/work_sets/abc.json")
      .to_return(body: {data: {uuid: "abc", works: [{id: 1, stock_number: "ABC00123"}]}}.to_json)
    invoice = invoices(:work_set_based_invoice)
    invoiceable_item_collection = invoice.invoiceable_item_collection
    assert_equal(CollectionManagement::WorkSet, invoiceable_item_collection.class)
  end

  test "#invoiceable_item_collection= sets invoiceable item collection" do
    stub_request(:get, "http://localhost:3000/api/v1/work_sets/abc.json")
      .to_return(body: {data: {uuid: "abc", works: [{id: 1, stock_number: "ABC00123"}]}}.to_json)

    invoice = invoices(:three)
    invoice.invoiceable_item_collection = CollectionManagement::WorkSet.find("abc")
    invoice.save
    invoice.reload
    assert_equal("abc", invoice.invoiceable_item_collection_external_id)
    assert_equal("abc", invoice.invoiceable_item_collection.external_id)
  end

  test "#invoiceable_item_collection_works_with_current_active_time_span returns nil by default (not appliccable)" do
    invoice = Invoice.new
    assert_nil(invoice.invoiceable_item_collection_works_with_current_active_time_span)
  end

  test "#invoiceable_item_collection_works_with_current_active_time_span returns empty array if no works found" do
    invoice = invoices(:work_set_based_invoice)
    works = []
    CollectionManagement::WorkSet.stub(:find, CollectionManagement::WorkSet.new(works: works)) do
      assert_equal([], invoice.invoiceable_item_collection_works_with_current_active_time_span)
    end
  end

  test "#invoiceable_item_collection_works_with_current_active_time_span returns empty array if no works with current active timespan are found" do
    invoice = invoices(:work_set_based_invoice)
    works = [CollectionManagement::Work.new(id: 1)]

    CollectionManagement::WorkSet.stub(:find, CollectionManagement::WorkSet.new(works: works)) do
      assert_equal([], invoice.invoiceable_item_collection_works_with_current_active_time_span)
    end
  end

  test "#invoiceable_item_collection_works_with_current_active_time_span returns array with works array if works with current active timespan are found" do
    invoice = invoices(:work_set_based_invoice)
    works = [CollectionManagement::Work.new(id: 1, current_active_time_span: {contact: {url: "http"}, status: :active, classification: :rental_outgoing})]
    CollectionManagement::WorkSet.stub(:find, CollectionManagement::WorkSet.new(works: works)) do
      assert_equal(works, invoice.invoiceable_item_collection_works_with_current_active_time_span)
    end
  end

  test "#invoiceable_item_collection= doesn't allow any type to be set" do
    invoice = invoices(:three)
    invoice.invoiceable_item_collection = CollectionManagement::Work.new(id: "abc")
    invoice.save
    assert(!invoice.valid?)
    assert(invoice.errors.map(&:attribute).include?(:invoiceable_item_collection_type))
  end

  test "#requires_review? returns false by default" do
    invoice = invoices(:work_set_based_invoice)
    assert(!invoice.requires_review?)
  end

  test "#requires_review! set requres_review? to true" do
    invoice = Invoice.create(created_by_user: users(:admin))
    invoice.requires_review!

    invoice = Invoice.find(invoice.id)
    assert(invoice.requires_review?)
  end

  test "#update_invoice_items_with_invoiceable_item_collection when no works of interests have been found [CALLBACK]" do
    invoice = invoices(:work_set_based_invoice)
    works = [CollectionManagement::Work.new(id: "abc")]
    CollectionManagement::WorkSet.stub(:find, CollectionManagement::WorkSet.new(works: works)) do
      invoice.save
    end
    invoice.reload
    assert_equal(0, invoice.invoice_items.length)
  end

  test "#update_invoice_items_with_invoiceable_item_collection when works of interests have been found [CALLBACK]" do
    invoice = invoices(:work_set_based_invoice)
    works = [CollectionManagement::Work.new(id: "abc", current_active_time_span: CollectionManagement::TimeSpan.new(contact_url: invoice.customer.uri, subject_type: "Work", classification: "rental_outgoing", status: "active", subject: CollectionManagement::Work.new(id: "abc")).to_h)]

    CollectionManagement::WorkSet.stub(:find, CollectionManagement::WorkSet.new(works: works)) do
      invoice.save
    end
    invoice.reload
    assert_equal(1, invoice.invoice_items.length)
    assert_equal(1.month.from_now.beginning_of_month.to_date, invoice.invoice_items.first.invoicing_period_starts_at)
    assert(invoice.invoice_items.first.rent_intent?)
  end

  test "#work_in_invoice_items?" do
    work = CollectionManagement::Work.new(id: 15)
    di = Invoice.new(created_by_user: users(:admin))
    di.purchase_invoice_items.new(invoiceable: work)
    di.save

    assert(di.work_in_invoice_items?(work))
    assert(di.work_in_invoice_items?(15))
    assert(di.work_in_invoice_items?("15"))
    assert(!di.work_in_invoice_items?("16"))
  end

  test "#to_html" do
    hash = {lines: [{id: "1", article: "HUUR", subarticle: "1", quantity: 1, units: 1, allowdiscountorpremium: "true", description: "Huur van iets beschreven", unitspriceexcl: 7.23, valueinc: 8.75, unitspriceinc: 8.75, freetext1: nil, freetext2: nil, freetext3: nil, dim1: "8000", vatcode: "VH", performancetype: nil, performancedate: nil}, {id: "2", article: "-", subarticle: nil, quantity: nil, units: nil, allowdiscountorpremium: nil, description: "Beschrijving", unitspriceexcl: nil, unitspriceinc: nil, freetext1: nil, freetext2: nil, freetext3: nil, dim1: nil, vatcode: nil, performancetype: nil, performancedate: nil}], vat_lines: [{vatcode: "VH", vatvalue: 1.73, performancetype: "", performancedate: "", vatname: "BTW 21%"}], invoicetype: "FACTUUR", invoicedate: "2022-06-24", duedate: "2022-07-24", performancedate: nil, bank: "BNK", invoiceaddressnumber: 1, deliveraddressnumber: 1, customer_code: 2321, period: "2022/6", currency: "EUR", status: "final", paymentmethod: "bank", headertext: "", footertext: "Footer", office: "NL123", invoicenumber: "2022-0003"}
    sales_invoice = Twinfield::SalesInvoice.new(**hash)

    Twinfield::SalesInvoice.stub :find, sales_invoice do
      stub_twinfield do
        invoice = invoices(:old)
        assert_match("Gelieve het bovenstaande bedrag van €8,75 te voldoen voor 24 juli 2022 onder vermelding van het factuurnummer 2022-0003", invoice.to_html)

        invoice.stub :collect_mandate?, true do
          sales_invoice.stub :uitleen_invoice, nil do
            # assert_no_match("Gelieve het bovenstaande bedrag van €8,75 te voldoen voor 24 juli 2022 onder vermelding van het factuurnummer 2022-0003", invoice.to_html)
            # assert_match("Het openstaande bedrag van €8,75 zal binnenkort automatisch worden afgeschreven conform de machtiging. Voor vragen kunt altijd contact opnemen: info@heden.nl", invoice.to_html)
          end
        end
      end
    end
  end

  test "#to_pdf" do
    hash = {lines: [{id: "1", article: "HUUR", subarticle: "1", quantity: 1, units: 1, allowdiscountorpremium: "true", description: "Huur van iets beschreven", unitspriceexcl: 7.23, valueinc: 8.75, unitspriceinc: 8.75, freetext1: nil, freetext2: nil, freetext3: nil, dim1: "8000", vatcode: "VH", performancetype: nil, performancedate: nil}, {id: "2", article: "-", subarticle: nil, quantity: nil, units: nil, allowdiscountorpremium: nil, description: "Beschrijving", unitspriceexcl: nil, unitspriceinc: nil, freetext1: nil, freetext2: nil, freetext3: nil, dim1: nil, vatcode: nil, performancetype: nil, performancedate: nil}], vat_lines: [{vatcode: "VH", vatvalue: 1.73, performancetype: "", performancedate: "", vatname: "BTW 21%"}], invoicetype: "FACTUUR", invoicedate: "2022-06-24", duedate: "2022-07-24", performancedate: nil, bank: "BNK", invoiceaddressnumber: 1, deliveraddressnumber: 1, customer_code: 2321, period: "2022/6", currency: "EUR", status: "final", paymentmethod: "bank", headertext: "", footertext: "Footer", office: "NL123", invoicenumber: "2022-0003"}
    sales_invoice = Twinfield::SalesInvoice.new(**hash)

    Twinfield::SalesInvoice.stub :find, sales_invoice do
      SecureRandom.stub :base58, "randomBase58" do
        Pupprb::Pdf.stub :write, "stubbedwrittenpdf" do
          stub_twinfield do
            invoice = invoices(:recently_expired_quarterly_interval)

            assert_equal("stubbedwrittenpdf", invoice.to_pdf)
          end
        end
      end
    end
  end

  #
  # CLASS METHODS
  #

  test ".from_hash" do
    json = <<JSON
      {"id": 4655,
      "customer_id": 1411,
      "footertext": "Let op: deze factuur is een kopie factuur vanuit vorige boekhoudpakket. Oorspronkelijk factuurnummer 131296",
      "created_by_user_id": null,
      "created_at": "2022-04-07T07:47:36.030+02:00",
      "updated_at": "2022-04-07T07:47:36.502+02:00",
      "intents": null,
      "twinfield_invoicenumber": "1051",
      "rental_type": "basic_rent",
      "previous_invoice_id": 350,
      "use_customer_savings_for_purchase": true,
      "import_id": null,
      "discount_percentage": "0.0",
      "recurrence_interval_in_months": 1,
      "old_data": {
      },
      "customer_uuid": "30e65ade-e127-4fbc-94e3-dc1d4b19d4bf",
      "invoice_items": [
        {
          "id": 23838,
          "invoice_id": 4655,
          "invoiceable_type": "CollectionManagement::Work",
          "invoiceable_id": "68225",
          "intent": "rent",
          "invoicing_period_starts_at": "2021-02-01",
          "invoicing_period_ends_at": "2021-02-28",
          "created_at": "2022-04-07T07:47:36.031+02:00",
          "updated_at": "2022-04-07T07:47:36.031+02:00",
          "object_cache": {
            "stock_number": "778001",
            "artist_name_rendered_without_years_nor_locality": "Babette Wagenvoort",
            "title_rendered": "Waar ben je nou stom beest? no.2"
          },
          "time_span_uuid": "bc54b5c9-1a14-41e0-a488-bced8bcb8d86",
          "quantity": "1.0",
          "unit_price_ex_vat": null,
          "note": null,
          "work_id": "68225"
        },
        {
          "id": 23839,
          "invoice_id": 4655,
          "invoiceable_type": "Article",
          "invoiceable_id": "6",
          "intent": "rent",
          "invoicing_period_starts_at": "2021-02-01",
          "invoicing_period_ends_at": "2021-02-28",
          "created_at": "2022-04-07T07:47:36.033+02:00",
          "updated_at": "2022-04-07T07:47:36.033+02:00",
          "object_cache": {
          },
          "time_span_uuid": null,
          "quantity": "1.0",
          "unit_price_ex_vat": null,
          "note": null,
          "article_code": "HUURMAATWERK",
          "article_subcode": null
        }
      ]
    }
JSON
    hash = JSON.parse(json)
    i = Invoice.from_h(hash)
    assert_equal(2, i.invoice_items.count)
    assert_equal("HUURMAATWERK", i.invoice_items.last.article.external_article_code)
    assert_equal("778001", i.invoice_items.first.work.stock_number)
  end

  test ".find_by_id_param" do
    assert_equal(invoices(:one).id, Invoice.find_by_id_param(invoices(:one).id.to_s).id)
    assert_equal(invoices(:finalized).id, Invoice.find_by_id_param("external_invoice_number:#{invoices(:finalized).twinfield_invoicenumber}").id)
  end

  test ".recently_expired" do
    travel_to(Time.now.beginning_of_month + 2.day) do
      stub_twinfield_invoice do
        Invoice.finalized.all.collect do |invoice|
          invoice.finalized_at = invoice.recurrence_interval_in_months.month.ago
          invoice.invoice_items.rent.update_all(invoicing_period_ends_at: 1.day.ago)
          invoice
        end.each(&:save) # make sure next_invoice_at is set

        assert_equal(3, Invoice.recently_expired.count)
      end
    end
  end

  test ".new with credits_invoice:" do
    old_invoice = invoices(:recently_expired)

    credit_invoice = Invoice.create(credits_invoice: old_invoice)
    assert(credit_invoice.credit_invoice?)
    assert(!old_invoice.credit_invoice?)
    assert(credit_invoice.valid?)

    stub_request(:post, "https://accounting.twinfield.com/webservices/processxml.asmx")

    CollectionManagement::TimeSpan.stub :find, CollectionManagement::TimeSpan.new(ends_at: 1.month.ago) do
      credit_invoice.stub :save_external_invoice!, nil do
        assert(credit_invoice.finalize!)
      end
    end

    stub_twinfield_invoice do
      assert_equal((-1 * old_invoice.total_price), credit_invoice.total_price)
    end
  end

  test ".new with duplicates_invoice" do
    old_invoice = invoices(:recently_expired)

    duplicate_invoice = Invoice.create(manually_create_next_for_invoice_id: old_invoice.id)
    assert(!duplicate_invoice.credit_invoice?)
    assert(duplicate_invoice.valid?)
    assert_equal(old_invoice.customer, duplicate_invoice.customer)
    assert_equal(old_invoice.total_internal_price, duplicate_invoice.total_internal_price)
  end
end
