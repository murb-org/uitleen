require "test_helper"

class UpdateCachesWorkerTest < ActiveSupport::TestCase
  test "doesn't schedule any new objects normally" do
    Finance.stub(:configure_twinfield, nil) do
      Twinfield::Customer.stub(:all, []) do
        initial_job_size = UpdateCustomerWorker.jobs.size
        UpdateCachesWorker.new.perform
        assert_equal(initial_job_size, UpdateCustomerWorker.jobs.size)
      end
    end
  end
end
