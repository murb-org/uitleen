require "test_helper"

class SendCreatedInvoiceWorkerTest < ActiveSupport::TestCase
  setup do
    Twinfield.configure do |config|
      config.company = 123
    end
  end

  def stub_get_time_span time_span
    stub_request(:get, "http://localhost:3000/api/v1/time_spans/#{time_span.uuid}")
      .to_return(body: {data: time_span.to_h}.to_json)
  end

  def trigger_send_created_invoice_worker
    Sidekiq::Testing.inline! do
      SendCreatedInvoiceWorker.perform_async(invoices(:finalized).id)
    end
  end

  test "generates an email with attachment" do
    invoice = Invoice.new(customer: customers(:one))
    assert_difference("ActionMailer::Base.deliveries.size", 1) do
      invoice.stub(:finalized?, true) do
        invoice.stub(:to_pdf, Rails.root.join("README.md")) do
          Invoice.stub(:find, invoice) do
            Finance.stub(:configure_twinfield, nil) do
              trigger_send_created_invoice_worker
            end
          end
        end
      end
    end
    mail = ActionMailer::Base.deliveries.last

    assert_match("# Uitleen", mail.attachments.first.body.to_s) # snippet from README.md
    assert_match("test@test.nl", invoice.customer.invoice_email_address)
  end
end
