require "test_helper"

class GenerateInvoicesWorkerTest < ActiveSupport::TestCase
  setup do
    Twinfield.configure do |config|
      config.company = 123
    end
  end

  def stub_get_time_span time_span
    stub_request(:get, "http://localhost:3000/api/v1/time_spans/#{time_span.uuid}")
      .to_return(body: {data: time_span.to_h}.to_json)
  end

  def trigger_generate_invoice_worker_perform_inlined(invoicenumber: 123)
    Sidekiq::Testing.inline! do
      new_sales_invoice = Twinfield::SalesInvoice.new(customer: 123, invoicetype: "FACTUUR", invoicenumber: invoicenumber)
      new_sales_invoice.stub :save, new_sales_invoice do
        Finance.stub(:configure_twinfield, nil) do
          Twinfield::SalesInvoice.stub :new, new_sales_invoice do
            SendCreatedInvoiceWorker.stub :perform_async, nil do
              Twinfield::SalesInvoice.stub :find, new_sales_invoice do
                GenerateInvoicesWorker.new.perform
              end
            end
          end
        end
      end
    end
  end

  test "doesn't generate invoices from undated invoices" do
    assert_difference("Invoice.count", 0) do
      trigger_generate_invoice_worker_perform_inlined
    end
  end

  test "does generate invoices from invoice from one month ago" do
    travel_to(Time.now.end_of_month + 2.day) do
      invoice = invoices(:old)
      invoice.finalized_at = 35.days.ago
      invoice.invoice_items.each { |a|
        a.invoicing_period_ends_at = Time.now.beginning_of_month - 1.day
        a.save
      }
      invoice.next_invoice_at = Time.now.beginning_of_month - 1.day

      stub_twinfield_invoice do
        invoice.save
      end
      stub_get_time_span CollectionManagement::TimeSpan.new(uuid: invoice_items(:rent).time_span_uuid)
      stub_get_time_span CollectionManagement::TimeSpan.new(uuid: invoice_items(:purchase).time_span_uuid, ends_at: 1.month.ago.to_s)

      assert_difference("Invoice.count", 1) do
        trigger_generate_invoice_worker_perform_inlined
      end
    end
  end

  test "it recreates the lines for a rent and does not generate new invoice for a completed sale" do
    travel_to(Time.now.end_of_month + 2.day) do
      invoice = invoices(:old)
      invoice.finalized_at = 35.days.ago
      invoice.invoice_items.each { |a|
        a.invoicing_period_ends_at = Time.now.beginning_of_month - 1.day
        a.save
      }
      invoice.next_invoice_at = Time.now.beginning_of_month - 1.day

      stub_twinfield_invoice do
        invoice.save
      end

      stub_get_time_span CollectionManagement::TimeSpan.new(uuid: invoice_items(:rent).time_span_uuid)
      stub_get_time_span CollectionManagement::TimeSpan.new(uuid: invoice_items(:purchase).time_span_uuid, ends_at: 1.month.ago.to_s)

      assert_equal(2, invoice.purchase_invoice_items.count)
      assert_equal(1, invoice.rent_invoice_items.count)

      invoice.stub(:fully_final?, true) do
        assert_equal(6, invoice.draft_invoice_lines.count) # 2 rent lines, 2 purchase lines regular, 2 consignation
      end

      trigger_generate_invoice_worker_perform_inlined

      new_invoice = invoice.next_invoice

      assert_equal(0, new_invoice.purchase_invoice_items.count)
      assert_equal(1, new_invoice.rent_invoice_items.count)

      new_invoice.stub(:fully_final?, true) do
        assert_equal(2, new_invoice.draft_invoice_lines.count) # rent lines
      end
    end
  end

  test "periods" do
    new_invoice = nil
    travel_to(Time.now.end_of_month + 2.day) do
      invoice = invoices(:old)
      invoice.finalized_at = 1.year.ago
      invoice.invoice_items.each { |a|
        a.invoicing_period_starts_at = 1.month.ago.beginning_of_month
        a.invoicing_period_ends_at = 1.month.ago.end_of_month
        a.save
      }
      invoice.next_invoice_at = 1.month.ago.end_of_month

      stub_twinfield_invoice do
        invoice.save
      end
      stub_get_time_span CollectionManagement::TimeSpan.new(uuid: invoice_items(:rent).time_span_uuid)
      stub_get_time_span CollectionManagement::TimeSpan.new(uuid: invoice_items(:purchase).time_span_uuid, ends_at: 1.month.ago.to_s)

      assert_equal(2, invoice.purchase_invoice_items.count)
      assert_equal(1, invoice.rent_invoice_items.count)

      invoice.stub(:fully_final?, true) do
        assert_equal(6, invoice.draft_invoice_lines.count) # 2 rent lines, 2 purchase lines regular, 2 consignation
      end

      trigger_generate_invoice_worker_perform_inlined

      new_invoice = invoice.next_invoice

      assert_equal(0, new_invoice.purchase_invoice_items.count)
      assert_equal(1, new_invoice.rent_invoice_items.count)
      assert_equal(Time.now.beginning_of_month.to_date, new_invoice.rent_invoice_items[0].invoicing_period_starts_at)
      assert_equal(Time.now.end_of_month.to_date, new_invoice.rent_invoice_items[0].invoicing_period_ends_at)
    end

    travel_to(Time.now.end_of_month + 20.day) do
      assert_difference("Invoice.count", 0) do
        trigger_generate_invoice_worker_perform_inlined(invoicenumber: 124)
      end
    end

    travel_to(Time.now.end_of_month + 33.day) do
      assert_difference("Invoice.count", 1) do
        trigger_generate_invoice_worker_perform_inlined(invoicenumber: 125)
      end

      newer_invoice = new_invoice.next_invoice
      assert_equal(1, newer_invoice.rent_invoice_items.count)
      assert_equal(Time.now.beginning_of_month.to_date, newer_invoice.rent_invoice_items[0].invoicing_period_starts_at)
      assert_equal(Time.now.end_of_month.to_date, newer_invoice.rent_invoice_items[0].invoicing_period_ends_at)
    end
  end

  test "it doesn't recreate an invoice if a follow up invoice has been created" do
    travel_to(Time.now.end_of_month + 2.day) do
      invoice = invoices(:old)
      invoice.finalized_at = 35.days.ago
      invoice.invoice_items.each { |a|
        a.invoicing_period_ends_at = Time.now.beginning_of_month - 1.day
        a.save
      }
      invoice.next_invoice_at = Time.now.beginning_of_month - 1.day
      stub_twinfield_invoice do
        invoice.save
      end
      stub_get_time_span CollectionManagement::TimeSpan.new(uuid: invoice_items(:rent).time_span_uuid)
      stub_get_time_span CollectionManagement::TimeSpan.new(uuid: invoice_items(:purchase).time_span_uuid, ends_at: 1.month.ago.to_s)

      Twinfield::SalesInvoice.new(customer: 123, invoicetype: "FACTUUR", invoicenumber: 202)

      assert_difference("Invoice.count", 1) do
        trigger_generate_invoice_worker_perform_inlined
      end

      assert_difference("Invoice.count", 0) do
        trigger_generate_invoice_worker_perform_inlined
      end
    end
  end

  test "it doesn't recreate an invoice if a it was put on hold" do
    travel_to(Time.now.end_of_month + 2.day) do
      invoice = invoices(:old)
      invoice.finalized_at = 35.days.ago
      invoice.invoice_items.each { |a|
        a.invoicing_period_ends_at = Time.now.beginning_of_month - 1.day
        a.save
      }
      invoice.next_invoice_at = Time.now.beginning_of_month - 1.day
      invoice.on_hold(users(:admin))
      stub_twinfield_invoice do
        invoice.save
      end
      # stub_get_time_span CollectionManagement::TimeSpan.new(uuid: invoice_items(:rent).time_span_uuid)
      # stub_get_time_span CollectionManagement::TimeSpan.new(uuid: invoice_items(:purchase).time_span_uuid, ends_at: 1.month.ago.to_s)

      Twinfield::SalesInvoice.new(customer: 123, invoicetype: "FACTUUR", invoicenumber: 202)

      assert_difference("Invoice.count", 0) do
        trigger_generate_invoice_worker_perform_inlined
      end
    end
  end

  test "it doesn't recreate the lines for a completed rent and does not generate new invoice for a completed sale" do
    invoice = invoices(:old)
    invoice.finalized_at = 35.days.ago
    stub_twinfield_invoice do
      invoice.save
    end
    stub_get_time_span CollectionManagement::TimeSpan.new(uuid: invoice_items(:rent).time_span_uuid, ends_at: 1.month.ago.to_s)
    stub_get_time_span CollectionManagement::TimeSpan.new(uuid: invoice_items(:purchase).time_span_uuid, ends_at: 1.month.ago.to_s)

    assert_equal(2, invoice.purchase_invoice_items.count)
    assert_equal(1, invoice.rent_invoice_items.count)

    invoice.stub(:fully_final?, true) do
      assert_equal(6, invoice.draft_invoice_lines.count) # 2 rent lines, 2 * 2 purchase lines
    end

    assert_difference("Invoice.count", 0) do
      trigger_generate_invoice_worker_perform_inlined
    end

    new_invoice = invoice.next_invoice

    assert_nil(new_invoice)
  end

  test "does generate invoices for invoices created one month ago" do
    invoice = invoices(:old)
    invoice.finalized_at = 35.days.ago
    stub_twinfield_invoice do
      invoice.save
    end
    stub_get_time_span CollectionManagement::TimeSpan.new(uuid: invoice_items(:rent).time_span_uuid)
    stub_get_time_span CollectionManagement::TimeSpan.new(uuid: invoice_items(:purchase).time_span_uuid, ends_at: 1.month.ago.to_s)

    trigger_generate_invoice_worker_perform_inlined
  end
end
