require "test_helper"

class UpdateTransactionCopiesWorkerTest < ActiveSupport::TestCase
  test "should perform" do
    Finance.stub :configure_twinfield, true do
      CostCenter.stub :update, true do
        CustomerTransaction.stub :update, true do
          assert(UpdateTransactionCopiesWorker.new.perform)
        end
      end
    end
  end

  test "returns false on a failure perform" do
    Finance.stub :configure_twinfield, true do
      CostCenter.stub :update, true do
        CustomerTransaction.stub :update, false do
          assert(!UpdateTransactionCopiesWorker.new.perform)
        end
      end
    end
  end
end
