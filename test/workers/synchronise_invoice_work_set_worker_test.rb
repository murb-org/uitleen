require "test_helper"

class SynchroniseInvoiceWorkSetWorkerTest < ActiveSupport::TestCase
  test "#update_invoice_items_with_invoiceable_item_collection when purchased work of interests have been found [CALLBACK]" do
    invoice = invoices(:work_set_based_invoice)
    works = [CollectionManagement::Work.new(id: "12", current_active_time_span: CollectionManagement::TimeSpan.new(contact_url: invoice.customer.uri, subject_type: "Work", classification: "purchase", status: "active", subject: CollectionManagement::Work.new(id: "12")).to_h)]
    CollectionManagement::WorkSet.stub(:find, CollectionManagement::WorkSet.new(works: works)) do
      invoice.save
    end
    invoice.reload
    assert_equal(1, invoice.invoice_items.length)
    assert(invoice.invoice_items.first.purchase_intent?)
  end

  test "#raises error when purchased work of interests have been found but customer is not set [CALLBACK]" do
    invoice = invoices(:work_set_based_invoice)
    works = [CollectionManagement::Work.new(id: "12", current_active_time_span: CollectionManagement::TimeSpan.new(subject_type: "Work", classification: "purchase", status: "active", subject: CollectionManagement::Work.new(id: "12")).to_h)]
    CollectionManagement::WorkSet.stub(:find, CollectionManagement::WorkSet.new(works: works)) do
      assert_raises(SynchroniseInvoiceWorkSetWorker::NoCustomerSetError) do
        invoice.save
      end
    end
    invoice.reload
    assert_equal(0, invoice.invoice_items.length)
  end

  test "#update_invoice_items_with_invoiceable_item_collection sets customer [CALLBACK]" do
    invoice = invoices(:work_set_based_invoice)
    invoice.update_columns(customer_id: nil)

    customer = customers(:four)
    works = [CollectionManagement::Work.new(id: "abc", current_active_time_span: CollectionManagement::TimeSpan.new(contact_url: customer.uri, subject_type: "Work", classification: "rental_outgoing", status: "active", subject: CollectionManagement::Work.new(id: "abc")).to_h)]
    CollectionManagement::WorkSet.stub(:find, CollectionManagement::WorkSet.new(works: works)) do
      invoice.save
    end
    invoice.reload
    customer.reload
    assert_equal(customer.uri, invoice.customer.uri)
  end

  test "#update_invoice_items_with_invoiceable_item_collection does not reset customer [CALLBACK]" do
    invoice = invoices(:work_set_based_invoice)
    original_customer_uri = invoice.customer.uri
    customer = customers(:four)
    assert_not_equal(original_customer_uri, customer.uri)
    works = [CollectionManagement::Work.new(id: "abc", current_active_time_span: CollectionManagement::TimeSpan.new(contact_url: customer.uri, subject_type: "Work", classification: "rental_outgoing", status: "active", subject: CollectionManagement::Work.new(id: "abc")).to_h)]
    CollectionManagement::WorkSet.stub(:find, CollectionManagement::WorkSet.new(works: works)) do
      assert_raises(SynchroniseInvoiceWorkSetWorker::CustomerMisMatchError) do
        invoice.save
      end
    end
    invoice.reload
    customer.reload
    assert_not_equal(customer.uri, invoice.customer.uri)
    assert_equal(original_customer_uri, invoice.customer.uri)
  end

  test "#update_invoice_items_with_invoiceable_item_collection puts invoice not to requires_review if work is already in list of invoice items [CALLBACK]" do
    invoice = invoices(:work_set_based_invoice_with_invoice_items)
    assert(invoice.invoice_items.length == 1)

    time_span = CollectionManagement::TimeSpan.new(uuid: "time_span_uuid", contact_url: invoice.customer.uri, subject_type: "Work", classification: "rental_outgoing", status: "active", subject: CollectionManagement::Work.new(id: "12"))
    works = [CollectionManagement::Work.new(id: "12", current_active_time_span: time_span.to_h)]
    CollectionManagement::WorkSet.stub(:find, CollectionManagement::WorkSet.new(works: works)) do
      invoice.save
    end

    invoice.reload

    assert(invoice.invoice_items.length == 1)
    assert(!invoice.requires_review?)
  end

  test "#update_invoice_items_with_invoiceable_item_collection puts invoice to requires_review if work is not in list of invoice items [CALLBACK]" do
    invoice = invoices(:work_set_based_invoice_with_invoice_items)
    assert(invoice.invoice_items.length == 1)

    time_span = CollectionManagement::TimeSpan.new(uuid: "time_span_uuid", contact_url: invoice.customer.uri, subject_type: "Work", classification: "rental_outgoing", status: "active", subject: CollectionManagement::Work.new(id: "ddd"))
    works = [CollectionManagement::Work.new(id: "ddd", current_active_time_span: time_span.to_h)]
    CollectionManagement::WorkSet.stub(:find, CollectionManagement::WorkSet.new(works: works)) do
      invoice.save
    end

    invoice.reload

    assert(invoice.requires_review?)
  end

  test "#update_invoice_items_with_invoiceable_item_collection when works of interests have been found and date in past [CALLBACK]" do
    invoice = invoices(:work_set_based_invoice)
    works = [CollectionManagement::Work.new(id: "abc", current_active_time_span: CollectionManagement::TimeSpan.new(contact_url: invoice.customer.uri, subject_type: "Work", classification: "rental_outgoing", status: "active", starts_at: Date.parse("2022-04-04"), subject: CollectionManagement::Work.new(id: "abc")).to_h)]

    CollectionManagement::WorkSet.stub(:find, CollectionManagement::WorkSet.new(works: works)) do
      invoice.save
    end
    invoice.reload
    assert_equal(1, invoice.invoice_items.length)
    assert_equal(Date.parse("2022-05-01"), invoice.invoice_items.first.invoicing_period_starts_at)
    assert(invoice.invoice_items.first.rent_intent?)
  end

  test "#update_invoice_items_with_invoiceable_item_collection when works of interests have been found and date in past, but already invoiced before [CALLBACK]" do
    invoice = invoices(:work_set_based_invoice)
    previous_invoice = invoices(:one)
    previous_invoice.invoice_items << InvoiceItem.create(invoiceable: CollectionManagement::Work.new(id: "abc"), intent: :rent, invoicing_period_ends_at: Date.parse("2023-03-31"))
    previous_invoice.save
    invoice.update_columns(previous_invoice_id: previous_invoice.id)

    # return
    works = [CollectionManagement::Work.new(id: "abc", current_active_time_span: CollectionManagement::TimeSpan.new(contact_url: invoice.customer.uri, subject_type: "Work", classification: "rental_outgoing", status: "active", starts_at: Date.parse("2022-04-04"), subject: CollectionManagement::Work.new(id: "abc")).to_h)]

    CollectionManagement::WorkSet.stub(:find, CollectionManagement::WorkSet.new(works: works)) do
      invoice.save
    end
    invoice.reload
    assert_equal(1, invoice.invoice_items.length)
    assert_equal(Date.parse("2023-04-01"), invoice.invoice_items.first.invoicing_period_starts_at)
    assert(invoice.invoice_items.first.rent_intent?)
  end

  test "#update_invoice_items_with_invoiceable_item_collection does not require review by default [CALLBACK]" do
    invoice = invoices(:work_set_based_invoice)
    works = [CollectionManagement::Work.new(id: "abc", current_active_time_span: CollectionManagement::TimeSpan.new(contact_url: invoice.customer.uri, subject_type: "Work", classification: "rental_outgoing", status: "active", subject: CollectionManagement::Work.new(id: "abc")).to_h)]

    CollectionManagement::WorkSet.stub(:find, CollectionManagement::WorkSet.new(works: works)) do
      assert_difference("Invoice.find(#{invoice.id}).works.count", 1) do
        invoice.save
      end
    end
    invoice.reload
    assert(!invoice.requires_review?)
  end
end
