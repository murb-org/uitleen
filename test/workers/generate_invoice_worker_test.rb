require "test_helper"

class GenerateInvoicesWorkerTest < ActiveSupport::TestCase
  setup do
    Twinfield.configure do |config|
      config.company = 123
    end
  end

  def stub_get_time_span time_span
    stub_request(:get, "http://localhost:3000/api/v1/time_spans/#{time_span.uuid}")
      .to_return(body: {data: time_span.to_h}.to_json)
  end

  def trigger_generate_invoice_worker_perform_inlined
    Sidekiq::Testing.inline! do
      GenerateInvoicesWorker.new.perform
    end
  end

  test "generates new invoice" do
    invoice = invoices(:old)
    new_invoice = Invoice.new(customer: invoices(:old).customer)
    Invoice.stub(:find, invoice) do
      new_invoice.stub(:finalize!, true) do
        invoice.stub(:initialize_next_invoice, new_invoice) do
          Finance.stub(:configure_twinfield, nil) do
            CollectionManagement::TimeSpan.stub :find, CollectionManagement::TimeSpan.new do
              assert(GenerateInvoiceWorker.new.perform(invoice.id))
            end
          end
        end
      end
    end
  end

  test "do not generate new invoice when on hold" do
    invoice = invoices(:old)
    invoice.on_hold(users(:admin))

    stub_twinfield_invoice do
      invoice.save
    end

    new_invoice = Invoice.new(customer: invoices(:old).customer)
    Invoice.stub(:find, invoice) do
      new_invoice.stub(:finalize!, true) do
        invoice.stub(:initialize_next_invoice, new_invoice) do
          Finance.stub(:configure_twinfield, nil) do
            CollectionManagement::TimeSpan.stub :find, CollectionManagement::TimeSpan.new do
              assert(!GenerateInvoiceWorker.new.perform(invoice.id))
            end
          end
        end
      end
    end
  end

  test "do generate new invoice when expired" do
    invoice = invoices(:old)

    new_invoice = Minitest::Mock.new
    new_invoice.expect(:finalize!, true)
    new_invoice.expect(:collect_mandate?, true)

    Invoice.stub(:find, invoice) do
      invoice.stub(:initialize_next_invoice, new_invoice) do
        Finance.stub(:configure_twinfield, nil) do
          invoice = GenerateInvoiceWorker.new.perform(invoice.id)
          assert(invoice)
        end
      end
    end

    new_invoice.verify
  end

  test "do generate, but do not finalize, invoice when customer has auto_finalize_invoices turned off" do
    invoice = invoices(:old)
    invoice.customer.auto_finalize_invoices = false
    invoice.customer.save

    new_invoice = Minitest::Mock.new
    Invoice.stub(:find, invoice) do
      invoice.stub(:initialize_next_invoice, new_invoice) do
        new_invoice.expect(:requires_review!, true)
        Finance.stub(:configure_twinfield, nil) do
          invoice = GenerateInvoiceWorker.new.perform(invoice.id)
          assert(invoice)
        end
        new_invoice.verify
      end
    end
  end

  test "it sends an error mail when it fails" do
    Sidekiq::Testing.server_middleware do |chain|
      chain.add Uitleen::SidekiqErrorLogger
    end

    assert_difference("ActionMailer::Base.deliveries.size", 1) do
      Finance.stub :configure_twinfield, nil do
        GenerateInvoiceWorker.perform_async(nil)
        assert_raise(ActiveRecord::RecordNotFound) do
          Sidekiq::Worker.drain_all
        end
      end
    end
  end
end
