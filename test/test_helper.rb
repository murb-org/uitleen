require "simplecov"
SimpleCov.start do
  add_filter %r{^/test/}
  add_filter %r{^/vendor/}
  add_filter %r{^/import/}
end

ENV["RAILS_ENV"] ||= "test"
require_relative "../config/environment"
require "rails/test_help"
require "minitest/mock"
require "webmock/minitest"
require "sidekiq/testing"
require_relative "../app/models/finance" # force stubbing finance

class ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  parallelize(workers: :number_of_processors)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  OmniAuth.config.test_mode = true

  parallelize_setup do |worker|
    SimpleCov.command_name "#{SimpleCov.command_name}-#{worker}"
  end

  parallelize_teardown do |_worker|
    SimpleCov.result
  end

  def sign_in(user)
    user = users(user) if user.is_a?(Symbol)
    OmniAuth.config.add_mock(:central_login, {uid: user.sub, credentials: {token: "token", refresh_token: "refresh_token", expires_at: 1.hour.from_now.to_i}, extra: {raw_info: {roles: user.roles, groups: user.groups, resources: user.resources}}})
    get "/auth/central_login/callback?response_type=code"
  end

  setup do
    stub_request(:post, "http://localhost:4000/oauth/token").with(body: {"client_id" => Rails.application.credentials.central_login_id, "client_secret" => Rails.application.credentials.central_login_secret, "grant_type" => "client_credentials"}).to_return(status: 200, body: {id_token: "fake.appid.token", expires_in: 7200}.to_json)
    stub_request(:post, "http://localhost:4000/oauth/token").with(body: {"client_id" => Rails.application.credentials.central_login_id, "client_secret" => Rails.application.credentials.central_login_secret, "grant_type" => "refresh_token", "refresh_token" => "old_refresh_token", "response_type" => "id_token"}).to_return(status: 200, body: {id_token: "fake.userid.token", expires_in: 7200}.to_json)
    stub_request(:post, "http://login.twinfield.com/auth/authentication/connect/token").with(body: {"grant_type" => "refresh_token", "refresh_token" => "MyString"}).to_return(body: {id_token: "fake.appid.token", expires_in: 7200}.to_json)
    stub_request(:post, "https://login.twinfield.com/auth/authentication/connect/token").with(body: {"grant_type" => "refresh_token", "refresh_token" => "MyString"}).to_return(body: {id_token: "fake.appid.token", expires_in: 7200}.to_json)

    Twinfield.configure do |config|
      config.company = 123
      config.session_type = "Twinfield::Api::OAuthSession"
      config.cluster = "http://testcluster.nl.example.com"
      config.access_token = "secretaccesstoken"
      config.log_level = :warn
    end

    User.update_all(access_token: "new_access_token", refresh_token: "new_refresh_token", expires_at: 2.hours.from_now, id_token: "new_id_token")
  end

  def stub_api_auth_response auth_response, &block
    JWT.stub :decode, [auth_response] do
      yield(block)
    end
  end

  def create_customer_savings customer, amount
    code = rand(10000000)
    number = rand(10000000)
    source = "truth"
    customer.gift_card_transactions.create!(value: -amount, code: code, number: number, source: source)
    CustomerTransaction.create!(code: code, number: number, source: source, customer_code: customer.external_customer_id)
  end

  def stub_create_twinfield_invoice invoice: Invoice.new
    index = 0
    lines = invoice.invoice_items.map do |invoice_item|
      invoice_item.to_twinfield_invoice_lines.map do |twinfield_invoice_item|
        index += 1
        twinfield_invoice_item.id = index
        if twinfield_invoice_item.unitspriceexcl
          vat_multiplier = (twinfield_invoice_item.article == "CADEAUTGD") ? 1 : 1.21
          valueinc = (twinfield_invoice_item.quantity * twinfield_invoice_item.unitspriceexcl * vat_multiplier).round(2)
          twinfield_invoice_item.to_xml.sub("</line>", "<valueinc>#{valueinc}</valueinc></line>")
        else
          twinfield_invoice_item.to_xml
        end
      end.join("\n")
    end.join("\n")

    invoice_number = "20220901-#{rand(100)}"

    xml_invoice = "<salesinvoice result=\"1\" raisewarning=\"false\" autobalancevat=\"true\">\n  <header>\n    <office>123</office>\n    <invoicetype>FACTUUR</invoicetype>\n    <invoicedate>20220901</invoicedate>\n<invoicenumber>#{invoice_number}</invoicenumber>    <duedate>20221001</duedate>\n    <bank/>\n    <invoiceaddressnumber>1</invoiceaddressnumber>\n    <deliveraddressnumber>1</deliveraddressnumber>\n    <customer>123</customer>\n    <currency>EUR</currency>\n    <status>final</status>\n    <paymentmethod/>\n    <headertext/>\n    <footertext/>\n  </header>\n  <financials><code>VRK</code><number>#{invoice_number}</number> </financials><lines>\n   #{lines}  </lines>\n</salesinvoice>"
    stub_request(:post, "https://accounting.twinfield.com/webservices/processxml.asmx")
      .with(
        headers: {
          "Soapaction" => '"http://www.twinfield.com/ProcessXmlString"'
        }
      )
      .to_return(status: 200, body: "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">
  <soap12:Body>
    <ProcessXmlStringResponse xmlns=\"http://www.twinfield.com/\">
      <ProcessXmlStringResult><![CDATA[#{xml_invoice}]]></ProcessXmlStringResult>
    </ProcessXmlStringResponse>
  </soap12:Body>
</soap12:Envelope>", headers: {})
  end

  def stub_twinfield_invoice customer: Twinfield::Customer.new(**customers(:one).twinfield_raw), invoice: nil, &block
    invoice ||= Twinfield::SalesInvoice.new(invoicetype: "FACTUUR", customer_code: customer.code)

    invoice.stub(:save, invoice) do
      Twinfield::SalesInvoice.stub :find, invoice do
        yield(block)
      end
    end
  end

  def stub_twinfield customer: Twinfield::Customer.new(**customers(:one).twinfield_raw), transactions: [], return_on: [], &block
    # Twinfield.configuration = Twinfield::Configuration.new
    OmniAuth.config.add_mock(:twinfield, {
      uid: "twinfield_sub",
      credentials: {token: "token", refresh_token: "abc", expires_at: 1.hour.from_now.to_i}
    })

    customer.stub :save, customer do
      Twinfield::Request::List.stub :offices, [{}] do
        Twinfield::Customer.stub :find, customer do
          Twinfield::Customer.stub :next_unused_twinfield_customer_code, :next_code do
            Twinfield::Browse::Transaction::Customer.stub :where, transactions do
              Finance.stub :configure_twinfield, nil do
                get "/auth/twinfield/callback?response_type=code" if defined?(get)
                yield(block)
              end
            end
          end
        end
      end
    end
  end

  def stub_collection_management collections: [CollectionManagement::Collection.new(id: 1, name: "Collection")], works: [CollectionManagement::Work.new(id: 1, collection_id: 1, title_rendered: "Work title")], work: CollectionManagement::Work.new(id: 1, collection_id: 1, title_rendered: "Work title", selling_price: 1000, default_rent_price: 14, business_rent_price_ex_vat: 12.4), &block
    CollectionManagement::Collection.stub :all, collections do
      CollectionManagement::Work.stub :where, works do
        CollectionManagement::Work.stub :find, work do
          yield(block)
        end
      end
    end
  end

  def stub_create_event time_span: CollectionManagement::TimeSpan.new(uuid: 123)
    stub_request(:post, "http://localhost:3000/api/v1/collections/13/works/123/work_events")
      .to_return({body: {data: time_span.to_h}.to_json})
  end
end
