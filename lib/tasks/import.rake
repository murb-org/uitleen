require File.join(Rails.root, "import", "models.rb")

# rails import:prepare # should typically be run locally, mapping to be shared among environments
# rails import:reservations # creates reservations
# rails import:qkunst_combine # prepares a QKunst import file (to be uploaded manually)
#        double check: are time spans generated for sold works now?!
# rails import:customers # creates customers in twinfield and in uitleen
# rails import:invoices # imports the invoices

dry_run = false
namespace :import do
  desc "Prepare shared uuids"
  task prepare: :environment do
    Kunstwerk.update_nummer_uuid_mapping
    Relatie.update_nummer_uuid_mapping
  end
  desc "Customer.count"
  task customers: :environment do
    Finance.configure_twinfield

    start_at = 10000
    end_at = 990000020
    Customer.update_all(old_data: nil)

    Lener.all.sort_by(&:external_customer_id).each do |lener|
      print "."
      if lener.external_customer_id >= start_at && lener.external_customer_id <= end_at
        customer = Customer.find_or_initialize_by(external_customer_id: lener.external_customer_id)
        twinfield_params = (customer.twinfield_raw || {}).deep_merge(lener.to_twinfield_hash)

        twinfield_customer = Twinfield::Customer.new(**twinfield_params)
        twinfield_customer.code ||= Twinfield::Customer.next_unused_twinfield_customer_code unless dry_run
        (twinfield_customer.save ? "" : "-FAIL!!!:#{lener.external_customer_id}-") unless dry_run

        customer.attributes = lener.to_customer_hash
        customer.external_customer_id = twinfield_customer.code
        customer.twinfield_raw = twinfield_customer.to_h
        (customer.save ? "" : "-FAIL!!!:#{lener.external_customer_id}-") unless dry_run
      end
    end
  end

  desc "Prepare QKunst import file; usage: rails import:qkunst_combine output.json"
  task qkunst_combine: :environment do
    collection_name = ARGV[0] || nil
    filename = "#{["works-all-merged", collection_name].compact.join("-")}.json"

    filename_sample = filename.sub(".json", "_sample.json")
    print "Writing to #{filename}..."
    CollectionManagement::Work.collection_name = collection_name
    hash = CollectionManagement::Work.all.map(&:merged_json_hash)
    File.write(filename_sample, hash[0..9].to_json)
    File.write(filename, hash.to_json)

    puts " done."
  end

  desc "Prepare QKunst import file; usage: rails import:qkunst_combine output.json"
  task qkunst_combine_with_just_old_data: :environment do
    collection_name = ARGV[0] || nil
    filename = "#{["works-all-merged", collection_name].compact.join("-")}.json"

    filename_sample = filename.sub(".json", "_sample.json")
    print "Writing to #{filename}..."
    CollectionManagement::Work.collection_name = collection_name
    hash = CollectionManagement::Work.all.map(&:merged_json_with_just_old_data)
    File.write(filename_sample, hash[0..9].to_json)
    File.write(filename, hash.to_json)

    puts " done."
  end

  desc "Only works that weren't part of QKunst migration"
  task post_qkunst_works: :environment do
    CollectionManagement::Work.collection_name = "Post QKunst"

    kunstwerken = Kunstwerk.all

    data = {data: kunstwerken.select { |w| w.datum_in_c && w.datum_in_c >= Date.new(2020, 1, 9) && w.kunsten_nr != "QIN" }.collect { |w| w.to_qkunst_werk }}.to_json
    File.write("new-works.json", data)
  end

  desc "Prepare QKunst import file for works that were not in QKunst CollectionManagement"
  task new_works: :environment do
    qkunst_ids = []

    ["actief", "archief", "ontzamelen", "westland"].each do |collection_name|
      CollectionManagement::Work.collection_name = collection_name
      qkunst_ids += CollectionManagement::Work.all.collect { |a| CollectionManagement::Work::QIN_LOOKUP_INVERSE[a.stock_number] || a.stock_number }
    end

    kids_ids = Kunstwerk.all.collect(&:kunstwerkn)
    missing = kids_ids - qkunst_ids
    materialen = []
    technieken = []
    techngroep = []

    work_data = []
    werken = []
    missing.each do |kunstwerkn|
      kunstwerk = Kunstwerk.find_by_number(kunstwerkn)
      work_data << kunstwerk.to_qkunst_werk
      werken << kunstwerk
      materialen << kunstwerk.materiaal.split(",").collect(&:strip)
      technieken << kunstwerk.techniques
      techngroep << kunstwerk.techngroep
    end

    File.write("works-all-merged-actief-new.json", work_data.to_json)

    binding.irb # rubocop:disable Lint/Debugger
  end

  desc "Import invoices"
  task invoices: :environment do
    start = 0
    stop = 5000 # max = ~4931
    Finance.configure_twinfield

    puts "Starts processing #{Factuur.all_after_date[start..stop].count} invoices"
    Factuur.all_after_date.each do |factuur|
      if factuur.customer
        invoice = Invoice.find_or_initialize_by(import_id: factuur.import_id)
        if !invoice.fully_final?
          invoice.attributes = factuur.to_invoice_hash
          if invoice.save
            puts "  Invoice #{factuur.import_id} opgeslagen (total €#{invoice.total_internal_price.to_f.round(2)})"
          else
            puts "! Invoice #{factuur.import_id} werd niet opgeslagen. #{factuur.to_invoice_hash} => #{p invoice.errors.messages} "
          end
        else
          puts "- Invoice #{factuur.import_id} niet opnieuw opgeslagen (total €#{invoice.total_internal_price.to_f.round(2)}) p"
        end
      else
        puts "! Geen klant gevonden voor #{factuur.import_id}"
      end
    rescue ActiveRecord::RecordNotUnique
      puts "  Already imported #{factuur.import_id}"
    rescue ActiveRecord::RecordNotFound => e
      puts factuur.import_id
      puts factuur.to_invoice_hash
      raise e
    end

    puts "Done"
  end

  desc "Duplicate invoices to twinfield"
  task invoices_twinfield: :environment do
    Finance.configure_twinfield
    start = 0
    stop = 6000 # max = ~4931

    puts "Starts processing #{Factuur.all_after_date[start..stop].count} invoices"
    Factuur.all_after_date[start..stop].each do |factuur|
      if factuur.customer
        invoice = Invoice.find_or_initialize_by(import_id: factuur.import_id)
        invoice.attributes = factuur.to_invoice_hash
        if invoice.save
          puts "  Invoice #{factuur.import_id} opgeslagen (total €#{invoice.total_price.to_f.round(2)})"

          unless invoice.has_external_invoice? && (invoice.external_invoice.status == "final")
            invoice.save_external_invoice! # (true)
            puts "    Twinfield invoice aangemaakt (nieuw nummer: #{invoice.twinfield_invoicenumber})"
          end

          if invoice.has_external_invoice? && !factuur.factuur_open? && (invoice.external_invoice&.transaction&.status == "available")
            puts "    Marking #{factuur.import_id} as paid in Twinfield as open amount == 0.0"
            invoice.create_payment_transaction_and_match!(:old)
          end
        else
          puts "  Invoice #{factuur.import_id} werd niet opgeslagen. #{factuur.to_invoice_hash} "
        end

      else
        puts "! Geen klant gevonden voor #{factuur.import_id}"
      end
    rescue ActiveRecord::RecordNotUnique
      puts "  Already imported #{factuur.import_id}"
    rescue ActiveRecord::RecordNotFound => e
      puts factuur.import_id
      puts factuur.to_invoice_hash
      raise e
    end

    puts "Done"
  end

  desc "Debugging (irb)"
  task debug: :environment do
    Finance.configure_twinfield
    binding.irb # rubocop:disable Lint/Debugger
  end

  task temp_savings: :environment do
    leners = Lener.all.select { |a| a.opgebouwd_ > 0 }

    ct_ccs = leners.collect do |a|
      customer = Customer.find_by_external_customer_id(a.external_customer_id)
      value = -a.opgebouwd_

      # correct for bookings that will be matched in twinfield
      if customer
        value = -a.opgebouwd_ - customer.saving_transactions.where(yearperiod: ["2022/01", "2022/02", "2022/03", "2022/04", "2022/05", "2022/06"]).where(source: "twinfield").sum(&:value)
      end

      ct = CustomerTransaction.find_or_initialize_by(source: "kids", code: "VRK", currency: "EUR", customer_code: a.external_customer_id, date: Date.new(2022, 6, 30), invoice_number: "kids-import", key: "kids-import-#{a.external_customer_id}-savings", number: "kids-import-#{a.external_customer_id}-savings", open_value: 0, status: :final)
      ct.value = value
      cc = CostCenter.find_or_initialize_by(source: ct.source, number: ct.number, code: ct.code, currency: ct.currency, key: ct.key, yearperiod: "2022/06", dim1: [Rails.application.config_for(:uitleen)[:cost_center][:customer_savings]].flatten.first, dim2: "00000", status: :final)
      cc.value = value
      [ct, cc]
    end

    ct_ccs.each { |a| a.each(&:save) }
  end

  task temp_gift_credit: :environment do
    leners = Lener.all.select { |a| a.cadeautego > 0 }

    ct_ccs = leners.collect do |a|
      customer = Customer.find_by_external_customer_id(a.external_customer_id)
      value = -a.cadeautego

      # correct for bookings that will be matched in twinfield
      if customer
        value = -a.cadeautego - customer.gift_card_transactions.where(yearperiod: ["2022/01", "2022/02", "2022/03", "2022/04", "2022/05", "2022/06"]).where(source: "twinfield").sum(&:value)
      end

      ct = CustomerTransaction.find_or_initialize_by(source: "kids", code: "VRK", currency: "EUR", customer_code: a.external_customer_id, date: Date.new(2022, 6, 30), invoice_number: "kids-import", key: "kids-import-#{a.external_customer_id}-giftcredit", number: "kids-import-#{a.external_customer_id}-giftcredit", open_value: 0, status: :final)
      ct.value = value
      cc = CostCenter.find_or_initialize_by(source: ct.source, number: ct.number, code: ct.code, currency: ct.currency, key: ct.key, yearperiod: "2022/06", dim1: Rails.application.config_for(:uitleen)[:cost_center][:gift_card_credit], dim2: "00000", status: :final)
      cc.value = value
      [ct, cc]
    end

    ct_ccs.each { |a| a.each(&:save) }
  end

  desc "Missing imports, writes missing jsons to new missing_merged.json"
  task qkunst_combine_missing_imports: :environment do
    # via API
    current_id_stock_numbers = CollectionManagement::Work.where(collection_id: 13, pluck: [:id, :stock_number])
    current_stock_numbers = current_id_stock_numbers.map { |a| a[1] }

    # from JSON
    all_stock_numbers = CollectionManagement::Work.all.map(&:stock_number)

    missing_stock_numbers = all_stock_numbers - current_stock_numbers

    filename = ARGV[0] || "missing_merged.json"
    print "Writing to #{filename}..."
    File.write(filename, CollectionManagement::Work.all.select { |a| missing_stock_numbers.include?(a.stock_number) }.map(&:merged_json_hash).to_json)
    puts " done."
  end

  desc "generate artist desc code"
  task :qkunst_artist_descriptions do
    CSV.parse(File.read("import/artist_descriptions.csv")).each do |a|
      puts "Artist.find_by_rkd_artist_id(#{a[5]})&.update(description: \"#{a[2]}\")"
    end
  end
end
