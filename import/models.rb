require "active_record"

def rtf_to_txt rtf
  if rtf.is_a?(Unrich::Text)
    rtf
  elsif rtf
    Unrich::Text.read(rtf)
  end
end

require "dbf"
require "active_model"
require_relative "../app/lib/collection_management/work"
require_relative "../app/models/twinfield/uitleen_extensions"

module CollectionManagement
  class Work
    QIN_LOOKUP = File.exist?(File.join(__dir__, "additional_mappings.json")) ? JSON.parse(File.read(File.join(__dir__, "additional_mappings.json"))) : {}
    QIN_LOOKUP_INVERSE = QIN_LOOKUP.invert

    def kunstwerkn
      QIN_LOOKUP_INVERSE[stock_number] || stock_number
    end

    def kids_work
      @kids_work ||= ::Kunstwerk.find_by_number(kunstwerkn)
    end

    # File activemodel/lib/active_model/model.rb, line 80
    def initialize(attributes = {})
      assign_attributes(attributes) if attributes
      @original_json_hash = attributes
      super()
    end

    attr_reader :original_json_hash

    def significantly_updated_at
      DateTime.parse(@significantly_updated_at) if @significantly_updated_at
    end

    def additional_json
      kids_work ? kids_work.additional_json(significantly_updated_at: significantly_updated_at) : {}
    end

    def archive_hash
    end

    def merged_json_hash
      original_json_hash.merge(additional_json)
    end

    def merged_json_with_just_old_data
      old_data = kids_work ? {old_data: kids_work.old_data} : {}
      original_json_hash.merge(old_data)
    end

    class << self
      def collection_name= cn
        @@collection_name = cn
      end

      def qkunst_collection_management_data(collection_name = @@collection_name)
        @@qkunst_collection_management_data ||= {}
        base_file = "works-all-#{collection_name}.json"

        @@qkunst_collection_management_data[base_file] ||= (File.exist?(File.join(__dir__, base_file)) ? JSON.parse(File.read(File.join(__dir__, base_file)))["data"] : [])
      end

      def stock_number_to_qkunst_id stock_number
        stock_number = QIN_LOOKUP_INVERSE[stock_number] || stock_number
        data = ["actief", "afstoten", "archief", "westland"].map do |collection_name|
          qkunst_collection_management_data(collection_name).find { |a| a["stock_number"] == stock_number }
        end.compact.first
        data["id"] if data
      end

      def find_by_number stock_number
        stock_number = QIN_LOOKUP_INVERSE[stock_number] || stock_number
        data = qkunst_collection_management_data.find { |a| a["stock_number"] == stock_number }
        new(data) if data
      end

      def all
        qkunst_collection_management_data.map { |a| new(a) }
      end
    end
  end
end

class DBF::Table
  # overridden; as many deleted records shouldn't be considered deleted.
  def deleted_record? # :nodoc:
    # flag = @data.read(1)
    # # flag ? flag.unpack1('a') == '*' : true
    false
  end
end

Dir.glob(File.join(__dir__, "*.DBF")).each do |file|
  csv_name = file.gsub(".DBF", ".csv")
  unless File.exist?(csv_name)
    table = DBF::Table.new(file, nil, "cp1252")
    table.to_csv(csv_name)
    puts "#{csv_name} done."
  end
end

module DbfBacked
  def dbf
    @@dbf = DBF::Table.new(File.join(__dir__, self::DBF_NAME), nil, "cp1252")
  end

  def length
    dbf.find(:all).count
  end

  def count
    dbf.find(:all).count
  end

  def all
    @all ||= find(:all)
  end

  def dbf_attributes
    dbf.find(0).attributes.keys.map(&:downcase).map(&:to_sym)
  end

  def find id, options = {}
    record = dbf.find(id, options)
    if !record
      puts "#{self::DBF_NAME} with #{options} not found..."
      nil
    elsif record.is_a? Array
      record.map { |a| new(a.attributes.map { |k, v| [k.downcase.to_sym, v] }.to_h) }
    else
      new(record.attributes.map { |k, v| [k.downcase.to_sym, v] }.to_h)
    end
  end
end

class Kunstwerk
  include ActiveModel::Model
  extend DbfBacked

  TIME_SPAN_UUID_MAPPING_VOOR_UITLEEN = File.exist?(File.join(__dir__, "timespan_uuid_mapping.json")) ? JSON.parse(File.read(File.join(__dir__, "timespan_uuid_mapping.json"))) : {}

  ATTRIBUTES = [:aangekocht, :aangekdoor, :aangek_van, :aangek_crn, :aangeen_in, :aangeen_op, :actie, :afbeelding, :afb_brdte, :afb_hgte, :afdracht_p, :barcodenum, :brdte, :btw_code, :btw_code_i, :datum_in_c, :datum_inli, :datum_tijd, :datum_uit_, :depot_loca, :diepte, :eigenaar, :etiket_pri, :filiaal, :foto_maken, :galleriere, :gewij_door, :gewijzigd_, :groep_id, :herkomst, :hgte, :huur_bet_t, :huur_betaa, :huur_bij_v, :huur_jn, :huur_max_p, :huurpecent, :ingelijst_, :ingel_z_gl, :ingev_door, :ingevoerd_, :inkoop, :inkoop_dat, :inkoop_fak, :internet, :jaar_van_c, :kerncectie, :kleuren, :kleursfeer, :kortiprijs, :kosten1, :kosten2, :kosten3, :kosteroep1, :kosteroep2, :kosteroep3, :kunsten_nr, :kunstuitle, :kunst_code, :kunstwerkn, :lijst, :lijst_eige, :lijst_form, :lijst_inko, :lijstkleur, :lijst_soor, :lijsttglas, :margeregel, :materiaal, :mee_naar_k, :memo, :memo_eschr, :memory, :naar_lijst, :omzet_plaa, :omzetreken, :ondergrond, :onderwerp, :op_slot, :oplage, :oplagenumm, :oudnummer, :plaats_bij, :produoepid, :projectnum, :pvlock, :pvsysdt, :pvsysid, :recnummer, :reden_tijd, :reden_uit_, :retour_op, :retourheri, :retourher2, :retourher3, :serieid, :setid, :soort_kw, :taxeren, :te_huur, :te_koop, :techniek, :techngroep, :tijdelijk_, :titel, :trefwoorde, :uit_aan, :uit_collec, :uit_op, :uitgelicht, :verh_code, :verh_grati, :verh_met_s, :verhuur_fa, :verkocht_a, :verkoop_in, :verzekerde, :verzeosten]
  DBF_NAME = "kunstwerken.DBF"

  attr_accessor(*ATTRIBUTES)

  def kunstenaar
    Kunstenaar.find_by_kunstenaar_nummer(kunsten_nr)
  end

  def kunstenaar_relatie
    Relatie.find_by_kunstenaar_nummer(kunsten_nr)
  end

  def import_id
    stock_number
  end

  def lener
    Lener.find_by_relatie_nummer(uit_aan)
    # Lener.find(:first, relatienum: uit_aan)
  end

  def memo_eschr
    rtf_to_txt(@memo_eschr)
  end

  def stock_number
    CollectionManagement::Work::QIN_LOOKUP[kunstwerkn] || kunstwerkn
  end

  def minimal_object_cache
    rv = {
      stock_number: stock_number,
      artist_name_rendered_without_years_nor_locality: kunstenaar&.artiesnaam || "-",
      title_rendered: titel
    }
    if eigenaar.to_i > 0
      rv["owner_id"] = 4
      rv["owner"] = {"name" => "Geen eigendom (Consignatie)", "id" => 4, "creating_artist" => true}
    end
    rv
  end

  def qkunst_werk
    QKunstWerk.find_by_kunstwerkn(kunstwerkn)
  end

  def leenhistories
    Leenhistorie.find_all_by_kunstwerknummer(kunstwerkn)
  end

  def uit_aan_key
    if uit_aan.to_i > 0
      "#{uit_aan}||#{uit_op}||#{stock_number}"
    elsif uit_collec && verkocht_a
      "#{uit_collec}||#{verkocht_a}||#{stock_number}"
    end
  end

  def techniques
    ts = techniek.split(/[,;]|\sand\s|\sen\s/).collect(&:strip)
    ts += materiaal.split(/[,;]|\sand\s|\sen\s/).collect(&:strip)

    trans = {
      "Olieverf op linnen" => "Olieverf",
      "Schilderij" => nil,
      "Spraypaint" => "Spray-paint",
      "Onbekend" => nil,
      "(Wand)object" => "Wandobject",
      "Overige techniek" => nil,
      "Polaroiddruk" => "Polaroid",
      "Inkjet print" => "Inktjetprint",
      "Inkjetprint" => "Inktjetprint",
      "Archival ink on rag paper" => "Archiefinkt",
      "Olieverf op canvas" => "Olieverf",
      "archival pigment print" => "Archiefinkt",
      "Acryl" => "Acrylverf",
      "olieverf" => "Olieverf",
      "Divers" => "Gemengde techniek",
      "canvas en staal" => "Staal",
      "canvas" => nil,
      "Fine art print" => nil
    }

    ts.collect { |a| trans.key?(a) ? trans[a] : a }.compact.uniq
  end

  def medium
    ts = ondergrond.split(/[,;]|\sand\s|\sen\s/).collect(&:strip)

    ts += materiaal.split(/[,;]|\sand\s|\sen\s/).collect(&:strip)

    trans = {
      "archival pigment print" => nil,
      "Acryl en olieverf" => nil,
      "Inkjetprint" => nil,
      "Spraypaint" => nil,
      "Divers" => nil,
      "Canvas en staal" => "Canvas",
      "Oil on canvas" => "Canvas",
      "chalk" => nil,
      "Polaroiddruk op folie" => "Folie",
      "Papier" => "Papier",
      "Gemengd" => nil,
      "Fine art print" => nil,
      "Archival ink on rag paper" => "Papier",
      "Olieverf" => nil,
      "linnen" => "Linnen",
      "Kunststof" => "Kunststof",
      "glas" => "Glas",
      "papier" => "Papier",
      "Hahnemühler" => "Papier"
    }

    ts.collect { |a| trans.key?(a) ? trans[a] : a }.compact.uniq.first
  end

  def medium_comments
    [ondergrond, materiaal].join(" ")
  end

  def object_category
    rv = techngroep
    trans = {
      "Schilderij" => "Schilderkunst",
      "Werk op papier" => "Uniek werk op papier",
      "Ruimtelijk" => "Sculptuur"
    }

    rv = trans[rv] || rv
    rv if rv.present?
  end

  def to_qkunst_werk
    rv = {}
    rv["stock_number"] = stock_number
    rv["artist_name_rendered_without_years_nor_locality"] = (kunstenaar&.artiesnaam || "-")
    rv["title"] = titel
    rv["title_rendered"] = titel
    rv["location"] = filiaal
    floor_detail_match = depot_loca.to_s.match(/(Depot\s.?\s)?(.*)/)
    rv["location_floor"] = floor_detail_match[1]
    rv["location_detail"] = floor_detail_match[2]
    rv["medium"] = {"name" => medium} if medium
    rv["medium_comments"] = medium_comments unless medium_comments == medium
    rv["object_categories"] = [{"name" => object_category}] if object_category
    rv["techniques"] = techniques.collect { |a| {"name" => a} }
    rv["print"] = objprint if objprint
    relatie = kunstenaar&.relatie
    rv["artists"] = [
      {
        "first_name" => [relatie&.voornaam, relatie&.voornaam2].compact.map(&:strip).select(&:present?).join(" "),
        "prefix" => [relatie&.tussenvoeg, relatie&.tussenvoe2].compact.map(&:strip).select(&:present?).join(" "),
        "last_name" => [relatie&.achternaam, relatie&.achternaa2].compact.map(&:strip).select(&:present?).join(" "),
        "date_of_birth" => kunstenaar&.geboorteda || nil,
        "place_of_birth" => kunstenaar&.geboortepl,
        "year_of_death" => kunstenaar&.datum_over || nil
      }
    ]
    rv["height"] = hgte if hgte && hgte > 0
    rv["width"] = brdte if brdte && brdte > 0
    rv["depth"] = diepte if diepte && diepte > 0
    rv["frame_height"] = afb_hgte if afb_hgte && afb_hgte > 0
    rv["frame_width"] = afb_brdte if afb_brdte && afb_brdte > 0
    rv["purchase_price"] = inkoop
    rv["purchased_on"] = inkoop_dat
    rv["object_creation_year"] = jaar_van_c.to_i
    rv["internal_comments"] = [(aangekdoor.present? ? "Aangekocht door #{aangekdoor}." : nil), ((inkoop.to_i > 0) ? "Aankoopprijs excl. btw" : nil), rtf_to_txt(memo)&.to_s].compact.join("\n\n")
    rv["tag_list"] = ["Import #{Time.now.to_date.iso8601}"]
    # ATTRIBUTES = [:aangekocht, :aangekdoor, :aangek_van, :aangek_crn, :aangeen_in, :aangeen_op, :actie, :afbeelding, :afb_brdte, :afb_hgte, :afdracht_p, :barcodenum, :brdte, :btw_code, :btw_code_i, :datum_in_c, :datum_inli, :datum_tijd, :datum_uit_, :depot_loca, :diepte, :eigenaar, :etiket_pri, :filiaal, :foto_maken, :galleriere, :gewij_door, :gewijzigd_, :groep_id, :herkomst, :hgte, :huur_bet_t, :huur_betaa, :huur_bij_v, :huur_jn, :huur_max_p, :huurpecent, :ingelijst_, :ingel_z_gl, :ingev_door, :ingevoerd_, :inkoop, :inkoop_dat, :inkoop_fak, :internet, :jaar_van_c, :kerncectie, :kleuren, :kleursfeer, :kortiprijs, :kosten1, :kosten2, :kosten3, :kosteroep1, :kosteroep2, :kosteroep3, :kunsten_nr, :kunstuitle, :kunst_code, :kunstwerkn, :lijst, :lijst_eige, :lijst_form, :lijst_inko, :lijstkleur, :lijst_soor, :lijsttglas, :margeregel, :materiaal, :mee_naar_k, :memo, :memo_eschr, :memory, :naar_lijst, :omzet_plaa, :omzetreken, :ondergrond, :onderwerp, :op_slot, :oplage, :oplagenumm, :oudnummer, :plaats_bij, :produoepid, :projectnum, :pvlock, :pvsysdt, :pvsysid, :recnummer, :reden_tijd, :reden_uit_, :retour_op, :retourheri, :retourher2, :retourher3, :serieid, :setid, :soort_kw, :taxeren, :te_huur, :te_koop, :techniek, :techngroep, :tijdelijk_, :titel, :trefwoorde, :uit_aan, :uit_collec, :uit_op, :uitgelicht, :verh_code, :verh_grati, :verh_met_s, :verhuur_fa, :verkocht_a, :verkoop_in, :verzekerde, :verzeosten]

    rv.merge(additional_json)
  end

  def objprint
    rv = "#{oplagenumm}/#{oplage}".strip
    unless ["/", "", "1/1"].include? rv
      "#{oplagenumm}/#{oplage}"
    end
  end

  def additional_json(significantly_updated_at: nil)
    rv = {}
    if eigenaar.to_i > 0
      raise if rv["owner_id"] && rv["owner_id"] != 4 # owner_id should be 4, otherwise contesting
      rv["owner_id"] = 4
      rv["owner"] = {"name" => "Geen eigendom (Consignatie)", "id" => 4, "creating_artist" => true}
    end

    if gewijzigd_ && gewijzigd_ > "2020-08-09".to_date || qkunst_werk.nil?
      puts "!!!! ALERT Kunstwerknummer #{kunstwerkn} has been significantly updated #{significantly_updated_at} > #{gewijzigd_}" if significantly_updated_at && significantly_updated_at > gewijzigd_
      rv["location"] = filiaal
      floor_detail_match = depot_loca.to_s.match(/(Depot\s.?\s)?(.*)/)
      rv["location_floor"] = floor_detail_match[1]
      rv["location_detail"] = floor_detail_match[2]
    end
    rv["public_description"] = rtf_to_txt(memo_eschr)&.to_s
    rv["for_purchase_at"] = (te_koop && !uit_collec) ? gewijzigd_ : nil
    rv["for_rent_at"] = (te_huur && !uit_collec) ? gewijzigd_ : nil
    rv["selling_price"] = verkoop_in
    rv["time_spans"] = leenhistories.map(&:to_time_span_hash)
    if uit_aan.to_i > 0
      rv["time_spans"] << {
        contact: {
          external: true,
          url: "http://localhost:5001/customers/#{Relatie::RELATIENUMMER_UUID_MAPPING[uit_aan.to_i.to_s]["uuid"]}"
        },
        starts_at: uit_op,
        status: :active,
        classification: :rental_outgoing,
        uuid: Kunstwerk::TIME_SPAN_UUID_MAPPING_VOOR_UITLEEN[uit_aan_key]
      }
    end
    if uit_collec && verkocht_a.to_i > 0
      time_span = {
        starts_at: datum_uit_,
        status: :active,
        classification: :purchase,
        uuid: Kunstwerk::TIME_SPAN_UUID_MAPPING_VOOR_UITLEEN[uit_aan_key]
      }
      if Relatie::RELATIENUMMER_UUID_MAPPING[verkocht_a.to_i.to_s]
        time_span[:contact] = {
          external: true,
          url: "http://localhost:5001/customers/#{Relatie::RELATIENUMMER_UUID_MAPPING[verkocht_a.to_i.to_s]["uuid"]}"
        }
        rv["time_spans"] << time_span
      end
    end
    Reservering.find_by_kunstwerkn(kunstwerkn).each do |reservation|
      rv["time_spans"] << reservation.to_time_span
    end
    rv["removed_from_collection_at"] = datum_uit_ if datum_uit_
    rv["highlight"] = uitgelicht
    rv["old_data"] = old_data
    rv
  end

  def old_data
    rv = instance_values
    rv["Kunstenaar"] = kunstenaar.instance_values
    rv["Kunstenaar Relatie"] = kunstenaar.relatie&.instance_values
    rv.delete("afbeelding")
    rv["memo"] = rtf_to_txt(rv["memo"])&.to_s
    rv
  end

  def memo
    rtf_to_txt(@memo)
  end

  class << self
    def update_nummer_uuid_mapping
      time_spans_with_uuid = TIME_SPAN_UUID_MAPPING_VOOR_UITLEEN.keys
      all_rented_works = all.map(&:uit_aan_key).compact
      missing_uuids = all_rented_works - time_spans_with_uuid
      mappings_to_add = missing_uuids.map { |a| [a, SecureRandom.uuid] }.to_h
      File.write(File.join(__dir__, "timespan_uuid_mapping.json"), mappings_to_add.merge(TIME_SPAN_UUID_MAPPING_VOOR_UITLEEN).to_json)
    end

    def find_by_number number
      all.find { |a| a.kunstwerkn == number }
    end

    def uitgeleend
      all.select { |a| a.uit_aan != 0 }
    end
  end
end

class Leenhistorie
  include ActiveModel::Model
  extend DbfBacked

  ATTRIBUTES = [:afbeelding, :huur_betaa, :huur_per_m, :inleverdat, :kunstenaar, :kunstwerkn, :lener_naam, :mee_naar_k, :pvlock, :pvsysdt, :pvsysid, :relatienum, :retourdatu, :sparen_per, :techniek, :titel, :uitleendat, :verkooppri, :volgnummer]
  attr_accessor(*ATTRIBUTES)

  DBF_NAME = "leenhistorie.DBF"

  def relatienummer
    relatienum.to_i
  end

  def uit_aan_key
    "#{relatienum}||#{uitleendat}||#{kunstwerkn}"
  end

  def to_time_span_hash
    {
      contact: {
        external: true,
        url: "http://localhost:5001/customers/#{Relatie::RELATIENUMMER_UUID_MAPPING[relatienummer.to_s] ? Relatie::RELATIENUMMER_UUID_MAPPING[relatienummer.to_s]["uuid"] : "old_id"}",
        name: lener_naam
      },
      starts_at: uitleendat,
      ends_at: inleverdat,
      uuid: Kunstwerk::TIME_SPAN_UUID_MAPPING_VOOR_UITLEEN[uit_aan_key],
      status: :finished,
      intent: :rent,
      classification: :rental_outgoing
    }
  end

  def to_old_time_span_hash
    {
      contact: {
        external: true,
        url: "http://localhost:5001/customers/#{Relatie::RELATIENUMMER_UUID_MAPPING[relatienummer.to_s] ? Relatie::RELATIENUMMER_UUID_MAPPING[relatienummer.to_s]["uuid"] : "old_id"}",
        name: lener_naam
      },
      starts_at: uitleendat,
      ends_at: retourdatu,
      status: :finished,
      intent: :rent,
      classification: :rental_outgoing
    }
  end

  class << self
    def all
      @@all ||= find(:all)
    end

    def find_all_by_kunstwerknummer number
      all.select { |a| a.kunstwerkn == number }
      # self.find(:all, kunstwerkn: number)
    end
  end
end

class QKunstWerk
  include ActiveModel::Model
  extend DbfBacked

  ATTRIBUTES = [:alt_nmer_1, :alt_nmer_2, :alt_nmer_3, :alt_nmer_4, :breedbeeld, :breedlijst, :cond_hting, :condibeeld, :condihting, :condilijst, :datering, :dieptbeeld, :dieptlijst, :drager, :gebouw, :geen_wezig, :geinvseerd, :gewijz_laa, :gewijzigd_, :hoogtbeeld, :hoogtlijst, :info_rkant, :ingev_op, :ingevoerd_, :invenummer, :kunstummer, :kunstwerkn, :locatcatie, :memo_inter, :nieuwoffen, :oplage, :oplagummer, :plaatrheid, :pvlock, :pvsysdt, :pvsysid, :relatienum, :signa_memo, :techniek, :techngroep, :terugonden, :titel, :type_beeld, :type_lijst, :validering, :validplage, :verdieping, :verv1rnaa1, :verv1nnaam, :verv1bjaar, :verv1_jaar, :verv1ssenv, :verv1rnaam, :verv2rnaa2, :verv2nnaam, :verv2bjaar, :verv2_jaar, :verv2ssenv, :verv2rnaam, :verv3rnaa3, :verv3nnaam, :verv3bjaar, :verv3_jaar, :verv3ssenv, :verv3rnaam]

  DBF_NAME = "qkunst.DBF"
  attr_accessor(*ATTRIBUTES)

  def stock_number
    invenummer
  end

  def kunstwerk
    Kunstwerk.find_by_number kunstwerkn
  end

  class << self
    def find_by_number number
      find(:first, invenummer: number)
    end

    def find_by_kunstwerkn number
      find(:first, kunstwerkn: number)
    end
  end
end

class Relatie
  include ActiveModel::Model
  extend DbfBacked

  RELATIENUMMER_UUID_MAPPING = File.exist?(File.join(__dir__, "relatienummer_uuid_mapping.json")) ? JSON.parse(File.read(File.join(__dir__, "relatienummer_uuid_mapping.json"))) : {}
  BANKACCOUNT_BIC_MAPPING = {
    "ASNB" => {bic: "ASNBNL21", name: "ASN Bank"},
    "ABNA" => {bic: "ABNANL2A", name: "ABN-AMRO bank"},
    "RABO" => {bic: "RABONL2U", name: "Rabobank"},
    "INGB" => {bic: "INGBNL2A", name: "ING bank"},
    "TRIO" => {bic: "TRIONL2U", name: "Triodos bank"}
  }

  ATTRIBUTES = [:achternaam, :achternaa2, :afdeling, :bankgironu, :bedrijf, :bezoek_hui, :bezoek_lan, :bezoek_pla, :bezoek_pos, :bezoek_str, :biccode, :branche, :btw_code, :btw_identi, :datumiging, :donatedrag, :donatatste, :donattekst, :donatenmet, :donatvanaf, :doorkiesnu, :e_mail, :e_mail2, :factuuradr, :fax, :functie, :functie2, :geboorteda, :geslacht, :geslacht2, :wijzigdoor, :wijzigop, :iban, :ic_blkkeer, :ic_rudatum, :informeel, :invoerdoor, :invoerop, :inschrijfd, :internerad, :internet_a, :mailing, :memo, :mobiel, :naam, :naam_bank, :nationalit, :oud_id_num, :overleden, :perioelden, :post_huisn, :post_land, :post_plaat, :post_postc, :post_straa, :pvlock, :pvsysdt, :pvsysid, :relatienum, :relatievan, :sofi_numme, :soort_rela, :telefoon, :telefoon2, :titels, :titels2, :tussenvoe2, :tussenvoeg, :voertaal, :voorletter, :voorlette2, :voornaam, :voornaam2, :zoeknaam, :zoeknaam2, :zoekplaats]
  DBF_NAME = "relatie.DBF"

  attr_accessor(*ATTRIBUTES)

  def soort_relaties
    soort_rela.split(",")
  end

  def business?
    bedrijf == true || bedrijf == "true"
  end

  def id
    relatienum
  end

  def import_id
    relatienum.to_i
  end

  def memo
    rtf_to_txt(@memo)
  end

  class << self
    def last_customer_code(range = nil)
      current_codes = RELATIENUMMER_UUID_MAPPING.map { |k, v| v.is_a?(String) ? nil : v["code"] }.compact.sort
      current_codes_in_range = current_codes & range.to_a if range
      latest = (current_codes_in_range || current_codes).last
      latest ? latest + 1 : range.to_a.first
    end

    def update_nummer_uuid_mapping
      business_customer_code = last_customer_code(Twinfield::Customer::BUSINESS_CUSTOMER_CODE_RANGE) - 1
      private_customer_code = last_customer_code(Twinfield::Customer::PRIVATE_CUSTOMER_CODE_RANGE) - 1

      updated_data_structure = RELATIENUMMER_UUID_MAPPING
      all_relaties = Relatie.find(:all)

      mappings = all_relaties.map do |relatie|
        relatienummer = relatie.relatienum.to_i

        if updated_data_structure[relatienummer.to_s]
          # don't create new ones when already exists
          [relatienummer, updated_data_structure[relatienummer.to_s]]
        elsif relatie.business?
          business_customer_code += 1
          [relatienummer, {uuid: SecureRandom.uuid, code: business_customer_code}]
        else
          private_customer_code += 1
          [relatienummer, {uuid: SecureRandom.uuid, code: private_customer_code}]
        end
      end.to_h

      File.write(File.join(__dir__, "relatienummer_uuid_mapping.json"), mappings.to_json)
    end

    def find_by_relatie_nummer nummer
      all.find { |a| a.relatienum.to_i == nummer.to_i }
    end

    def kunstenaars
      all.select { |r| r.soort_relaties.include?("Kunstenaar") }
    end

    def filter_email string
      if string
        # slightly modified from default mail matcher from ruby; twinfield requires a tld
        string.match(/[a-zA-Z0-9.!\#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+/).to_a.first
      end
    end

    def iban_to_bic_name iban
      if iban
        BANKACCOUNT_BIC_MAPPING[iban[4..7]] || {}
      end
    end
  end
end

class Kunstenaar
  include ActiveModel::Model
  extend DbfBacked

  ATTRIBUTES = [:afbeeding1, :artiesnaam, :besluit_be, :bezoek_hui, :bezoek_pla, :bezoek_pos, :bezoekadre, :datum_corr, :datum_over, :diploma, :discipline, :e_mail, :fax, :fku_nummer, :geboorteda, :geboortela, :geboortepl, :wijzigdoor, :wijzigop, :hfd_werk_a, :homepage, :huisnummer, :invoerdoor, :invoerdop, :internet_a, :internet_1, :internet_2, :internet_3, :internet_4, :kunsten_nr, :leenr_vrij, :lid_van_be, :materialen, :memo, :memo_beroe, :mobiel, :mw_nummer, :opl_dat_to, :opl_dat_va, :opl_plaats, :opleiding, :plaats, :postcode, :prive_mail, :prive_fax, :prive_tele, :pvlock, :pvsysdt, :pvsysid, :reden_besl, :setid, :statement, :straat, :studie, :technieken, :telefoon, :toets_dat_, :verval_dat, :volgend_kw, :zoeknaam, :zoeknaam_a]
  DBF_NAME = "kninfo.DBF"

  attr_accessor(*ATTRIBUTES)

  def soort_relaties
    soort_rela.split(",")
  end

  def relatie
    Relatie.find_by_relatie_nummer(mw_nummer)
  end

  def kunstwerken
    Kunstwerk.find(:all, kunsten_nr: kunsten_nr)
  end

  class << self
    def kunstenaars
      Relatie.find(:all).select { |r| r.soort_relaties.include?("Kunstenaar") }
    end

    def find_by_kunstenaar_nummer(kunsten_nr)
      Kunstenaar.all.find { |a| a.kunsten_nr == kunsten_nr }
    end
  end
end

# facturen.DBF
# Factuurregels.DBF
# BalieMutaties.DBF # Events (in en uitgaand; links to RELATIENUM)
# Leners.DBF Oude en huidige leners; uniq key: RELATIENUM
# Mandaten.DBF # Wellicht wat beperkt => Automatische incasso? => Hoe wordt hier een incasso ID van gemaakt? RELATUMMER
# Reserveringen.DBF # Reserveringen

class Factuur
  include ActiveModel::Model
  extend DbfBacked

  DBF_NAME = "facturen.DBF"
  ATTRIBUTES = [:ac_datum_v, :ar_beginda, :ar_periode, :bankgironu, :betaalwijz, :biccode, :bonnummer_, :datum_beta, :datum_bet2, :datum_her2, :datum_heri, :datumiging, :fac_bedr_e, :fac_bedr_i, :factuur_da, :factuurbed, :factuurnum, :factu_open, :filiaal, :geprint, :iban, :ic_bedrag, :ic_blokkee, :ic_datum, :ic_run_dat, :ic_run_id, :ic_storner, :jaar, :memo, :naam_bank, :omschrijvi, :periode, :pvlock, :pvsysdt, :pvsysid, :relatienum, :relat_open, :setid, :statuctuur, :tekst_op_n, :terug_stor]

  attr_accessor(*ATTRIBUTES)

  def import_id
    factuurnum.to_i
  end

  def relatie
    Relatie.find_by_relatie_nummer(relatienum)
  end

  # == Schema Information
  #
  # Table name: invoices
  #
  #  id                                :integer          not null, primary key
  #  discount_percentage               :decimal(, )      default(0.0)
  #  finalized_at                      :datetime
  #  footertext                        :text
  #  recurrence_interval_in_months     :integer          default(1)
  #  rental_type                       :string           default("basic_rent")
  #  twinfield_invoicenumber           :string
  #  use_customer_savings_for_purchase :boolean          default(TRUE)
  #  created_at                        :datetime         not null
  #  updated_at                        :datetime         not null
  #  created_by_user_id                :string
  #  customer_id                       :integer
  #  import_id                         :string
  #  previous_invoice_id               :integer
  #
  def customer
    Customer.find_by_import_id(relatienum.to_i)
  end

  def factuur_open?
    ic_bedrag.to_i == 0
  end

  def to_invoice_hash
    {
      # twinfield_invoicenumber: factuurnum.to_i,
      finalized_at: factuur_da,
      updated_at: factuur_da,
      footertext: ["Let op: deze factuur is een kopie factuur vanuit vorige boekhoudpakket. Oorspronkelijk factuurnummer #{import_id}", rtf_to_txt(tekst_op_n)].select(&:present?).join("\n\n"),
      invoice_items: factuur_regels.map(&:to_invoice_item_hash).map { |item| InvoiceItem.new(item) if item }.compact,
      customer_id: customer&.id,
      rental_type: rental_type,
      import_id: import_id,
      recurrence_interval_in_months: factuur_regels.map(&:to_invoice_item_hash).compact.select { |a| a[:intent] == "rent" }.map { |a| a[:quantity] }.max || 1,
      old_data: instance_values.merge({factuur_regels: factuur_regels.map(&:instance_values)})
    }
  end

  def rental_type
    ((factuur_regels.map(&:artikelcod) & ["SPAREN", "SPAREN HIS"]).count > 0) ? "saving" : "basic_rent"
  end

  def factuur_regels
    Factuurregel.find_all_by_factuurnummer(factuurnum)
  end

  class << self
    def all_after_date(date = Date.new(2021, 1, 1))
      @@all_after_date ||= {}
      @@all_after_date[date] ||= all.select { |a| a.factuur_da >= date }
    end

    def find_all_by_relatienum(relatienum)
      all.select { |a| a.relatienum == relatienum }
    end

    def find_by_factuurnummer factuurnummer
      all.find { |a| a.factuurnum.to_i == factuurnummer }
    end
  end
end

class Factuurregel
  include ActiveModel::Model
  extend DbfBacked

  DBF_NAME = "Factuurregels.DBF"
  ATTRIBUTES = [:aangekocht, :aantal, :abonnement, :artikelcod, :btw_afdr_h, :btw_afdr_l, :btw_ink, :btwcode, :factuurnum, :filiaal, :galleriere, :geboekt, :geprint, :gewij_door, :gewijzigd_, :herkomst, :huur_bet_t, :huur_per_m, :huur_tot_e, :huur_van_d, :ingev_door, :ingevoerd_, :ink_ex_btw, :ink_in_btw, :inleverdat, :jaar, :kunstuitle, :kunstwerkn, :margeregel, :mutatietel, :omschrijvi, :omzet_plaa, :omzetreken, :periode, :prijs_per_, :pvlock, :pvsysdt, :pvsysid, :relatienum, :retourdatu, :soort_kuns, :soort_muta, :spaartegoe, :sparen_per, :telt_voor, :uitleendat, :verh_met_s]

  attr_accessor(*ATTRIBUTES)

  def kunstwerk
    if kunstwerkn
      Kunstwerk.find_by_number(kunstwerkn)
    end
  end

  def stock_number
    if kunstwerkn
      CollectionManagement::Work::QIN_LOOKUP[kunstwerkn] || kunstwerkn
    end
  end

  def ignore?
    {
      "RESERVERING" => "Kids specifiek",
      "INGELEVERD" => "Kids specifiek",
      "UITGELEEND" => "Kids specifie",
      "3004" => "Geen klantfacturen",
      "4820" => "Geen klantfacturen",
      "8211" => "Geen klantfacturen"
    }.key?(artikelcod)
  end

  def factuur
    Factuur.all.find { |a| a.factuurnum == factuurnum }
  end

  def invoice_item_config
    @invoice_item_config ||= Rails.application.config_for(:uitleen)[:invoice_items]
  end

  def rental_type
    ["SPAREN", "SPAREN HIS"].include?(artikelcod) ? "saving" : "basic_rent"
  end

  def unit_price_ex_vat
    if btwcode == 4
      prijs_per_.to_d
    else
      prijs_per_.to_d - (btw_afdr_h.to_d / aantal.to_d) - (btw_afdr_l.to_d / aantal.to_d)
    end
  end

  def to_invoice_item_hash
    return nil if ignore?

    raise "Voor verhuur wordt de periode nog niet overgenomen huur_van_d huur_tot_e => invoicing_period_ends_at invoicing_period_starts_at waarbij aangetekend moet worden dat invoicing_period starts at altijd aan begin is, en eind periode altijd aan einde maand is."
    # btw_afdr_l is alleen > 0 bij AANKOOP; minimal object cache should ensure owner is set properly to make sure 6% tax rate is applied
    # artikelcodes uniek voor werken: ["INGELEVERD", "HUURI", "SPAREN", "HUUR HISI", "SPAREN HIS", "AANKOOP", "UITGELEEND", "HUUR", "CREDITERING", "HUUR HIS", "RESERVERING"]
    # if kunstwerkn && artikelcod == "AANKOOP"
    #   object_id = CollectionManagement::Work.stock_number_to_qkunst_id(stock_number)
    #   {
    #     object_type: "CollectionManagement::Work",
    #     object_id: object_id,
    #     object_cache: kunstwerk&.minimal_object_cache,
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat
    #   }
    # elsif kunstwerkn && artikelcod == "CREDITERING"
    #   object_id = CollectionManagement::Work.stock_number_to_qkunst_id(stock_number)
    #   return nil if object_id.nil?
    #   {
    #     object_type: "CollectionManagement::Work",
    #     object_id: object_id,
    #     object_cache: kunstwerk&.minimal_object_cache,
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat
    #   }
    # elsif kunstwerkn && ["SPAREN", "SPAREN HIS", "HUURI", "HUUR HIS", "HUUR HISI", "HUUR"].include?(artikelcod)
    #   # diff between sparen en huuri is made using invoice's setting
    #   object_id = CollectionManagement::Work.stock_number_to_qkunst_id(stock_number)
    #   return nil if object_id.nil?
    #   {
    #     object_type: "CollectionManagement::Work",
    #     object_id: object_id,
    #     object_cache: kunstwerk&.minimal_object_cache,
    #     time_span_uuid: Kunstwerk::TIME_SPAN_UUID_MAPPING_VOOR_UITLEEN[kunstwerk&.uit_aan_key],
    #     quantity: aantal,
    #     rental_type: rental_type,
    #     intent: "rent",
    #     unit_price_ex_vat: unit_price_ex_vat
    #   }
    #   # coming up: non-article lines
    #   # Done: "0101-1 3004 4820 8000 8004 8140 8195 8211 8301-1 8305  8305-1  CADEAU TGD "CORRECTIE HUUR P" "HUUR BASISI" KORTING KORTING-1 KORTING-5 KORTINGI  LIJST OMZET OVERIGE SPAREN BASIS
    #   # ,  SPRINT TRANSPORT PARTICULIEREN VERVALLEN SPAARTEGOED WINKELARTIKEL"
    # elsif ["KORTING-1"].include?(artikelcod)
    #   {
    #     object: Article.find_by!(external_article_code: invoice_item_config[:sell_art_external][:article], external_article_subcode: invoice_item_config[:sell_art_external][:subarticle]),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat,
    #     note: "Korting"
    #   }
    # elsif ["KORTING-2"].include?(artikelcod)
    #   {
    #     object: Article.find_by!(external_article_code: invoice_item_config[:sell_art_owned][:article], external_article_subcode: invoice_item_config[:sell_art_owned][:subarticle]),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat,
    #     note: "Korting"
    #   }
    # elsif ["KORTING-5"].include?(artikelcod)
    #   {
    #     object: Article.find_by!(external_article_code: invoice_item_config[:sell_art_artist][:article], external_article_subcode: invoice_item_config[:sell_art_artist][:subarticle]),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat,
    #     note: "Korting"
    #   }
    # elsif artikelcod == "0101-1"
    #   {
    #     object: Article.find_by!(external_article_code: "PUBLICATIE"),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat
    #   }
    # elsif ["KORTING", "8000"].include?(artikelcod)
    #   {
    #     object: Article.find_by!(external_article_code: "HUURCORRECTIE", external_article_subcode: "HUURCORRZAK"),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat,
    #     note: omschrijvi
    #   }
    # elsif artikelcod == "CORRECTIE HUUR P"
    #   {
    #     object: Article.find_by!(external_article_code: "HUURCORRECTIE", external_article_subcode: "HUURCORRECTIE"),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat,
    #     note: omschrijvi
    #   }
    # elsif artikelcod == "8004"
    #   {
    #     object: Article.find_by!(external_article_code: "HUURMAATWERK", external_article_subcode: "HUURMAATWERKZAK"),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat,
    #     note: omschrijvi
    #   }
    # elsif ["SPAREN BASIS"].include?(artikelcod)
    #   {
    #     object: Article.find_by!(external_article_code: invoice_item_config[:customer_savings][:use][:article]),
    #     quantity: aantal,
    #     intent: "rent",
    #     rental_type: rental_type,
    #     unit_price_ex_vat: unit_price_ex_vat,
    #     note: omschrijvi
    #   }
    # elsif ["SPRINT CORRECTIE", "SPRINT"].include?(artikelcod)
    #   {
    #     object: Article.find_by!(external_article_code: "SPAAR", external_article_subcode: "SPRSPAARTGD"),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat, # can be a negative value in case of vervallen spaartegoed
    #     note: omschrijvi
    #   }
    # elsif ["VERVALLEN SPAARTEGOED"].include?(artikelcod)
    #   {
    #     object: Article.find_by!(external_article_code: invoice_item_config[:customer_savings][:use][:article]),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat, # can be a negative value in case of vervallen spaartegoed
    #     note: omschrijvi
    #   }
    # elsif ["HUUR BASISI", "KORTINGI"].include?(artikelcod)
    #   {
    #     object: Article.find_by!(external_article_code: "HUURMAATWERK", external_article_subcode: "HUURMAATWERK"),
    #     quantity: aantal,
    #     intent: "rent",
    #     rental_type: rental_type,

    #     unit_price_ex_vat: unit_price_ex_vat,
    #     note: omschrijvi
    #   }
    # elsif ["OMZET OVERIGE", "4820", "8110", "1021"].include?(artikelcod)
    #   {
    #     object: Article.find_by!(external_article_code: "OMZETOVERIGE"),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat,
    #     note: omschrijvi
    #   }
    # elsif ["WINKELARTIKEL"].include?(artikelcod)
    #   {
    #     object: Article.find_by!(external_article_code: "WINKEL"),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat,
    #     note: omschrijvi
    #   }
    # elsif ["LIJST", "8140", "8140-3"].include?(artikelcod)
    #   {
    #     object: Article.find_by!(external_article_code: "MATERIAAL"),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat,
    #     note: omschrijvi
    #   }
    # elsif ["TRANSPORT", "TRANSPORT PARTICULIEREN"].include?(artikelcod)
    #   {
    #     object: Article.find_by!(external_article_code: "TRANSPORT"),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat,
    #     note: omschrijvi
    #   }
    # elsif artikelcod == "8195"
    #   {
    #     object: Article.find_by!(external_article_code: "HUURRUIMTE"),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat,
    #     note: omschrijvi
    #   }
    # elsif artikelcod == "8301-1"
    #   {
    #     object: Article.find_by!(external_article_code: "ADVIES"),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat,
    #     note: omschrijvi
    #   }
    # elsif artikelcod == "8305"
    #   {
    #     object: Article.find_by!(external_article_code: "ARTHANDLING"),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat,
    #     note: omschrijvi
    #   }
    # elsif artikelcod == "8305-1"
    #   {
    #     object: Article.find_by!(external_article_code: "VEILINGKOSTEN"),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat,
    #     note: omschrijvi
    #   }
    # elsif artikelcod == "CADEAU TGD"
    #   {
    #     object: Article.find_by!(external_article_code: "CADEAUTGD", external_article_subcode: "GEBRUIK"),
    #     quantity: aantal,
    #     intent: "purchase",
    #     unit_price_ex_vat: unit_price_ex_vat
    #   }
    # end
  end

  class << self
    def find_all_by_factuurnummer(factuurnum, after_date = Date.new(2020, 12, 1))
      all_after_date(after_date).select { |a| a.factuurnum == factuurnum }
    end

    def all_after_date(date = Date.new(2020, 12, 1))
      @@all_after_date ||= {}
      @@all_after_date[date] ||= all.select { |a| a.ingevoerd_ >= date }
    end

    def find_all_by_relatienum(relatienum)
      all.select { |a| a.relatienum == relatienum }
    end
  end
end

class BalieMutatie
  include ActiveModel::Model
  extend DbfBacked

  DBF_NAME = "BalieMutaties.DBF"
  ATTRIBUTES = [:aantal, :abonnement, :artikelcod, :btwcode, :eenheid, :factuurnum, :filiaal, :gewij_door, :gewijzigd_, :historie_v, :huur_bet_t, :huur_per_m, :huur_tot_e, :huur_van_d, :ingev_door, :ingevoerd_, :inleverdat, :kortigperc, :kunstwerkn, :mee_naar_k, :mutatietel, :omschrijvi, :prijs_per_, :prijs_van_, :pvlock, :pvsysdt, :pvsysid, :relatienum, :retourdatu, :setid, :soort_muta, :spaartegoe, :sparen_per, :telt_voor, :uitleendat, :verh_met_s]

  attr_accessor(*ATTRIBUTES)
end

class Lener
  include ActiveModel::Model
  extend DbfBacked

  LANDNAAM_LANDCODE = {"Nederland" => "NL", "Duitsland" => "DE"}
  DBF_NAME = "Leners.DBF"
  ATTRIBUTES = [:ab_gratis, :abonnement, :actie, :automatisc, :bankgironu, :barcoummer, :ber_ab_van, :berekend_a, :besteed_sp,
    :betaal_wij, :bet_p_kas, :betalingsh, :bezoek_hui, :bezoek_pla, :bezoek_pos, :bezoekadre, :biccode, :blokkeer, :blokkeer_d,
    :blokkeer_r, :cadeaement, :cadeautego, :dat_laasts, :datumiging, :filiaal, :geboorteda, :gefacturee, :wijzigdoor, :wijzigop,
    :iban, :invoerdoor, :invoerop, :inschrijfd, :internet, :klant_via, :korting, :korti_verk, :kortingper, :laatstefac, :laatstemut,
    :leg_bewijs, :leg_geldig, :leg_nr, :lidmasgeld, :memo, :naam_bank, :nota_via_e, :opgebouwd_, :opzegdatum, :opzegreden,
    :oud_id_num, :periode_ab, :pvlock, :pvsysdt, :pvsysid, :relatienum, :retourheri, :service, :setid, :soort_lene, :sparen,
    :uitleenter, :verzeng_kw, :zoeknaam, :zoeknaam_a, :zoeknaam2]

  # RELATIE
  # ATTRIBUTES = [:achternaam, :achternaa2, :afdeling, :bankgironu, :bedrijf, :bezoek_hui, :bezoek_lan, :bezoek_pla, :bezoek_pos, :bezoek_str, :biccode, :branche, :btw_code, :btw_identi, :datumiging, :donatedrag, :donatatste, :donattekst, :donatenmet, :donatvanaf, :doorkiesnu, :e_mail, :e_mail2, :factuuradr, :fax, :functie, :functie2, :geboorteda, :geslacht, :geslacht2, :wijzigdoor, :wijzigop, :iban, :ic_blkkeer, :ic_rudatum, :informeel, :invoerdoor, :invoerop, :inschrijfd, :internerad, :internet_a, :mailing, :memo, :mobiel, :naam, :naam_bank, :nationalit, :oud_id_num, :overleden, :perioelden, :post_huisn, :post_land, :post_plaat, :post_postc, :post_straa, :pvlock, :pvsysdt, :pvsysid, :relatienum, :relatievan, :sofi_numme, :soort_rela, :telefoon, :telefoon2, :titels, :titels2, :tussenvoe2, :tussenvoeg, :voertaal, :voorletter, :voorlette2, :voornaam, :voornaam2, :zoeknaam, :zoeknaam2, :zoekplaats]

  attr_accessor(*ATTRIBUTES)

  def facturen
    Factuur.find_all_by_relatienum(relatienum)
  end

  def import_id
    relatienum.to_i
  end

  def factuur_regels
    Factuurregel.find_all_by_relatienum(relatienum)
  end

  def memo
    rtf_to_txt(@memo)
  end

  def twinfield_code
    Relatie::RELATIENUMMER_UUID_MAPPING[relatienum.to_i.to_s]["code"]
  end

  def render_name
    achternaam = [relatie.achternaam, relatie.achternaa2].select { |a| a.present? }.join("-")
    initialen = [relatie.voorletter, relatie.voorlette2].select { |a| a.present? }.compact.join(" ")
    last_name_prefix = relatie.tussenvoeg || relatie.tussenvoe2
    [initialen, last_name_prefix, achternaam].select(&:present?).join(" ")
  end

  def business?
    relatie.business? || soort_lene.start_with?("Bedrijf")
  end

  def to_twinfield_transaction(source_book:, store_book:)
    {
      code: "VRK",
      currency: "EUR",
      date: Date.new(2022, 1, 1),
      lines: [
        {debitcredit: :credit, dim1: "0000", type: :total, value: 0, description: "Spaartegoed uit KIDS"},
        {debitcredit: :credit, dim1: store_book, value: opgebouwd_, dim2: twinfield_code, description: "Spaartegoed uit KIDS"},
        {debitcredit: :debit, dim1: source_book, value: opgebouwd_, description: "Spaartegoed uit KIDS"}
      ]
    }
  end

  def to_customer_hash
    {
      name: [relatie.naam, zoeknaam, relatie.zoeknaam].find { |a| a.present? },
      company_name: [relatie.naam, zoeknaam, relatie.zoeknaam].find { |a| a.present? && business? },
      first_name: relatie.voornaam,
      initials: [relatie.voorletter, relatie.voorlette2].select { |a| a.present? }.compact.join(" "),
      last_name: [relatie.achternaam, relatie.achternaa2].select { |a| a.present? }.join("-"),
      title: [relatie.titels, relatie.titels2].select { |a| a.present? }.compact.join(" "),
      last_name_prefix: relatie.tussenvoeg || relatie.tussenvoe2,
      sex: relatie.geslacht,
      born_on: relatie.geboorteda,
      created_at: invoerop,
      comments: [memo, relatie.memo].select { |a| a.present? }.join("\n---\n"),
      old_data: instance_values.merge({relatie: relatie.instance_values, mandaten: mandaten.map(&:instance_values)}),
      customer_type: (business? ? :business : :private),
      id_document_type: leg_bewijs,
      id_document_number: leg_nr,
      id_document_valid_until: leg_geldig,
      deceased: relatie.overleden.present?,
      blocked_at: blokkeer_d.present? ? blokkeer_d : nil,
      blocked_reason: blokkeer_r,
      postal_code: relatie.bezoek_pos,
      import_id: import_id,
      email: Relatie.filter_email(relatie.e_mail),
      uuid: Relatie::RELATIENUMMER_UUID_MAPPING[relatienum.to_i.to_s]["uuid"]
    }.select { |k, v| v.present? }
  end

  def to_twinfield_hash
    rv = {
      name: [relatie.naam, zoeknaam, relatie.zoeknaam].find { |a| a.present? },
      website: ([relatie.internerad, relatie.internet_a].find(&:present?) ? [relatie.internerad, relatie.internet_a].find(&:present?)[0..39] : nil),
      vatnumber: relatie.btw_identi,
      code: twinfield_code,
      addresses: [
        {
          id: 1,
          name: [relatie.naam, zoeknaam, relatie.zoeknaam].find { |a| a.present? },
          field1: render_name,
          field2: [relatie.bezoek_str, relatie.bezoek_hui].select { |a| a.present? }.join(" "),
          country: LANDNAAM_LANDCODE[relatie.bezoek_lan],
          city: relatie.bezoek_pla,
          postcode: relatie.bezoek_pos,
          default: true,
          email: Relatie.filter_email(relatie.e_mail),
          type: "contact",
          telefax: relatie.fax,
          telephone: relatie.telefoon
        }.select { |k, v| v.present? },
        {
          id: 2,
          name: [relatie.naam, zoeknaam, relatie.zoeknaam].find { |a| a.present? },
          field1: render_name,
          field2: [relatie.post_straa, relatie.post_huisn].join(" "),
          country: LANDNAAM_LANDCODE[relatie.post_land],
          city: relatie.post_plaat,
          postcode: relatie.post_postc,
          email: Relatie.filter_email(relatie.e_mail2),
          default: false,
          telephone: relatie.telefoon2,
          type: "postal"
        }.select { |k, v| v.present? }
      ]
    }.select { |k, v| v.present? }

    if mandaat && datumiging
      rv[:financials] = {
        payavailable: true,
        paycode: "SEPANLDD",
        collectmandate: {
          signaturedate: datumiging,
          id: "#{datumiging} #{relatienum.to_i}"
        }
      }
    end

    if iban.present?
      rv[:banks] = [{
        iban: iban,
        ascription: [relatie.naam, zoeknaam, relatie.zoeknaam].find { |a| a.present? }[0..31],
        # accountnumber: bankgironu.present? ? bankgironu : relatie.bankgironu,
        bankname: Relatie.iban_to_bic_name(iban)[:name] || (naam_bank.present? ? naam_bank : relatie.naam_bank),
        biccode: Relatie.iban_to_bic_name(iban)[:bic] || (biccode.present? ? biccode : relatie.biccode),
        country: iban[0..1],
        default: true
      }.select { |k, v| v.present? }]
    end

    rv
  end

  def relatie
    Relatie.find_by_relatie_nummer(relatienum)
  end

  def mandaat
    Mandaat.find_by_relatie_nummer(relatienum) if betaal_wij == "Automatische incasso"
  end

  def mandaten
    Mandaat.find_all_by_relatie_nummer(relatienum)
  end

  class << self
    # getest: :relatienum is een uniq id
    def find_by_relatie_nummer(nummer)
      all.find { |a| a.relatienum.to_i == nummer.to_i }
    end
  end
end

class Mandaat
  include ActiveModel::Model
  extend DbfBacked

  DBF_NAME = "Mandaten.DBF"
  ATTRIBUTES = [:datumcasso, :datumiging, :eerstctuur, :pvlock, :pvsysdt, :pvsysid, :relatummer]

  attr_accessor(*ATTRIBUTES)

  class << self
    # getest: :relatienum is een uniq id
    def find_all_by_relatie_nummer(nummer)
      all.select { |a| a.relatummer.to_i == nummer.to_i }
    end

    def find_by_relatie_nummer(nummer)
      find_all_by_relatie_nummer(nummer).max_by(&:datumcasso)
    end
  end
end

class Reservering
  include ActiveModel::Model
  extend DbfBacked

  DBF_NAME = "Reserveringen.DBF"
  ATTRIBUTES = [:afhaacatie, :datum_eers, :gew_door, :gewijzigd_, :ingevoerd_, :ingev_op, :klaars_ret, :klaarsp_op, :kunstwerkn, :melden_via, :memo1, :op_fietour, :op_losinds, :op_trns_op, :op_trp_ret, :opgehld_op, :pvlock, :pvsysdt, :pvsysid, :relatienum, :reserverin, :retour_op, :soort_rese, :volgnummer]

  attr_accessor(*ATTRIBUTES)

  def relatienum
    @relatienum.to_i
  end

  def lener
    Lener.find_by_relatie_nummer(relatienum)
  end

  def relatie
    Relatie.find_by_relatie_nummer(relatienum)
  end

  #
  # def to_reservation_hash(customer_class: nil)
  #   work = CollectionManagement::Work.find_by_number(kunstwerkn)
  #   if work
  #     work_id = CollectionManagement::Work.find_by_number(kunstwerkn).id
  #     {
  #       import_id: volgnummer,
  #       old_data: instance_values,
  #       customer_id: customer_class&.find_by(import_id: relatienum)&.id,
  #       created_at: ingev_op,
  #       work_id: work_id,
  #       name: [relatie.naam, relatie.zoeknaam].select { |a| a.present? }.first,
  #       address: [relatie.bezoek_str, relatie.bezoek_hui].select { |a| a.present? }.join(" "),
  #       city: relatie.bezoek_pla,
  #       postal_code: relatie.bezoek_pos,
  #       email: relatie.e_mail,
  #       telephone: relatie.telefoon
  #     }
  #   else
  #     puts "Work #{kunstwerkn} could not be found in the active collection "
  #   end
  # end
  #
  def to_time_span
    {
      starts_at: ingev_op,
      intent: :rent,
      subject_id: CollectionManagement::Work.find_by_number(kunstwerkn)&.id,
      subject_type: "Work",
      status: :reservation,
      classification: :rental_outgoing,
      contact: {
        name: [relatie.naam, relatie.zoeknaam].find { |a| a.present? },
        external: true,
        remote_data: {
          address: [relatie.bezoek_str, relatie.bezoek_hui].select { |a| a.present? }.join(" "),
          city: relatie.bezoek_pla,
          postal_code: relatie.bezoek_pos,
          email: relatie.e_mail,
          telephone: relatie.telefoon
        }.to_json
      }
    }
  end

  class << self
    def find_by_kunstwerkn number
      find(:all, kunstwerkn: number)
    end
  end
end
