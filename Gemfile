source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }
git_source(:gitlab) { |repo| "https://gitlab.com/#{repo}.git" }

# ruby '3.0.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails', branch: 'main'
gem "bundler"
gem "rails", "~> 7.2"
# Use sqlite3 as the database for Active Record
gem "sqlite3"
# Use Puma as the app server
gem "puma"
# Use SCSS for stylesheets
gem "sass-rails"
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
# gem "webpacker", "~> 5.0"
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem "turbo-rails"
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem "jbuilder"
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

gem "mail"
gem "foreman"
gem "stimulus-rails"
gem "unrich", github: "murb/unrich"
gem "paper_trail"

gem "murb_design_system", gitlab: "murb-org/murb-design-system"

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem "bootsnap", require: false

gem "dbf"

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem "byebug", platforms: [:mri, :mingw, :x64_mingw]
  gem "annotate"
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem "web-console"
  # Display performance information such as SQL time and flame graphs for each request in your browser.
  # Can be configured to work on production as well see: https://github.com/MiniProfiler/rack-mini-profiler/blob/master/README.md
  gem "rack-mini-profiler"
  gem "listen"
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  # gem 'spring'
  gem "capistrano"
  gem "capistrano-rbenv"
  gem "capistrano-bundler"
  gem "capistrano-rails"
  gem "capistrano-sidekiq" # , group: :development
end

group :development, :staging do
  gem "letter_opener"
  gem "letter_opener_web"
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem "capybara"
  gem "selenium-webdriver"
  # Easy installation and use of web drivers to run system tests with browsers
  gem "webdrivers"
  gem "webmock"
  gem "simplecov", require: false
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# Added:

gem "omniauth"
gem "omniauth-rails_csrf_protection"
gem "savon"
gem "nokogiri"

gem "twinfield", git: "https://github.com/murb/twinfield.git"
# gem "twinfield", path: "/Users/murb/Documents/Zakelijk/Projecten/gems/twinfield"

gem "omniauth-twinfield" # , git: "https://gitlab.com/murb-org/omniauth-twinfield"
gem "omniauth-central_login" # , git: "https://gitlab.com/murb-org/omniauth-centrallogin"
# gem "omniauth-central_login", path: "/Users/murb/Documents/Zakelijk/Projecten/gems/omniauth-central_login"
gem "simple_form" # Post-install run: rails g simple_form:install --foundation
gem "pg"
gem "iban-tools"
gem "sidekiq"
gem "sidekiq-scheduler" # , ">= 3.1.0"
gem "exception_notification" # , git: "https://github.com/smartinez87/exception_notification.git"
gem "cancancan"
gem "strip_attributes"
gem "act_as_time_as_boolean"

gem "pupprb", git: "https://gitlab.com/murb/pupprb.git"
gem "standard"

gem "importmap-rails"
