module ApplicationHelper
  def menu_link_to desc, path, options = {}
    test_path = (options[:path] || path).include?("//") ? "/#{(options[:path] || path).split("/")[3..1000].join("/")}" : (options[:path] || path)
    test_url = "/#{request.url.split("/")[3..1000].join("/")}"

    class_name = if options[:only_exact_path_match]
      (test_url == test_path.to_s) ? "active" : ""
    else
      test_url.starts_with?(test_path.to_s) ? "active" : ""
    end
    link = link_to desc.to_s, path, class: class_name
    if options[:wrap]
      sanitize "<li>#{link}</li>"
    else
      link
    end
  end

  def dt_dd_for_hash_when_set hash, key, scope: :hash
    value = hash&.fetch(key, nil)
    if value.present?
      value = humanize_value(value)
      sanitize "<dt>#{I18n.t(key, scope: scope)}</dt><dd>#{value}</dd>"
    end
  end

  def collection_management_collection_work_url collection, work
    collection_param = collection.is_a?(CollectionManagement::Collection) ? collection.to_param : collection
    work_param = work.is_a?(CollectionManagement::Work) ? work.to_param : work
    URI.join(Rails.application.credentials.collection_management_site, "collections/#{collection_param}/works/#{work_param}").to_s
  end

  def collection_management_collection_work_time_span_url time_span
    collection_param = time_span.work.collection_id
    work_param = time_span.work.to_param
    time_span_param = time_span.to_param
    URI.join(Rails.application.credentials.collection_management_site, "collections/#{collection_param}/works/#{work_param}/time_spans/#{time_span_param}").to_s
  end

  def collection_management_work_set_url work_set
    URI.join(Rails.application.credentials.collection_management_site, "work_sets/#{work_set.to_param}").to_s
  end

  def collection_management_work_path work
    collection_work_path(work.collection_id, work.id)
  end

  def sales_invoice_path sales_invoice, options = {}
    invoice_path("external_invoice_number:#{sales_invoice.invoicenumber}", options)
  end

  def page_scope? scope = nil
    params[:scope] && (params[:scope].to_sym == scope || scope.nil?)
  end
end
