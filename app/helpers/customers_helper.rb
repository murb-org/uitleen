module CustomersHelper
  def twinfield_customer_path twinfield_customer
    customer_path(Customer.find_by(external_customer_id: twinfield_customer.code))
  end

  def global_or_customer_invoices_path options
    if @customer
      customer_invoices_path(@customer, options)
    else
      invoices_path(options)
    end
  end
end
