module PeriodHelper
  def describe_period(from, to)
    if !(from && to)
      nil
    elsif from.end_of_month == to
      I18n.l(from, format: :month_year).to_s
    elsif from == to.beginning_of_year && from.end_of_year == to
      I18n.l(from, format: :year).to_s
    else
      "#{I18n.l(from, format: :month_year)} t/m #{I18n.l(to, format: :month_year)}"
    end
  end
end
