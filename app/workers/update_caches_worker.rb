class UpdateCachesWorker
  include Sidekiq::Worker

  sidekiq_options retry: false, backtrace: true, queue: :uitleen_default, lock: :until_and_while_executing

  def perform
    Finance.configure_twinfield

    remote_twinfield_customer_codes = Twinfield::Customer.all.map(&:code)
    local_twinfield_customer_codes = Customer.pluck(:external_customer_id)
    missing_codes = remote_twinfield_customer_codes - local_twinfield_customer_codes
    missing_codes.each { |c| UpdateCustomerWorker.perform_async(c.to_s) }
  end
end
