class SynchroniseInvoiceWorkSetWorker
  include Sidekiq::Worker

  sidekiq_options retry: true, backtrace: true, queue: :uitleen_default, lock: :until_and_while_executing

  attr_reader :invoice

  delegate :recurrence_interval_in_months, to: :invoice
  delegate :previous_invoice, to: :invoice
  delegate :invoiceable_item_collection_works_with_current_active_time_span, to: :invoice
  delegate :customer, to: :invoice
  delegate :invoice_items, to: :invoice
  delegate :errors, to: :invoice

  class UnexpectedMultipleCustomersError < StandardError; end

  class NoCustomerSetError < StandardError; end

  class CustomerMisMatchError < StandardError; end

  def perform(invoice_or_invoice_id)
    @invoice = invoice_or_invoice_id.is_a?(Invoice) ? invoice_or_invoice_id : Invoice.find(invoice_or_invoice_id)

    return if invoice.finalized?
    return if invoiceable_item_collection_works_with_current_active_time_span.blank?

    update_invoice_items_with_invoiceable_item_collection
  end

  private

  def update_or_verify_customer_based_on_invoice_items_with_invoiceable_item_collection(time_spans)
    customer_uris = time_spans.map { |ts| ts.contact_url || ts.contact&.url }.uniq
    if customer_uris.count > 1
      raise UnexpectedMultipleCustomersError.new("Meer dan twee klanten in de werkgroepering")
    elsif customer_uris.compact.empty?
      raise NoCustomerSetError.new("Geen klanten geselecteerd voor de werkgroepering")
    elsif customer && customer_uris.count == 1 && customer_uris.first != customer.uri
      raise CustomerMisMatchError.new("Factuur is voor een andere klant")
    elsif !customer && customer_uris.count == 1
      new_customer = Customer.find_by_uri(customer_uris.first)
      if invoice.persisted?
        invoice.update_column(:customer_id, new_customer.id)
      else
        invoice.customer = new_customer
      end
    end
  end

  def any_invoice_items?
    invoice.invoice_items.any?
  end

  def requires_review!
    if invoice.persisted? && !invoice.requires_review?
      invoice.update_column(:requires_review_at, Time.current)
    elsif invoice.requires_review?
      invoice.requires_review_at = Time.current
    end
  end

  def update_invoice_items_with_invoiceable_item_collection
    time_spans = invoiceable_item_collection_works_with_current_active_time_span.collect(&:current_active_time_span)

    update_or_verify_customer_based_on_invoice_items_with_invoiceable_item_collection(time_spans)

    # invoice_items may still be an unstored array
    current_rent_work_ids = invoice_items.map { |ii| ii.work&.id if ii.rent_intent? }.compact.map(&:to_s)
    time_spans_to_add = time_spans.select { |ts| !current_rent_work_ids.include?(ts.subject&.id&.to_s) && ts.rental_outgoing? }

    time_spans_to_add.each do |time_span|
      requires_review! if any_invoice_items?
      invoice_item = InvoiceItem.new(intent: :rent, invoiceable: time_span.subject, time_span: time_span, quantity: recurrence_interval_in_months)

      if (previous_invoice.nil? || previous_invoice.invoice_items.where(invoiceable_id: time_span.subject.id, invoiceable_type: time_span.subject.class.to_s).empty?) && time_span.invoicing_period_starts_at
        invoice_item.invoicing_period_starts_at = time_span.invoicing_period_starts_at
        invoice_item.invoicing_period_ends_at = invoice_item.end_date_based_on_quantity
      elsif previous_invoice && previous_invoice.invoice_items.where(invoiceable_id: time_span.subject.id, invoiceable_type: time_span.subject.class.to_s).any?
        invoice_item.invoicing_period_starts_at = previous_invoice.invoice_items.where(invoiceable_id: time_span.subject.id, invoiceable_type: time_span.subject.class.to_s).first.next_invoicing_period_starts_at
        invoice_item.invoicing_period_ends_at = invoice_item.end_date_based_on_quantity
      end

      invoice_items << invoice_item
    end

    # invoice_items may still be an unstored array
    current_purchase_work_ids = invoice_items.map { |ii| ii.work&.id if ii.purchase_intent? }.compact.map(&:to_s)
    time_spans_to_add = time_spans.select { |ts| !current_purchase_work_ids.include?(ts.subject&.id&.to_s) && ts.purchase? }

    time_spans_to_add.each do |time_span|
      requires_review! if any_invoice_items?
      invoice_items << InvoiceItem.new(intent: :purchase, invoiceable: time_span.subject, time_span: time_span)
    end

    invoice_items.map(&:save)
  end
end
