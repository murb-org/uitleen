# frozen_string_literal: true

class SendCreatedInvoiceWorker
  include Sidekiq::Worker

  sidekiq_options retry: true, backtrace: true, queue: :uitleen_default, lock: :until_and_while_executing

  def perform(invoice_id, options = {})
    chain = options.delete("chain") if options["chain"]

    Finance.configure_twinfield

    invoice = Invoice.find(invoice_id)

    if !invoice.pdf.present?
      GeneratePdfInvoiceWorker.perform_async(invoice_id, {"chain" => self.class.name})
    end

    message = InvoiceMailer.created(invoice)

    if message.deliver_now

      if invoice.invoice_sent! && chain
        chain.constantize.perform_async(invoice_id)
      end
    end
  end
end
