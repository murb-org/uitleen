# frozen_string_literal: true

class GeneratePdfInvoiceWorker
  include Sidekiq::Worker

  sidekiq_options retry: true, backtrace: true, queue: :uitleen_default, lock: :until_and_while_executing

  def perform(invoice_id, options = {})
    chain = options.delete("chain") if options["chain"]

    invoice = Invoice.find(invoice_id)

    Finance.configure_twinfield

    if options["force"]
      invoice.pdf.destroy
    end

    if invoice.finalized? && !invoice.pdf.present?
      invoice.pdf.attach(io: File.open(invoice.to_pdf), filename: "invoice.pdf")
    end

    if invoice.save && invoice.finalized? && chain
      chain.constantize.perform_async(invoice_id)
    end
  end
end
