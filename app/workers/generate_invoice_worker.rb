class GenerateInvoiceWorker
  include Sidekiq::Worker

  sidekiq_options retry: true, backtrace: true, queue: :uitleen_default, lock: :until_and_while_executing

  # expose id
  attr_reader :id

  def perform(id)
    return false if !Rails.application.config_for(:uitleen)[:finalize_invoices]

    @id = id
    expired_invoice = Invoice.find(id)

    return false if expired_invoice.on_hold?

    Finance.configure_twinfield

    new_invoice = expired_invoice.initialize_next_invoice

    if new_invoice
      if expired_invoice.customer.auto_finalize_invoices?
        new_invoice.finalize!
        # send email when not collect mandata present
        if !new_invoice.collect_mandate? && (new_invoice.total_price > 0)
          SendCreatedInvoiceWorker.perform_async(new_invoice.id)
        end
        # Manually performed:
        # Invoice.where(finalized_at: (1.week.ago...Time.now)).where(invoice_sent_at: nil).select{|a| !a.collect_mandate? && a.total_price > 0}.each{|a| SendCreatedInvoiceWorker.perform_async a.id}
      else
        new_invoice.requires_review!
      end

      new_invoice
    end
  end
end
