class UpdateCustomerWorker
  include Sidekiq::Worker

  sidekiq_options retry: true, backtrace: true, queue: :uitleen_default, lock: :until_and_while_executing

  def perform(twinfield_id)
    Finance.configure_twinfield

    Customer.create_or_update_from_twinfield_customer_code(twinfield_id)
    sleep(3)
  end
end
