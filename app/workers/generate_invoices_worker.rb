class GenerateInvoicesWorker
  include Sidekiq::Worker

  sidekiq_options retry: true, backtrace: true, queue: :uitleen_default, lock: :until_and_while_executing

  def perform(*args)
    recently_expired_invoices = Invoice.recently_expired.not_on_hold

    recently_expired_invoices.each do |expired_invoice|
      GenerateInvoiceWorker.perform_async(expired_invoice.id)
    end

    # Do something
  end
end
