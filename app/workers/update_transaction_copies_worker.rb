class UpdateTransactionCopiesWorker
  include Sidekiq::Worker

  sidekiq_options retry: true, backtrace: true, queue: :uitleen_default, lock: :until_and_while_executing

  def perform
    Finance.configure_twinfield && CostCenter.update && CustomerTransaction.update
  end
end
