# frozen_string_literal: true

class TestWorker
  SIDEKIQ_TEST_FILENAME = Rails.root.join("tmp", "sidekiqtest.txt")
  include Sidekiq::Worker

  sidekiq_options retry: false, backtrace: true, queue: :uitleen_default

  def perform
    FileUtils.touch(SIDEKIQ_TEST_FILENAME)
  end

  def self.alive?
    File.mtime(SIDEKIQ_TEST_FILENAME) > 1.hour.ago
  end
end
