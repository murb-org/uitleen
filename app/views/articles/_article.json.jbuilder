json.extract! article, :id, :external_article_code, :name, :vat_rate, :created_at, :updated_at
json.url article_url(article, format: :json)
