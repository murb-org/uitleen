class AdminConstraint
  def matches?(request)
    return false unless request.session[:user_sub]
    user = User.find_by_sub request.session[:user_sub]
    user&.admin?
  end
end
