module CollectionManagement
  class TimeSpan
    include ActiveModel::Model
    include ActiveModel::Validations

    CLASSIFICATIONS = [:rental_outgoing, :transport, :exhibition, :purchase] # :rental_incoming was part of this as well, but
    SUBJECT_TYPES = ["Work", "WorkSet"]
    STATUSSES = [:concept, :reservation, :active, :finished]

    attr_accessor :status, :contact_url, :subject_id, :subject_type, :classification, :uuid, :comments
    attr_reader :subject, :contact, :starts_at, :ends_at

    validates :classification, inclusion: CLASSIFICATIONS.map(&:to_s), presence: true
    validates :subject_type, inclusion: SUBJECT_TYPES, presence: true
    validates :status, inclusion: STATUSSES.map(&:to_s), presence: true

    class << self
      def where(**options)
        current_user = options.delete(:current_user)
        response = Faraday.new(URI.join(Rails.application.credentials.collection_management_site, "api/v1/time_spans").to_s, params: options, headers: Collection.authorization_header(current_user: current_user)).get
        if response.status == 200
          json = JSON.parse(response.body)["data"]
          json.map { |c| TimeSpan.new(**c) }
        else
          current_user.refresh!(force: true)
          raise ActiveRecord::RecordNotFound.new, "not found (#{response.status})"
        end
      end

      def find(id)
        response = Faraday.new(URI.join(Rails.application.credentials.collection_management_site, "api/v1/time_spans/#{id}").to_s, headers: Collection.authorization_header).get
        if response.status == 200
          json = JSON.parse(response.body)["data"]
          TimeSpan.new(**json)
        else
          raise ActiveRecord::RecordNotFound.new, "not found (#{response.status})"
        end
      end
    end

    def customer
      if contact_url
        Customer.find_by_uri(contact_url)
      end
    end

    def rental_outgoing?
      classification&.to_sym == :rental_outgoing
    end

    def purchase?
      classification&.to_sym == :purchase
    end

    def reservation
      if contact_url
        id = contact_url.split("/").last
        Reservation.find_by(id: id)
      end
    end

    def name_with_fallback
      customer&.name || contact&.name || reservation&.name
    end

    def work
      Work.find(subject_id) if subject_type == "Work"
    end

    def subject= subject
      @subject = if subject.is_a? Work
        self.subject_type = "Work"
        self.subject_id ||= subject.id
        subject
      elsif subject_type == "Work"
        Work.new(**subject)
      end
    end

    def contact= contact
      @contact = Contact.new(**contact)
    end

    def reservation?
      status == "reservation"
    end

    def current?
      (ends_at.nil? || ends_at > Time.now) && starts_at < Time.now
    end

    def current_reservation?
      reservation? && current?
    end

    def starts_at= datetime
      @starts_at = datetime.is_a?(String) ? DateTime.parse(datetime) : datetime
    end

    def ends_at= datetime
      @ends_at = datetime.is_a?(String) ? DateTime.parse(datetime) : datetime
    end

    def invoicing_period_starts_at
      if starts_at.nil?
        nil
      elsif starts_at.to_date == starts_at.beginning_of_month.to_date
        starts_at
      else
        starts_at.beginning_of_month + 1.month
      end
    end

    def expired?
      ends_at && ends_at < Time.current
    end

    def to_param
      uuid
    end

    def to_h
      {
        status: status,
        contact_url: contact_url,
        subject_id: subject_id,
        subject_type: subject_type,
        starts_at: starts_at,
        ends_at: ends_at,
        classification: classification,
        subject: subject&.to_minimal_h,
        uuid: uuid,
        comments: comments
      }
    end
  end
end
