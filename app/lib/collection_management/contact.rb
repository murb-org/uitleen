module CollectionManagement
  class Contact
    include ActiveModel::Model
    include ActiveModel::Validations

    attr_accessor :name, :address, :external, :url, :remote_data

    def to_h
      {
        name: name,
        address: address,
        external: external,
        url: url,
        remote_data: remote_data.to_json
      }
    end

    def uuid
      url.split("customers/").last
    end

    def parsed_remote_data
      JSON.parse(remote_data).symbolize_keys if remote_data
    end

    def time_span_provided_address
      [parsed_remote_data[:address],
        [parsed_remote_data[:postal_code], parsed_remote_data[:city]].join(" ")].join("\n")
    end
  end
end
