module CollectionManagement
  class WorkEvent
    include ActiveModel::Model

    attr_accessor :work, :contact_uri, :event_type, :status, :time_span_uuid, :comments, :contact

    def to_json
      JSON({
        work_event: {
          contact_uri: contact_uri,
          event_type: event_type,
          status: status,
          time_span_uuid: time_span_uuid,
          comments: comments,
          contact: contact&.to_h
        }
      })
    end

    def save(current_user:)
      url = URI.join(Rails.application.credentials.collection_management_site, "api/v1/collections/#{work.collection_id}/works/#{work.id}/work_events").to_s
      response = Faraday.new.post url do |req|
        req.headers[:content_type] = "application/json"
        req.headers["Authorization"] = Collection.authorization_header(current_user: current_user)["Authorization"]
        req.body = to_json
      end

      if response.status == 200
        json = JSON.parse(response.body)
        json["data"] ? TimeSpan.new(**json["data"]) : TimeSpan.new(**json)
      else
        current_user.refresh!(force: true)
        raise ActiveRecord::RecordNotFound.new, "not found (#{response.status})"
      end
    end
  end
end
