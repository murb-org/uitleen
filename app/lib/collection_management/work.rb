module CollectionManagement
  class Work
    PRIVATE_ATTRIBUTES = [:purchase_price, :fin_balance_item_id, :removed_from_collection_note, :valuation_on]
    ATTRIBUTES_TO_EXCLUDE_FOR_MINIMAL = [:time_spans, :current_active_time_span]
    class Collection
      include Enumerable

      attr_accessor :count, :total_count

      def initialize(*)
        @list = Array.new(*)
      end

      def each(...)
        @list.each(...)
      end
    end

    class << self
      def primary_key
        :id
      end

      def name_ids_attr_accessor *args
        args.each do |arg|
          class_eval("def #{arg}= array;@#{arg} = (array || []).map { |a| NameId.new(**a) };end", __FILE__, __LINE__)
          class_eval("def #{arg};@#{arg} || [];end", __FILE__, __LINE__)
        end
      end

      def name_id_attr_accessor *args
        args.each do |arg|
          class_eval("def #{arg}= hash;@#{arg} = NameId.new(**hash) if hash;end", __FILE__, __LINE__)
          class_eval("def #{arg};@#{arg};end", __FILE__, __LINE__)
        end
      end

      def where(collection_id:, current_user: nil, recursive: false, **options)
        base_uri = URI.join(Rails.application.credentials.collection_management_site, "api/v1/collections/#{collection_id}/works").to_s
        response = Faraday.new(base_uri, params: options, headers: CollectionManagement::Collection.authorization_header(current_user: current_user)).get

        if response.status == 200
          json = JSON.parse(response.body)["data"]
          return json if options[:pluck]

          collection = CollectionManagement::Work::Collection.new json.map { |c| Work.new(**c.merge(current_user: current_user)) }
          collection.count = JSON.parse(response.body).dig("meta", "count")
          collection.total_count = JSON.parse(response.body).dig("meta", "total_count")
          collection
        elsif response.status == 404
          raise ActiveRecord::RecordNotFound.new, "not found (#{response.status})"
        elsif response.status == 400
          raise ActionController::BadRequest.new, JSON.parse(response.body)["error"]
        elsif response.status == 401 && recursive == false
          current_user&.refresh!(force: true)
          where(collection_id: collection_id, current_user: current_user, recursive: true)
        else
          current_user&.refresh!(force: true)
          raise ActiveRecord::RecordNotFound.new, "not found (#{response.status})"
        end
      end

      def find(id, current_user: nil, collection_id: nil)
        url = collection_id ? URI.join(Rails.application.credentials.collection_management_site, "api/v1/collections/#{collection_id}/works/#{id}").to_s : URI.join(Rails.application.credentials.collection_management_site, "api/v1/works/#{id}").to_s
        response = Faraday.new(url, headers: CollectionManagement::Collection.authorization_header(current_user: current_user)).get
        if response.status == 200
          json = JSON.parse(response.body)["data"]
          Work.new(**json.merge(current_user: current_user))
        else
          current_user&.refresh!(force: true)
          raise ActiveRecord::RecordNotFound.new, "not found (#{response.status})"
        end
      end
    end

    include ActiveModel::Model

    attr_reader :owner, :current_active_time_span, :selling_price, :default_rent_price, :business_rent_price_ex_vat

    attr_accessor :id, :stock_number, :collection_id, :current_user, :location, :alt_number_1, :alt_number_2, :alt_number_3, :photo_front, :photo_back, :photo_detail_1, :photo_detail_2, :object_creation_year, :condition_work_comments, :condition_frame_comments,
      :location_detail, :location_floor, :artist_name_rendered, :artist_name_rendered_without_years_nor_locality, :frame_size, :work_size, :object_format_code, :url, :description, :title_rendered, :title,
      :publish_selling_price, :minimum_bid, :purchase_price, :purchased_on, :purchase_year, :purchase_price_currency_id, :selling_price_minimum_bid_comments, :balance_category_id, :work_status, :medium_comments, :frame_type, :abstract_or_figurative_rendered,
      :collection_name_extended, :locality_geoname_name, :cluster, :print_rendered, :market_value, :market_value_range, :replacement_value, :replacement_value_range, :market_value_complete, :replacement_value_complete, :market_value_range_complete, :replacement_value_range_complete, :valuation_on, :cached_tag_list, :condition_work_rendered, :condition_frame_rendered,
      :main_collection, :grade_within_collection, :source_comments, :available, :tag_list,
      :title_unknown, :object_creation_year_unknown, :medium_id, :frame_type_id, :print, :print_unknown, :frame_height, :frame_width, :frame_depth, :frame_diameter, :height, :width, :depth, :diameter, :subset_id, :public_description, :abstract_or_figurative, :import_collection_id,
      :height_with_fallback, :width_with_fallback, :depth_with_fallback, :diameter_with_fallback, :for_purchase, :for_rent, :highlight,
      :signature_rendered, :artist_name_for_sorting, :alt_number_4, :alt_number_5, :alt_number_6, :condition_work, :condition_frame, :collection_branch_names, :orientation, :created_at, :availability_status, :purchase_price_currency_iso_4217_code, :updated_at, :significantly_updated_at,
      :artist_unknown, :signature_comments, :no_signature_present, :information_back, :other_comments, :entry_status, :entry_status_description, :image_rights, :publish, :cluster_name, :cluster_id, :owner_id, :permanently_fixed, :placeability_id, :appraisals, :internal_comments, :inventoried, :refound, :new_found, :inventoried_at, :refound_at, :new_found_at,
      :locality_geoname_id, :imported_at, :removed_from_collection_at, :removed_from_collection_note, :fin_balance_item_id, # export fields, not necessarily used in api
      :collection_attributes, :current_active_timespan

    def to_param
      id.to_s
    end

    def to_invoice_line_description_string
      "#{stock_number}: #{artist_name_rendered_without_years_nor_locality} - #{title_rendered}"
    end

    def reload(current_user:)
      self.class.find(id, current_user: current_user)
    end

    def reload!(current_user:)
      result = reload(current_user: current_user)
      return result if result
      raise
    end

    def external_id
      id
    end

    def to_h(minimal: false)
      rv = {
        id: id,
        stock_number: stock_number,
        collection_id: collection_id,
        current_user: current_user,
        location: location,
        alt_number_1: alt_number_1,
        alt_number_2: alt_number_2,
        alt_number_3: alt_number_3,
        photo_front: photo_front,
        photo_back: photo_back,
        photo_detail_1: photo_detail_1,
        photo_detail_2: photo_detail_2,
        object_creation_year: object_creation_year,
        condition_work_comments: condition_work_comments,
        condition_frame_comments: condition_frame_comments,
        location_detail: location_detail,
        location_floor: location_floor,
        artists: artists.map(&:to_h),
        object_categories: object_categories.map(&:to_h),
        techniques: techniques.map(&:to_h),
        damage_types: damage_types.map(&:to_h),
        frame_damage_types: frame_damage_types.map(&:to_h),
        themes: themes.map(&:to_h),
        sources: sources.map(&:to_h),
        artist_name_rendered: artist_name_rendered,
        artist_name_rendered_without_years_nor_locality: artist_name_rendered_without_years_nor_locality,
        frame_size: frame_size,
        work_size: work_size,
        object_format_code: object_format_code,
        url: url,
        description: description,
        title_rendered: title_rendered,
        title: title,
        selling_price: selling_price.to_f,
        minimum_bid: minimum_bid,
        purchase_price: purchase_price,
        purchased_on: purchased_on,
        purchase_year: purchase_year,
        purchase_price_currency_id: purchase_price_currency_id,
        selling_price_minimum_bid_comments: selling_price_minimum_bid_comments,
        balance_category_id: balance_category_id,
        work_status: work_status,
        medium: medium&.to_h,
        medium_comments: medium_comments,
        frame_type: frame_type&.to_h,
        abstract_or_figurative_rendered: abstract_or_figurative_rendered,
        style: style&.to_h,
        subset: subset&.to_h,
        placeability: placeability&.to_h,
        collection_name_extended: collection_name_extended,
        locality_geoname_name: locality_geoname_name,
        cluster: cluster&.to_h,
        print_rendered: print_rendered,
        market_value_complete: market_value_complete,
        replacement_value_complete: replacement_value_complete,
        market_value_range_complete: market_value_range_complete,
        replacement_value_range_complete: replacement_value_range_complete,
        valuation_on: valuation_on,
        cached_tag_list: cached_tag_list,
        condition_work_rendered: condition_work_rendered,
        condition_frame_rendered: condition_frame_rendered,
        main_collection: main_collection,
        grade_within_collection: grade_within_collection,
        source_comments: source_comments,
        owner: owner&.to_h,
        title_unknown: title_unknown,
        object_creation_year_unknown: object_creation_year_unknown,
        print: print,
        print_unknown: print_unknown,
        height_with_fallback: height_with_fallback,
        width_with_fallback: width_with_fallback,
        depth_with_fallback: depth_with_fallback,
        diameter_with_fallback: diameter_with_fallback,
        public_description: public_description,
        abstract_or_figurative: abstract_or_figurative,
        availability_status: availability_status,
        default_rent_price: default_rent_price.to_f,
        for_purchase: for_purchase,
        for_rent: for_rent,
        highlight: highlight,
        publish: publish,
        business_rent_price_ex_vat: business_rent_price_ex_vat.to_f,
        removed_from_collection_at: removed_from_collection_at
      }
      rv[:current_active_time_span] = current_active_time_span.to_h unless minimal
      rv
    end

    def to_minimal_h
      to_h(minimal: true)
    end

    def public_to_h
      hash = to_h.except(*(PRIVATE_ATTRIBUTES + ATTRIBUTES_TO_EXCLUDE_FOR_MINIMAL))
      if !publish_selling_price
        hash[:default_rent_price_ex_vat] = nil
        hash[:default_rent_price] = nil
        hash[:selling_price] = nil
      end
      hash
    end

    name_ids_attr_accessor :object_categories, :techniques, :frame_damage_types, :damage_types, :sources, :themes
    name_id_attr_accessor :medium, :style, :subset, :placeability

    def to_h_without_empty_values
      public_to_h.select { |k, v| v.present? || v == false }
    end

    def collection
      CollectionManagement::Collection.find(collection_id)
    end

    def artists= artists
      @artists = artists.map { |hash| Artist.new(**hash) }
    end

    def artists
      @artists || []
    end

    def work_sets= work_sets
      @work_sets = work_sets.map { |hash| CollectionManagement::WorkSet.new(**hash) }
    end

    def work_sets
      @work_sets || []
    end

    def time_spans= time_spans
      @time_spans = time_spans.collect { |a| CollectionManagement::TimeSpan.new(**a) }.each { |a| a.subject = self }
    end

    def time_spans
      @time_spans || []
    end

    def current_reservations
      time_spans.select(&:current_reservation?)
    end

    def owner= hash
      @owner = Owner.new(**hash) if hash
    end

    def current_active_time_span= hash
      ts = CollectionManagement::TimeSpan.new(**hash)
      ts.subject = self
      @current_active_time_span = ts
    end

    def new_event
      WorkEvent.new(work: self)
    end

    def mark_event!(event_type:, current_user:, contact_uri: nil, status: :concept, time_span_uuid: nil)
      event = new_event
      event.contact_uri = contact_uri
      event.event_type = event_type
      event.status = status
      event.time_span_uuid = time_span_uuid
      event.save(current_user: current_user)
    end

    # for activemodel integration

    def _read_attribute a
      send(a)
    end

    def marked_for_destruction?
      false
    end

    def destroyed?
      false
    end

    def new_record?
      false
    end

    def lent?
      ["lend", "lent"].include? availability_status.to_s
    end

    def reserved?
      availability_status.to_s == "reserved"
    end

    def == other
      id == other.id
    end

    def availability_status_extended
      if availability_status.to_s == "available" && for_purchase && for_rent
        availability_status
      elsif availability_status.to_s == "available" && for_purchase
        :available_for_purchase
      elsif availability_status.to_s == "available" && for_rent
        :available_for_rent
      elsif availability_status.to_s == "available"
        :available_not_for_rent_or_purchase
      else
        availability_status
      end
    end

    def selling_price= selling_price
      @selling_price = selling_price unless selling_price == 0
    end

    def default_rent_price= price
      @default_rent_price = if price == 0 || price.blank?
        nil
      else
        price.to_d
      end
    end

    def business_rent_price_ex_vat= price
      @business_rent_price_ex_vat = if price == 0 || price.blank?
        nil
      else
        price.to_d
      end
    end

    def default_rent_price_ex_vat
      return nil unless default_rent_price
      default_rent_price / 1.21
    end

    def artist_owned?
      !!owner&.creating_artist
    end

    def external_owner?
      !!owner&.name.to_s.start_with?("Eigendom Gemeente")
    end

    private

    def _assign_attribute(k, v)
      setter = :"#{k}="
      if respond_to?(setter)
        public_send(setter, v)
      elsif defined?(Rails)
        Rails.logger.warn("UnknownAttributeError for: #{k}")
      else
        puts "UnknownAttributeError for: #{k}"
      end
    end
  end
end
