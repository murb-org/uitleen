module CollectionManagement
  class Owner < NameId
    attr_accessor :creating_artist

    def to_h
      {creating_artist: creating_artist}
    end
  end
end
