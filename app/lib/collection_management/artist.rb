module CollectionManagement
  class Artist
    include ActiveModel::Model

    attr_accessor :name, :id, :first_name, :prefix, :last_name, :year_of_birth, :year_of_death, :rkd_artist_id, :artist_name, :place_of_birth, :place_of_death, :description, :description_in_context, :collection_attributes

    def to_h
      {name: name, id: id, first_name: first_name, prefix: prefix, last_name: last_name, year_of_birth: year_of_birth, year_of_death: year_of_death, rkd_artist_id: rkd_artist_id, artist_name: artist_name}
    end
  end
end
