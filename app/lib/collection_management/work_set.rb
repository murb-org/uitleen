module CollectionManagement
  class WorkSet
    include ActiveModel::Model

    attr_accessor :work_set_type, :identification_number, :appraisal_notice, :comment, :uuid, :id
    attr_reader :works

    class << self
      def find(uuid, options = {})
        base_uri = URI.join(Rails.application.credentials.collection_management_site, "api/v1/work_sets/#{uuid}.json").to_s
        response = Faraday.new(base_uri, params: options, headers: CollectionManagement::Collection.authorization_header).get

        if response.status == 200
          json = JSON.parse(response.body)["data"]

          WorkSet.new(**json)
        elsif response.status == 400
          raise ActionController::BadRequest.new, JSON.parse(response.body)["error"]
        else
          raise ActiveRecord::RecordNotFound.new, "not found (#{response.status})"
        end
      end
    end

    def works=(works)
      @works = works.map do |work|
        if work.is_a? Work
          work
        else
          Work.new(**work)
        end
      end
    end

    def to_param
      uuid
    end

    def to_h
      {name: name, uuid: uuid}
    end

    alias_attribute :external_id, :uuid
  end
end
