module CollectionManagement
  class Collection
    include ActiveModel::Model
    attr_accessor :name, :id, :current_user, :parent_collection_id, :name_extended

    def to_param
      id.to_s
    end

    def works(options = {})
      options = {limit: 24}.merge(options)
      Work.where(current_user: current_user, collection_id: id, **options)
    end

    def find_work id
      Work.find(id, current_user: current_user, collection_id: self.id)
    end

    def to_h
      {name: name, id: id, parent_collection_id: parent_collection_id}
    end

    class << self
      def authorization_header(current_user: nil)
        if current_user
          {"Authorization" => "Bearer #{current_user.refresh!.id_token}"}
        else
          {"Authorization" => "Bearer #{OAuthClient.collection_management.access_token}"}
        end
      end

      def all(current_user: nil, include_children: false, recursive: false)
        response = Faraday.new(URI.join(Rails.application.credentials.collection_management_site, "api/v1/collections").to_s, params: {include_children: include_children}, headers: authorization_header(current_user: current_user)).get
        if response.status == 200
          json = JSON.parse(response.body)["data"]
          json.map { |c| Collection.new(**c.merge(current_user: current_user)) }
        elsif [401, 404].include?(response.status)
          if recursive == false && current_user
            current_user.refresh!(force: true)
            all(current_user: current_user, include_children: include_children, recursive: true)
          else
            raise ActiveRecord::RecordNotFound.new, "not found (#{response.status})"
          end
        elsif response.status == 400
          raise ActionController::BadRequest.new, "bad request (#{response.status})"
        elsif current_user
          current_user.refresh!(force: true)
          []
        else
          []
        end
      rescue JSON::ParserError => e
        if e.message.match?("JWT::ExpiredSignature")
          current_user.refresh!(force: true)
        end
      end

      def find(id, current_user: nil)
        all(current_user: current_user, include_children: true).find { |c| c.id.to_s == id.to_s }
      end
    end
  end
end
