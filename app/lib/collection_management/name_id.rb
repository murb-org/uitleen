module CollectionManagement
  class NameId
    include ActiveModel::Model

    attr_accessor :name, :id

    def to_h
      {name: name, id: id}
    end
  end
end
