module ClearRedirect
  extend ActiveSupport::Concern

  included do
    def clear_redirect_destination dest, default: root_url
      if dest&.start_with?(root_url)
        dest
      else
        default
      end
    end
  end
end
