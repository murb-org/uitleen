class InvoicesController < ApplicationController
  before_action :configure_twinfield, only: %i[show show_print index]
  before_action :set_invoice, except: [:index, :create]
  before_action :set_sales_invoice, except: [:index, :create]
  before_action :set_customer

  def show
    respond_to do |format|
      format.html { render :show }
      format.pdf do
        if !@invoice.finalized?
          redirect_to invoice_url(@invoice), notice: "Factuur is niet definitief"
        elsif @invoice.pdf.present? && !force_new?
          redirect_to url_for(@invoice.pdf)
        else
          count = params[:count].to_i + 1
          if count > 8
            redirect_to invoice_url(@invoice, format: :html), notice: "PDF kon niet worden gegenereerd, probeer het later nog eens."
          else
            GeneratePdfInvoiceWorker.perform_async(@invoice.id, {"force" => force_new?})
            sleep(3)
            redirect_to invoice_url(@invoice, format: :pdf, count: count)
          end
        end
      end
    end
  end

  def edit
  end

  def update
    authorize! :update, @invoice
    if @invoice.update(update_params)
      @invoice.pdf = nil
      @invoice.save
      redirect_to @invoice, notice: "De factuur is aangepast."
    else
      render :edit
    end
  end

  def show_print
  end

  def specification
  end

  def register_payment
    authorize! :register_payment, @invoice

    @invoice.create_payment_transaction_and_match!(payment_type_param)
    UpdateTransactionCopiesWorker.perform_async

    redirect_to invoice_path(@invoice), notice: "De betaling is verwerkt"
  rescue Invoice::NotFinalError
    redirect_to invoice_path(@invoice), alert: "De factuur is nog niet defintief"
  end

  def old_data
  end

  def create
    authorize! :create, @invoice

    new_invoice = Invoice.new(invoice_params.merge(created_by_user: current_user))

    if new_invoice.save
      notice = if new_invoice.credit_invoice?
        "Creditfactuur concept aangemaakt. Let op: met het maken van een creditfactuur worden eventuele uitleenacties niet stopgezet."
      else
        "Factuur in concept aangemaakt. Let op: indien deze niet voor het einde van de maand wordt verstuurd wordt er een maand gemist in de facturering."
      end
      redirect_to draft_invoice_path(new_invoice), notice: notice
    else
      redirect_to invoice_path(new_invoice.credits_invoice), notice: "Creditfactuur kon niet gemaakt worden (#{new_invoice.errors.full_messages.to_sentence})"
    end
  end

  def on_hold
    authorize! :on_hold, @invoice

    message = "Het veranderen van de on-hold status is niet gelukt."

    if @invoice.on_hold?
      @invoice.cancel_on_hold
      if @invoice.save
        message = "De factuur staat niet langer on hold en zal automatisch worden verstuurd indien deze niet ouder is dan 1 week."
      end
    else
      @invoice.on_hold(current_user)
      if @invoice.save
        message = "De factuur is on hold geplaatst en de vervolgfactuur zal niet meer automatisch worden verstuurd."
      end
    end

    redirect_to invoice_path(@invoice, {preview: true}), notice: message
  end

  def index
    authorize! :read, Invoice

    if @customer && !params[:scope]
      return redirect_to customer_invoices_path(@customer, scope: "final")
    elsif !params[:scope]
      return redirect_to invoices_path(scope: "recent")
    end

    invoices = if @customer
      @customer.invoices
    else
      Invoice
    end.includes(:invoice_items).order(finalized_at: :desc, updated_at: :desc)

    @period = @customer ? 10.year.ago : 1.month.ago
    open_invoices = invoices.where(finalized_at: nil)
    @open_invoices = open_invoices.no_review_required.limit(50).where(updated_at: (2.week.ago...))
    @review_invoices = open_invoices.requires_review.where(created_at: (2.month.ago...))

    @finalized_invoices = invoices.where.not(finalized_at: nil).where(finalized_at: (@period...))
    upcoming_invoices = invoices.soon_to_expire(1.month.from_now).unscope(:order).order(:next_invoice_at)

    @upcoming_invoices_not_on_hold = upcoming_invoices.not_on_hold
    @upcoming_invoices_on_hold = upcoming_invoices.on_hold
  end

  private

  def id_param
    params[:invoice_id] || params[:id]
  end

  def set_sales_invoice
    return @sales_invoice if @sales_invoice

    external_invoice_number_param = if id_param.starts_with? "external_invoice_number:"
      id_param.split(":")[1]
    end

    if external_invoice_number_param
      @sales_invoice = Twinfield::SalesInvoice.find(external_invoice_number_param, invoicetype: "FACTUUR")
      authorize! :show, @sales_invoice
    end
  end

  def set_invoice
    @invoice ||= Invoice.find_by_id_param!(id_param)

    authorize! :show, @invoice

    if @invoice.has_external_invoice?(false)
      @sales_invoice = @invoice.external_invoice # cached_external_invoice_with_fallback(max_age: 3.minutes)
    end
  end

  def invoice_params
    params.require(:invoice).permit(:credits_invoice_id, :manually_create_next_for_invoice_id)
  end

  def set_customer
    @customer = if @sales_invoice.is_a?(Twinfield::SalesInvoice) && @sales_invoice.customer_code
      Customer.create_or_update_from_twinfield_customer_code(@sales_invoice.customer_code)
    elsif @invoice
      @invoice.customer
    else
      Customer.find_by_uuid(params[:customer_id])
    end
  end

  def payment_type_param
    Rails.application.config_for(:uitleen)[:payment_transactions].keys.find { |a| a.to_s == params[:payment_type] }
  end

  def force_new?
    params["force_new"] == "true"
  end

  def update_params
    params.require(:invoice).permit(:po_number, :reference, :footertext)
  end
end
