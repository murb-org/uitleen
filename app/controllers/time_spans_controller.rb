class TimeSpansController < ApplicationController
  DISPLAY_SCOPES = [:reservations, :rental_outgoing, :history]
  before_action :set_customer
  before_action :set_time_span, only: [:show]

  def index
    @display_scope = DISPLAY_SCOPES.include?(params[:scope]&.to_sym) ? params[:scope].to_sym : DISPLAY_SCOPES.first

    if @display_scope == :reservations
      @current_reservations = CollectionManagement::TimeSpan.where(current_user: current_user, contact_url: @customer.uri, status: [:reservation], current: true, limit: 100).select(&:subject).select(&:subject)
    elsif @display_scope == :rental_outgoing
      @current_rentals = CollectionManagement::TimeSpan.where(current_user: current_user, contact_url: @customer.uri, status: [:active], classification: :rental_outgoing, limit: 100).select(&:subject)
    elsif @display_scope == :history
      @purchases = CollectionManagement::TimeSpan.where(current_user: current_user, contact_url: @customer.uri, status: [:active, :finished], classification: :purchase, limit: 100).select(&:subject)
      @finalized_rentals = CollectionManagement::TimeSpan.where(current_user: current_user, contact_url: @customer.uri, status: [:finished], classification: :rental_outgoing, limit: 100).select(&:subject)
    end
  end

  def show
  end

  private

  def set_time_span
    @time_span = CollectionManagement::TimeSpan.find(params[:id])
    raise ActiveRecord::RecordNotFound.new("Timespan not found for this user") if @time_span.contact_url != @customer.uri
  end

  def set_customer
    @customer = Customer.find_by!(uuid: params[:customer_id])
  end
end
