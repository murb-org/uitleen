class CustomersController < ApplicationController
  before_action :configure_twinfield, except: [:new]
  before_action :set_customer, except: [:index, :new, :create]

  def index
    authorize! :index, Customer

    @customers = Customer.search(params[:q])

    respond_to do |format|
      format.html { @customers = @customers.limit(100).all }
      format.csv do
        authorize!(:download, Customer)

        fields = %w[name postal_code external_customer_id title initials first_name last_name_prefix last_name import_id customer_type company_name email last_invoice_finalized_at]
        table = [fields]
        @customers.includes(:invoices).all.each do |customer|
          table << fields.map { |field| customer.send(field) }
        end

        render body: table.collect { |a| a.to_csv }.join("")
      end
    end
  end

  def new
    authorize! :new, Customer
    @customer = Customer.new
  end

  def create
    authorize! :create, Customer
    twinfield_params = twinfield_customer_params
    range = (customer_params[:customer_type] == "business") ? Twinfield::Customer::BUSINESS_CUSTOMER_CODE_RANGE : Twinfield::Customer::PRIVATE_CUSTOMER_CODE_RANGE
    twinfield_params[:code] = Twinfield::Customer.next_unused_twinfield_customer_code(range)
    twinfield_customer = Twinfield::Customer.new(**twinfield_params)
    twinfield_customer = twinfield_customer.save
    if twinfield_customer&.uid
      @customer = Customer.create_or_update_from_twinfield_customer_code(twinfield_customer.code)
      @customer.update(customer_params)
      redirect_to customer_path(@customer), notice: "Klant is toegevoegd"
    else
      render :new
    end
  end

  def show
    authorize! :show, @customer
  end

  def edit
    authorize! :edit, @customer
    @customer.update_with_twinfield_data!(max_age: 1.minute)

    if params[:add_address] == "true"
      @customer.twinfield_customer.addresses << Twinfield::Customer::Address.new(name: "Nieuw adres", id: (rand * 1000000).to_i)
    end
  end

  def update
    authorize! :update, @customer

    twinfield_save = true

    if twinfield_customer_params
      twinfield_params = @customer.twinfield_raw.deep_merge(twinfield_customer_params)
      twinfield_customer = Twinfield::Customer.new(**twinfield_params)
      twinfield_save = twinfield_customer.save
    end

    if @customer.update(customer_params) && twinfield_save
      redirect_to customer_path(@customer), notice: "Klant is bijgewerkt"
    else
      render :edit
    end
  rescue Twinfield::Create::Error => e
    flash[:alert] = e.message
    render :edit
  end

  def destroy
    authorize! :destroy, @customer
    @customer.deactivate!
    if @customer.valid?
      @customer.twinfield_customer.destroy
    end
    if @customer.save
      redirect_to customers_path, notice: "De klant is gedeactiveerd."
    else
      redirect_to customer_path(@customer), notice: "De klant kon niet worden verwijderd, controleer de status van de facturen."
    end
  end

  def raw
  end

  private

  def set_customer
    @customer = Customer.find_by!(uuid: params[:customer_id] || params[:id])
    @customer.update_with_twinfield_data!(max_age: 3.hour)
  end

  def customer_params
    params.require(:customer).permit(:comments, :first_name, :last_name_prefix, :last_name, :company_name, :customer_type, :email, :auto_finalize_invoices, :initials, :company_vat, :company_coc)
  end

  def permitted_twinfield_customer_params
    params.require(:customer).permit(twinfield_customer: [
      :name, :website, :cocnumber, :vatnumber,
      twinfield_customer_banks: [
        :iban, :accountnumber, :bankname, :biccode, :city, :country, :natbiccode, :postcode, :state, :default, :ascription, address: [
          :name, :field1, :field2, :field3, :field4, :field5, :field6, :contact, :postcode, :city, :telephone, :email, :telefax, :type, :country
        ]
      ],
      twinfield_customer_addresses: [
        :_destroy, :postcode, :id, :name, :field1, :field2, :field3, :field4, :field5, :field6, :contact, :telephone, :email, :telefax, :city, :country, :default, :type
      ]
    ]).to_h.deep_symbolize_keys[:twinfield_customer]
  end

  def twinfield_customer_params
    return nil if params.require(:customer)[:twinfield_customer].nil?
    return @twinfield_customer_params if @twinfield_customer_params

    unprepared_permitted_twinfield_customer_params = permitted_twinfield_customer_params

    temp_customer = Customer.new(customer_params)

    unprepared_permitted_twinfield_customer_params[:name] = temp_customer.derived_name

    twinfield_customer_addresses = unprepared_permitted_twinfield_customer_params.delete(:twinfield_customer_addresses)

    address_index = 1

    unprepared_permitted_twinfield_customer_params[:addresses] = (twinfield_customer_addresses&.values || []).map do |a|
      if a.delete(:_destroy) != "1"
        a[:default] = (a[:default] == "1")
        a[:name] = "Adres #{address_index}" unless a[:name].present?
        a[:id] = address_index
        address_index += 1
        a
      end
    end.compact

    bank_index = 1
    twinfield_customer_banks = unprepared_permitted_twinfield_customer_params.delete(:twinfield_customer_banks)
    unprepared_permitted_twinfield_customer_params[:banks] = (twinfield_customer_banks&.values || []).map do |a|
      a[:default] = (a[:default] == "1")
      a[:id] = bank_index
      a[:biccode] = nil
      a[:natbiccode] = nil
      a[:bankname] = nil
      bank_index += 1
      a
    end

    unprepared_permitted_twinfield_customer_params[:remittanceadvice] = {sendtype: "ByEmail", sendmail: temp_customer.email} if temp_customer.email
    unprepared_permitted_twinfield_customer_params[:financials] = {ebillmail: temp_customer.email, ebilling: true} if temp_customer.email

    @twinfield_customer_params = unprepared_permitted_twinfield_customer_params
  end
end
