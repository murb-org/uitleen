class SessionsController < ApplicationController
  include ClearRedirect

  # If you're using a strategy that POSTs during callback, you'll need to skip the authenticity token check for the callback action only.
  skip_before_action :verify_authenticity_token, only: :create
  skip_before_action :authenticate!

  def create
    redirect_dest = clear_redirect_destination(session[:redirect_to])

    if params[:provider] == "central_login"
      user = User.from_auth_hash(auth_hash)
      session[:user_sub] = user.sub
      session[:last_action_at] = Time.current
      @current_user ||= user
    else
      OAuthClient.find_or_create_from_auth_hash(auth_hash)
    end
    redirect_to redirect_dest, notice: "Koppeling met #{params[:provider]} gemaakt"
  end

  def new
    @redirect_to = clear_redirect_destination(params[:redirect_to])
    session[:redirect_to] = @redirect_to
  end

  def destroy
    session[:user_sub] = nil
    redirect_to root_path, notice: "Je bent uitgelogd."
  end

  protected

  def auth_hash
    request.env["omniauth.auth"]
  end
end
