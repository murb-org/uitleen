class ReturnsController < ApplicationController
  before_action :set_collection
  before_action :set_work
  def new
    redirect_to work_path(@work) if @work.current_active_time_span.nil?

    # @return_form = ReturnFrom.new(condition_work_comments: @work.condition_work_comments, condition_frame_comments: @work.condition_frame_comments)
  end

  def create
    work_event = CollectionManagement::WorkEvent.new(work: @work, contact_uri: @work.current_active_time_span.contact_url, time_span_uuid: @work.current_active_time_span.uuid, status: :finished)
    if work_event.save(current_user: current_user)
      redirect_to @work.current_active_time_span.contact_url, notice: "Het retourneren is verwerkt."
    else
      redirect_to new_collection_work_return_path(@collection, @work), notice: "Het verwerken is niet goed gegaan, probeer het opnieuw"
    end
    # @return_form
  end

  private

  def set_collection
    current_user.refresh!
    @collection = CollectionManagement::Collection.find(params[:collection_id], current_user: current_user)
    raise ActionController::RoutingError.new("Not Found") if @collection.nil?
  end

  def set_work
    @work = @collection.find_work(params[:work_id])
  end
end
