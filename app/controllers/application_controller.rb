class ApplicationController < ActionController::Base
  helper_method :current_user
  helper_method :draft_invoice
  before_action :authenticate!, except: :status
  before_action :set_paper_trail_whodunnit

  def home
    Finance.configure_twinfield
    OAuthClient.collection_management(force_refresh: true)
  end

  def status
    require Rails.root.join("app", "workers", "test_worker")

    OAuthClient.collection_management
    collections = CollectionManagement::Collection.all(current_user: nil)
    collection = collections.first
    work = collection.works.first
    @work = CollectionManagement::Work.find(work.id)

    TestWorker.perform_async
    @sidekiq_running = TestWorker.alive?

    # can compose a message with header configuration (reason: this failed badly with upgrade of mailer 2.7.1 to 2.8)
    # SystemMailer.error_message(StandardError.new(message: "test"))
  end

  private

  def authenticate!
    unless current_user
      redirect_to auth_new_path(redirect_to: request.url), alert: "Je moet ingelogd zijn om deze pagina te kunnen bekijken."
    end
  end

  def current_user
    return unless session[:user_sub] && session.loaded? # pdf/html generation

    if session[:user_sub] && (session[:last_action_at].nil? || session[:last_action_at] < 2.hours.ago)
      session[:user_sub] = nil
    else
      session[:last_action_at] = Time.current
      @current_user ||= User.find_by(sub: session[:user_sub])
    end
  end

  def draft_invoice
    @draft_invoice ||= Invoice.where(id: session[:draft_invoice_id]).first || Invoice.create(created_by_user_id: current_user.id)
    session[:draft_invoice_id] = @draft_invoice.id
    @draft_invoice
  end

  def configure_twinfield
    Finance.configure_twinfield
  rescue Finance::TwinfieldNotConfiguredError
    redirect_to auth_new_path, notice: "Twinfield connectie is niet gemaakt"
  end

  rescue_from CanCan::AccessDenied do
    if current_user
      redirect_to root_path, alert: "Je hebt geen toegang tot deze pagina"
    else
      authenticate!
    end
  end
end
