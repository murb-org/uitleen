class CollectionsController < ApplicationController
  def index
    current_user.refresh!
    @collections = CollectionManagement::Collection.all(current_user: current_user, include_children: true)
    if @collections.count == 1
      redirect_to collection_works_path(@collections.first)
    end
  end

  def show
    if params[:id] == "auto"
      redirect_to collection_works_path(Rails.application.config_for(:uitleen)[:collection_id])
    else
      redirect_to collections_path, notice: "Not supported"
    end
  end
end
