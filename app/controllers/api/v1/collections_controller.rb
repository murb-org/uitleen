class Api::V1::CollectionsController < Api::V1::ApiController
  before_action :authenticate_activated_user!

  def index
    @collections = (params[:include_children] == "true") ? current_api_user.accessible_collections : current_api_user.collections
    render json: @collections.map(&:to_h)
  end

  def show
    @collection = current_api_user.accessible_collections.find(params[:id])
    render json: @collection.to_h
  end
end
