class Api::V1::CustomersController < Api::V1::ApiController
  before_action :authenticate_activated_user!
  def index
    api_authorize! :read, Customer
    render json: {data: Customer.select(:name, :uuid, :customer_type).map(&:to_h)}
  end

  def show
    api_authorize! :read, Customer
    render json: {data: Customer.select(:name, :uuid, :customer_type).find_by_uuid(params[:id]).to_h}
  end
end
