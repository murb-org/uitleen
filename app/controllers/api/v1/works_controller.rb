# frozen_string_literal: true

class Api::V1::WorksController < Api::V1::ApiController
  before_action :authenticate_activated_user!
  before_action :set_collection

  def index
    options = {}
    options[:limit] = params[:limit].to_i if params[:limit].to_i > 0
    options[:from] = params[:from].to_i if params[:from].to_i > 0
    options[:significantly_updated_since] = DateTime.parse(params[:significantly_updated_since]).iso8601 if params[:significantly_updated_since]
    options[:sort] = params[:sort] if params[:sort]
    options[:id_gt] = params[:id_gt] if params[:id_gt]
    options[:q] = params[:q] if params[:q]

    raise ActiveRecord::RecordNotFound.new("collection not found") if @collection.nil?

    @works = @collection.works(options)

    render json: {meta: {count: @works.count, total_count: @works.total_count}, data: @works.each { |a| a.current_user = nil }.map(&:to_h_without_empty_values)}
  end

  def show
    @work = CollectionManagement::Work.find(params[:id], current_user: current_api_user)
    @work.current_user = nil

    render json: {data: @work.to_h_without_empty_values}
  end

  private

  def set_collection
    @collection ||= current_api_user.accessible_collections.find { |c| c.id.to_s == params[:collection_id] } if params[:collection_id]
  end
end
