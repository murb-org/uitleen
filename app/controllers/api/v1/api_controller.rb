# frozen_string_literal: true

class Api::V1::ApiController < ActionController::Base
  helper_method :current_api_user

  def api_authorize! action, subject
    Ability.new(current_api_user).authorize! action, subject
  end

  def current_api_user
    return @user if @user
    if current_user
      @user ||= Api::V1::Human.new(user_data: current_user.attributes, id_token: current_user.id_token)
    elsif request.headers["Authorization"]&.starts_with?("Bearer ")
      id_token = request.headers["Authorization"].gsub(/\ABearer\s+/, "")
      user_data = central_login_oauth_strategy.validate_id_token(id_token)

      if user_data["sub"].starts_with?("app-")
        @user = Api::V1::App.new(user_data: user_data, id_token: id_token)
      else
        @user = Api::V1::Human.new(user_data: user_data, id_token: id_token)

        not_authorized("Insufficient rights to use the API") unless Ability.new(@user).can?(:access, Api::V1::ApiController)
      end
    else
      not_authorized("No Bearer token supplied")
    end
  rescue JWT::ExpiredSignature
    not_authorized("JWT token is expired")
  end

  def authenticate_activated_user!
    current_api_user
  end

  private

  def unprocessable_entity
    render json: {
      message: "Unprocessable entity"
    }, status: 422
    false
  end

  def not_authorized additional_message = nil
    render json: {
      message: ["Not authorized", additional_message].compact.join(" "),
      nuid: request.headers["X-user-id"].to_i,
      data: "#{request.remote_ip}#{request.url}#{request.body&.read})",
      your_remote_ip: request.remote_ip
    }, status: 401
    false
  end

  def central_login_oauth_strategy
    @central_login_oauth_strategy ||= ::OmniAuth::Strategies::CentralLogin.new(:central_login, Rails.application.credentials.central_login_id, Rails.application.credentials.central_login_secret, client_options: {site: Rails.application.credentials.central_login_site})
  end

  def current_user
    @current_user ||= User.find_by(sub: session[:user_sub]) if session[:user_sub]
  end
end
