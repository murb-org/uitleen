class ReservationsController < ApplicationController
  before_action :set_work
  before_action :set_reservation, only: %i[show]

  # GET /reservations or /reservations.json
  def index
    @reservations = if @work
      @work.current_reservations
    else
      raise "Not implemented"
    end
  end

  # GET /reservations/1 or /reservations/1.json
  def show
    redirect_to collection_work_reservation_path(@work.collection_id, @work.id, @reservation) unless params[:work_id]
  end

  # GET /reservations/new
  def new
    @reservation = Reservation.new
    @reservation.customer_id = draft_invoice.customer_id
  end

  # POST /reservations or /reservations.json
  def create
    if reservation_params[:customer_id]
      @work_event = CollectionManagement::WorkEvent.new(status: :reservation, comments: reservation_params[:comments], event_type: reservation_params[:intent], work: @work, contact_uri: url_for(Customer.find(reservation_params[:customer_id])))
    else
      @reservation = Reservation.new(reservation_params)
      @reservation.work = @work

      @work_event = CollectionManagement::WorkEvent.new(status: :reservation, comments: reservation_params[:comments], event_type: reservation_params[:intent], work: @work, contact: CollectionManagement::Contact.new(name: reservation_params[:name], external: true, remote_data: @reservation.attributes))

    end
    if @work_event.save(current_user: current_user)
      redirect_to collection_work_path(@work.collection_id, @work), notice: "De reservering is gemaakt."
    else
      render :new, status: :unprocessable_entity
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_reservation
    @reservation = CollectionManagement::TimeSpan.find(params[:id])
    @work = @reservation.work
  end

  def set_work
    if params[:work_id]
      @work = CollectionManagement::Work.find(params[:work_id], current_user: current_user)
    else
      @work
    end
  end

  # Only allow a list of trusted parameters through.
  def reservation_params
    params.require(:reservation).permit(:work_uuid, :customer_id, :name, :address, :postal_code, :city, :email, :telephone, :comments, :intent)
  end
end
