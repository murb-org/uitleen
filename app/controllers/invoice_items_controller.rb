class InvoiceItemsController < ApplicationController
  before_action :set_invoice
  before_action :set_invoice_item, only: %i[edit update destroy edit_discount]
  authorize_resource

  # GET /invoice_items/new
  def new
    @invoice_item = InvoiceItem.new(intent: :purchase)
  end

  # GET /invoice_items/1/edit
  def edit
  end

  # POST /invoice_items or /invoice_items.json
  def create
    @invoice_item = InvoiceItem.new(invoice_item_params)
    @invoice_item.invoice = @draft_invoice

    if @invoice_item.save
      redirect_to draft_invoice_url(@draft_invoice), notice: "Te factureren item toegevoegd."
    else
      render :new, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /invoice_items/1 or /invoice_items/1.json
  def update
    if @invoice_item.update(invoice_item_params)
      redirect_to draft_invoice_url(@draft_invoice), notice: "Te factureren item is bijgewerkt."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  # DELETE /invoice_items/1 or /invoice_items/1.json
  def destroy
    @invoice_item.destroy

    redirect_to draft_invoice_url(@draft_invoice), notice: "De regel is verwijderd"
  end

  def edit_discount
    authorize! :edit, @invoice_item
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_invoice_item
    @invoice_item = InvoiceItem.find(params[:invoice_item_id] || params[:id])
    @draft_invoice = @invoice_item.invoice
  end

  # Only allow a list of trusted parameters through.
  def invoice_item_params
    invoice_item_parameters = params.require(:invoice_item).permit(:input_price_ex_vat, :quantity, :invoiceable_id, :unit_price, :note, :intent, :invoiceable_type, :discount_amount, :discount_type, gift_card: [:amount, :code, :action])
    if invoice_item_parameters[:gift_card] && invoice_item_parameters[:gift_card][:code] && invoice_item_parameters[:gift_card][:action]
      invoice_item_parameters[:unit_price_ex_vat] = invoice_item_parameters[:gift_card][:amount]
      invoice_item_parameters[:quantity] = 1
      invoice_item_parameters[:intent] = :purchase
      invoice_item_parameters.delete(:unit_price)
    end
    invoice_item_parameters
  end

  def set_invoice
    @draft_invoice ||= Invoice.find(params[:draft_invoice_id])
    Invoice.find(-1) if @draft_invoice.finalized?
  end
end
