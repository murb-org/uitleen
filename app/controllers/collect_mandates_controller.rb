class CollectMandatesController < ApplicationController
  before_action :configure_twinfield
  before_action :set_customer
  before_action :set_collect_mandate, only: [:show, :edit, :update]

  def show
    case @collect_mandate&.status
    when :active
      redirect_to customer_path(@customer), notice: "Automatische incasso is geconfigureerd voor #{@customer.name}"
    when :signed
      redirect_to edit_customer_collect_mandate_path(@customer), notice: "Automatische incasso is getekend"
    when :draft
      redirect_to edit_customer_collect_mandate_path(@customer)
    when :initialized
      redirect_to edit_customer_collect_mandate_path(@customer)
    when nil
      redirect_to new_customer_collect_mandate_path(@customer)
    else
      redirect_to new_customer_collect_mandate_path(@customer), notice: "Onbekende status van automatische incasso, probeer het opnieuw #{@customer.status}"
    end
  end

  def new
    if !@collect_mandate || @collect_mandate&.status == :active
      @collect_mandate = CollectMandate.new(signed_on: Time.now.to_date, iban: @customer.twinfield_customer.banks.map(&:iban).first, customer: @customer)
    else
      redirect_to edit_customer_collect_mandate_path(@customer)
    end
  end

  def edit
  end

  def update
    if @collect_mandate.update(collect_mandate_params)
      if @collect_mandate.status == :signed
        @customer.twinfield_customer.financials.collectmandate = Twinfield::Customer::CollectMandate.new(signaturedate: @collect_mandate.signed_on, id: @collect_mandate.sepa_proof_uuid)
        @customer.twinfield_customer.financials.payavailable = true
        @customer.twinfield_customer.financials.paycode = "SEPANLDD"
        @customer.twinfield_customer.save
        @customer.update_with_twinfield_data!(max_age: 0.seconds)
      end
      redirect_to customer_collect_mandate_path(@customer)
    else
      render :edit
    end
  end

  def create
    @collect_mandate = CollectMandate.new(collect_mandate_params)
    @collect_mandate.customer = @customer
    if @collect_mandate.save
      if @customer.twinfield_customer.banks.select(&:default).map(&:iban).first != @collect_mandate.iban
        @customer.twinfield_customer.banks.each { |a| a.default = false }
        @customer.twinfield_customer.banks << Twinfield::Customer::Bank.new(default: true, iban: @collect_mandate.iban, ascription: @customer.name)
        @customer.twinfield_customer.financials.collectmandate = nil
        @customer.twinfield_customer.save
      end
      redirect_to edit_customer_collect_mandate_path(@customer)
    else
      render :new
    end
  end

  private

  def collect_mandate_params
    params.require(:collect_mandate).permit(:iban, :signed_on, :sign_location, :signed_document)
  end

  def set_customer
    @customer = Customer.find_by!(uuid: params[:customer_id])
  end

  def set_collect_mandate
    @collect_mandate = @customer.collect_mandate
  end
end
