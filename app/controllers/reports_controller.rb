class ReportsController < ApplicationController
  LIMIT = 50

  before_action :authorize_show_report, except: [:index]

  def savings
    @customers_with_savings = Customer.with_savings.offset(params[:offset].to_i).limit(LIMIT)
  end

  def savings_per_year
    @data_per_year = {}
    customers = Customer.with_savings.select(:name, :external_customer_id, :customer_type, :uuid, :id).to_a.group_by(&:external_customer_id)

    @years = 2021..Time.now.year
    @year_data = {}
    @years.each do |year|
      data = ActiveRecord::Base.connection.execute(CostCenter.select("sum(cost_centers.value) AS value, customer_transactions.customer_code").customer_savings.final.join_customer_transactions.group("customer_transactions.customer_code").year(year).to_sql)
      customers.each do |customer_id, customer_set|
        customer = customer_set.first
        @year_data[year] = {data:}
        @data_per_year[customer_id] ||= {name: customer.name, type: customer.customer_type, customer_code: customer.external_customer_id, customer: customer}
        @data_per_year[customer_id][year] = -1 * data.find { |ii| ii["customer_code"] == customer_id }&.fetch("value", 0).to_f
        @data_per_year[customer_id]["#{year}_sum"] = @data_per_year[customer_id][year] + @data_per_year[customer_id]["#{year - 1}_sum"].to_f
      end
    end
    # data = ActiveRecord::Base.connection.execute(CostCenter.select("sum(cost_centers.value) AS value, customer_transactions.customer_code").customer_savings.final.join_customer_transactions.group("customer_transactions.customer_code").year(2021).to_sql)

    @years.each do |year|
      @year_data[year][:delta] = @year_data[year][:data].sum { |ii| -1 * ii["value"].to_f }
      @year_data[year][:sum] = @year_data[year][:delta] + @year_data[year - 1]&.[](:sum).to_f
    end

    # @header = @data_per_year.first.keys
  end

  def lent
    @time_spans = CollectionManagement::TimeSpan.where(current_user: current_user, classification: :rental_outgoing, status: :active, limit: 99999).select { |ts| ts.subject.is_a?(CollectionManagement::Work) }
    time_span_work_ids = @time_spans.map { |ts| ts.subject.id }.compact
    contact_uuids = @time_spans.map { |ts| ts.contact&.uuid }.compact

    @customer_hash = Customer.where(uuid: contact_uuids).map { |ii| [ii.uuid, ii] }.to_h

    credit_invoice_or_credited_invoice_ids = Invoice.where.not(credits_invoice_id: nil).pluck(:id, :credits_invoice_id).flatten
    invoice_items = InvoiceItem.includes(:invoice).works.where(invoiceable_id: time_span_work_ids).where.not(invoices: {id: credit_invoice_or_credited_invoice_ids})
    @invoice_hash = invoice_items.invoicing_period_spans_current.map { |ii| [ii.invoiceable_id, ii.invoice] }.to_h
    @invoice_last_month_hash = invoice_items.invoicing_period_spans_one_month_ago.map { |ii| [ii.invoiceable_id, ii.invoice] }.to_h
  end

  def unfinished_ended
    @time_spans = CollectionManagement::TimeSpan.where(current_user: current_user, classification: :rental_outgoing, status: :active, ends_at_lt: DateTime.current, limit: 99999).select { |ts| ts.subject.is_a?(CollectionManagement::Work) }

    time_span_work_ids = @time_spans.map { |ts| ts.subject.id }.compact
    contact_uuids = @time_spans.map { |ts| ts.contact&.uuid }.compact

    @customer_hash = Customer.where(uuid: contact_uuids).map { |ii| [ii.uuid, ii] }.to_h
    @invoice_hash = InvoiceItem.includes(:invoice).invoicing_period_spans_current.works.where(invoiceable_id: time_span_work_ids).map { |ii| [ii.invoiceable_id, ii.invoice] }.to_h
    @invoice_last_month_hash = InvoiceItem.includes(:invoice).invoicing_period_spans_one_month_ago.works.where(invoiceable_id: time_span_work_ids).map { |ii| [ii.invoiceable_id, ii.invoice] }.to_h
  end

  def index
    authorize! :index, ReportsController
  end

  private

  def authorize_show_report
    authorize! :show, ReportsController
  end
end
