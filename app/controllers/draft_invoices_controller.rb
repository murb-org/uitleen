class DraftInvoicesController < ApplicationController
  include ClearRedirect
  before_action :configure_twinfield, only: %i[create_invoice show finalize_invoice]
  before_action :set_draft_invoice, only: %i[show edit update destroy create_invoice finalize_invoice mutation_form reviewed refresh]
  before_action :sync_remote_invoice, only: :show

  # GET /draft_invoices/1 or /draft_invoices/1.json
  def show
    if @draft_invoice.finalized?
      redirect_to invoice_path(@draft_invoice)
    else
      authorize! :show, @draft_invoice
    end
  end

  # GET /draft_invoices/1/edit
  def edit
    authorize! :edit, @draft_invoice
  end

  # POST /draft_invoices or /draft_invoices.json
  def create
    authorize! :create, Invoice

    @draft_invoice = Invoice.new(draft_invoice_params)

    respond_to do |format|
      if @draft_invoice.save
        format.html { redirect_to draft_invoice_path(@draft_invoice), notice: "De voorlopige factuur is aangemaakt." }
        format.json { render :show, status: :created, location: @draft_invoice }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @draft_invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /draft_invoices/1 or /draft_invoices/1.json
  def update
    authorize! :update, @draft_invoice

    if @draft_invoice.update(draft_invoice_params)
      @draft_invoice.update_rent_quantities_based_on_recurrence_interval
      redirect_to draft_invoice_path(@draft_invoice), notice: "De voorlopige factuur is bijgewerkt."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize! :destroy, @draft_invoice

    if @draft_invoice.destroy
      redirect_to new_draft_invoice_url, notice: "De concept factuur is verwijderd"
    else
      redirect_to draft_invoice_url(@draft_invoice), notice: "De factuur kon niet worden verwijderd"
    end
  end

  def refresh
    authorize! :update, @draft_invoice

    @draft_invoice.invoice_items.works.destroy_all
    @draft_invoice.update_rent_quantities_based_on_recurrence_interval
    @draft_invoice.save
    redirect_to draft_invoice_path(@draft_invoice), notice: "De voorlopige factuur is bijgewerkt."
  end

  def create_invoice
    if @draft_invoice.save_external_invoice!
      redirect_to draft_invoice_url(@draft_invoice), notice: "De factuur in het boekhoudprogramma is bijgewerkt"
    else
      redirect_to draft_invoice_url(@draft_invoice), notice: "Er is een fout opgetreden bij het maken van de factuur in het boekhoudprogramma"
    end
  rescue Twinfield::Create::Error => e
    redirect_to draft_invoice_url(@draft_invoice), notice: "Er is een fout opgetreden bij het maken van de factuur in het boekhoudprogramma: #{e.message}"
  end

  # PATCH
  def finalize_invoice
    authorize! :finalize, Invoice

    if @draft_invoice.finalize!
      finalized_invoice = @draft_invoice

      reset_draft_invoice

      UpdateTransactionCopiesWorker.perform_async

      redirect_to invoice_url(finalized_invoice), notice: "De factuur is defintief in het boekhoudprogramma opgeslagen."
    else
      message = @draft_invoice.errors.full_messages.to_sentence
      if message.blank?
        redirect_to draft_invoice_url(@draft_invoice), notice: "Er is een fout opgetreden bij het defintief aanmaken van de factuur in het boekhoudprogramma"
      else
        redirect_to draft_invoice_url(@draft_invoice), notice: message
      end
    end
  rescue Twinfield::Create::Finalized => e
    redirect_to invoice_url(finalized_invoice), notice: "De factuur kon niet definitief worden gemaakt (#{e.message})"
  end

  def new
    invoiceable_item_collection = if params[:invoiceable_item_collection_type] == "CollectionManagement::WorkSet" && params[:invoiceable_item_collection_external_id]
      CollectionManagement::WorkSet.find(params[:invoiceable_item_collection_external_id])
    end

    reset(with: {
      invoiceable_item_collection: invoiceable_item_collection
    })
  rescue SynchroniseInvoiceWorkSetWorker::UnexpectedMultipleCustomersError, SynchroniseInvoiceWorkSetWorker::NoCustomerSetError, SynchroniseInvoiceWorkSetWorker::CustomerMisMatchError => e
    reset(alert: "Er zit een fout in de werkgroepering en de gekoppelde tijdsspanne, kijk de groepering na in CollectionManagement: #{e.message}")
  end

  def reset(with: {}, notice: nil, alert: nil)
    authorize! :new, Invoice

    @draft_invoice = Invoice.create(with.merge(created_by_user: current_user))
    session[:draft_invoice_id] = @draft_invoice.id
    clean_redirect = clear_redirect_destination(params[:return_to], default: nil)
    if clean_redirect
      redirect_to clean_redirect, notice: notice || "Klantsessie is gereset"
    else
      redirect_to draft_invoice_path(@draft_invoice), notice: notice || "Een leeg voorstel is klaargezet", alert: alert
    end
  end

  def mutation_form
  end

  def reviewed
    authorize! :review, @draft_invoice

    if @draft_invoice.update(requires_review_at: nil)
      redirect_to draft_invoice_path(@draft_invoice), notice: "De factuur is gemarkeerd als gecontroleerd"
    else
      redirect_to draft_invoice_path(@draft_invoice), alert: "De factuur kon niet worden gemarkeerd als gecontroleerd"
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_draft_invoice
    @draft_invoice = Invoice.includes(:invoice_items).find_by_id_param!(id_param)
    if @draft_invoice.finalized?
      # create a new invoice when finalized on draft invoices
      @draft_invoice = Invoice.create(created_by_user: current_user)
      session[:draft_invoice_id] = @draft_invoice.id
    end
    @draft_invoice.created_by_user = current_user

    set_twinfield_invoice
  end
  alias_method :reset_draft_invoice, :set_draft_invoice

  def set_twinfield_invoice
    if @draft_invoice.twinfield_invoicenumber
      @twinfield_invoice = Twinfield::SalesInvoice.find(@draft_invoice.twinfield_invoicenumber, invoicetype: "FACTUUR")
      @twinfield_invoice.uitleen_invoice = @draft_invoice
    end
  rescue Savon::SOAPFault
    Finance.configure_twinfield
  end

  # Only allow a list of trusted parameters through.
  def draft_invoice_params
    params.require(:invoice).permit(:twinfield_customer_code, :footertext, :po_number, :customer_id, :rental_type, :use_customer_savings_for_purchase, :mutation_form, :recurrence_interval_in_months, :requires_review, :reference, :invoice_item_start_date_reset, new_intent_attributes: [:work_id, :intent], remove_intent_attributes: [:work_id, :intent])
  end

  def sync_remote_invoice
    if @draft_invoice.customer && @draft_invoice.invoice_items.count > 0 && !@draft_invoice.finalized_at
      @draft_invoice.save_external_invoice!
      set_twinfield_invoice
    end
  rescue Twinfield::Create::Error => e
    @invoice_error = "Er is iets mis gegaan bij het synchoniseren van de factuur met het boekhoudpakket: #{e.message}"
    Rails.logger.warn "Invoice not synced as it didn't comply (#{e.message})"
  end

  def id_param
    params[:invoice_id] || params[:draft_invoice_id] || params[:id]
  end
end
