class WorksController < ApplicationController
  before_action :set_work, only: :show
  before_action :set_collection

  def index
    authorize! :index, CollectionManagement::Work
    @filter_available = ["on", "1", "true"].include?(params[:filter_available].to_s)
    works_filter = {q: params[:q]}
    works_filter[:filter] = {availability_status: [:available]} if @filter_available
    @works = @collection.works(works_filter)
  rescue ActionController::BadRequest
    @works = []
    @notice = "De zoekvraag kon niet worden begrepen, pas deze aan."
  end

  def show
    @reservations_count = @work.current_reservations.count
  end

  private

  def set_work
    authorize! :show, CollectionManagement::Work
    @work = CollectionManagement::Work.find(params[:id])
    redirect_to collection_work_path(@work.collection, @work) if params[:collection_id].nil?
  end

  def set_collection
    current_user.refresh!

    @collection = CollectionManagement::Collection.find(params[:collection_id], current_user: current_user)
    raise ActionController::RoutingError.new("Not Found") if @collection.nil?
  end
end
