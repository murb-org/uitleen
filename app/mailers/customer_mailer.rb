class CustomerMailer < ApplicationMailer
  helper ApplicationHelper

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.customer_mailer.welcome.subject
  #
  def welcome(customer)
    if customer.email
      @customer = customer
      mail to: customer.email, subject: "Welkom bij Heden"
    end
  end
end
