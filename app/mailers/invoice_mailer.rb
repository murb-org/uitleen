class InvoiceMailer < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.invoice_mailer.created.subject
  #

  def created(invoice)
    if invoice.pdf.present? && !invoice.invoice_sent?
      invoice.pdf.attachment.blob.open do |file|
        attachments["factuur.pdf"] = file.read
      end

      @invoice = invoice
      mail to: invoice.invoice_email_address, subject: "Factuur #{invoice.invoice_number}"
    end
  end
end
