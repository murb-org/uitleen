class ApplicationMailer < ActionMailer::Base
  default from: "info@heden.nl"
  layout "mailer"
end
