# == Schema Information
#
# Table name: articles
#
#  id                       :integer          not null, primary key
#  allow_discount           :boolean          default(TRUE)
#  external_article_code    :string
#  external_article_subcode :string
#  name                     :string
#  vat_rate                 :decimal(, )
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#
class Article < ApplicationRecord
  validates_presence_of :name, :external_article_code
end
