class InvoiceItem::Minimal
  include ActiveModel::Model
  include PeriodHelper

  GROUP_BY = [:note, :intent, :invoiceable_type, :invoicing_period_starts_at, :invoicing_period_ends_at]
  PLUCK = Arel.sql("invoice_items.intent, invoice_items.invoiceable_type, sum(quantity) as quantity, invoicing_period_starts_at, invoicing_period_ends_at, note, min(id) as id")

  attr_accessor :intent, :invoiceable_type, :quantity, :invoicing_period_starts_at, :invoicing_period_ends_at, :note

  class << self
    def summarize(invoice_items)
      invoice_items.group(GROUP_BY).pluck(PLUCK).map { |line| new(*line) }
    end
  end

  def initialize(intent, invoiceable_type, quantity, invoicing_period_starts_at, invoicing_period_ends_at, note, *attrs)
    self.intent = intent
    self.invoiceable_type = invoiceable_type
    self.quantity = quantity
    self.invoicing_period_starts_at = invoicing_period_starts_at
    self.invoicing_period_ends_at = invoicing_period_ends_at
    self.note = note
  end

  def lines
    InvoiceItem.where(id: ids)
  end

  def description
    if intent == "rent" && invoiceable_type == "CollectionManagement::Work"
      I18n.t :rent_works, scope: "activerecord.values.invoice_item.description"
    elsif intent == "purchase" && invoiceable_type == "CollectionManagement::Work"
      I18n.t :purchased_works, scope: "activerecord.values.invoice_item.description"
    elsif note
      note
    elsif intent == "rent"
      I18n.t :rent_others, scope: "activerecord.values.invoice_item.description"
    elsif intent == "purchase"
      I18n.t :purchased_others, scope: "activerecord.values.invoice_item.description"
    end
  end

  def invoicing_period_desc
    describe_period(invoicing_period_starts_at, invoicing_period_ends_at)
  end
end
