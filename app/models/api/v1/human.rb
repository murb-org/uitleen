module Api
  module V1
    class Human
      def initialize(user_data:, id_token:)
        @user_data = user_data
        @id_token = id_token
      end

      def refresh!(options = {})
        self
      end

      def roles
        @user_data["roles"] || []
      end

      def ability
        Ability.new(self)
      end

      def authorize!(thing, subject)
        ability.authorize!(thing, subject)
      end

      def admin?
        roles.include? "uitleen:admin"
      end

      def works_only?
        admin? || (roles || []).include?("uitleen:works_only")
      end

      def clerk?
        admin? || roles.include?("uitleen:clerk") || roles.include?("uitleen:balie")
      end

      attr_reader :id_token

      def collections
        CollectionManagement::Collection.all(current_user: self)
      end

      def accessible_collections
        CollectionManagement::Collection.all(current_user: self, include_children: true)
      end
    end
  end
end
