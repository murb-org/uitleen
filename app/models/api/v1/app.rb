module Api
  module V1
    class App
      def initialize(user_data:, id_token:)
        @user_data = user_data
        @id_token = id_token
      end

      def refresh!(options = {})
        self
      end

      def admin?
        false
      end

      attr_reader :id_token

      def collections
        CollectionManagement::Collection.all(current_user: self)
      end

      def accessible_collections
        CollectionManagement::Collection.all(current_user: self, include_children: true)
      end
    end
  end
end
