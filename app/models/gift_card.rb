# == Schema Information
#
# Table name: gift_cards
#
#  id          :integer          not null, primary key
#  action      :string
#  amount      :decimal(, )
#  code        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  customer_id :integer
#
class GiftCard < ApplicationRecord
  ACTIONS = [:trade_in, :purchase]

  belongs_to :customer, optional: true
  validates :action, inclusion: {in: ACTIONS.map(&:to_s)}, presence: true
  validates :amount, inclusion: {within: (0.1...20000)}, presence: true

  def trade_in?
    action.to_s == "trade_in"
  end

  def purchase?
    action.to_s == "purchase"
  end
end
