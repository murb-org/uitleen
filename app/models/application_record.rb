require_relative "twinfield/uitleen_extensions"

class ApplicationRecord < ActiveRecord::Base
  include ActAsTimeAsBoolean

  self.abstract_class = true
  strip_attributes
end
