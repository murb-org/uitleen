module Finance
  class TwinfieldNotConfiguredError < StandardError
  end

  class << self
    def configure_twinfield
      oauth_client = OAuthClient.where(name: "twinfield").last

      raise TwinfieldNotConfiguredError.new("No twinfield configuration found") if oauth_client.nil?

      oauth_client.refresh! unless Rails.env.test?

      if !Twinfield.configuration || (!Rails.env.test? && Twinfield.configuration.access_token_expired?)
        Twinfield.configure do |config|
          config.session_type = "Twinfield::Api::OAuthSession"
          config.cluster = Rails.env.test? ? "http://testcluster.nl.example.com" : oauth_client.token_data["twf.clusterUrl"]
          config.access_token = oauth_client.access_token
          config.logger = Rails.logger
          config.log_level = Rails.env.development? ? :debug : :info
        end

        Twinfield.configuration.company = Rails.application.config_for(:uitleen)[:twinfield][:company_code] || auto_company_code
      end

      Twinfield.configuration
    end

    private

    def auto_company_code
      offices = Twinfield::Request::List.offices

      raise "Invalid number of offices returned for OAuth Client (nr. of offics = #{offices.count}; #{offices.map { |a| a[:code] }.join(",")})" unless offices.count == 1

      offices.first.code
    end
  end
end
