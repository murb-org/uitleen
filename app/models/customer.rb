# == Schema Information
#
# Table name: customers
#
#  id                      :integer          not null, primary key
#  auto_finalize_invoices  :boolean          default(TRUE)
#  blocked_at              :datetime
#  blocked_reason          :string
#  born_on                 :date
#  comments                :text
#  company_name            :string
#  company_vat             :string
#  customer_type           :string
#  deactivated_at          :datetime
#  deceased_at             :datetime
#  email                   :string
#  first_name              :string
#  id_document_number      :string
#  id_document_type        :string
#  id_document_valid_until :date
#  initials                :string
#  last_name               :string
#  last_name_prefix        :string
#  name                    :string
#  old_data                :text
#  postal_code             :string
#  sex                     :string
#  title                   :string
#  twinfield_raw           :string
#  uuid                    :string           not null
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  external_customer_id    :string
#  import_id               :string
#
# Indexes
#
#  index_customers_on_uuid  (uuid)
#

class Customer < ApplicationRecord
  has_paper_trail

  TYPES = [:private, :business]

  serialize :twinfield_raw, coder: YAML
  has_many :invoices
  before_validation :set_uuid
  before_validation :set_name

  validates_presence_of :uuid, :name
  validate :can_be_deactivated?
  has_one :collect_mandate, -> { order(:created_at) }
  has_many :transactions, class_name: "CustomerTransaction", primary_key: :external_customer_id, foreign_key: :customer_code
  after_create :send_welcome_mail

  default_scope -> { where(deactivated_at: nil) }
  scope :search, ->(q) {
    where(arel_table[:name].matches("#{q}%"))
      .or(where(arel_table[:postal_code].matches("#{q}%")))
      .or(where(arel_table[:last_name].matches("#{q}%")))
      .or(where(arel_table[:company_name].matches("#{q}%")))
      .or(where(arel_table[:external_customer_id].matches("#{q}%")))
      .or(where(arel_table[:company_name].matches("% #{q}%")))
  }
  scope :with_savings, -> { Customer.where(external_customer_id: CostCenter.customer_savings.join_customer_transactions.select("customer_transactions.customer_code")) }

  time_as_boolean :deceased
  store :old_data, coder: YAML
  alias_attribute :twinfield_code, :external_customer_id

  class << self
    def create_or_update_from_twinfield_customer_code twinfield_customer_code
      twinfield_customer = Twinfield::Customer.find(twinfield_customer_code)
      if twinfield_customer
        customer = find_or_initialize_by(external_customer_id: twinfield_customer_code)
        customer.twinfield_raw = twinfield_customer.to_h
        customer.name = twinfield_customer.name
        customer.email = twinfield_customer.financials&.ebillmail
        customer.postal_code = twinfield_customer.addresses[0]&.postcode
        customer.save
        customer
      end
    end

    def last_updated_at
      order(updated_at: :desc).select(:updated_at).first&.updated_at
    end

    def format_address(address_hash)
      if address_hash
        [address_hash[:field1], address_hash[:field2], "#{address_hash[:country]} #{address_hash[:postcode]} #{address_hash[:city]}"].select(&:present?).join("\n")
      end
    end

    def find_by_uri(uri)
      uuid = uri.to_s.split("/").last
      find_by(uuid: uuid)
    end

    def find_by_old_data_relation_number(relation_number)
      where("customers.old_data LIKE '%\"relatienum\":#{relation_number.to_i}.%'").first
    end
  end

  def update_with_twinfield_data!(max_age: 1.month)
    if updated_at < max_age.ago
      @twinfield_customer = Twinfield::Customer.find(external_customer_id)
      self.twinfield_raw = twinfield_customer.to_h
      self.name = twinfield_customer.name
      self.postal_code = twinfield_customer.addresses[0]&.postcode
      save
    end
  rescue SocketError
  end

  def invoice_address
    format_address(invoice_address_hash)
  end

  def contact_address
    format_address(contact_address_hash)
  end

  def time_span_provided_address
    [@reservation.contact.parsed_remote_data[:address],
      [@reservation.contact.parsed_remote_data[:postal_code], @reservation.contact.parsed_remote_data[:city]].join(" ")].join("\n")
  end

  def invoice_email_address
    email
  end

  def invoice_address_hash
    return nil unless twinfield_raw
    twinfield_raw[:addresses].find { |a| a[:type] == "invoice" } || twinfield_raw[:addresses].find { |a| a[:default] == true }
  end

  def contact_address_hash
    return nil unless twinfield_raw
    twinfield_raw[:addresses].find { |a| a[:type] == "contact" } || twinfield_raw[:addresses].find { |a| a[:default] == true }
  end

  def invoice_address_id
    if external_customer_id
      twinfield_raw[:addresses].index(invoice_address_hash) + 1
    end
  end

  def to_h
    {uri: uri, name: name, public_name: business?, customer_type: customer_type}
  end

  def business?
    customer_type == "business"
  end

  def private?
    customer_type == "private"
  end

  def cost_centers
    CostCenter.for_customer(self)
  end

  def saving_transactions
    cost_centers.final.customer_savings.distinct
  end

  def last_invoice_finalized_at
    invoices.to_a.select(&:finalized_at).max_by(&:finalized_at)&.finalized_at
  end

  def total_savings
    @total_savings ||= (0 - saving_transactions.sum(&:value))
  end

  def gift_card_transactions
    cost_centers.final.customer_gift_card_credit.distinct
  end

  def total_gift_card_credit
    @total_gift_card_credit ||= (0 - gift_card_transactions.sum(&:value))
  end

  def contact_address_id
    if external_customer_id
      twinfield_raw[:addresses].index(contact_address_hash) + 1
    end
  end

  def deactivate!
    update(deactivated_at: Time.now)
  end

  def iban
    bank&.iban
  end

  def bank
    cached_twinfield_customer&.banks&.select(&:default)&.first
  end

  def uri
    action_mailer_config = Rails.application.config.action_mailer.default_url_options || {}
    host = action_mailer_config[:host]
    host = "#{host}:#{action_mailer_config[:port]}" unless [80, 443, 0].include?(action_mailer_config[:port].to_i)
    protocol = Rails.env.development? ? "http" : "https"
    "#{protocol}://#{host}/customers/#{uuid}"
  end

  def collect_mandate?
    !!cached_twinfield_customer.financials.collectmandate&.signaturedate
  end

  def to_param
    uuid
  end

  def twinfield_customer
    @twinfield_customer ||= external_customer_id ? Twinfield::Customer.find(external_customer_id) : Twinfield::Customer.new(addresses: [Twinfield::Customer::Address.new(id: 1, name: "", default: true, country: "NL", type: "contact")])
  end

  def cached_twinfield_customer
    Twinfield::Customer.new(**twinfield_raw) if twinfield_raw.is_a? Hash
  end

  def individual_name
    rv = [first_name || initials, last_name_prefix, last_name].compact.join(" ")
    rv if rv.present?
  end

  def derived_name
    if business? && company_name.present?
      self.name = company_name
    elsif individual_name.present?
      self.name = individual_name
    end
  end

  def email
    read_attribute(:email) || (twinfield_raw.is_a?(Hash) ? twinfield_raw&.[](:financials)&.[](:ebillmail) : nil)
  end

  def existing_customer?
    (created_at && (created_at < 1.minute.ago)) || old_data.present?
  end

  def to_s
    name
  end

  private

  def set_name
    self.name = derived_name
  end

  def set_uuid
    self.uuid ||= SecureRandom.uuid
  end

  def format_address(address_hash)
    Customer.format_address(address_hash)
  end

  def can_be_deactivated?
    if changes["deactivated_at"] && changes["deactivated_at"][0].nil? && changes["deactivated_at"][1].present?
      errors.add(:base, :invalid, message: "Niet alle facturen zijn betaald") if transactions.sales.pluck(:status).include?("available")
    end
  end

  def send_welcome_mail
    CustomerMailer.welcome(self).deliver_now unless existing_customer?
  end
end
