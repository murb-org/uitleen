# == Schema Information
#
# Table name: cost_centers
#
#  id         :integer          not null, primary key
#  code       :string
#  currency   :string           default("EUR")
#  dim1       :string
#  dim2       :string
#  key        :string
#  number     :string
#  source     :string
#  status     :string
#  value      :decimal(16, 2)
#  yearperiod :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_cost_centers_on_source_and_key  (source,key) UNIQUE
#
class CostCenter < ApplicationRecord
  DIMENSIONS = {
    gift_card_credit: Rails.application.config_for(:uitleen)[:cost_center][:gift_card_credit],
    customer_savings: Rails.application.config_for(:uitleen)[:cost_center][:customer_savings]
  }

  scope :join_customer_transactions, -> { joins("LEFT JOIN (SELECT DISTINCT customer_transactions.code, customer_transactions.number, customer_transactions.source, customer_transactions.customer_code FROM customer_transactions) AS customer_transactions ON cost_centers.code = customer_transactions.code AND cost_centers.number = customer_transactions.number AND cost_centers.source = customer_transactions.source") }
  scope :for_customer, ->(customer) { join_customer_transactions.where("customer_transactions.customer_code = ?", customer.external_customer_id) }
  scope :final, -> { where(status: :final) }
  scope :matched_or_unmatchable, -> { join_customer_transactions.where(customer_transactions: {status: [:matched, :notmatchable, :final]}) }
  scope :customer_savings, -> { where(dim1: DIMENSIONS[:customer_savings]) }
  scope :customer_gift_card_credit, -> { where(dim1: DIMENSIONS[:gift_card_credit]) }
  scope :year, ->(year) { where("yearperiod LIKE '?%'", year.to_i) }

  def customer_transaction
    CustomerTransaction.find_by(code: code, number: number, source: source)
  end

  def customer
    customer_transaction.customer
  end

  class << self
    def update
      Finance.configure_twinfield
      CostCenter::DIMENSIONS.values.flatten.each do |dimension|
        Twinfield::Browse::Transaction::GeneralLedger.where(dim1: dimension).each do |transaction|
          create(
            source: "twinfield",
            code: transaction.code,
            currency: transaction.currency,
            dim1: transaction.dim1,
            dim2: transaction.dim2,
            key: transaction.key,
            number: transaction.number,
            status: transaction.status,
            value: transaction.value,
            yearperiod: transaction.yearperiod
          )
        rescue ActiveRecord::RecordNotUnique
        end
      end
    end
  end
end
