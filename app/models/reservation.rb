class Reservation
  include ActiveModel::Model
  include ActiveModel::Validations

  attr_accessor :id
  attr_accessor :address
  attr_accessor :city
  attr_accessor :comments
  attr_accessor :email
  attr_accessor :intent
  attr_accessor :name
  attr_accessor :old_data
  attr_accessor :postal_code
  attr_accessor :telephone
  attr_accessor :created_at
  attr_accessor :updated_at
  attr_accessor :customer_id
  attr_accessor :import_id
  attr_accessor :work_id

  validates_presence_of :work_id
  validates :intent, inclusion: CollectionManagement::TimeSpan::CLASSIFICATIONS.map(&:to_s), presence: true

  # serialize :old_data

  def work(current_user: nil)
    @work ||= CollectionManagement::Work.find(work_id, current_user: current_user)
  end

  def work= work
    self.work_id = work.id
  end

  def name_with_fallback
    customer&.name || name || "Onbekende klant"
  end

  def self.uri
    "#{protocol}://#{host}/collections/#{work.collection_id}/works/#{work.id}/reservations/#{id}"
  end

  def attributes
    {
      id: id,
      address: address,
      city: city,
      comments: comments,
      email: email,
      intent: intent,
      name: name,
      old_data: old_data,
      postal_code: postal_code,
      telephone: telephone,
      created_at: created_at,
      updated_at: updated_at,
      customer_id: customer_id,
      import_id: import_id,
      work_id: work_id
    }
  end
end
