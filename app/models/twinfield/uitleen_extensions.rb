module Twinfield
end

module Twinfield::UitleenExtensions
end

class Twinfield::SalesInvoice
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_writer :uitleen_invoice

  def uitleen_invoice
    @uitleen_invoice ||= if defined?(::Invoice)
      ::Invoice.includes(:invoice_items).find_by(twinfield_invoicenumber: invoicenumber)
    else
      raise "Invoice not implemented"
    end
  end

  def uitleen_invoice_items
    @uitleen_invoice_items ||= uitleen_invoice.invoice_items.order(:id)
  end

  def associated_lines_mapped
    return associated_lines unless uitleen_invoice

    invoice_items_bucket = uitleen_invoice.invoice_items.to_a
    lines = []
    associated_lines.each do |line|
      invoice_item_mapped = line.uitleen_invoice_item(unmapped_invoice_items: invoice_items_bucket)
      if invoice_item_mapped
        invoice_items_bucket -= [invoice_item_mapped]
      end
      lines << line
    end
    lines
  end

  def total_as_intended
    return total unless uitleen_invoice

    associated_lines_mapped.map(&:valueinc_as_intended).compact.sum
  end
end

class Twinfield::SalesInvoice::Line
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  def uitleen_invoice
    invoice&.uitleen_invoice
  end

  # return the total price from the original invoice
  def valueinc_as_intended
    if uitleen_invoice_item && ((uitleen_invoice_item.total_price.to_d - valueinc.to_d).abs <= 0.01)
      uitleen_invoice_item.total_price
    else
      valueinc
    end
  end

  def invoicing_period_desc
    uitleen_invoice_item&.invoicing_period_desc
  end

  def uitleen_invoice_item(unmapped_invoice_items: nil)
    return @uitleen_invoice_item if @uitleen_invoice_item
    if uitleen_invoice
      invoice_items = unmapped_invoice_items || uitleen_invoice.invoice_items
      @uitleen_invoice_item = invoice_items.find { |a| a.quantity == quantity && a.unit_price_ex_vat&.round(2) == unitspriceexcl }
    end
  end
end

class Twinfield::SalesInvoice::VatLine
  include ActiveModel::Conversion
  extend ActiveModel::Naming
end

module Twinfield::Browse; end

module Twinfield::Browse::Transaction; end

class Twinfield::Browse::Transaction::Customer
  include ActiveModel::Conversion
  extend ActiveModel::Naming
end

class Twinfield::Customer
  extend ActiveModel::Naming
  extend ActiveModel::Model
  include ActiveModel::Conversion

  PRIVATE_CUSTOMER_CODE_RANGE = 110_000_000..119_999_999
  BUSINESS_CUSTOMER_CODE_RANGE = 140_000_000..149_999_999

  def persisted?
    !!uid
  end
end

class Twinfield::Customer::Address
  extend ActiveModel::Naming

  attr_accessor :_destroy
end

class Twinfield::Customer::CollectMandate
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  def persisted?
    !!id
  end
end
