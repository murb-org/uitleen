# == Schema Information
#
# Table name: o_auth_clients
#
#  id            :integer          not null, primary key
#  access_token  :string
#  expires_at    :bigint
#  name          :string
#  raw_data      :string
#  refresh_token :string
#  sub           :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class OAuthClient < ApplicationRecord
  store :raw_info

  def refresh!(force = false)
    raise("A refresh_token is not available") unless refresh_token

    return unless refresh_required? || force

    params = {}
    params[:grant_type] = "refresh_token"
    params[:refresh_token] = refresh_token
    new_token = client.get_token(params)

    validated_token = OmniAuth::Strategies::Twinfield.validate_token(new_token.token)

    if validated_token["sub"] == sub
      update(access_token: new_token.token, refresh_token: new_token.refresh_token, expires_at: new_token.expires_at)
    end
  rescue OAuth2::ConnectionError
  end

  # token is still valid one minute from now?
  def refresh_required?
    return true if expires_at.nil?
    Time.at(expires_at.to_i) < (Time.now + 1.minute)
  end

  def token_data
    JWT.decode(access_token, nil, false).first
  end

  private

  def client
    if name.to_sym == :twinfield
      @client ||= "::OmniAuth::Strategies::#{name.capitalize}".constantize.new(name, Rails.application.credentials.oauth_twinfield_id, Rails.application.credentials.oauth_twinfield_secret).client
    end
  end

  class << self
    def find_or_create_from_auth_hash(auth_hash)
      client = find_or_create_by(name: auth_hash.provider, sub: auth_hash.uid)
      client.access_token = auth_hash.credentials.token
      client.refresh_token = auth_hash.credentials.refresh_token
      client.expires_at = auth_hash.credentials.expires_at
      client.raw_info = auth_hash&.extra&.raw_info&.to_h
      client.save
      client
    end

    def collection_management(force_refresh: false)
      client = find_or_create_by(name: :central_login)
      if client.refresh_required? || force_refresh
        url = URI.join(Rails.application.credentials.central_login_site, "/oauth/token").to_s
        response = Faraday.new.post url, {
          client_id: Rails.application.credentials.central_login_id,
          client_secret: Rails.application.credentials.central_login_secret,
          grant_type: :client_credentials
        }

        if response.status == 200
          json = JSON.parse(response.body)
          client.access_token = json["id_token"]
          client.expires_at = 1.hour.from_now
          if !Rails.env.test?
            client.expires_at = client.token_data["exp"]
          end
          client.save
          client
        else
          raise "respnse error #{response.status}"
        end
      end
      client
    end
  end
end
