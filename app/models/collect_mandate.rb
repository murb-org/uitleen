# == Schema Information
#
# Table name: collect_mandates
#
#  id            :integer          not null, primary key
#  data          :text
#  sign_location :string
#  signed_on     :date
#  uuid          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  customer_id   :integer
#
class CollectMandate < ApplicationRecord
  # before_create :set_uuid
  # after_initialize :set_uuid
  attr_writer :iban

  has_one_attached :signed_document

  before_validation :set_uuid

  belongs_to :customer

  validate :iban, :valid_iban_format
  validates_presence_of :iban, :uuid

  def status
    if signed_document.present? && signed_on && customer.collect_mandate?
      :active
    elsif signed_document.present? && signed_on
      :signed
    elsif signed_on
      :draft
    else
      :initialized
    end
  end

  def sepa_proof_uuid
    uuid&.delete("-")
  end

  def iban
    @iban || customer.iban
  end

  def iban_tools_iban
    IBANTools::IBAN.new(iban)
  end

  private

  def set_uuid
    self.uuid ||= SecureRandom.uuid
  end

  def valid_iban_format
    unless IBANTools::IBAN.valid?(iban)
      errors.add(:iban, "Ongeldige IBAN")
    end
  end
end
