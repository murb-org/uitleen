# == Schema Information
#
# Table name: invoice_items
#
#  id                         :integer          not null, primary key
#  discount_amount            :decimal(, )      default(0.0)
#  discount_type              :string
#  input_price_ex_vat         :boolean
#  intent                     :string
#  invoiceable_type           :string
#  invoicing_period_ends_at   :date
#  invoicing_period_starts_at :date
#  note                       :string
#  object_cache               :text
#  quantity                   :decimal(, )      default(1.0)
#  rental_type                :string
#  time_span_uuid             :string
#  unit_price_ex_vat          :decimal(, )
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  invoice_id                 :integer
#  invoiceable_id             :string
#
class InvoiceItem < ApplicationRecord
  NotSupportedError = Class.new(StandardError)

  include PeriodHelper
  include ActiveSupport::NumberHelper

  DISCOUNT_TYPES = [:percentage, :absolute]
  TYPES = [:rent, :purchase]
  INTENT_EVENT_TYPE_MAPPING = {rent: :rental_outgoing, purchase: :purchase}
  DEFAULT_VAT_RATE = 21.0
  LOWERED_VAT_RATE = 9.0
  SUMMARIZE_CUT_OFF = 2

  belongs_to :invoice
  has_one :customer, through: :invoice
  belongs_to :invoiceable, polymorphic: true, optional: true

  scope :purchase, -> { where(intent: :purchase) }
  scope :rent, -> { where(intent: :rent) }
  scope :works, -> { where(invoiceable_type: "CollectionManagement::Work") }
  scope :time_span_attached, -> { where.not(time_span_uuid: nil) }
  scope :not_works, -> { where.not(invoiceable_type: "CollectionManagement::Work") }
  scope :invoicing_period_spans_current, -> { where(invoicing_period_ends_at: (Time.current...), invoicing_period_starts_at: (...Time.current)) }
  scope :invoicing_period_spans_one_month_ago, -> { where(invoicing_period_ends_at: ((Time.current - 1.month)...), invoicing_period_starts_at: (...(Time.current - 1.month))) }

  validates :intent, inclusion: TYPES.map(&:to_s), presence: true
  validates :rental_type, inclusion: Invoice::RENTAL_TYPES.map(&:to_s), presence: false, allow_blank: true
  validates :invoicing_period_starts_at, presence: true, if: -> { recurring? }
  validates :invoicing_period_ends_at, presence: true, if: -> { recurring? }
  validates :quantity, presence: true

  store :object_cache

  after_initialize :set_invoice_period
  # alias_attribute :object, :invoiceable

  class << self
    def summarize(cut_off: SUMMARIZE_CUT_OFF)
      items = Minimal.summarize(all)
      items.delete_if { |item| item.quantity && item.quantity < cut_off }
    end
  end

  def work?
    invoiceable_type == "CollectionManagement::Work"
  end

  def gift_card?
    invoiceable_type == "GiftCard" || gift_card
  end

  def object_cache= hash
    super(JSON.parse(hash.to_json))
  end

  def invoiceable= object
    self.invoiceable_id = object.id
    self.invoiceable_type = object.class
    self.object_cache = object.to_h if work?
    @invoiceable = object
  end
  alias_method :object=, :invoiceable=

  def article= article
    if article.is_a? String
      self.invoiceable = Article.find(article)
    elsif article.is_a? Article
      self.invoiceable_id = article.id
      self.invoiceable_type = article.class
    end
  end

  def vat_rate
    if article
      article.vat_rate
    elsif gift_card?
      0
    elsif work? && saving_rental_type?
      0
    elsif work? && invoiceable.owner&.creating_artist && purchase_intent?
      LOWERED_VAT_RATE
    else
      DEFAULT_VAT_RATE
    end
  end

  def vat_multiplier
    (1 + (vat_rate / BigDecimal(100)))
  end

  def unit_price= price
    self.unit_price_ex_vat = if input_price_ex_vat?
      price
    else
      price.to_d / vat_multiplier.to_d
    end
  end

  def unit_price
    if input_price_ex_vat?
      self.unit_price_ex_vat = unit_price_ex_vat
    else
      (unit_price_ex_vat.to_d * vat_multiplier.to_d).round(2).to_d
    end
  end

  def total_price
    (unit_price * quantity) + savings_mutation + gift_card_credit_mutation
  end

  def total_price_ex_credits
    (unit_price * quantity)
  end

  def unit_price_ex_vat
    return read_attribute(:unit_price_ex_vat) if read_attribute(:unit_price_ex_vat)

    if invoiceable.is_a?(CollectionManagement::Work)
      if rent_intent? && basic_rental_type? && invoice.customer&.business?
        work.business_rent_price_ex_vat
      elsif rent_intent? && basic_rental_type?
        work.default_rent_price_ex_vat
      elsif rent_intent? && saving_rental_type?
        work.selling_price.to_f * invoice_item_config[:customer_savings][:add][:rate]
      else
        invoiceable.selling_price.to_f / vat_multiplier
      end
    end
  end

  def article?
    invoiceable_type == "Article"
  end

  def article
    if article?
      Article.find(invoiceable_id)
    end
  end

  def article_id
    if article?
      invoiceable_id
    end
  end

  def work_related_article?
    article? && ["HUURMAATWERK", "B", invoice_item_config[:customer_savings][:use][:article]].include?(article.external_article_code)
  end

  def gift_card
    if invoiceable_type == "GiftCard" && invoiceable_id
      GiftCard.find(invoiceable_id)
    end
  end

  def gift_card= gc
    if gc.is_a?(Hash)
      self.gift_card = GiftCard.find_or_create_by(gc)
    elsif gc.is_a?(GiftCard) && gc.id
      self.invoiceable = gc
    end
  end

  def invoiceable
    if @invoiceable
      @invoiceable
    elsif object_cache && work?
      invoiceable_type.constantize.new(object_cache.merge({"id" => invoiceable_id}))
    elsif invoiceable_id
      invoiceable_type.constantize.find(invoiceable_id)
    end
  end

  def work
    invoiceable if work?
  end

  def recurring?
    [:rent].include?(intent&.to_sym)
  end

  def initialize_next_intent
    ii = self
    self.class.new(
      input_price_ex_vat: ii.input_price_ex_vat,
      intent: ii.intent,
      note: ii.note,
      object_cache: ii.object_cache,
      invoiceable_type: ii.invoiceable_type,
      quantity: ii.quantity,
      rental_type: ii.rental_type,
      invoicing_period_starts_at: ii.next_invoicing_period_starts_at,
      invoicing_period_ends_at: (ii.invoicing_period_ends_at ? (ii.invoicing_period_ends_at + ii.quantity.round.month).end_of_month : nil),
      time_span_uuid: ii.time_span_uuid,
      unit_price_ex_vat: ii.unit_price_ex_vat,
      invoiceable_id: ii.invoiceable_id,
      discount_amount: ii.discount_amount,
      discount_type: ii.discount_type
    )
  end

  def next_invoicing_period_starts_at
    (invoicing_period_ends_at ? (invoicing_period_ends_at + 1.day).beginning_of_month : nil)
  end

  def invoicing_period_desc
    describe_period(invoicing_period_starts_at, invoicing_period_ends_at)
  end

  #
  #  id                 :integer          not null, primary key
  #  input_price_ex_vat :boolean
  #  intent             :string
  #  note               :string
  #  object_cache       :text
  #  invoiceable_type        :string
  #  quantity           :decimal(, )      default(1.0)
  #  rental_type        :string
  #  time_span_uuid     :string
  #  unit_price_ex_vat  :decimal(, )
  #  created_at         :datetime         not null
  #  updated_at         :datetime         not null
  #  invoice_id         :integer
  #  invoiceable_id          :string
  #

  def time_span
    @time_span ||= if time_span_uuid
      CollectionManagement::TimeSpan.find(time_span_uuid)
    end
  rescue ActiveRecord::RecordNotFound
    logger.warn "Timespan not found #{time_span_uuid}"
    nil
  end

  def time_span= time_span
    if time_span.is_a? CollectionManagement::TimeSpan
      self.time_span_uuid = time_span.uuid
    end
  end

  def invoiced_invoicing_period_overlap?
    invoice_items = InvoiceItem.joins(:invoice).where(invoiceable_type: invoiceable_type, invoiceable_id: invoiceable_id, intent: intent, time_span_uuid: time_span_uuid, invoicing_period_starts_at: invoicing_period_starts_at, invoicing_period_ends_at: invoicing_period_ends_at).where.not(id: id).where.not(invoices: {finalized_at: nil})
    invoice_items.sum(&:total_price_ex_credits) > 0
  end

  def time_span_not_expired?
    (work? && !time_span&.expired?) || (!work? && !time_span_uuid)
  end

  def finalize!
    return true if invoice.credit_invoice? && !(quantity.to_f > 0 && work?)

    if work? && !time_span_uuid
      start_event
    elsif work? && time_span&.expired?
      destroy
    elsif work? && time_span&.reservation?
      start_event(time_span_uuid: time_span_uuid)
    elsif work? && invoiced_invoicing_period_overlap?
      destroy
    elsif work_related_article? && recurring?
      # automatically stop HUURMAATWERK if a recurring item is stopped.
      invoice.invoice_items.works.time_span_attached.each do |invoice_item|
        if invoice_item.time_span&.expired?
          destroy
        end
      end
    end

    save
  end

  def name
    if gift_card
      "Cadeaukaart #{I18n.t(gift_card.action, scope: "activerecord.values.gift_card.action")}"
    elsif article
      article.name
    elsif work
      work.to_invoice_line_description_string
    end
  end

  def purchase_intent?
    intent.to_s == "purchase"
  end

  def rent_intent?
    intent.to_s == "rent"
  end

  def gift_card_credit_mutation(credit = invoice.customer_total_gift_card_credit(self))
    if rent_intent? && credit.to_f > 0 && work?
      -[((unit_price.to_d * quantity.to_d) - discount_price_incl.to_d), credit.to_d].min
    elsif purchase_intent? && gift_card&.trade_in?
      unit_price_ex_vat.to_d
    else
      0
    end
  end

  def savings_mutation(credit = invoice.customer_total_savings(self))
    if rent_intent? && saving_rental_type?
      unit_price.to_d - discount_price_excl.to_d
    elsif work? && purchase_intent?
      -[(work.selling_price.to_d - discount_price_incl.to_d), credit.to_d].min
    else
      0
    end
  end

  def saving_rental_type?
    rental_type.present? ? rental_type == "saving" : invoice.saving_rental_type?
  end

  def basic_rental_type?
    rental_type.present? ? rental_type == "basic_rent" : invoice.basic_rental_type?
  end

  def to_twinfield_invoice_lines
    lines = []

    if purchase_intent?
      # Basic lines
      if work&.artist_owned?
        lines << Twinfield::SalesInvoice::Line.new(article: invoice_item_config[:sell_art_artist][:article], subarticle: invoice_item_config[:sell_art_artist][:subarticle], unitspriceexcl: unit_price_ex_vat, quantity: quantity)
        lines << Twinfield::SalesInvoice::Line.new(description: work.to_invoice_line_description_string)

        lines += to_twinfield_invoice_discount_lines(lines.first)
      elsif work&.external_owner?
        lines << Twinfield::SalesInvoice::Line.new(article: invoice_item_config[:sell_art_external][:article], subarticle: invoice_item_config[:sell_art_external][:subarticle], unitspriceexcl: unit_price_ex_vat, quantity: quantity)
        lines << Twinfield::SalesInvoice::Line.new(description: work.to_invoice_line_description_string)
        lines += to_twinfield_invoice_discount_lines(lines.first)
      elsif work
        lines << Twinfield::SalesInvoice::Line.new(article: invoice_item_config[:sell_art_owned][:article], subarticle: invoice_item_config[:sell_art_owned][:subarticle], unitspriceexcl: unit_price_ex_vat, quantity: quantity)
        lines << Twinfield::SalesInvoice::Line.new(description: work.to_invoice_line_description_string)
        lines += to_twinfield_invoice_discount_lines(lines.first)
      elsif article
        lines << Twinfield::SalesInvoice::Line.new(article: article.external_article_code, subarticle: article.external_article_subcode, unitspriceexcl: unit_price_ex_vat, quantity: quantity, allowdiscountorpremium: article.allow_discount)
        lines << Twinfield::SalesInvoice::Line.new(description: note) if note
      elsif gift_card? && gift_card.purchase?
        lines << Twinfield::SalesInvoice::Line.new(article: invoice_item_config[:gift_card][:purchase][:article], subarticle: invoice_item_config[:gift_card][:purchase][:subarticle], allowdiscountorpremium: false, unitspriceexcl: unit_price_ex_vat, quantity: quantity)
      elsif gift_card? && gift_card.trade_in?
        lines << Twinfield::SalesInvoice::Line.new(article: invoice_item_config[:gift_card][:add][:article], subarticle: invoice_item_config[:gift_card][:add][:subarticle], allowdiscountorpremium: false, unitspriceexcl: -1 * unit_price_ex_vat, quantity: quantity)
        lines << Twinfield::SalesInvoice::Line.new(article: invoice_item_config[:gift_card][:add][:additional_line][:article], subarticle: invoice_item_config[:gift_card][:add][:additional_line][:subarticle], allowdiscountorpremium: false, unitspriceexcl: unit_price_ex_vat, quantity: quantity)
      elsif invoiceable.nil? && note
        lines << Twinfield::SalesInvoice::Line.new(description: note)
      else
        raise NotSupportedError.new "not supported yet"
      end

      # Apply savings
      if work && invoice.use_customer_savings_for_purchase && invoice&.customer&.total_savings.to_f > 0
        lines << Twinfield::SalesInvoice::Line.new(article: invoice_item_config[:customer_savings][:use][:article], subarticle: invoice_item_config[:customer_savings][:use][:subarticle], unitspriceexcl: savings_mutation, quantity: quantity)
      end
    elsif rent_intent?
      if work && basic_rental_type?
        customer_type = invoice.customer&.business? ? :business : :private
        lines << Twinfield::SalesInvoice::Line.new(article: invoice_item_config[:rent][customer_type][:article], subarticle: invoice_item_config[:rent][customer_type][:subarticle], unitspriceexcl: unit_price_ex_vat, quantity: quantity)
        lines << Twinfield::SalesInvoice::Line.new(description: work.to_invoice_line_description_string)

        lines += to_twinfield_invoice_discount_lines(lines.first)

      elsif work && saving_rental_type?
        lines << Twinfield::SalesInvoice::Line.new(article: invoice_item_config[:customer_savings][:add][:article], subarticle: invoice_item_config[:customer_savings][:add][:subarticle], unitspriceexcl: unit_price_ex_vat, allowdiscountorpremium: false, quantity: quantity)
        lines << Twinfield::SalesInvoice::Line.new(description: work.to_invoice_line_description_string)

        lines += to_twinfield_invoice_discount_lines(lines.first)

      elsif article
        lines << Twinfield::SalesInvoice::Line.new(article: article.external_article_code, subarticle: article.external_article_subcode, unitspriceexcl: unit_price_ex_vat, quantity: quantity, allowdiscountorpremium: article.allow_discount)
        lines << Twinfield::SalesInvoice::Line.new(description: note) if note
      else
        raise NotSupportedError.new "not supported yet"
      end

      this_gift_card_credit_mutation = gift_card_credit_mutation

      if this_gift_card_credit_mutation < 0
        lines << Twinfield::SalesInvoice::Line.new(article: invoice_item_config[:gift_card][:use][:article], subarticle: invoice_item_config[:gift_card][:use][:subarticle], allowdiscountorpremium: false, unitspriceexcl: this_gift_card_credit_mutation, quantity: 1)
      end
    end

    if lines == []
      raise NotSupportedError.new "Unconverted line"
    end

    lines
  end

  def discount_type_absolute?
    discount_type.to_sym == :absolute
  end

  def discount_type_percentage?
    discount_type.to_sym == :percentage
  end

  def discount_price_excl
    if discount_amount.present? && discount_amount > 0
      if discount_type_absolute?
        discount_amount
      elsif discount_type_percentage?
        (unit_price_ex_vat.to_d * quantity) / BigDecimal("100.0") * discount_amount
      end
    end
  end

  def discount_price_incl
    if discount_price_excl
      (discount_price_excl * vat_multiplier).round(2)
    end
  end

  def to_h
    additional = {}
    if gift_card
      additional["gift_card"] = gift_card.attributes
    elsif article
      additional["article_code"] = article.external_article_code
      additional["article_subcode"] = article.external_article_subcode
    elsif work
      additional["work_id"] = invoiceable_id
    end
    attributes.merge(additional)
  end

  def end_date_based_on_quantity
    (invoicing_period_starts_at - 1.day + quantity.to_i.month).end_of_month
  end

  private

  def start_event(time_span_uuid: nil)
    event = work.mark_event!(contact_uri: invoice.customer.uri, event_type: INTENT_EVENT_TYPE_MAPPING[intent.to_sym], status: :active, current_user: invoice.created_by_user, time_span_uuid: time_span_uuid)
    self.time_span_uuid = event.uuid
  end

  def invoice_item_config
    Rails.application.config_for(:uitleen)[:invoice_items]
  end

  def to_twinfield_invoice_discount_lines(base_line)
    lines = []
    # Apply discount; use first line to discount
    if discount_price_excl
      discount_line = Twinfield::SalesInvoice::Line.new(**base_line.to_h)
      discount_line.quantity = -1
      discount_line.unitspriceexcl = discount_price_excl
      lines << discount_line
      lines << if discount_type_percentage?
        Twinfield::SalesInvoice::Line.new(description: "#{number_to_human(discount_amount.to_f)}% korting")

      else
        Twinfield::SalesInvoice::Line.new(description: "#{number_to_currency(discount_amount)} korting")

      end
    end
    lines
  end

  def set_invoice_period
    if recurring? && attributes.key?("invoicing_period_starts_at")
      self.invoicing_period_starts_at ||= 1.month.from_now.beginning_of_month
      self.invoicing_period_ends_at ||= end_date_based_on_quantity
    end
  end
end
