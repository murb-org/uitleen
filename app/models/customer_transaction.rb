# == Schema Information
#
# Table name: customer_transactions
#
#  id             :integer          not null, primary key
#  code           :string
#  currency       :string
#  customer_code  :string
#  date           :date
#  invoice_number :string
#  key            :string
#  number         :string
#  open_value     :decimal(, )
#  source         :string
#  status         :string
#  value          :decimal(, )
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_customer_transactions_on_source_and_key  (source,key) UNIQUE
#
class CustomerTransaction < ApplicationRecord
  CODES = ["VRK"]

  scope :join_cost_centers, -> { joins("LEFT JOIN cost_centers ON cost_centers.code = customer_transactions.code AND cost_centers.number = customer_transactions.number AND cost_centers.source = customer_transactions.source") }
  scope :sales, -> { where(code: "VRK") }

  def customer
    if source == "twinfield" || source == "kids"
      Customer.find_by_external_customer_id(customer_code)
    end
  end

  def cost_centers
    CostCenter.where(code: code, number: number, source: source)
  end

  def matched?
    status == "matched"
  end

  def notmatchable?
    status == "notmatchable"
  end

  class << self
    def update
      Finance.configure_twinfield
      CustomerTransaction::CODES.each do |dimension|
        Twinfield::Browse::Transaction::Customer.where(code: dimension).each do |transaction|
          customer_transaction = CustomerTransaction.find_or_initialize_by(
            source: "twinfield",
            code: transaction.code,
            currency: transaction.currency,
            customer_code: transaction.customer_code,
            date: transaction.date,
            key: transaction.key,
            number: transaction.number,
            invoice_number: transaction.invoice_number,
            value: transaction.value
          )
          customer_transaction.status = transaction.status
          customer_transaction.open_value = transaction.open_value
          customer_transaction.save
        rescue ActiveRecord::RecordNotUnique
        end
      end
    end
  end
end
