# == Schema Information
#
# Table name: invoices
#
#  id                                      :integer          not null, primary key
#  discount_percentage                     :decimal(, )      default(0.0)
#  external_invoice_data                   :text
#  finalized_at                            :datetime
#  footertext                              :text
#  intents                                 :text
#  invoice_sent_at                         :datetime
#  invoiceable_item_collection_type        :string
#  next_invoice_at                         :date
#  old_data                                :text
#  on_hold_at                              :datetime
#  po_number                               :string
#  recurrence_interval_in_months           :integer          default(1)
#  reference                               :string
#  rental_type                             :string           default("basic_rent")
#  requires_review_at                      :datetime
#  twinfield_invoicenumber                 :string
#  use_customer_savings_for_purchase       :boolean          default(TRUE)
#  created_at                              :datetime         not null
#  updated_at                              :datetime         not null
#  created_by_user_id                      :string
#  credits_invoice_id                      :integer
#  customer_id                             :integer
#  import_id                               :string
#  invoiceable_item_collection_external_id :string
#  on_hold_by_user_id                      :integer
#  previous_invoice_id                     :integer
#
# Indexes
#
#  index_invoices_on_twinfield_invoicenumber  (twinfield_invoicenumber) UNIQUE
#
class Invoice < ApplicationRecord
  has_paper_trail

  RENTAL_TYPES = [:basic_rent, :saving]
  INVOICEABLE_ITEM_COLLECTIONS = ["CollectionManagement::WorkSet"]
  INVOICE_ITEM_SUMMARIZE_TRESHOLD = 25

  class UniqueWorkError < StandardError; end

  class MissingConfig < StandardError; end

  class NotFinalError < StandardError; end

  # relations
  belongs_to :customer, optional: true
  belongs_to :credits_invoice, optional: true, class_name: "Invoice"

  # callbacks
  after_initialize :initialize_invoice_items_as_array
  before_create :copy_invoice_data_when_credit_invoice

  # validations
  validate :unique_works_in_invoice_items
  validates :rental_type, inclusion: RENTAL_TYPES.map(&:to_s), presence: true
  validates :invoiceable_item_collection_type, inclusion: INVOICEABLE_ITEM_COLLECTIONS.map(&:to_s), allow_nil: true

  validates_uniqueness_of :previous_invoice_id, allow_blank: true
  validates_uniqueness_of :credits_invoice, allow_blank: true
  validates_presence_of :recurrence_interval_in_months, allow_blank: true

  belongs_to :created_by_user, class_name: "User", optional: true
  belongs_to :on_hold_by_user, class_name: "User", optional: true

  # stores invoice_items to be invoiced; will be used to make the final calculation
  # store :invoice_items, accessors: [:purchase_invoice_items, :rent_invoice_items]

  has_many :invoice_items, dependent: :destroy

  belongs_to :previous_invoice, optional: true, class_name: "Invoice"
  has_one :next_invoice, class_name: "Invoice", foreign_key: :previous_invoice_id

  has_one_attached :mutation_form
  has_one_attached :pdf

  scope :finalized, -> { where.not(finalized_at: nil) }
  scope :not_finalized, -> { where(finalized_at: nil) }
  scope :not_on_hold, -> { where(on_hold_at: nil) }
  scope :on_hold, -> { where.not(on_hold_at: nil) }
  scope :recently_expired, -> { where(next_invoice_at: (2.week.ago...Time.current)) }
  scope :soon_to_expire, ->(distance = 1.week.from_now) { where(next_invoice_at: (Time.current...distance)) }
  scope :no_review_required, -> { where(requires_review_at: nil) }
  scope :without_next_invoice, -> { left_outer_joins(:next_invoice).where(next_invoice: {id: nil}) }
  scope :credit, -> { where.not(credits_invoice_id: nil) }

  store :old_data, coder: JSON
  store :external_invoice_data, coder: JSON

  time_as_boolean :finalized
  time_as_boolean :invoice_sent
  time_as_boolean :requires_review

  attr_accessor :manually_create_next_for_invoice_id

  before_save :update_invoice_items_with_invoiceable_item_collection
  before_save :set_next_invoice_at

  def cancel_on_hold
    self.on_hold_at = nil
    self.on_hold_by_user = nil
  end

  def collect_mandate?
    customer&.collect_mandate?
  end

  def credited?
    !!credited_by_invoice
  end

  def credited_by_invoice
    @credited_by_invoice ||= Invoice.find_by_credits_invoice_id(id)
  end

  def on_hold(user)
    self.on_hold_at = Time.current
    self.on_hold_by_user = user
  end

  def credit_invoice?
    !!credits_invoice_id
  end

  def on_hold?
    !!on_hold_at
  end

  def purchase_invoice_items
    invoice_items.purchase
  end

  def rent_invoice_items
    invoice_items.rent
  end

  def transaction
    CustomerTransaction.where(code: "VRK").find_by(invoice_number: twinfield_invoicenumber)
  end

  def non_work_invoice_items
    invoice_items.not_works
  end

  def summarize_invoice_items?
    invoice_items.count > INVOICE_ITEM_SUMMARIZE_TRESHOLD
  end
  alias_method :offer_invoice_specification?, :summarize_invoice_items?

  def to_html
    renderer = InvoicesController.renderer.new

    renderer.render :show_print, layout: "application", assigns: {
      sales_invoice: external_invoice,
      uitleen_invoice: self
    }
  end

  def invoice_item_start_date_reset=(value)
    if value.present?
      invoice_items.update_all(invoicing_period_starts_at: value) unless finalized?
    end
  end

  def invoice_item_start_date_reset
    dates = invoice_items.map(&:invoicing_period_starts_at)
    (dates.uniq.count == 1) ? dates.first : nil
  end

  def to_pdf
    if finalized?
      html = to_html
      filename = Rails.root.join("/tmp/#{SecureRandom.base58(32)}.html")

      File.write(filename, html)
      Pupprb::Pdf.write(filename)
    else
      raise "Invoice needs to be finalized"
    end
  end

  def paid?
    fully_final? && transaction && (transaction.matched? || transaction.notmatchable?)
  end

  def new_intent_attributes= attributes
    work_id = attributes.delete(:work_id).to_s
    work = CollectionManagement::Work.find(work_id, current_user: created_by_user)

    if attributes[:intent].to_sym == :rent
      work.mark_event!(contact_uri: customer&.uri, event_type: :rental_outgoing, current_user: created_by_user)
      rent_invoice_items.new(invoiceable: work, invoicing_period_starts_at: 1.month.from_now.beginning_of_month, invoicing_period_ends_at: 1.month.from_now.end_of_month)
    end
    if attributes[:intent].to_sym == :purchase
      work.mark_event!(contact_uri: customer&.uri, event_type: :purchase, current_user: created_by_user)
      purchase_invoice_items.new(invoiceable: work)
    end
  end

  def remove_intent_attributes= attributes
    return if finalized?
    work_id = attributes.delete(:work_id).to_s
    intent = attributes.delete(:intent).to_sym
    invoice_items.where(intent: intent).where(invoiceable_id: work_id).where(invoiceable_type: "CollectionManagement::Work").destroy_all
  end

  def recurring?
    invoice_items.collect(&:recurring?).include?(true) && total_internal_price >= 0
  end

  def initialize_next_invoice
    unless next_invoice
      invoice = Invoice.new(previous_invoice: self, created_by_user: created_by_user)
      invoice.customer = customer

      new_invoice_items = invoice_items.collect do |item|
        item.initialize_next_intent if item.recurring? && item.time_span_not_expired?
      end.compact
      invoice.invoice_items = new_invoice_items
      invoice.footertext = footertext
      invoice.rental_type = rental_type
      invoice.reference = reference
      invoice.recurrence_interval_in_months = recurrence_interval_in_months
      invoice.discount_percentage = discount_percentage
      invoice unless new_invoice_items.empty?
    end
  end

  def invoiceable_item_collection
    if invoiceable_item_collection_type && invoiceable_item_collection_external_id && valid?
      @invoiceable_item_collection ||= invoiceable_item_collection_type.constantize.find(invoiceable_item_collection_external_id)
    end
  end

  def invoiceable_item_collection= collection_with_items
    if collection_with_items.methods.include? :external_id
      self.invoiceable_item_collection_type = collection_with_items.class.to_s
      self.invoiceable_item_collection_external_id = collection_with_items.external_id
    end
  end

  def invoiceable_item_collection_works_with_current_active_time_span
    @invoiceable_item_collection_works_with_current_active_time_span ||= invoiceable_item_collection&.works&.select(&:current_active_time_span)
  end

  def works
    invoice_items.works.map { |item| item.work }
  end

  def work_in_invoice_items? work
    work_id = (work.is_a?(CollectionManagement::Work) ? work.id : work).to_s
    work_ids_in_invoice_items = invoice_items.works.map(&:invoiceable_id).map(&:to_s)

    work_ids_in_invoice_items.include? work_id
  end

  def open_value
    booked_transaction&.open_value
  end

  def booked_transaction
    @booked_transaction ||= cached_external_invoice_with_fallback&.transaction
  end
  # Balans aanmaken:
  # 1250 Oude KIDS boekingen
  # .  -> rapportage: Standaard-RGS\Liquide middelen\Kruisposten (100300) | Balance
  # Kasboek aanmaken => https://accounting.twinfield.com/UI/#/Bank/Cashbooks
  # 1251

  #
  # Dagboek PIN (https://accounting.twinfield.com/UI/#/Settings/Company/TransactionTypes)
  # 1230 is Pin betalingen balans = 0.0
  # 1300 is debiteuren
  # 1234 is

  def create_payment_transaction(transaction_type = :pin)
    raise NotFinalError.new("transaction / invoice isn't final") unless fully_final?

    payment_transaction_config = Rails.application.config_for(:uitleen)[:payment_transactions][transaction_type]
    raise MissingConfig.new("no payment transaction configuration for transaction type '#{transaction_type}'") unless payment_transaction_config

    customer_invoice_ledger = 1300
    general_ledger = payment_transaction_config[:ledger_code]
    transaction_code = payment_transaction_config[:code]
    balance_code = payment_transaction_config[:balance_code]

    payment_transaction = Twinfield::Transaction.new(code: transaction_code, currency: "EUR")
    payment_transaction.lines << Twinfield::Transaction::Line.new(type: :total, balance_code: general_ledger, value: 0.0, debitcredit: :debit)
    payment_transaction.lines << Twinfield::Transaction::Line.new(type: :detail, description: "detail", balance_code: customer_invoice_ledger, value: open_value, debitcredit: :credit, customer_code: customer_code, invoicenumber: twinfield_invoicenumber)
    payment_transaction.lines << Twinfield::Transaction::Line.new(type: :detail, description: "detail", balance_code: balance_code, value: open_value, debitcredit: :debit)
    payment_transaction.save ? payment_transaction : false
  end

  def create_payment_transaction_and_match!(transaction_type = :pin)
    twinfield_invoice.transaction.match!(create_payment_transaction(transaction_type))
  end

  def fully_final?
    finalized? && cached_external_invoice_with_fallback.status == "final" && booked_transaction
  end

  def finalize!
    if !Rails.application.config_for(:uitleen)[:finalize_invoices]
      errors.add(:base, "Factuur definitief maken is uitgeschakeld")
      return false
    elsif requires_review?
      errors.add(:base, "Factuur moet eerst gecontroleerd worden (verwijder de factuur uit de review lijst)")
      return false
    elsif finalized?
      errors.add(:base, "Factuur is al definitief gemaakt")
      return false
    elsif external_invoice.nil?
      errors.add(:base, "De factuur kon niet gevonden worden in het boekhoudpakket; mogelijk missen er regels")
      return false
    end

    external_invoice.status = "final"

    invoice_items.each { |item| item.finalize! }

    # force reloading
    if InvoiceItem.where(id: invoice_items.pluck(:id)).length == 0
      destroy
      return false
    end

    save_external_invoice!

    self.finalized_at = Time.current
    store_external_invoice_data_if_finalized

    save
  end

  def store_external_invoice_data_if_finalized
    @external_invoice = nil
    self.external_invoice_data = external_invoice.to_h if finalized_at && external_invoice.status == "final"
  end

  def customer_total_gift_card_credit(until_invoice_item = nil)
    credit = customer&.total_gift_card_credit.to_d || BigDecimal(0)

    unless fully_final?
      invoice_items.select(&:purchase_intent?).each do |invoice_item|
        credit += invoice_item.gift_card_credit_mutation(credit)
      end
      invoice_items.select(&:rent_intent?).each do |invoice_item|
        return credit if until_invoice_item == invoice_item
        credit += invoice_item.gift_card_credit_mutation(credit)
      end
    end

    credit
  end

  def customer_total_savings(until_invoice_item = nil)
    credit = customer&.total_savings.to_d || BigDecimal(0)

    unless fully_final?
      invoice_items.select(&:rent_intent?).each do |invoice_item|
        credit += invoice_item.savings_mutation(credit)
      end
      invoice_items.select(&:purchase_intent?).each do |invoice_item|
        return credit if until_invoice_item&.id && until_invoice_item.id == invoice_item&.id
        credit += invoice_item.savings_mutation(credit)
      end

    end

    credit
  end

  def sync_remotely?
    [finalized_at, updated_at].compact.first > Rails.application.config_for(:uitleen)[:sync_from]
  end

  def save_external_invoice!(final = false)
    save_twinfield_invoice!(final) if sync_remotely?
  end

  def invoice_number
    twinfield_invoicenumber
  end
  alias_method :invoicenumber, :invoice_number

  def status
  end

  def invoice_address
    customer.invoice_address
  end

  def deliver_address
    customer.contact_address
  end

  def invoice_email_address
    customer&.invoice_email_address.present? ? customer&.invoice_email_address : "info@heden.nl"
  end

  def invoice_date
    if finalized? && has_external_invoice? && cached_external_invoice&.invoicedate
      cached_external_invoice.invoicedate
    elsif finalized? && has_external_invoice? && external_invoice.invoicedate
      twinfield_invoice.invoicedate
    elsif finalized_at
      finalized_at.to_date
    else
      Time.now.to_date
    end
  end
  alias_method :invoicedate, :invoice_date

  def next_invoice_date
    if invoice_items.rent.count > 0 && !credit_invoice?
      if on_hold_at
        nil
      elsif next_invoice_at
        next_invoice_at
      elsif previous_invoice
        (invoice_date + recurrence_interval_in_months.month).beginning_of_month
      else
        (invoice_items.rent.collect(&:invoicing_period_ends_at).compact.min + 3.days).beginning_of_month
      end
    end
  end

  def next_invoice?
    !!next_invoice
  end

  def invoice_lines
    has_external_invoice? ? twinfield_invoice.lines : invoice_items
  end

  def draft_invoice_lines
    derived_twinfield_invoice_lines
  end

  def vat_lines
    has_external_invoice? ? twinfield_invoice.vat_lines : []
  end

  def total_price
    has_external_invoice? ? total_external_price : total_internal_price
  end

  def total_price_as_intended
    has_external_invoice? ? total_external_price_as_intended : total_internal_price
  end

  def total_internal_price
    invoice_items.map(&:total_price).compact.sum
  end

  def total_external_price
    if has_external_invoice?
      cached_external_invoice_with_fallback.associated_lines_mapped.map(&:valueinc).compact.sum
    end
  end

  def total_external_price_as_intended
    if has_external_invoice?
      cached_external_invoice_with_fallback.associated_lines_mapped.map(&:valueinc_as_intended).compact.sum
    end
  end

  def name
    customer ? "Actieve klantsessie: #{customer.name}" : "Actieve klantsessie"
  end

  def basic_rental_type?
    rental_type.to_sym == :basic_rent
  end

  def saving_rental_type?
    rental_type.to_sym == :saving
  end

  def has_external_invoice?(cached = true)
    if cached && external_invoice_data.present?
      invoice_number && cached_external_invoice && cached_external_invoice.invoicenumber.to_s == invoice_number.to_s
    else
      invoice_number && external_invoice && external_invoice.invoicenumber.to_s == invoice_number.to_s
    end
  end

  def cached_external_invoice
    Twinfield::SalesInvoice.new(**external_invoice_data.deep_symbolize_keys) if external_invoice_data.present?
  end

  def cached_external_invoice_with_fallback(max_age: nil)
    if !cached_external_invoice || (max_age && updated_at < max_age.ago)
      external_invoice_result = external_invoice
      update_columns(external_invoice_data: external_invoice_result.to_h, twinfield_invoicenumber: external_invoice_result.invoicenumber, updated_at: Time.current)
    end
    cached_external_invoice
  end

  def external_invoice
    @external_invoice ||= twinfield_invoice
  end

  def to_h
    attributes.merge("customer_uuid" => customer&.uuid, "invoice_items" => invoice_items.map(&:to_h))
  end

  def update_rent_quantities_based_on_recurrence_interval
    invoice_items.rent.each do |invoice_item|
      invoice_item.quantity = recurrence_interval_in_months
      invoice_item.invoicing_period_ends_at = invoice_item.end_date_based_on_quantity
      invoice_item.save
    end
  end

  def customer_code
    customer&.external_customer_id
  end

  private

  def twinfield_invoice
    invoice = twinfield_invoicenumber ? Twinfield::SalesInvoice.find(twinfield_invoicenumber, invoicetype: "FACTUUR") : Twinfield::SalesInvoice.new(customer: customer_code, invoicetype: "FACTUUR")
    invoice ||= Twinfield::SalesInvoice.new(customer: customer_code, invoicetype: "FACTUUR")
    invoice.currency = "EUR"
    invoice
  rescue
    Finance.configure_twinfield
    nil
  end

  def derived_twinfield_invoice_lines
    invoice_items.order(:id).flat_map(&:to_twinfield_invoice_lines)
  end

  def update_invoice_items_with_invoiceable_item_collection
    SynchroniseInvoiceWorkSetWorker.new.perform(self)
  end

  def initialize_invoice_items_as_array
    self.purchase_invoice_items ||= []
    self.rent_invoice_items ||= []
  end

  def unique_works_in_invoice_items
    ids = invoice_items.select(&:work?).map(&:invoiceable_id)
    errors.add(:invoice_items, :unique) unless (ids.count == ids.uniq.count) || import_id.present?
  end

  def save_twinfield_invoice!(final = false)
    invoice = external_invoice
    invoice.lines = derived_twinfield_invoice_lines
    if customer
      invoice.customer_code = customer_code
      invoice.invoiceaddressnumber = customer.invoice_address_id
      invoice.deliveraddressnumber = customer.contact_address_id
    end
    invoice.invoicedate = finalized_at || Time.now
    invoice.duedate = Time.now + 1.month
    invoice.footertext = footertext ? footertext.sub(/Let op: deze factuur is een kopie factuur vanuit vorige boekhoudpakket. Oorspronkelijk factuurnummer \d*/, "") : nil
    invoice.status = "final" if final
    # invoice.invoicenumber = twinfield_invoicenumber
    invoice = invoice.save
    @external_invoice = nil

    if update_columns(external_invoice_data: invoice.to_h, twinfield_invoicenumber: invoice.invoicenumber, updated_at: Time.current)
      invoice
    end
  rescue Twinfield::Create::Finalized => e
    self.twinfield_invoicenumber ||= invoice.invoicenumber
    save
    raise e
  end

  def set_next_invoice_at
    if recurring? && finalized?
      self.next_invoice_at ||= next_invoice_date
    end
  end

  def copy_invoice_data_when_credit_invoice
    if credit_invoice?
      self.customer = credits_invoice.customer

      new_invoice_items = credits_invoice.invoice_items.collect do |item|
        new_item = item.initialize_next_intent
        new_item.invoicing_period_starts_at = item.invoicing_period_starts_at
        new_item.invoicing_period_ends_at = item.invoicing_period_ends_at
        new_item.quantity = -1 * item.quantity
        new_item
      end.compact
      self.invoice_items = new_invoice_items
      self.footertext = "Dit is een creditfactuur van Factuur #{credits_invoice.invoice_number}"
      self.rental_type = credits_invoice.rental_type
      self.recurrence_interval_in_months = nil
      self.discount_percentage = credits_invoice.discount_percentage
    elsif manually_create_next_for_invoice_id
      invoice_to_duplicate = Invoice.find(manually_create_next_for_invoice_id)
      self.customer = invoice_to_duplicate.customer
      self.invoice_items = invoice_to_duplicate.invoice_items.collect(&:initialize_next_intent).compact
      self.rental_type = invoice_to_duplicate.rental_type
      self.reference = invoice_to_duplicate.reference
      self.discount_percentage = invoice_to_duplicate.discount_percentage
      self.previous_invoice = invoice_to_duplicate
    end
  end

  class << self
    def from_h hash
      hash["customer"] = Customer.find_by_uuid(hash.delete("customer_uuid"))
      hash["invoice_items"] = hash.delete("invoice_items")&.compact&.map do |invoice_item|
        objekt = if invoice_item["invoiceable_type"] == "GiftCard"
          gift_card_hash = invoice_item.delete("gift_card")
          gc = GiftCard.find_or_initialize_by(id: gift_card_hash["id"])
          gc.attributes = gift_card_hash
          gc.save
          gc
        elsif invoice_item["invoiceable_type"] == "Article"
          article_code = invoice_item.delete("article_code")
          article_subcode = invoice_item.delete("article_subcode")
          Article.find_by(external_article_code: article_code, external_article_subcode: article_subcode)
        end
        if invoice_item.is_a? InvoiceItem
          return InvoiceItem
        end

        invoice_item.delete("work_id")
        invoice_item.delete("article_code")
        invoice_item.delete("article_subcode")

        if objekt
          invoice_item.delete("invoiceable_type")
          invoice_item.delete("object_cache")
          invoice_item.delete("invoiceable_id")
          invoice_item["object"] = objekt
        end

        ii = InvoiceItem.find_or_initialize_by(id: invoice_item["id"])
        ii.attributes = invoice_item
        ii.save
        ii
      end&.compact || []

      invoice = Invoice.find_or_initialize_by(id: hash["id"])
      invoice.attributes = hash
      invoice.save
      invoice
    end

    def find_by_id_param id_param
      external_invoice_number_param = if id_param.starts_with? "external_invoice_number:"
        id_param.split(":")[1]
      end

      import_id_param = if id_param.starts_with? "import_id:"
        id_param.split(":")[1]
      end

      if external_invoice_number_param
        Invoice.find_by(twinfield_invoicenumber: external_invoice_number_param)
      elsif import_id_param
        Invoice.find_by(import_id: import_id_param)
      else
        Invoice.find_by(id: id_param)
      end
    end

    def find_by_id_param!(id_param)
      invoice = find_by_id_param(id_param)
      raise ActiveRecord::RecordNotFound.new("Factuur met #{id_param} niet gevonden") if invoice.nil?
      invoice
    end
  end
end
