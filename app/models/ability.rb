# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    return unless user

    if user.admin?
      can :manage, :all
    elsif user.is_a?(Api::V1::App)
      can :read, CollectionManagement::Work
      can :access, Api::V1::ApiController
      can [:read], Customer
    elsif user.clerk?
      can :read, CollectionManagement::Work
      can [:read, :create, :update], Customer
      can [:read, :create, :register_payment, :finalize], Invoice
      can [:update], Invoice, finalized: nil
      can [:read, :create, :destroy], InvoiceItem
      can [:register_payment], Twinfield::SalesInvoice
      can :access, Api::V1::ApiController
    elsif user.works_only?
      can :read, CollectionManagement::Work
      can :access, Api::V1::ApiController
    end

    # cannot :destroy, InvoiceItem, InvoiceItem.finalized
    cannot :destroy, Invoice do |invoice|
      invoice.finalized?
    end
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
