# == Schema Information
#
# Table name: users
#
#  id            :integer          not null, primary key
#  access_token  :string
#  email         :string
#  expires_at    :integer
#  groups        :text
#  id_token      :text
#  name          :string
#  refresh_token :string
#  resources     :text
#  roles         :text
#  sub           :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class User < ApplicationRecord
  serialize :roles, coder: YAML
  serialize :groups, coder: YAML
  serialize :resources, coder: YAML

  def initialize(*attributes)
    super
    self.roles ||= []
    self.groups ||= []
    self.resources ||= []
  end

  def refresh!(force: false)
    raise("A refresh_token is not available") unless refresh_token
    # puts (refresh_required? && !force)
    # return self unless (refresh_required? && !force)
    return self if !refresh_required? && !force

    params = {}
    params[:grant_type] = "refresh_token"
    params[:refresh_token] = refresh_token
    params[:response_type] = "id_token"

    new_token = client.get_token(params)
    id_token = new_token.params["id_token"]

    validated_token = oauth_strategy.validate_id_token(id_token)

    if validated_token["sub"] == sub
      update(access_token: new_token.token, refresh_token: new_token.refresh_token, expires_at: Time.at(new_token.expires_at, in: "UTC").to_datetime, id_token: id_token)
    end

    self
  end

  # token is still valid one minute from now?
  def refresh_required?
    Time.at(expires_at.to_i) < (Time.now + 1.minute)
  end

  def admin?
    (roles || []).include? "uitleen:admin"
  end

  def clerk?
    admin? || (roles || []).include?("uitleen:clerk") || (roles || []).include?("uitleen:balie")
  end

  def works_only?
    admin? || (roles || []).include?("uitleen:works_only")
  end

  def oauth_strategy
    @oauth_strategy ||= ::OmniAuth::Strategies::CentralLogin.new(name, Rails.application.credentials.central_login_id, Rails.application.credentials.central_login_secret, client_options: {site: Rails.application.credentials.central_login_site})
  end

  def client
    @client ||= oauth_strategy.client
  end

  def self.from_auth_hash(auth_hash)
    u = find_or_create_by(sub: auth_hash.uid)
    u.update(
      email: auth_hash.info.email,
      name: auth_hash.info.name,
      sub: auth_hash.uid,
      id_token: auth_hash&.extra&.id_token,
      access_token: auth_hash.credentials.token,
      refresh_token: auth_hash.credentials.refresh_token,
      expires_at: Time.at(auth_hash.credentials.expires_at, in: "UTC").to_datetime,
      roles: auth_hash&.extra&.raw_info&.roles&.to_a,
      groups: auth_hash&.extra&.raw_info&.groups&.to_a,
      resources: auth_hash&.extra&.raw_info&.resources&.to_a
    )
    u
  end
end
